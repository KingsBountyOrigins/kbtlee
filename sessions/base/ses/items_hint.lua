function gen_itm_menu_hint()

    local res = ""
    if Obj.where()~=2 and not Obj.props("wife") then
        res="<label=itm_main_menu>"
    end
  return res

end

function gen_itm_name()

	if Obj.props(0)~="" and Obj.props(0)~=nil then
  	if Obj.props("wife") then
   		return " ( <label=itm_wife> )"
  	end 	
  	if Obj.props("child") then
   		return " ( <label=itm_chid> )"
  	end 
  	return ""
  else 
  	return ""
  end 
end

function gen_itm_race()
 local mr = "<br><label=itm_race> "
	
 if Obj.props(0)~="" and Obj.props(0)~=nil then
   local race = Obj.race()
 	 return mr.."<label=itm_"..race..">"
 end 

 return ""

end

function gen_itm_count()
 local count=Obj.get_param("item_count")
 if count ~= "" and count ~= nil then
 	return count
 else
 	return ""
 end 
	
end

function gen_itm_item()
 local count=Obj.get_param("item_add")
 if count ~= "" and count ~= nil then
 	return "<label=itm_"..count.."_name>"
 else
 	return ""
 end 
	
end

function gen_itm_gold(par)
 local param=Obj.get_param("gold")
 if param~= "" and param ~= nil then
  param=tonumber(param)
 local mnog=1
 if par=="level" then
 		mnog=tonumber(Logic.hero_lu_var("level"))
  end 
  return tostring(param*mnog)
 else 
  return ""
 end 
 	return ""
end

function gen_itm_param(par)
 local count=text_par_count(par)
 if count>1 then 
 	local param=Obj.get_param(text_dec(par,1))
 	local suffiks=text_dec(par,2)
 	return param..suffiks
 else 
 	return Obj.get_param(par)
 end 
 	return ""
end

function gen_itm_dragon(par)
 	local param=Obj.get_param("dragon")
 	if param~="" and param~="0" and param~=nil then
 		return "<label=cpn_"..param..">"
 	else 
 		return "<label=itm_nodragon>"
 	end 
end

function gen_itm_zlogn(par)
 	local param=Obj.get_param("use")
 	if param~="" and param~=nil then
 		local cnt=tonumber(param)
 		if cnt>=11 then return "<label=itm_egnum_st_4>" end 
 		if cnt>=8 then return "<label=itm_egnum_st_3>" end 
 		if cnt>=4 then return "<label=itm_egnum_st_2>" end 
 		if cnt>=1 then return "<label=itm_egnum_st_1>" end 
 		if cnt==0 then return "<label=itm_egnum_st_0>" end 
 	else 
 		return "<label=itm_egnum_st_0>"
 	end 
end
function gen_itm_scroll()
 local count=Obj.get_param("scroll")
 if count ~= "" and count ~= nil then
 	return "<label=spell_"..count.."_name>"
 else
 	return ""
 end 
	
end

function gen_item_text()
 local label=Obj.get_param("label")
 local text_tmp=Obj.get_param("text")
 local text = "<label="..string.gsub(label, "number", text_tmp)..">"
 	return text

end

function gen_itm_upgrade()
 local mr = "<br>"
 local upgrade = Obj.get_param("upgrade")
 if upgrade~="" and upgrade~=nil then
 	local count=text_par_count(upgrade)
 	local fnt=""
 	for i=1,count do
 		if Obj.name()==text_dec(upgrade,i) then
 			fnt="<label=itm_upgrade_cur>"
 		else
 			fnt="<label=itm_upgrade_up>"
 		end 
 		mr=mr.."<br>"..fnt.." - <label=itm_"..text_dec(upgrade,i).."_name>"
 	end 
 	return mr
 else
 	return ""
 end 
	
end

function gen_itm_state()
 local mr = ""
 local upgrade = Obj.get_param("upgrade")
 if Obj.moral()==0 and Obj.props("moral") then mr="<br><label=itm_state_out>" end 
 if Obj.upg()~=nil and Obj.upg()~="" then --Obj.moral()~=0 and
 	mr="<br><label=itm_state_upgrade>"
 end 
 
 if Obj.where()>2 and Obj.where()<5 then 
 	return mr	
 else
 	return ""
 end 
end

function gen_itm_type()
	local mr="<br><br><label=itm_type> "
	if Obj.props(0)~="" and Obj.props(0)~=nil then
   return mr.."<label=itm_"..Obj.props(0)..">"
  end 
  if Obj.props("quest") then
   return mr.."<label=itm_quest>"
  end 
  if Obj.props("wife") then
   return "" --<br><br><label=itm_wife>"
  end 
  if Obj.props("child") then
   return "" --<br><br><label=itm_child>"
  end 
  if Obj.props("map") then
   return mr.."<label=itm_map>"
  end 
  if Obj.props("usable") then
   return mr.."<label=itm_usual>"
  end 
  if Obj.props("container") then
   return mr.."<label=itm_resource>"
  end 
  
  --itm_diamond
  
  if Obj.get_param("sell")=="1" then
  	return mr.."<label=itm_diamond>"
  end
  if Obj.get_param("recept")=="1" then
  	return mr.."<label=itm_recept>"
  end

  return mr.."Unknow item type!"
end

function gen_itm_price()
  if Obj.props("quest") or Obj.props("wife") or Obj.props("child") or Obj.where()==2 then
   return ""
  else
   return "<br><label=itm_price> "..tostring(Obj.price())
  end 
  return mr.."Not price!"
end

function gen_itm_moral()

-- 0-5 5-20 20-40 --- 60-80 80-95 95-100  

  local moral = Obj.moral()
  local mr = "<br><label=itm_moral> "
	
 if Obj.props("moral") then
  if moral<=5 then
    return mr.."<label=itm_moral_l3> (" .. tostring(moral) .. ")"
  end
  if moral>5 and moral<=20 then
    return mr.."<label=itm_moral_l2> (" .. tostring(moral) .. ")"
  end
  if moral>20 and moral<=40 then
    return mr.."<label=itm_moral_l1> (" .. tostring(moral) .. ")"
  end
  if moral>40 and moral<=60 then
    return mr.."<label=itm_moral_n0> (" .. tostring(moral) .. ")"
  end
  if moral>60 and moral<=80 then
    return mr.."<label=itm_moral_h1> (" .. tostring(moral) .. ")"
  end
  if moral>80 and moral<=95 then
    return mr.."<label=itm_moral_h2> (" .. tostring(moral) .. ")"
  end
  if moral>95 then
    return mr.."<label=itm_moral_h3> (" .. tostring(moral) .. ")"
  end
 else 
	return ""
 end 	
		return ""
end

function map_image_gen()
 local map=Obj.get_param("map")
 local image=Obj.get_param("image")

 image = "<image="..string.gsub(image,"number",map)..".png>"
 return image

end
