
function scn_fullstop ( param, callpar )

  Action.act_run_scenario( 0 )
  return true

end


function scn_stop_and_work ( param, callpar )

  if (param == nil) then

    Scenario.stop()
    Scenario.animate("idle", "s1")

  elseif (param == "s1") then

    Scenario.animate("spare", "s2")
    if Logic.is_available() then
      Logic.cur_lu_var("idle", "spare")
    end

  elseif (param == "s2") then
    return true
  end

  return false

end


function scn_miner_stop ( param, callpar )

  if (param == nil) then

    Scenario.stop()
    Scenario.animate("idle", "s1")

  elseif (param == "s1") then

    return true
  end

  return false

end

function scn_army_wait ( param, callpar )

    local stop_time=1
    if (param == nil) then

        if callpar ~= nil then
          stop_time = tonumber( "0" .. callpar )
        end

        Scenario.nomove()
        Scenario.animate("idle", "await_001")
        Scenario.delay( stop_time, "await_000" )
        Scenario.breakable()

    elseif (param == "await_000") then
        return true
    elseif (param == "await_001") then
        Scenario.animate("idle", "await_001")
        return false
    end

    return false
end

function scn_army_stop1 ( param )

    local stop_time=1
    if (param == nil) then

        local stop_time_var = Logic.cur_lu_var("stop_time1")
        if (stop_time_var ~= nil) and (stop_time_var ~= 0) then
          stop_time = tonumber("0" .. stop_time_var)
        end

        Scenario.stop()
        Scenario.animate("idle", "army_001")
        Scenario.delay( stop_time, "army_000" )

    elseif (param == "army_000") then
        return true
    elseif (param == "army_001") then
        Scenario.animate("idle", "army_001")
        return false
    end

    return false
end

function scn_army_stop2 ( param )

    local stop_time=5

    if (param == nil) then

        local stop_time_var = Logic.cur_lu_var("stop_time2")
        if (stop_time_var ~= nil) and (stop_time_var ~= 0) then
          stop_time = tonumber("0" .. stop_time_var)
        end

        Scenario.stop()
        Scenario.animate("idle", "army_001")
        Scenario.delay( stop_time, "army_000" )
        Scenario.breakable()

    elseif (param == "army_000") then
        return true
    elseif (param == "army_001") then
        Scenario.animate("idle", "army_001")
        return false
    end

    return false
end

function scn_army_tupka ( param )

    local stop_time=0
    if (param == nil) then

        Scenario.nomove()
        Scenario.breakable()
        return not Scenario.animate("lost", "army_001")

    elseif (param == "army_001") then
        return true
    end

    return false
end


function scn_army_rare ( param )

    if (param == nil) then
        Scenario.breakable()
        return not Scenario.animate("rare", "army_001")
    elseif (param == "army_001") then
        return true
    end

    return false
end
function scn_army_spare ( param )

    if (param == nil) then
        Scenario.breakable()
        return not Scenario.animate("spare", "army_001")
    elseif (param == "army_001") then
        return true
    end

    return false
end
function scn_army_special ( param )

    if (param == nil) then
        return not Scenario.animate("special", "army_001")
    elseif (param == "army_001") then
        return true
    end

    return false
end
function scn_army_cast ( param )

    if (param == nil) then
        return not Scenario.animate("cast", "army_001")
    elseif (param == "army_001") then
        return true
    end

    return false
end
function scn_army_idle ( param )

    if (param == nil) then
        Scenario.breakable()
        return not Scenario.animate("idle", "army_001")
    elseif (param == "army_001") then
        return true
    end

    return false
end
function scn_army_attack ( param )

    if (param == nil) then
        Scenario.nomove()
        Scenario.rotate_to_hero()
        Atom.waitmode() -- unit should play idle after scenario end to avoid ass-show
        return not Scenario.animate("attack", "army_001")
    elseif (param == "army_001") then
        return true
    end

    return false
end
function scn_army_damage ( param )

    if (param == nil) then
        return not Scenario.animate("damage", "army_001")
    elseif (param == "army_001") then
        return true
    end

    return false
end
function scn_army_death ( param )

    if (param == nil) then
        return not Scenario.animate("death", "army_001")
    elseif (param == "army_001") then
        return true
    end

    return false
end
function scn_army_happy ( param )

    if (param == nil) then
        return not Scenario.animate("happy", "army_001")
    elseif (param == "army_001") then
        return true
    end

    return false
end
function scn_army_victory ( param )

    if (param == nil) then
        return not Scenario.animate("victory", "army_001")
    elseif (param == "army_001") then
        return true
    end

    return false
end
function scn_army_appear ( param )

    if (param == nil) then
        return not Scenario.animate("appear", "army_001")
    elseif (param == "army_001") then
        return true
    end

    return false
end

function scn_army_custom_animation ( param )

        local animation=Logic.cur_lu_var("animation")
        local anim_time=tonumber(Logic.cur_lu_var("time"))

    if (param == nil) then

        Scenario.stop()
        Scenario.animate(animation, "army_001")
        Scenario.delay( anim_time, "army_000" )

    elseif (param == "army_000") then
        return true
    elseif (param == "army_001") then
        Scenario.animate(animation, "army_001")
        return false
    end

    return false
end

function army_scenario ( scn, param, callpar ) --ftag:mascn

    local scenario_handlers = {
        tupka = scn_army_tupka,
        stop1 = scn_army_stop1,
        stop2 = scn_army_stop2,
        custom_animation = scn_army_custom_animation,
        army_idle = scn_army_idle,
        army_rare = scn_army_rare,
        army_spare = scn_army_spare,
        army_attack = scn_army_attack,
        army_damage = scn_army_damage,
        army_death = scn_army_death,
        army_victory = scn_army_victory,
        army_happy = scn_army_happy,
        army_cast = scn_army_cast,
        army_special = scn_army_special,
        army_appear = scn_army_appear,
        await = scn_army_wait,
        fullstop = scn_fullstop,
        stop_and_work=scn_stop_and_work,
        miner_stop = scn_miner_stop
    }

    local f = scenario_handlers[ scn ]

    if (f ~= nil) then
        return f(param, callpar)
    end

end
