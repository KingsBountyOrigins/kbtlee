
function scn_army_pause_1 ( param )

    if (param == nil) then

        Scenario.stop()
        Scenario.animate("idle", "army_001")
        Scenario.delay( 5, "army_000" )

    elseif (param == "army_000") then
        return true
    elseif (param == "army_001") then
        Scenario.animate("idle", "army_001")
        return false
    end

    return false
end

function scn_army_pause_2 ( param )

    if (param == nil) then

        Scenario.stop()
        Scenario.animate("idle", "army_001")
        Scenario.delay( 10, "army_000" )

    elseif (param == "army_000") then
        return true
    elseif (param == "army_001") then
        Scenario.animate("idle", "army_001")
        return false
    end

    return false
end



function common_army_scenario ( scn, param ) --ftag:mascn

    local scenario_handlers = {
        pause_5 = scn_army_pause_1,
        pause_10 = scn_army_pause_2
    }

    local f = scenario_handlers[ scn ]

    if (f ~= nil) then
        return f(param)
    end

end
