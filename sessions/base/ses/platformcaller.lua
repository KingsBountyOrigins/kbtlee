function pc_idle()

  if Logic.is_available() and Logic.cur_lu_var("call") == "1" then
    Atom.animate("wait")
  else
    Atom.animate("idle")
  end

end

function scn_pc_wait ( param )

    if (param == nil) then

      Scenario.animate("wait","wait")
      Logic.cur_lu_var("call", "1")

    elseif (param == "wait") then
        return true
    end

    return false

end

function scn_pc_idle ( param )

    if (param == nil) then

      Scenario.animate("idle","idle")
      Logic.cur_lu_var("call", "0")

    elseif (param == "idle") then
        return true
    end

    return false

end


function pc_scenario ( scn, param ) --ftag:mascn

    local scenario_handlers = {
      pcwait = scn_pc_wait,
      pcidle = scn_pc_idle
    }

    local f = scenario_handlers[ scn ]

    if (f ~= nil) then
        return f(param)
    end

end
