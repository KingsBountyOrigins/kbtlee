
function minelift()

end

function scn_lift_idle ( param )

    Atom.animate("idle")
    return true

end


function scn_lift_down ( param )

    Atom.swstate("down")
    return true

end


function scn_lift_up ( param )

    Atom.swstate("up")
    return true

end

function lift_scenario ( scn, param ) --ftag:mascn

    local scenario_handlers = {
      liftidle = scn_lift_idle,
      liftup = scn_lift_up,
      liftdown = scn_lift_down
    }

    local f = scenario_handlers[ scn ]

    if (f ~= nil) then
        return f(param)
    end

end
