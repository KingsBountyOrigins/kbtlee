-- ***********************************************
-- * Ray
-- ***********************************************

function beholder_ray_attack()

  local dist2table = {
    { d = 3, a = "ray02m" },
    { d = 5, a = "ray05m" },
    { d = 10, a = "ray10m" }

    -- d   - distance
    -- a   - atom tag
  }

  local target = Attack.get_target()                            -- epicentre

  --local target = Attack.get_targets_count()

  Attack.aseq_rotate(0, target, "cast")
  Attack.act_rotate(0,0.1, 0, target)
  local atk_x = Attack.aseq_time(0, "x")                   -- x time of attacker

  local d = Attack.cell_mdist(0,target) -- dist in meters
  d = math.floor( d +1)

  --local found_item = nil
  --local ldist = 10000

  --for ii,ti in ipairs(dist2table) do
    --local dd = math.abs(ti.d - d)
    --if dd < ldist then
      --ldist = dd
      --found_item = ti
    --end
  --end
  local found_item = "ray02m"
  if d>2 and d<10 then found_item="ray0"..d.."m" end
  if d>9 then found_item="ray"..d.."m" end

  --if found_item ~= nil then

    Attack.act_damage(target)

    local a = Attack.atom_spawn(0,0,found_item,Attack.angleto(0,target))
    local acid_x = Attack.aseq_time(a, "x")
    local acid_y = Attack.aseq_time(a, "y")
    Attack.aseq_timeshift( 0, acid_x - atk_x )

    -- damage time
    Attack.dmg_timeshift( target, acid_y )

      -- seqs of receiver time
    local hit_x = Attack.aseq_time( target, "x" )
    Attack.aseq_timeshift( target, acid_y - hit_x )
    Attack.atom_spawn(target, acid_y - hit_x*2, "hll_beholder_post", 0, true)


    return true
  --end

  --return false
end

-- ***********************************************
-- * Lightning
-- ***********************************************

function archmage_lightning_attack()

  local dist2table = {
    { d = 2, a = "hll_attack_archmage_2lighting" },
    { d = 4, a = "hll_attack_archmage_4lighting" },
    { d = 6, a = "hll_attack_archmage_6lighting" },
    { d = 8, a = "hll_attack_archmage_8lighting" },
    { d = 10, a = "hll_attack_archmage_10lighting" },
    { d = 12, a = "hll_attack_archmage_12lighting" },
    { d = 15, a = "hll_attack_archmage_15lighting" }

    -- d   - distance
    -- a   - atom tag
  }

  local target = Attack.get_target()                            -- epicentre


  --local target = Attack.get_targets_count()

  Attack.aseq_rotate(0, target, "attack")
  local atk_x = Attack.aseq_time(0, "x")                   -- x time of attacker

  local d = Attack.cell_mdist(0,target) -- dist in meters

  local found_item = nil
  local ldist = 10000

  for ii,ti in ipairs(dist2table) do
    local dd = math.abs(ti.d - d)
    if dd < ldist then
      ldist = dd
      found_item = ti
    end
  end

  if found_item ~= nil then

    Attack.act_damage(target)

    local a = Attack.atom_spawn(0,0,found_item.a,Attack.angleto(0,target))
    local acid_x = Attack.aseq_time(a, "x")
    local acid_y = Attack.aseq_time(a, "y")
    Attack.aseq_timeshift( 0, acid_x - atk_x )

    -- damage time
    Attack.dmg_timeshift( target, acid_y )
    Attack.atom_spawn(target, acid_y, "hll_post_archmage_lighting",0,true)


      -- seqs of receiver time
    local hit_x = Attack.aseq_time( target, "x" )
    Attack.aseq_timeshift( target, acid_y - hit_x )


    return true
  end

  return false
end

function archmage_lightning_selcells()

  Attack.railgun(false)
  local ccnt = Attack.cell_count()
  for i=0,ccnt-1 do

    local cell = Attack.cell_get(i)

    if (not Attack.cell_is_empty(cell)) then        -- is empty (has no live object)
      if Attack.act_enemy(cell) then      -- can receive this attack
        if Attack.act_applicable(cell) then      -- can receive this attack
          local d = Attack.cell_dist(0,cell)
          if d < 4 then
            Attack.marktarget(cell)           -- select it
          end
        end
      end
    end
  end
  return true
end

-- ***********************************************
-- * Shaman badabum
-- ***********************************************

function special_shaman_spirit_dance_attack()

  local target = Attack.get_target()                            -- epicentre
  local text=""

  Attack.aseq_remove(0)
  Attack.act_aseq(0,"cast")

  local dmgts1 = Attack.aseq_time(0, "x")                   -- x time of attacker
  local dmgts2 = Attack.aseq_time("oldaxe", "x")   -- x time of effect atom
  Attack.atom_spawn(target, dmgts1-dmgts2, "oldaxe" )      -- summon atom at x time
  local dmgts3 = Attack.aseq_time("oldaxe", "y")

  local count=Attack.act_size(0)
  local dmg_min,dmg_max = text_range_dec(Attack.get_custom_param("damage"))
  local power=tonumber(Attack.get_custom_param("power"))

  local damage=Game.Random(dmg_min,dmg_max)
  local typedmg=Attack.get_custom_param("typedmg")
  Attack.atk_set_damage(typedmg,damage,damage)

  local dmgt = dmgts1-dmgts2+dmgts3
  common_cell_apply_damage(target , dmgt)
  damage = math.min(Attack.act_totalhp(target), (Attack.act_damage_results(target)))

  if not Attack.act_feature(target,"undead,plant,pawn,golem") then --�������� ���� ������ �� �����

  local function check_cure(i)
  	return (Attack.act_ally(i)) and (Attack.act_need_cure(i)) and not Attack.act_feature(i,"plant,undead,pawn,golem") and not Attack.act_temporary(i) 
  end
  -- ������� ������� ������ ����� ������
  local acnt = Attack.act_count()
    local need_cure=0
      for i=1,acnt-1 do
        if check_cure(i) then need_cure=need_cure+1 end
      end
  -- ������������� ���� � �������
  local cure=math.ceil(damage/need_cure*power/100)

  --�����
    for i=1,acnt-1 do
      if check_cure(i) then
      --and (Attack.act_name(i)=="shaman")
        --local name=Attack.act_name(i)
        --  ������� �������� ������� �� ������������ ��� ����������� �����
        --local max_hp = Attack.act_getpar(i,"hitpoint")
        local max_hp = Attack.act_get_par(i,"health")

        local cure_hp=cure
        local cur_hp = Attack.act_hp(i)
        if cure_hp > max_hp - cur_hp then cure_hp = max_hp - cur_hp end

        local a = Attack.atom_spawn(i, dmgt, "effect_total_cure")
        local dmgts = Attack.aseq_time(a, "x")
        Attack.act_cure(i, cure_hp, dmgt+dmgts)

        local name,tcure="",""
        if Attack.act_size(i)>1 then
          name="<label=cpsn_"..Attack.act_name(i)..">"
          tcure="<label=add_blog_healtext_22>"
        else
          name="<label=cpn_"..Attack.act_name(i)..">"
          tcure="<label=add_blog_healtext_21>"
        end

         if Attack.act_size(i)>1 then
            Attack.log(dmgt,"add_blog_cure_2","target",blog_side_unit(i,0),"special",cure_hp)
         else
            Attack.log(dmgt,"add_blog_cure_1","target",blog_side_unit(i,0),"special",cure_hp)
         end

  --      text=text.."<label=add_blog_healtext_1>"..name..tcure..cure_hp.."<label=add_blog_healtext_3>"

       end
    end
  end

--  Attack.act_damage_addlog(target,"add_blog_heal_",true)
--  Attack.log_special(text) -- ��������

  return true
end

-- ***********************************************
-- * Shaman totems
-- ***********************************************

function special_shaman_totem()

  local target, name, health = Attack.get_target(), Attack.get_custom_param("atomname"), tonumber(Attack.get_custom_param("health"))

  local totem = Attack.atom_spawn(target, 0, name)
  Attack.act_aseq( totem, "appear" )

  local count = Attack.act_size(0)
  local hp = health * count
  Attack.act_hp( totem, hp )
  Attack.act_set_par( totem, "health", hp )

  local edg = 1
  if Attack.act_belligerent(0) == 1 then
    edg = 0
  end

  Attack.act_edge( totem, edg )
  Attack.val_store( totem, "power", count )
  Attack.val_store( totem, "belligerent", Attack.act_belligerent(0))
  Attack.log_label("add_blog_place_") -- ��������

  return true

end

function special_demoness_pentagramm()

  local target, name, duration = Attack.get_target(), Attack.get_custom_param("atomname"), Attack.get_custom_param("duration")

  Attack.aseq_rotate(0, target, "cast")
  local pentagramm = Attack.atom_spawn(target, Attack.aseq_time(0, "x"), name, 0)
  --local count = Attack.act_size(0)
  --local hp = health * count
  --Attack.act_hp( totem, hp )

  Attack.val_store( pentagramm, "belligerent", Attack.act_belligerent(0) )
  Attack.val_store( pentagramm, "time_last", duration )
  Attack.log_label("add_blog_place_") -- ��������

  return true

end


-- ***********************************************
-- * alchemist acid cannon
-- ***********************************************

function alchemist_acidcannon_attack()

  local dist2table = {
    { d = 1.8, a = "hll_acidcannon1" },
    { d = 3.6, a = "hll_acidcannon2" },
    { d = 5.4, a = "hll_acidcannon3" }

    -- d   - distance
    -- a   - atom tag
  }

  local target = Attack.get_target()                            -- epicentre


  --local target = Attack.get_targets_count()

  Attack.aseq_rotate(0, target, "attack")
  local atk_x = Attack.aseq_time(0, "x")                   -- x time of attacker

  local d = Attack.cell_mdist(0,target) -- dist in meters

  local found_item = nil
  local ldist = 10000

  for ii,ti in ipairs(dist2table) do
    local dd = math.abs(ti.d - d)
    if dd < ldist then
      ldist = dd
      found_item = ti
    end
  end

  if found_item ~= nil then

    --Attack.act_damage(target)

    local a = Attack.atom_spawn(0,0,found_item.a,Attack.angleto(0,target))
    local acid_x = Attack.aseq_time(a, "x")
    local acid_y = Attack.aseq_time(a, "y")
    Attack.aseq_timeshift( 0, acid_x - atk_x )

    -- damage time
--[[    Attack.dmg_timeshift( target, acid_y )

      -- seqs of receiver time
    local hit_x = Attack.aseq_time( target, "x" )
    Attack.aseq_timeshift( target, acid_y - hit_x )]]

  local targets = {}
  alchemist_acidcannon_enum_targets(target, function (c) table.insert(targets, c) end)

  for i,target in ipairs(targets) do
      common_cell_apply_damage(target, acid_y)       -- apply damage
  end


    return true
  end

  return false
end

function alchemist_acidcannon_filter( cell, present )
  if present then
    return true
  end
  return Attack.act_enemy(cell) and Attack.act_applicable(cell)
end

function alchemist_acidcannon_selcells()

  --Attack.railgun(true)
  local ccnt = Attack.cell_count()
  for i=0,ccnt-1 do

    local cell = Attack.cell_get(i)

    if (not Attack.cell_is_empty(cell)) then        -- is empty (has no live object)
      if Attack.act_enemy(cell) or Attack.act_pawn(cell) then      -- can receive this attack
        if Attack.act_applicable(cell) then      -- can receive this attack
          local d = Attack.cell_dist(0,cell)
          if d < 4 then
            Attack.marktarget(cell)           -- select it
          end
        end
      end
    end
  end
  return true
end


function alchemist_acidcannon_enum_targets(target, func)

  local ang = Attack.angleto(0, target)
  local dir = Game.Ang2Dir(ang)
  local ang2 = Game.Dir2Ang(dir)

  function select(c)
    --local id = Attack.cell_id(c)
    if Attack.cell_present(c) and (Attack.act_enemy(c) or Attack.act_ally(c) or Attack.act_pawn(c)) then
        func(c)
    end
  end

  if (math.abs(ang - ang2) < .001) then -- �������� �� ������
    local c = 0
    while true do
      c = Attack.cell_adjacent(c, dir)
      if c == nil then break end
      select(c)
      if Attack.cell_id(c) == Attack.cell_id(target) then break end
    end
  elseif Attack.cell_dist(0, target) == 2 then
    select(target)
  else
    c = Attack.cell_adjacent(0, dir)
    if c == nil then return true end
    select(c)
    if wrap(ang - ang2, -math.pi, math.pi) > 0 then
      c = Attack.cell_adjacent(c, wrap(dir+1, 6))
    else
      c = Attack.cell_adjacent(c, wrap(dir-1, 6))
    end
    if c == nil then return true end
    select(c)
    c = Attack.cell_adjacent(c, dir)
    if c == nil then return true end
    select(c)
  end

end

function alchemist_acidcannon_highlight(target)

  alchemist_acidcannon_enum_targets(target, function (c) Attack.cell_select(c, "avenemy") end)

  return true

end


-- ***********************************************
-- * Ogre Rage
-- ***********************************************

function special_ogre_rage_attack()

--  local target = Attack.get_target()
  local duration = Attack.get_custom_param("duration")
  local power = Attack.get_custom_param("power")
  local attack = Attack.act_get_par(0, "attack")

--      if target == nil then

    Attack.act_del_spell(0,"spell_weakness")
    Attack.act_del_spell(0,"special_ogre_rage")

		Attack.act_ap(0, Attack.act_ap(0) + tonumber(Attack.get_custom_param("ap")))
    Attack.act_apply_spell_begin( 0, "special_ogre_rage", duration, false )
        Attack.act_apply_par_spell( "attack", attack*power/100, 0, 0, duration, false)
    Attack.act_apply_spell_end()
        Attack.act_aseq( 0, "cast" )
        --Attack.atom_spawn(0, 0, "magic_bondage", Attack.angleto(0))
--      end
  return true
end

-- ***********************************************
-- * Magic shield
-- ***********************************************
function special_magic_shield_attack()

  local target = Attack.get_target()

  if (target ~= nil) then
    local duration = Logic.obj_par("special_magic_shield","duration")
    local dfactor = Logic.obj_par("special_magic_shield","power")

    Attack.act_del_spell(target,"special_magic_shield")

    Attack.act_apply_spell_begin( target, "special_magic_shield", duration, false )
    --Attack.act_apply_par_spell( "dfactor", dfactor/100, 0, 0, duration, false)
      Attack.act_posthitslave(target, "post_magic_shield", duration)
    Attack.act_apply_spell_end()

    Attack.aseq_rotate(0, target, "cast")
--    Attack.act_aseq( 0, "cast" )
    dmgts = Attack.aseq_time(0, "x")

    Attack.atom_spawn(target, dmgts , "shield" )
  end

  return true
end

-- ***********************************************
-- * Berserk
-- ***********************************************

function special_berserk_attack()

  local duration = Logic.obj_par("special_berserk","duration")
  local power = Logic.obj_par("special_berserk","power")
  local penalty = Logic.obj_par("special_berserk","penalty")

  local attack = Attack.act_get_par(0, "attack")
  local initiative = Attack.act_get_par(0, "initiative")
  local defense = Attack.act_get_par(0, "defense")
  local krit = Attack.act_get_par(0, "krit")

--    if target == nil then
    Attack.act_apply_spell_begin( 0, "special_berserk", duration, false )
        Attack.act_apply_par_spell( "attack", 0, power, 0, duration, false)
        Attack.act_apply_par_spell("autofight", 1, 0, 0, duration, false)
        Attack.act_apply_par_spell( "initiative", 0, power, 0, duration, false)
        Attack.act_apply_par_spell( "defense", 0, -penalty, 0, duration, false)
        Attack.act_apply_par_spell( "krit", krit*power/100, 0, 0, duration, false)
    Attack.act_apply_spell_end()
    Attack.act_aseq( 0, "cast" )

  --  Attack.atom_spawn(0, 0, "magic_lioncup", Attack.angleto(0))
--    end
  return true
end

-- ***********************************************
-- * Haste
-- ***********************************************

function special_haste_attack()

  local duration = Logic.obj_par("special_haste","duration")
  local power = Logic.obj_par("special_haste","power")

  local speed = Attack.act_get_par(0, "speed")

--    if target == nil then

--    Attack.act_del_spell(0,"special_haste")
--    Attack.act_del_spell(0,"spell_haste")

--    Attack.act_apply_spell_begin( 0, "special_haste", duration, false )
--        Attack.act_apply_par_spell( "speed", speed*power/100, 0, 0, duration, false)
--    Attack.act_apply_spell_end()
      Attack.act_aseq( 0, "speed" )
		    Attack.act_ap(0, Attack.act_ap(0) + Attack.act_ap(0))
      Attack.atom_spawn(0, 0, "magic_haste", Attack.angleto(0))
      --Attack.resort(0)
--        end
  return true
end

-- ***********************************************
-- * Spider Web
-- ***********************************************
function special_spider_web_attack()

  local target = Attack.get_target()

  if (target ~= nil) then
    local duration = Logic.obj_par("special_spider_web","duration")

    Attack.act_del_spell(target,"special_spider_web")

    Attack.act_apply_spell_begin( target, "special_spider_web", duration, false )
        --Attack.act_skipmove(target,duration)
      Attack.act_apply_par_spell( "dismove", 1, 0, 0, duration, false)
    Attack.act_apply_spell_end()
    --Attack.angleto(0, target)
    Attack.aseq_rotate(0,target)
    local start = Attack.aseq_time(0)

	Attack.atom_spawn(target, start, "spiderweb_effect", Attack.angleto(0,target))
    Attack.act_attach_atom( target, "spiderweb", start )
    Attack.act_aseq( 0, "cast" )
  end

  return true
end

function spider_web_onremove(caa,duration_end)
    Attack.act_deattach_atom( caa, "spiderweb", 0 )
    return true
end

-- ***********************************************
-- * Bless
-- ***********************************************

function special_bless_attack()

  local target = Attack.get_target()

    Attack.aseq_rotate(0, target, "cure")
--    Attack.act_aseq( 0, "cast" )
    dmgts = Attack.aseq_time(0, "x")

  if target~=nil  then
    --Attack.act_ally(target) then
if Attack.act_race(target)~="undead" then
    Attack.act_del_spell(target,"spell_bless")
    --Attack.act_del_spell(target,"special_holy_rage")
    Attack.act_del_spell(target,"spell_weakness")
    Attack.act_del_spell(target,"special_weakness")

    local mind, mindfire = Attack.act_get_dmg_min(target, "fire")
    local mind, mindpoison = Attack.act_get_dmg_min(target, "poison")
    local mind, mindmagic = Attack.act_get_dmg_min(target, "magic")
    local mind, mindphysical = Attack.act_get_dmg_min(target, "physical")
    local maxd, maxdfire = Attack.act_get_dmg_max(target, "fire")
    local maxd, maxdpoison = Attack.act_get_dmg_max(target, "poison")
    local maxd, maxdmagic = Attack.act_get_dmg_max(target, "magic")
    local maxd, maxdphysical = Attack.act_get_dmg_max(target, "physical")

    local bonfire = maxdfire - mindfire
    local bonpoison = maxdpoison - mindpoison
    local bonmagic = maxdmagic - mindmagic
    local bonphysical = maxdphysical - mindphysical

    local duration = Logic.obj_par("special_holy_rage","duration")
    Attack.act_apply_spell_begin( target, "spell_bless", duration, false )
      --[[Attack.act_apply_dmgmin_spell( "fire", bonfire, 0, 0, duration, false)
      Attack.act_apply_dmgmin_spell( "poison", bonpoison, 0, 0, duration, false)
      Attack.act_apply_dmgmin_spell( "magic", bonmagic, 0, 0, duration, false)
      Attack.act_apply_dmgmin_spell( "physical", bonphysical, 0, 0, duration, false)]]
    Attack.act_apply_spell_end()
    if Attack.get_custom_param("rage")=="0" then
        Attack.atom_spawn(target, dmgts, "magic_bless", Attack.angleto(target))
    else

      local damage=Logic.obj_par("special_holy_rage","damage")
      --local duration = Logic.obj_par("special_holy_rage","duration")
    if not Attack.act_feature(target, "plant") and not Attack.act_feature(target, "golem") then 
      Attack.act_apply_spell_begin( target, "special_holy_rage", duration, false )
        Attack.act_posthitmaster(target, "features_holy_attack" ,duration)
      Attack.act_apply_spell_end()
      Attack.atom_spawn(target, dmgts+0.3, "effect_hard_bless",0,true)
    end 
    if Attack.act_human(0) then
      local rage=Game.Random(text_range_dec(Logic.obj_par("special_holy_rage","rage")))
      local cur_rage=Logic.hero_lu_item("rage","count")
      Logic.hero_lu_item("rage","count",cur_rage+rage)
    end
		
		  Attack.atom_spawn(target, dmgts, "magic_bless", Attack.angleto(target))
    end
    else
    	if not Attack.act_feature(target, "plant") and not Attack.act_feature(target, "golem") then
      	local duration = Logic.obj_par("effect_holy","duration")
      	Attack.act_del_spell(target,"effect_holy")
      	effect_holy_attack(target,dmgts,duration)
      end 
    end

  end
  return true
end

-- ***********************************************
-- * Stupid
-- ***********************************************
function special_stupid_attack()

  local target = Attack.get_target()

  if (target ~= nil) then
    local duration = Logic.obj_par("special_stupid","duration")

    Attack.act_del_spell(target,"special_stupid")
    Attack.act_del_spell(target,"spell_magic_bondage")

    Attack.act_apply_spell_begin( target, "special_stupid", duration, false )
      Attack.act_apply_par_spell( "disreload", 10, 0, 0, duration, false)
      Attack.act_apply_par_spell( "disspec", 10, 0, 0, duration, false)
    Attack.act_apply_spell_end()

    Attack.act_aseq( 0, "cast" )
    dmgts = Attack.aseq_time(0, "x")

    Attack.atom_spawn(target, dmgts, "magic_bondage", Attack.angleto(target))

  end

  return true
end

-- ***********************************************
-- * Wolf cry
-- ***********************************************

function check_wolf_cry(i)

    local level = tonumber(Logic.obj_par("special_wolf_cry","level"))
    return Attack.act_enemy(i) and (Attack.act_race(i)=="human" or Attack.act_race(i)=="elf" or Attack.act_race(i)=="dwarf" or (string.find(Attack.act_name(i),"pirat") or string.find(Attack.act_name(i),"barbarian"))) and Attack.act_level(i)<=level

end

function special_wolf_cry_attack()

--  local target = Attack.get_target()
  local duration = tonumber(Logic.obj_par("special_wolf_cry","duration"))
  local power = tonumber(Logic.obj_par("special_wolf_cry","power"))

--      if target == nil then

  Attack.act_aseq( 0, "rare" )
  dmgts = Attack.aseq_time(0, "x")


  local acnt = Attack.act_count()
  for i=1,acnt-1 do
    if check_wolf_cry(i) then
      local rnd=Game.Random(100)+1

            Attack.act_apply_spell_begin( i, "effect_fear", duration, false )
              Attack.act_apply_par_spell("autofight", 1, 0, 0, duration, false)
              if rnd<=power then Attack.act_skipmove(i,1) end
            Attack.act_apply_spell_end()
            Attack.atom_spawn(i, dmgts, "magic_scare", Attack.angleto(i))
            if Attack.act_size(i)>1 then
              Attack.log(0.001,"add_blog_fear_2", "targets", "   "..blog_side_unit(i,0))
            else
              Attack.log(0.001,"add_blog_fear_1", "target", "   "..blog_side_unit(i,0))
            end
    end
  end

        --Attack.atom_spawn(0, 0, "magic_bondage", Attack.angleto(0))
--      end
  return true
end


-- ***********************************************
-- * Werewolf
-- ***********************************************

function special_transform()
  Attack.act_aseq( 0, "transform" )
  Attack.act_del_spell(0)
  Attack.act_transform(0,Attack.aseq_time(0))
  local name=Attack.act_name(0)
  if string.find(name,"bat") or name=="wolf" then
    Attack.log_label("add_blog_tr_") -- ��������
  end
  if string.find(name,"vampire") then
    Attack.log_label("add_blog_trbat_") -- ��������
  end
  if name=="werewolf" then
    Attack.log_label("add_blog_trwolf_") -- ��������
  end


  return true

end


-- ***********************************************
-- * Resurrect (phoenix)
-- ***********************************************

function special_resurrect()
	
 for i=1,10 do
 	
 	local name = Attack.act_name(Attack.cell_get_corpse(0))
 	if string.find(name,"phoenix") then
  	Attack.act_resurrect(0)
  	Attack.val_store(0,"lifes",Attack.val_restore(0,"lifes")-1);
  	return true
  else
  	Attack.cell_remove_last_corpse(0)
  end 
 end 
end

function special_change_place()

  local cycle = Attack.get_cycle()
  local target
  local sname=""

  if Attack.is_computer_move() then
    if cycle ~= 0 then Game.MessageBox('����� �� ����� ���� ������ ��� ����� �� ����� ����') end
      Attack.val_store("change_place", Attack.get_target())
  --[[        local targets = {}
      calccells_change_place( function (i) table.insert(targets,i) end )
      if table.getn(targets) == 0 then return true end
    target = targets[ Game.Random(1,table.getn(targets)) ] ]]
    target = Attack.cell_id(tonumber(Attack.val_restore("target")))
    cycle = 1
  end

  if (cycle == 0) then
    Attack.val_store("change_place", Attack.get_target())
   Attack.next(0)
  elseif (cycle == 1) then
    local source = Attack.val_restore("change_place")

    if target == nil then
      target = Attack.get_target()
    end

    if ((source ~= nil) and (target ~= nil)) then
      local tx,ty = Attack.act_get_center(target)
      local sx,sy = Attack.act_get_center(source)

          Attack.act_aseq(source, "telein")
          Attack.act_aseq(target, "telein")
          Attack.atom_spawn(source, 0, "hll_telein" )
          Attack.atom_spawn(target, 0, "hll_telein" )

          local t = Attack.aseq_time(source)

          Attack.act_aseq(source, "teleout" )
          Attack.act_aseq(target, "teleout" )
          Attack.atom_spawn(source, t, "hll_teleout" )
          Attack.atom_spawn(target, t, "hll_teleout" )

          Attack.act_teleport(source, target, t)
      --Attack.act_teleport(target, source, 0)

         Attack.log("change_place","name",blog_side_unit(0),"target",blog_side_unit(source,-1),"targeta",blog_side_unit(target,-1))
      end
    end
--      if Attack.act_size(target)>1 then
--        sname="<label=cpsn_"..Attack.act_name(target)..">"
--      else
--        sname="<label=cpn_"..Attack.act_name(target)..">"
--
--      Attack.log_special(sname) -- ��������

--  end

      --Attack.log_label("add_blog_change_") -- ��������
      Attack.act_aseq( 0, "castmagic" )

  return true

end

-- ***********************************************
-- * Summon Thorn
-- ***********************************************

function special_cast_thorn()

  local target = Attack.get_target()

    Attack.aseq_rotate(0,target)
    local dmgts = Attack.aseq_time(0)
    if string.find(Attack.act_name(0),"thorn") then
      Attack.act_aseq( 0, "special" )
    else
      Attack.act_aseq( 0, "cast" )
    end
    dmgts = dmgts+Attack.aseq_time(0, "x")*2

    --������� ���������� �����
  local nearest_dist, nearest_enemy, ang_to_enemy = 10e10, nil, 0
   for i=1,Attack.act_count()-1 do
    if Attack.act_enemy(i) then
      local d = Attack.cell_dist(target,i)
      if d < nearest_dist then nearest_dist = d; nearest_enemy = i; end
    end
   end

    if nearest_enemy ~= nil then ang_to_enemy = Attack.angleto(target, nearest_enemy) end

    local count=Attack.act_size(0)
    local count_f=0
    local min_count,max_count=text_range_dec(Attack.get_custom_param("count"))

    local count_f=Game.Random(min_count*count,max_count*count)

    local type_thorn=Game.Random(100)
    local count_summon=0
    local unit_summon=""
    if type_thorn<50 then
      unit_summon="thorn"
    else
      unit_summon="thorn_warrior"
    end
    count_summon=math.ceil(count_f/Attack.atom_getpar(unit_summon,"leadership"))
    Attack.act_spawn(target, 0, unit_summon, ang_to_enemy, count_summon)
    Attack.act_nodraw(target, true)
    local t = Attack.aseq_time(0) - .3
    Attack.act_animate(target, "grow", t)
    Attack.act_nodraw(target, false, t)
      Attack.log_label("add_blog_summon_") -- ��������
      Attack.log_special(count_summon) -- ��������

    if Attack.act_name(0)=="thorn" or Attack.act_name(0)=="thorn_warrior" then
      local t = 2.
      if Attack.cell_remove_last_corpse(Attack.get_target(),t+.01) then
        Attack.act_fadeout(Attack.cell_get_corpse(Attack.get_target()),0,t)
      end
    end
    --Attack.act_aseq(target, "grow")

    --Attack.act_aseq( 0, "cast" )

  return true

end

-- ***********************************************
-- * Preparation
-- ***********************************************

function special_preparation_attack()

--  local target = Attack.get_target()
  local duration = Logic.obj_par("special_preparation","duration")
  local speed = Logic.obj_par("special_preparation","power")
--  local krit = Logic.obj_par("special_preparation","krit")
  --local cur_krit=Attack.act_get_par(0, "krit")

  --local krit_bonus=krit-cur_krit
--      if target == nil then

    Attack.act_del_spell(0,"special_preparation")

    Attack.act_apply_spell_begin( 0, "special_preparation", duration, false )
--        Attack.act_apply_par_spell( "krit", krit, 0, 0, duration, false)
--        Attack.act_apply_par_spell( "speed", speed, 0, 0, duration, false)
    Attack.act_apply_spell_end()
--      end
        Attack.act_aseq( 0, "special" )
  return true
end

-- ***********************************************
-- * Suicide
-- ***********************************************

function special_suicide()

  Attack.act_aseq( 0, "death" )
  dmgts = Attack.aseq_time(0, "x")

  Attack.atom_spawn(0, dmgts, "catapultfireball")
  local burn=tonumber(Attack.get_custom_param("burn"))

  local acnt = Attack.act_count()
  for i=1,acnt-1 do
    if (Attack.act_enemy(i) or Attack.act_ally(i)) and Attack.cell_dist(0,i)==1 then      -- contains enemy and level
      if Attack.act_applicable(i) then      -- can receive this attack
         local rnd=Game.Random(100)+1
         common_cell_apply_damage(i,dmgts)
         if rnd<=burn then
            effect_burn_attack(i,dmgts,3)
         end
      end
    end
  end
  Attack.act_kill(0,true)

  return true
end

-- ***********************************************
-- * Summon Bear
-- ***********************************************

function special_cast_bear()

  local target = Attack.get_target()

    Attack.aseq_rotate(0,target)
    Attack.act_aseq( 0, "cast" )
    local dmgts = Attack.aseq_time(0, "x")

    --������� ���������� �����
  local nearest_dist, nearest_enemy, ang_to_enemy = 10e10, nil, 0
   for i=1,Attack.act_count()-1 do
    if Attack.act_enemy(i) then
      local d = Attack.cell_dist(target,i)
      if d < nearest_dist then nearest_dist = d; nearest_enemy = i; end
    end
   end

    if nearest_enemy ~= nil then ang_to_enemy = Attack.angleto(target, nearest_enemy) end

    local count=Attack.act_size(0)
    local count_bear=tonumber(Attack.get_custom_param("count"))
    local caster=tonumber(Attack.get_custom_param("caster"))
--    local cast_bear=0
    local min_count,max_count=text_range_dec(Attack.get_custom_param("count"))

    local type_bear=Game.Random(100)+1
    --local d = Attack.atom_spawn(target, dmgts, "effect_cast_bear", Attack.angleto(0,target))
--    local dmgts1 = Attack.aseq_time(d, "x")
    local count_summon=0
    local unit_summon=""
    if type_bear<50 then
      unit_summon="bear2"
    else
          unit_summon="bear"
    end
		-- ���������� � ������� �������, �� �� ������ 1 �����. ������������� ��������
    count_summon=math.max(1,math.ceil(Game.Random(min_count*count,max_count*count)/Attack.atom_getpar(unit_summon,"hitpoint")))

    Attack.act_spawn(target, 0, unit_summon, ang_to_enemy,count_summon)

    Attack.act_nodraw(target, true)
    local t = Attack.aseq_time(0) - dmgts
    Attack.act_animate(target, "castbear", dmgts)
    Attack.act_nodraw(target, false, dmgts)
      Attack.log_label("add_blog_summon_") -- ��������
      Attack.log_special(count_summon) -- ��������

    --Attack.act_aseq( 0, "cast" )

  return true

end

-- ***********************************************
-- * Faery
-- ***********************************************

function special_heal()

  local target = Attack.get_target()
  local count=Attack.act_size(0)

  local heal=tonumber(Attack.get_custom_param("heal"))*count
  Attack.act_aseq( 0, "cure" )
  local dmgts = Attack.aseq_time(0, "x")
  local a = Attack.atom_spawn(target, dmgts, "hll_priest_heal_post")
  local dmgts1 = Attack.aseq_time(a, "x")
        local max_hp = Attack.act_get_par(target,"health")
        local cur_hp = Attack.act_hp(target)
        if heal > max_hp - cur_hp then heal = max_hp - cur_hp end
        Attack.act_cure(target, heal, dmgts)
        Attack.log_label("cure_") -- ��������
        Attack.log_special(heal)
  return true
end

function special_presurrect()

  local target = Attack.get_target()
  local count=Attack.act_size(0)
  local count_1=Attack.act_size(target)
  local hp_1= Attack.act_hp(target)

  local heal=tonumber(Attack.get_custom_param("heal"))*count
  Attack.act_aseq( 0, "cure" )
  local dmgts = Attack.aseq_time(0, "x")
  local a = Attack.atom_spawn(target, dmgts, "hll_priest_resur_cast")
  local dmgts1 = Attack.aseq_time(a, "x")

--        if max_hp==0 then max_hp=Attack.act_get_par(Attack.cell_get_corpse(target),"health") end

        Attack.cell_resurrect(target, heal, dmgts+dmgts1)
  --        if cure>0 and Attack.cell_get_corpse(target)~=nil then res=res+1 end
        Attack.log_label("respawn_") -- ��������
    local count_2=Attack.act_size(target)
    local hp_2= Attack.act_hp(target)

      if count_1<count_2 then
        Attack.act_damage_addlog(target,"res_")
        Attack.log_special(count_2-count_1)
      elseif count_1>count_2 or (hp_1==0 and count_1==count_2) then
        Attack.act_damage_addlog(target,"res_")
        Attack.log_special(count_2)
      elseif hp_1<hp_2 and hp_1~=0 then --
        Attack.act_damage_addlog(target,"cur_")
        Attack.log_special(hp_2-hp_1)
      end

  return true
end

-- ***********************************************
-- * Dispell
-- ***********************************************

function special_dispell()

  local target = Attack.get_target()
  Attack.act_aseq( 0, "special" )
  local dmgts = Attack.aseq_time(0, "x")

  Attack.act_del_spell(target)
  Attack.atom_spawn(target, dmgts, "magic_dispel")
  return true
end

-- ***********************************************
-- * Battle Mage
-- ***********************************************

function special_battle_mage_attack()

  local duration = Logic.obj_par("special_battle_mage","duration")
  local power = tonumber(Logic.obj_par("special_battle_mage","power"))
  local penalty = tonumber(Logic.obj_par("special_battle_mage","penalty"))
  local shock = Logic.obj_par("special_battle_mage","shock")
  local bon_krit = Logic.obj_par("special_battle_mage","krit")

  local krit = Attack.act_get_par(0, "krit")

    --if target == nil then
    Attack.act_apply_spell_begin( 0, "special_battle_mage", duration, false )
        Attack.act_apply_dmgmin_spell( "magic", 0, power, 0, duration, false)
        Attack.act_apply_dmgmax_spell( "magic", 0, power, 0, duration, false)
        Attack.act_apply_par_spell( "defense", 0, -penalty, 0, duration, false)
        Attack.act_apply_par_spell( "krit", (bon_krit-krit), 0, 0, duration, false)
      --Attack.act_apply_par_spell( "disreload", 10, 0, 0, duration, false)
      Attack.act_apply_par_spell( "disspec", 10, 0, 0, duration, false)
    Attack.act_apply_spell_end()
    Attack.act_spell_param(0, "special_battle_mage", "shock", shock)
        Attack.act_aseq( 0, "special" )
    --Attack.resort(0)

        --Attack.atom_spawn(0, 0, "magic_lioncup", Attack.angleto(0))
    --    end
  return true
end

-- ***********************************************
-- * Telekines
-- ***********************************************

function special_telekines()
  local cycle = Attack.get_cycle()
  local target

  if (cycle == 0) then
    Attack.val_store("telekines", Attack.get_target())
    if Attack.is_computer_move() then
        local targets = {}
        calccells_telekines( function (i) table.insert(targets,i) end )
        if table.getn(targets) == 0 then return true end
    target = targets[ Game.Random(1,table.getn(targets)) ]
    cycle = 1
    else
      Attack.next(0)
  end
  end

  if (cycle == 1) then
    local source = Attack.val_restore("telekines")
    if target == nil then target = Attack.get_target() end

    Attack.aseq_rotate(0,source)
    Attack.act_aseq(0,"cast")
    local dmgts = Attack.aseq_time(0, "x")

    local angle = Attack.angleto(target,source)
    Attack.atom_spawn(target, dmgts, "effect_telekines",angle)
    if ((source ~= nil) and (target ~= nil)) then
      Attack.act_move( 1, 3, source, target )
      --common_cell_apply_damage(t2, (movtime0+movtime1)/2)
    end
  end

  return true

end

-- ***********************************************
-- * ������������
-- ***********************************************
function special_aiming_attack()

  local duration = tonumber(Logic.obj_par("special_aiming","duration"))
  local power = tonumber(Logic.obj_par("special_aiming","power"))

--    if target == nil then
    Attack.act_apply_spell_begin( 0, "special_aiming", duration, false )
        Attack.act_apply_par_spell( "attack", 0, power, 0, duration, false)
        Attack.act_apply_dmg_spell( "physical", 0, power, 0, duration, false)
    Attack.act_apply_spell_end()
    Attack.act_aseq( 0, "rare" )
--    end
  return true
end

-- ***********************************************
-- * ���������� �����
-- ***********************************************
function special_elf_song_attack()

  local duration = tonumber(Logic.obj_par("special_elf_song","duration"))
  local power = tonumber(Logic.obj_par("special_elf_song","power"))

--    if target == nil then

    Attack.act_aseq(0,"song")
    local dmgts = Attack.aseq_time(0, "x")                   -- x time of attacker

    acnt = Attack.act_count()
    for i=1,acnt-1 do
      if (Attack.act_ally(i)) and (Attack.act_race(i)=="elf") then        -- contains ally
        local a = Attack.atom_spawn(i, dmgts, "effect_elvensong")
        Attack.act_del_spell( i, "special_elf_song" )
        Attack.act_apply_spell_begin( i, "special_elf_song", duration, false )
          Attack.act_apply_par_spell("initiative", power, 0, 0, duration, false)
        Attack.act_apply_spell_end()
       end
    end

--    end
  return true
end

-- ***********************************************
-- * ��������� �������
-- ***********************************************

function check_cast_sleep(i, level, nfeatures)

    return Attack.act_enemy(i) and Attack.act_level(i)<=level and not Attack.act_feature(i, nfeatures)

end

function special_cast_sleep()

--  local target = Attack.get_target()
  local duration = tonumber(Attack.get_custom_param("duration"))
  local level = tonumber(Attack.get_custom_param("level"))
  local nfeatures = Attack.get_custom_param("nfeatures")

--      if target == nil then

  Attack.act_aseq( 0, "sleep" )
  local dmgts = Attack.aseq_time(0, "x")                   -- x time of attacker

  local acnt = Attack.act_count()
  for i=1,acnt-1 do
    if check_cast_sleep(i, level, nfeatures) then
      effect_sleep_attack(i,dmgts,duration)
            if Attack.act_size(i)>1 then
              Attack.log(0.001,"add_blog_sleep_2", "targets", "   "..blog_side_unit(i,0))
            else
              Attack.log(0.001,"add_blog_sleep_1", "target", "   "..blog_side_unit(i,0))
            end
    end
  end

        --Attack.atom_spawn(0, 0, "magic_bondage", Attack.angleto(0))
--      end
  return true
end

-- ***********************************************
-- * Giant Quake
-- ***********************************************

function special_giant_quake()

--  local target = Attack.get_target()
  --local dmg_min,dmg_max = text_range_dec(Attack.get_custom_param("damage"))
  --local power=tonumber(Attack.get_custom_param("power"))

  Attack.act_aseq( 0, "special" )
  local dmgts = Attack.aseq_time(0, "x")
  local dmg_min,dmg_max = text_range_dec(Attack.get_custom_param("damage"))
  local k=tonumber(Attack.get_custom_param("k"))
  local typedmg=Attack.get_custom_param("typedmg")

  local acnt = Attack.act_count()
  for i=1,acnt-1 do
    if Attack.act_enemy(i) and Attack.act_mt(i)==0 and Attack.act_applicable(i) then
      local dist = Attack.cell_dist(0,i)-1
      Attack.atk_set_damage(typedmg,dmg_min*(1-k*dist/100),dmg_max*(1-k*dist/100))
      common_cell_apply_damage(i, dmgts)
    end
    if Attack.act_enemy(i) and Attack.act_mt(i)==2 and Attack.act_applicable(i) then
      Attack.act_aseq( i, "takeoff" )
      Attack.act_aseq( i, "descent" )
    end
  end

  --      if target == nil then

  return true
end

-- ***********************************************
-- * Poison Cloud
-- ***********************************************

function special_poison_cloud()

  Attack.act_aseq( 0, "special" )
  dmgts = Attack.aseq_time(0, "x")
  --Attack.atom_spawn(0, dmgts, "hll_shaman_post")
  local poison=tonumber(Attack.get_custom_param("poison"))

  local acnt = Attack.act_count()
  for i=1,acnt-1 do
    if (Attack.act_enemy(i) or Attack.act_ally(i)) and Attack.cell_dist(0,i)==1 then      -- contains enemy and level
      if Attack.act_applicable(i) then      -- can receive this attack
         local rnd=Game.Random(100)+1
         common_cell_apply_damage(i,dmgts)
         if rnd<=poison then
            effect_poison_attack(i,dmgts,3)
         end
      end
    end
  end

  return true
end


function spell_spawn_atom()

  Attack.atom_spawn(atom_spawn_target, 0, atom_spawn_name)
  return true

end

-- ***********************************************
-- * �����������
-- ***********************************************

function special_beast_training()
    local level = tonumber(Attack.get_custom_param("level"))
    local duration = tonumber(Attack.get_custom_param("duration"))
    local target = Attack.get_target()

    Attack.aseq_rotate(0,target)
    Attack.act_aseq( 0, "cast" )
    dmgts = Attack.aseq_time(0, "x")

    effect_charm_attack(target,dmgts ,duration,"effect_beast_training")


  return true
end

-- ***********************************************
-- * Summon Undead
-- ***********************************************

function special_animate_dead()

    local target = Attack.cell_get_corpse(Attack.get_target())

    Attack.aseq_rotate(0,target)
    Attack.act_aseq( 0, "cast" )
    local dmgts = Attack.aseq_time(0, "x")

    local name=actor_name(target)
    local unit_animate=necro_get_unit(name)

    local bel = 0
    if string.find(name,"priest") then --����������� - � �����������

        unit_animate = name

        local necro_side=Attack.act_belligerent(0)
        -- ������������ ���������� ������ �����������
        if necro_side == 1 or necro_side == 2 then bel = 4 else bel = 2 end

    end

    local animate_count_base=Attack.act_initsize(target)  --������� ������ � �����
    local k=Game.Random(text_range_dec(Attack.get_custom_param("k"))) --����.

    --if nearest_enemy ~= nil then ang_to_enemy = Attack.angleto(target, nearest_enemy) end

    local necro_count=Attack.act_size(0)  -- ������� �����
    local necro_name=Attack.act_name(0)
    --��������� ����� � ������
    local necro_lead=Attack.atom_getpar(necro_name,"leadership")
    local undead_lead=Attack.atom_getpar(unit_animate,"leadership")
    -- ������� ����� ������� �� ���������
    local animate_count_lead=math.floor(necro_lead*necro_count/undead_lead*k/100)
    -- �������
    local animate_real=math.min(animate_count_lead,animate_count_base)
    if animate_real<1 then
      unit_animate = "skeleton"
      undead_lead=Attack.atom_getpar(unit_animate,"leadership")
      animate_count_lead=math.floor(necro_lead*necro_count/undead_lead*k/100)
      animate_real=math.min(animate_count_lead,animate_count_base)
      bel = 0
    end

--    local cast_bear=0
      animate_dead(Attack.get_target(), target, Attack.aseq_time(0, "x"), bel, unit_animate, animate_real)

    --Attack.act_aseq(target, "respawn")
--  add_blog_necro_1=^blog_td0^[name] ��������� ������ ������ � ������ ����� ([target]) ����������� � ���� ������. [targeta]: [g][special][/c] �������� �� ����� �� ���� �������.
      if animate_real>1 then
        Attack.log("add_blog_necro_2","name",blog_side_unit(0,1),"target",blog_side_unit(target,0),"targeta",blog_side_unit(Attack.get_target(),0),"special",animate_real)
      else
Attack.log("add_blog_necro_1","name",blog_side_unit(0,1),"target",blog_side_unit(target,0),"targeta",blog_side_unit(Attack.get_target(),0),"special",animate_real)
      end

  return true

end


--special_demoness_charm
-- ***********************************************
-- * �����������
-- ***********************************************

function special_demoness_charm()

  local target = Attack.get_target()

    Attack.aseq_rotate(0,target)
    Attack.act_aseq( 0, "special" )
    local dmgts = Attack.aseq_time(0, "x")

    local k=text_range_dec(Attack.get_custom_param("k"))  --����.
    local duration=tonumber(Attack.get_custom_param("duration"))  --����.

    local demon_count=Attack.act_size(0)  -- ������� �����
    local demon_name=Attack.act_name(0)
    --��������� ����� � ����
    local demon_lead=Attack.atom_getpar(demon_name,"leadership")
    local target_lead=Attack.act_leadership(target)
    local target_count=Attack.act_size(target)
    -- ������� ����� ���������� �� ���������
    if demon_lead*demon_count*k/100>target_lead*target_count and Attack.act_level(target)<5 and effect_charm_attack(target,dmgts ,duration) then
      Attack.log_label("charm_")
    else
      if Attack.cell_dist(0,target)==1 then
        Attack.act_aseq( 0, "attack" )
      else
        Attack.act_aseq( 0, "longattack" )
      end
      local dmgts1 = Attack.aseq_time(0, "x")

      --local dmg_min,dmg_max = text_range_dec(Attack.get_custom_param("damage"))
      --local typedmg=Attack.get_custom_param("typedmg")
      --Attack.atk_set_damage(typedmg,dmg_min,dmg_max)

      common_cell_apply_damage(target, dmgts1)
      Attack.log_label("")
    end

    return true

end

-- ***********************************************
-- * Summon Demon
-- ***********************************************

function special_summon_demon()

  local target = Attack.get_target()

    Attack.aseq_rotate(0,target)
    Attack.act_aseq( 0, "cast" )
    dmgts = Attack.aseq_time(0, "x")

    --������� ���������� �����
  local nearest_dist, nearest_enemy, ang_to_enemy = 10e10, nil, 0
   for i=1,Attack.act_count()-1 do
    if Attack.act_enemy(i) then
      local d = Attack.cell_dist(target,i)
      if d < nearest_dist then nearest_dist = d; nearest_enemy = i; end
    end
   end

   local mas={"imp","imp2","cerberus","demoness","demon"}

    local k=Game.Random(text_range_dec(Attack.get_custom_param("k"))) --����.

    if nearest_enemy ~= nil then ang_to_enemy = Attack.angleto(target, nearest_enemy) end

    local demon_count=Attack.act_size(0)  -- ������� �����
    local summon_unit=mas[Game.Random(1,5)]
    local demon_name=Attack.act_name(0)
    --��������� ����� � ltvjyjd
    local demon_lead=Attack.atom_getpar(demon_name,"leadership")
    local summon_lead=Attack.atom_getpar(summon_unit,"leadership")
    -- ������� ����� �������� �� ���������
    local summon_count=math.ceil(demon_lead*demon_count/summon_lead*k/100)
    -- �������

    local a = Attack.atom_spawn(target,dmgts,"effect_demonsummon",Attack.angleto(target))
    local dmgts1 = Attack.aseq_time(a, "x")

--    local far=0
    Attack.act_spawn(target, 0, summon_unit, ang_to_enemy, summon_count)
    Attack.act_nodraw(target, true)
--    Attack.act_animate(target, "descent", t)
    Attack.act_nodraw(target, false, dmgts+dmgts1 )
      Attack.log_label("add_blog_summon_") -- ��������
      Attack.log_special(summon_count) -- ��������



    --Attack.act_aseq(target, "respawn")

  return true

end


-- ******************************************************* --
-- * ������������� ���� (� ���������, ���������� � ����) * --
-- ******************************************************* --

function special_long_hit()

  local target = Attack.get_target()
  Attack.aseq_rotate(0, target)
  Attack.act_aseq(0, "longattack")

  common_cell_apply_damage(target, Attack.aseq_time(0, "x"))
  return true

end

-- ******************************************************* --
-- * ���������� * --
-- ******************************************************* --

function special_rooted()

  --Attack.aseq_rotate(0, target)
  Attack.act_aseq(0, "rare")
  local name=Attack.act_name(0)
  local base_hp = Attack.atom_getpar(name,"hitpoint")
  local change_hp= tonumber(Attack.get_custom_param("HP"))
  local cur_hp=Attack.act_hp(0)
  local duration=tonumber(Attack.get_custom_param("duration"))  --����.

    Attack.act_apply_spell_begin( 0, "special_rooted", duration, false )
      Attack.act_apply_par_spell( "health", 0, change_hp, 0, duration, false)
      Attack.act_apply_par_spell( "dismove", 1, 0, 0, duration, false)
      Attack.act_hp(0,cur_hp+base_hp*change_hp/100) -- ��������� � �������� ����� ����
    Attack.act_apply_spell_end()

  return true

end

-- ******************************************************* --
-- * ���������� * --
-- ******************************************************* --

function special_digout()

  --Attack.aseq_rotate(0, target)
  Attack.act_del_spell(0,"special_rooted")

  local cur_hp=Attack.act_hp(0)
  local name=Attack.act_name(0)
  local base_hp = Attack.atom_getpar(name,"hitpoint")
  local change_hp= tonumber(Attack.get_custom_param("HP"))

  if cur_hp>base_hp then Attack.act_hp(0,cur_hp-base_hp) end

  Attack.act_charge(0,1)

  return true

end


-- ******************************************************* --
-- * ��������
-- ******************************************************* --

function special_greediness()

  local target = Attack.get_target()

      Attack.act_aseq(0, "telein")
      Attack.atom_spawn(0, 0, "hll_telein" )

      local t = Attack.aseq_time(0)

      Attack.act_aseq(0, "teleout" )
      Attack.atom_spawn(target, t, "hll_teleout" )

    local box
    for d=0,5 do
      box = Attack.cell_adjacent(target, d)
      if Attack.cell_is_box( box ) then break end --����� ����
    end

      Attack.act_teleport(0, target, t, Attack.angleto(target,box))
      Attack.act_ap(0, 1);

  return true

end

-- ******************************************************* --
-- * �����������* --
-- ******************************************************* --

function special_looter()

  local target = Attack.cell_get_corpse(0)
  --Attack.aseq_rotate(0, target)

  if target~=nil then
    Attack.act_aseq(0,"special")
    local dmgts = Attack.aseq_time(0, "x")                   -- x time of attacker


    Attack.loot_it(dmgts)
    Attack.act_fadeout(target,0,dmgts)
    Attack.cell_remove_last_corpse(target,dmgts+.01)

    return true
  else

--      Game.InvokeMsgBox("warning","<label=warning_loot>")
    return false
  end

end

-- ******************************************************* --
-- * ����� ���������� * --
-- ******************************************************* --

function check_ghost_cry(i, dist)

  return not Attack.act_equal(0,i) and (Attack.cell_dist(0,i)<=dist) and Attack.act_level(i)<4

end

function special_ghost_cry()

  --local target=Attack.get_target()
  --Attack.atom_spawn(0, 0, "hll_post_archmage_lighting")
  local dmg_min,dmg_max = text_range_dec(Attack.get_custom_param("damage"))
  local typedmg=Attack.get_custom_param("typedmg")
  local dist=tonumber(Attack.get_custom_param("dist"))
  Attack.act_aseq(0,"special")
  local dmgts = Attack.aseq_time(0, "x")                   -- x time of attacker

  local busy_cells = {}

  for i=1,Attack.act_count()-1 do

    --local i = Attack.cell_get(c)
    if check_ghost_cry(i,dist) and (Attack.act_enemy(i) or Attack.act_ally(i)) and Attack.act_applicable(i) then      -- ����� ����
      -- ���� ��� ���� ����������� ��������� �� ���������� ������
      local max_dist=Attack.cell_dist(0,i) -- ������ ���������� �������
      --Attack.atom_spawn(i, 2, "magic_haste")
      local cell=nil
      for j=0,5 do -- ���������� ��� ����������� ��� ����
        local trg=Attack.cell_adjacent(i,j)
        local dist=Attack.cell_dist(0,trg) -- ���������� ���������� �� ���������� �� ������ � ���� �����������
        if dist>max_dist and Attack.cell_present(trg) and Attack.cell_is_empty(trg)
          and Attack.cell_is_pass(trg) and not busy_cells[Attack.cell_id(trg)] then -- ���� ��� ������ ����������� ����������, ��� ���� ������ ����, �������� � ���������, �� ����� �� � ����
          --Attack.atom_spawn(trg, 4, "magic_slow")
          max_dist=dist
          cell=trg
          busy_cells[Attack.cell_id(trg)]=true
        end
      end

    local time1,time2=1,2
      -- ���� ���� ������� ��������� ���������, �� ���� ��������:
      if cell~=nil and Attack.act_get_par(i, "dismove") == 0 then
        Attack.act_move(1,2,i,cell)
        time1,time2=2,3
      end

    local d = Attack.cell_dist(0,i)

    if not Attack.act_feature(i,"mind_immunitet,undead") and Attack.act_enemy(i) then
      Attack.atk_set_damage(typedmg,dmg_min/d,dmg_max/d) --*Attack.act_size(0
      common_cell_apply_damage(i, time1 --[[+ time2]])
    end
    end
  end

  return true

end

-- ***********************************************
-- * Green Dragon Rail
-- ***********************************************

function special_dragon_rail()
  --  local count=Attack.act_size(0)      -- ���������� ��������
    --local dmg_min,dmg_max = text_range_dec(Attack.get_custom_param("damage"))
    --local typedmg=Attack.get_custom_param("typedmg")
    --Attack.atk_set_damage(typedmg,dmg_min,dmg_max)
    local burn=tonumber(Attack.get_custom_param("burn"))

    local target = Attack.get_target()
    local angle=Attack.angleto(0,target)
    local dir=Game.Ang2Dir(angle)
    target = Attack.cell_adjacent(0, dir)

    Attack.aseq_rotate(0, target)
    Attack.act_aseq(0, "cast")

    local cell_count=Attack.trace (target, dir)
    local c1=Attack.cell_adjacent(0,dir)
    local c2=Attack.cell_adjacent(c1,dir)
    local c3=Attack.cell_adjacent(c2,dir)
    local c4=Attack.cell_adjacent(c3,dir)

    local d = math.floor(cell_count * 0.7)
    if d > 3 then
      Attack.atom_spawn(c4, Attack.aseq_time(0, "z"), string.format("flame%02ihx",d),angle)
    end
    local dmgts, dt = Attack.aseq_time(0, "x"), Attack.aseq_time("reddragon", "cast", "y")
    for i=0,cell_count-1 do
        local cell_found=Attack.trace(i)
        if Attack.cell_present(cell_found) then
            if --[[(Attack.act_enemy(cell_found) or Attack.act_ally(cell_found))]]Attack.get_caa(cell_found)~=nil and Attack.act_applicable(cell_found) then
                local burn_rnd=Game.Random(100)+1
                common_cell_apply_damage(cell_found,dmgts)
                if burn_rnd<=burn then
                    effect_burn_attack(cell_found,dmgts,3)
                end
            end
        end
        dmgts = dmgts + dt
    end

  return true
end

function special_reddragon_rail_highlight()

    local target = Attack.get_target()
    local dir = Game.Ang2Dir(Attack.angleto(0,target))
    target = Attack.cell_adjacent(0, dir)

    local cell_count=Attack.trace(target, dir)
    for i=0,cell_count-1 do
        local cell_found=Attack.trace(i)
        if Attack.cell_present(cell_found) then
            if (Attack.act_enemy(cell_found) or Attack.act_ally(cell_found)) and Attack.act_applicable(cell_found) then
                Attack.cell_select(cell_found, "avenemy")
            else
                Attack.cell_select(cell_found, "destination")
            end
        end
    end

    return true

end

function special_reddragon_rail_calccells()

    for dir=0,5 do
        local c = Attack.cell_adjacent(0, dir)
        local cell_count=Attack.trace(c, dir)
        for i=0,cell_count-1 do
            local cell_found=Attack.trace(i)
            if Attack.cell_present(cell_found) then
                Attack.marktarget(cell_found)
            end
        end
    end

    return true

end


-- ******************************************************* --
-- * �������� ����
-- ******************************************************* --

function calccells_gain_mana()
    for j=0,5 do
        local t=Attack.cell_adjacent(0,j)
        if t~=nil and Attack.cell_present(t) and Attack.act_enemy(t) and Attack.act_applicable(t) then
	        Attack.multiselect(0)
	        break
		end
    end
    return true
end

function cur_hero_item_count(name, val)

	local func
	if Attack.act_belligerent()==1 then func = Logic.hero_lu_item
	else func = Logic.enemy_lu_item end

    if val == nil then return func(name,"count")
    else return func(name,"count",val) end

end

function special_gain_mana()

  Attack.act_aseq( 0, "mana" )
  local count=Attack.act_size(0)      -- ���������� ��������
    local mana_k = tonumber(Attack.get_custom_param("mana_k"))
    local mana, damages = 0, 0

    for j=0,5 do
        local t=Attack.cell_adjacent(0,j)
        if t~=nil and Attack.cell_present(t) and Attack.act_enemy(t) and Attack.act_applicable(t) then
            common_cell_apply_damage(t, Attack.aseq_time(0, "x"))
	  		local damage = Attack.act_damage_results(t)
	  		mana = mana + math.min(Attack.act_totalhp(t),damage)
	  		damages = damages + 1
	  	end
	end

	mana = math.floor(mana/mana_k)

    Attack.log_label("")
	local curmana = cur_hero_item_count("mana")
	if curmana ~= nil then
	    cur_hero_item_count("mana",curmana+mana)
	    if damages > 1 then
	    	Attack.log_label("add_blog_mana_") -- ��������
		else
	    	Attack.log_label("add_blog_mana1_") -- ��������
		end
	end
    Attack.log_special(mana) -- ��������


    return true
end

-- ******************************************************* --
-- * �������������
-- ******************************************************* --

function special_blood()

  local target = Attack.get_target()

  Attack.aseq_rotate(0,target)
  Attack.act_aseq( 0, "cast" )
  local dmgts = Attack.aseq_time(0, "x")

  if target~=nil  then
    local a = Attack.atom_spawn(target,dmgts,"blood_rune",0)
    Attack.act_aseq(a, "appear" )
    Attack.aseq_timeshift(a,dmgts)
  end
  return true
end

-- ***********************************************
-- * Stun
-- ***********************************************

function special_cyclop_push()

  local target = Attack.get_target()

  -- ������������� �����

  Attack.aseq_rotate( 0,target)

  -- �������
  Attack.act_aseq( 0, "push" )
  local dmgts = Attack.aseq_time(0, "x") --+ Attack.aseq_time(0,"","x")atomtag, animation, key

  local dir=Game.Ang2Dir(Attack.angleto(0,target))
  local cell=Attack.cell_adjacent(target,dir)

  if Attack.cell_present(cell) and Attack.cell_is_empty(cell) and Attack.cell_is_pass(cell) and Attack.act_get_par(target, "dismove") == 0 then
    Attack.act_move(dmgts, dmgts+1, target, cell)
  end

  common_cell_apply_damage(target,dmgts)

  return true
end

-- ***********************************************
-- * ������ ����
-- ***********************************************

function special_capture_target()

  local target = Attack.get_target()

  -- ������������� �����

  --Attack.aseq_rotate(target,0)
  Attack.aseq_rotate(0,target)

  local dmgtsR = Attack.aseq_time(0)

  -- �������
  Attack.act_aseq( 0, "special" )
  local dmgts1 = Attack.aseq_time(0, "x") --+ Attack.aseq_time(0,"","x")atomtag, animation, key
  local dmgts2 = Attack.aseq_time(0, "y") --+ Attack.aseq_time(0,"","x")atomtag, animation, key
  local dmgtsD = Attack.aseq_time(0, "z") --+ Attack.aseq_time(0,"","x")atomtag, animation, key

  local dir=Game.Ang2Dir(Attack.angleto(target,0))
  local cell=Attack.cell_adjacent(target,dir)
  --local pp=0

  --if Attack.cell_present(cell) and Attack.cell_is_empty(cell) and Attack.cell_is_pass(cell) then
    Attack.act_move(dmgts1,dmgts2,target,cell)
  --  pp=dmgts+0.4
  --end

  common_cell_apply_damage(target,dmgtsD)

  return true
end

-- ***********************************************
-- * plague
-- ***********************************************
function special_plague()

  local level = tonumber(Attack.get_custom_param("level"))
  local var = tonumber(Attack.get_custom_param("plague"))
  --calccells_all_enemy_around()
  Attack.act_aseq(0,"cast")
  local dmgts = Attack.aseq_time(0, "x")

  for c=0,Attack.cell_count()-1 do
    local i = Attack.cell_get(c)

    if Attack.act_applicable(i) and (Attack.act_enemy(i) or Attack.act_ally(i)) then                  -- can receive this attack
      local rnd=Game.Random(0,100)+1
      if rnd<=var then
        spell_plague_attack(i,level,dmgts)
      end
    end

  end

  return true
end

-- ***********************************************
-- * Griffin split
-- ***********************************************
function special_griffin_split()

  local target = Attack.get_target()

  local halfSize = Attack.act_size(0)/2.
  Attack.act_size( 0, math.ceil(halfSize) )
  local newTroopSize = math.floor(halfSize)
  Attack.act_initsize( 0, Attack.act_initsize(0) - newTroopSize )

  Attack.aseq_rotate(0, target)
  Attack.act_aseq(0, "special")

  Attack.act_spawn(target, 0, "griffin", Attack.angleto(0,target), newTroopSize, Attack.act_slot(0))

  -- ��������� ������� ����� � �������������� ������
  Attack.val_store(0, "clone", 1)
  Attack.act_enable_attack(0, "split", false)
  Attack.val_store(target, "clone", 1)
  Attack.act_enable_attack(target, "split", false)

  Attack.act_nodraw(target, true)
  local t = Attack.aseq_time(0) - .3
  Attack.act_animate(target, "descent", t)
  Attack.act_nodraw(target, false, t)
  Attack.log_label("add_blog_split_")

  Attack.resort( target ) -- ���� �������� ������ ������ � ���� ������

  return true

end

function griffin_check_split()

    if Attack.val_restore(0, "clone") ~= 1 then -- ��� �� ����
        if Attack.act_size(0) > 1 and Attack.act_belligerent(0)==Attack.act_belligerent(0,nil) then
            Attack.act_enable_attack(0, "split", true)
        else
            Attack.act_enable_attack(0, "split", false)
        end
    end
    return true

end

-- ***********************************************
-- * Ent Reload
-- ***********************************************
function special_reload()

  local count = tonumber(Attack.get_custom_param("count"))
  Attack.act_aseq(0, "special")
  Attack.act_charge(0,count)

  return true

end

-- ***********************************************
-- * Gibe
-- ***********************************************
function special_gibe()

  local target = Attack.get_target()
  Attack.aseq_rotate(0, target)
  Attack.act_aseq(0, "special")
  local dmgts = Attack.aseq_time(0, "x")                   -- x time of attacker

  Attack.val_store(target,"gibe_target",Attack.cell_id(Attack.get_cell(0)))
  Attack.act_attach_modificator(target,"initiative","temp_i",100,0,0,1,true,100)
  Attack.act_attach_modificator(target,"autofight","temp_a",1,0,0,1,true,100)
    Attack.atom_spawn(target, dmgts, "effect_bullhead",0,true)
      Attack.log_label("add_blog_gibe_") -- ��������
  Attack.act_again(target, true)
  Attack.resort()
  return true

end

-- ***********************************************
-- * Fire Power
-- ***********************************************
function special_blackdragon_firepower()

  local path = Attack.calc_path(0, Attack.get_target())
  if path == nil then return false end

  Attack.act_no_cell_update(0)
    Attack.aseq_rotate(0, path[2].cell)
    Attack.aseq_start(0, "fire_takeoff", "fire_flight")

  local i, dir = 2, path[1].dir

  while i < table.getn(path) do

    local ndir = path[i].dir

    if ndir == dir then
      if i+2 < table.getn(path) and path[i+1].dir == dir and path[i+2].dir == dir then
        Attack.aseq_waft(0, "fire_flight", "fire_waft")
          common_cell_apply_damage(path[i].cell, Attack.aseq_time(0, "v"))
          common_cell_apply_damage(path[i+1].cell, Attack.aseq_time(0, "w"))
        i = i + 2
      else
        Attack.aseq_move(0, "fire_flight")
      end
    else
      if wrap(ndir - dir, -3, 3) == 1 then
        Attack.aseq_move(0, "fire_flight", "fire_rdivert", 1)
      else
        Attack.aseq_move(0, "fire_flight", "fire_ldivert", -1)
      end

      dir = ndir
    end

        common_cell_apply_damage(path[i].cell, Attack.aseq_time(0, "x"))

    i = i + 1

  end

  Attack.act_aseq(0, "fire_descent", false, true)
  Attack.atk_min_time(Attack.aseq_time(0) + .5)

  return true

end

function special_blackdragon_firepower_highlight(target)

  local path = Attack.calc_path(0, target)
  if path ~= nil then
    for i=2,table.getn(path)-1 do
      local c = path[i].cell
      if (Attack.act_enemy(c) or Attack.act_ally(c)) and Attack.act_takesdmg(c) then
        Attack.cell_select(c, "avenemy")
      else
        Attack.cell_select(c, "path")
      end
    end
    Attack.cell_select(path[table.getn(path)].cell, "destination")
  end
  return true

end


function special_spell()
-- ����� �����:
-- ���������: +�������� ������, +������, +�������� �����, +���������� �����, +�������� �����, +���������� ���
-- �����: +����������, +���� ���, +��������, +�������������,
-- ��� ��������: +������, +�����, +������, +����, +����������
-- �� �� �����: +���, 

    Attack.act_aseq(0,"cast")
    local dmgts = Attack.aseq_time(0, "x")

  local target = Attack.get_target()
  local num=string.gsub(Attack.act_name(0),"evilbook","")
  local level = tonumber(num)

  local spells1 = {spell_magic_axe_attack, spell_fire_arrow_attack,  spell_smile_skull_attack}
  local spells2 = {spell_lightning_attack, spell_oil_fog_attack, spell_magic_axe_attack, spell_smile_skull_attack, spell_ghost_sword_attack}
  local spells3 = {spell_lightning_attack, spell_lightning_attack, spell_magic_axe_attack, spell_ghost_sword_attack, spell_smile_skull_attack,}

  local tmp_spells = {}

  -- ��������� ����������� ���������� �� ������ ��� ������ � ������
  if not Attack.act_feature(target,"pawn") and not Attack.act_feature(target,"boss") then

  if not Attack.act_feature(target,"plant") and not Attack.act_feature(target,"golem") and not Attack.act_feature(target,"undead") then
    if level>1 then table.insert(tmp_spells, spell_weakness_attack) end
    --if level>1 then table.insert(tmp_spells, effect_curse_attack) end
  end
  if not Attack.act_feature(target,"pawn") and not Attack.act_feature(target,"boss")  then
    if level<3 then table.insert(tmp_spells, spell_slow_attack) end
    --if level>1 then table.insert(tmp_spells, effect_curse_attack) end
  end

    if Attack.act_level(target)>2 and Attack.act_get_par(target,"defense")>5 then   table.insert(tmp_spells, spell_defenseless_attack) end

  if Attack.act_level(target)<=(level+1) then
    if level>2 then table.insert(tmp_spells, spell_ram_attack) end
    if level>1 then table.insert(tmp_spells, spell_pygmy_attack) end
    if level>1 then table.insert(tmp_spells, spell_crue_fate_attack) end
    if not Attack.act_feature(target,"mind_immunitet") and not Attack.act_feature(target,"undead") then
      if level>2 then table.insert(tmp_spells, spell_hypnosis_attack) end
      if level>1 and level<=Attack.act_level(0) then table.insert(tmp_spells, spell_scare_attack) end
      if level>2 then table.insert(tmp_spells, effect_sleep_attack) end
    end
    if not Attack.act_feature(target,"eyeless") then
      if level>2 then table.insert(tmp_spells, spell_blind_attack) end
    end
  end
  end

    if level==1 then
        for i=1,table.getn(spells1) do
            table.insert(tmp_spells,spells1[i])
        end
    end
    if level==2 then
        for i=1,table.getn(spells2) do
            table.insert(tmp_spells,spells2[i])
        end
    end
    if level==3 then
        for i=1,table.getn(spells3) do
            table.insert(tmp_spells,spells3[i])
        end
    end

  cast=Game.Random(1,table.getn(tmp_spells))
  if tmp_spells[cast]~=effect_curse_attack and tmp_spells[cast]~=effect_sleep_attack then
    tmp_spells[cast](level,dmgts)
  else
    tmp_spells[cast](target,dmgts,level)
  end

    local spell_count = Attack.val_restore(0,"spell_count")
    if spell_count~=nil then
        spell_count=tonumber(spell_count)
        if spell_count>1 then
            Attack.val_store(0,"spell_count",spell_count-1)
        end
        if spell_count==1 then
            Attack.act_enable_attack(0,"gulp")
        Attack.act_charge(0,1,"gulp")
        Attack.val_store(0,"spell_count",0)
    end
  end
  if spell_count==nil then
    Attack.act_enable_attack(0,"gulp")
  end

  Attack.log_label('')

  return true

end

-- ***********************************************
-- * Dominator
-- ***********************************************
function special_dominator()

  local cycle = Attack.get_cycle()

  if cycle == 0 and not Attack.is_computer_move() then
    Attack.val_store("dominator_control", Attack.get_target())
    Attack.next(0)
    return true
  end

  local source
  if Attack.is_computer_move() then
    source = Attack.get_target()
  else
    source = Attack.val_restore("dominator_control")
    local target = Attack.get_target()
    Attack.val_store(source,"gibe_target",Attack.cell_id(Attack.get_cell(target)))
  end

    Attack.aseq_rotate(0, source)
    Attack.act_aseq(0, "special")
    local dmgts = Attack.aseq_time(0, "x")                   -- x time of attacker
    Attack.act_del_spell(source,"effect_charm")
    Attack.act_del_spell(source,"effect_sleep")

    if Attack.act_enemy(source) then
        effect_charm_attack(source,dmgts,1,"magic_hypnosis")
    else
        Attack.atom_spawn(source, dmgts, "magic_hypnosis",Attack.angleto(source))
    end

    --Attack.act_again(source)
    Attack.act_attach_modificator(source,"initiative","temp_i",100,0,0,1,true,100)
    Attack.act_attach_modificator(source,"autofight","temp_a",1,0,0,1,true,100)
    Attack.act_again(source, true)
    Attack.resort()
		Attack.log_label("null")
    --      Attack.log_label("add_blog_gibe_") -- ��������

  return true

end

function special_gulp()

  local target = Attack.get_target()

    Attack.aseq_rotate(0, target, "special")
  local dmgts = Attack.aseq_time(0, "x")                   -- x time of attacke
  Attack.act_nodraw(target, true, dmgts); -- �������� ����, ����� ����� ��� ����������
  --Attack.act_kill(target)
  Attack.act_charge(0,2,"attack_spell")
    Attack.val_store(0, "spell_count", 2)
    Attack.act_enable_attack(0,"gulp",false)
    Attack.act_remove(target,dmgts)

  return true

end

-- ***********************************************
-- * Run
-- ***********************************************
function special_run()

    Attack.act_ap(0, Attack.act_ap(0) + tonumber(Attack.get_custom_param("ap")))
    Attack.atom_spawn(0, 0, "effect_run", 0)
	return true

end
