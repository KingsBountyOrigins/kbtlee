-- ***********************************************
-- * Dragon Arrow
-- ***********************************************

function spell_dragon_arrow_attack()

  local target = Attack.get_target()

  if (target ~= nil) then
  
    local level=Obj.spell_level()
    if level==0 then level=1 end
		Attack.act_enable_attack(target,"dragon")
		local count=pwr_dragon_arrow()
--		if level>1 then
			Attack.act_charge(target,0,"dragon") -- ������ ��������� ����� (1)
			if count > 1 then -- ����� count 1, �� ����� ������ ������ � �� ������
				Attack.act_charge(target,count-1,"dragon")
			end
--		end 
    Attack.atom_spawn(target, 0, "magic_dragon_slayer", Attack.angleto(target))
--        Attack.act_damage_addlog(target,"add_blog_fear_")
  end

  return true
end

--*************************************************************************
--   �����������
--*************************************************************************
function necro_get_unit(name)

	local pas_unit={}
	-- ���������� ������ � ���������� ������ ��������� ����
	for i,mas in ipairs({"skeleton","archer","spider_undead","zombie","zombie2","ghost","ghost2","vampire","vampire2","blackknight","necromant","bonedragon"}) do
		if string.find(Logic.obj_par("spell_necromancy",mas), ','..name..',', 1, true) then
			table.insert(pas_unit, mas)
		end 
	end

	return pas_unit[Game.Random(1,table.getn(pas_unit))] --�������� ���� �� ������ ����

end

function animate_dead(cell, target, dmgts, bel, unit_animate, count)

	  local ang_to_enemy=Game.Dir2Ang(Attack.act_dir(target))

      Attack.atom_spawn(cell, dmgts+0.3 , "effect_deadarise",0)

      local tx,ty,tz = Attack.act_get_center(target)
      Attack.act_move( dmgts, dmgts+1, target, tx,ty,tz-2.7, false ) -- ��������� ����

--    Attack.cell_remove_last_corpse(cell,0)
      Attack.act_spawn(cell, bel, unit_animate, ang_to_enemy, count)
      --Attack.act_spawn(cell, 0, unit_animate, 0, animate_real)

      Attack.act_nodraw(cell, true)
      local t = dmgts + 0.7
      Attack.act_animate(cell, "respawn", t)
      Attack.act_nodraw(cell, false, t)
	  Attack.act_fadeout(cell, t, t+.5, 1)

      Attack.act_remove(target, 1.1 + dmgts)

end

function spell_necromancy_attack()

	local target = Attack.cell_get_corpse(Attack.get_target())

--		Attack.aseq_rotate(0,target)
--		Attack.act_aseq( 0, "cast" )
--		dmgts = Attack.aseq_time(0, "x")

	  --������� ���������� �����
		--[[local nearest_dist, nearest_enemy, ang_to_enemy = 10e10, nil, 0
		for i=1,Attack.act_count()-1 do
	 	if Attack.act_enemy(i) then
	 		local d = Attack.cell_dist(target,i)
	 		if d < nearest_dist then nearest_dist = d; nearest_enemy = i; end
		end
	 	end]]
	 
	local unit_animate=necro_get_unit(actor_name(target))
	local animate_count_base=Attack.act_initsize(target)	--������� ������ � �����
	local power=pwr_necromancy()

	-- if nearest_enemy ~= nil then ang_to_enemy = Attack.angleto(target, nearest_enemy) end

	local undead_HP=Attack.atom_getpar(unit_animate,"hitpoint")
	-- ������� ����� ������� �� ��������
	local animate_count=math.floor(power/undead_HP)
	-- �������
	local animate_real=math.min(animate_count,animate_count_base)

	if animate_real~=0 then

        animate_dead(Attack.get_target(), target, 0, 0, unit_animate, animate_real)

		if animate_real>1 then
			Attack.log("add_blog_snecro_2","hero_name",blog_side_unit(Attack.get_target(),4)..Attack.hero_name(),"spell",blog_side_unit(Attack.get_target(),3).."<label=spell_necromancy_name>","targeta",blog_side_unit(target,0),"target",blog_side_unit(Attack.get_target(),0),"special",animate_real)
		else 
			Attack.log("add_blog_snecro_1","hero_name",blog_side_unit(Attack.get_target(),4)..Attack.hero_name(),"spell",blog_side_unit(Attack.get_target(),3).."<label=spell_necromancy_name>","targeta",blog_side_unit(target,0),"target",blog_side_unit(Attack.get_target(),0),"special",animate_real)
		end 

		return true			

	else
		return false  
	end 
	--Attack.act_aseq(target, "respawn")
  
end

--*************************************************************************
--   ����
--*************************************************************************

function spell_plague_attack(target,level,dmgts)
	
	 if level==nil then level=Obj.spell_level() end
		if level==0 then level=1 end

	local duration = 0
	local type_trigger=10
	if target ~= nil then type_trigger=0 end -- ����� �� ������� � �������� ����� 
  if target == nil  then target=Attack.get_target() end 
	if Attack.act_is_spell(target,"spell_plague") and type_trigger==10 then type_trigger=1 end -- ��������� ����������� �����
	if not Attack.act_is_spell(target,"spell_plague") and type_trigger==10 then type_trigger=2 end -- ����������
	
	if type_trigger == 2 then
		duration = tonumber("0" .. text_dec(Logic.obj_par("spell_plague","duration"),level))
		if Logic.obj_par("spell_plague","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 
		
		--+ math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
	end 
	if type_trigger == 0 then
  	level=tonumber(level) 
   	duration = tonumber("0" .. text_dec(Logic.obj_par("spell_plague","duration"),level))
  end  
  
	if dmgts==nil then dmgts=0 end
	
	
  local power=pwr_plague(level)
  
	
	-- ������ ����. ���� ���� ��� - ����� ����������. 
 	--if target==nil then target = Attack.get_target() end 	
		
		-- ���� �� ���� ��� ���������� - �����������
    --if not Attack.act_is_spell(target,"spell_plague") then 
    if type_trigger~=1 then 
    	Attack.act_apply_spell_begin( target, "spell_plague", duration, false )
    	-- �� �� ������ ������ ������
    		if Attack.act_race(target)~="undead" then
      	  Attack.act_apply_par_spell( "health", 0, -power, 0, duration, false)
 					Attack.act_apply_par_spell("attack", 0, -power, 0, duration, false)
					Attack.act_apply_par_spell("defense", 0, -power, 0, duration, false)
				end
				Attack.act_spell_param(target, "spell_plague", "level",level)
				Attack.act_posthitmaster(target,"post_spell_plague",duration)
    	Attack.act_apply_spell_end()
    	Attack.atom_spawn(target, dmgts , "magic_greenfly",Attack.angleto(target),true)	
    -- ���� ���� ���������� - �������� ������������ �������:
		else 
			if target==nil then target=0 end
		  for i=0,5 do
    		level = tonumber(Attack.act_spell_param(target, "spell_plague", "level"))
    		duration = tonumber("0" .. text_dec(Logic.obj_par("spell_plague","duration"),level))
 		
    		local trg=Attack.cell_adjacent(target,i)
    		if (Attack.act_enemy(trg) or Attack.act_ally(trg)) and Attack.act_level(trg)<5 and Attack.act_applicable(trg) then 
          if not Attack.act_is_spell(trg,"spell_plague") then 
            local rnd = Game.Random(0,100)+1     -- ��� �� �� �������
            if rnd<=50 then 
            Attack.act_apply_spell_begin( trg, "spell_plague", duration, false )
    				-- �� �� ������ ������ ������
    				if Attack.act_race(trg)~="undead" then
      	  		Attack.act_apply_par_spell( "health", 0, -power, 0, duration, false)
 							Attack.act_apply_par_spell("attack", 0, -power, 0, duration, false)
							Attack.act_apply_par_spell("defense", 0, -power, 0, duration, false)
						end
						Attack.act_posthitmaster(trg,"post_spell_plague",duration)
						Attack.act_spell_param(trg, "spell_plague", "level",level)
    				Attack.act_apply_spell_end()
    				Attack.atom_spawn(trg, dmgts , "magic_greenfly",Attack.angleto(trg),true)	
    				end 
          end
    		end 
    	end 
		end 
	
  return true
end


--*************************************************************************
--   Post plague
--*************************************************************************

function post_spell_plague(damage,addrage,attacker,receiver,minmax )

		if minmax==0 and not (Attack.act_feature(receiver,"plant") or Attack.act_feature(receiver,"golem") or  Attack.act_feature(receiver,"demon")) and Attack.cell_dist(attacker,receiver)==1 then 
			local target_spell_level=0
			local level = tonumber(Attack.act_spell_param(attacker, "spell_plague", "level"))
				if not Attack.act_is_spell(receiver,"spell_plague") and level~=nil then
					--target_spell_level=tonumber(Attack.act_spell_param(receiver, "spell_plague", "level"))
				--end 
				--if level>=target_spell_level then 
					spell_plague_attack(receiver,level,0)
				end 
		end 
  	return damage,addrage
end

function spell_plague_onremove(caa)

		Attack.act_posthitmaster(caa,"post_spell_plague",0)
  	return true
  	
end

-- ***********************************************
-- * Plague 
-- ***********************************************
function spell_plague_attack1(target,level,dmgts)
	
	local duration = 0
	
  if level==nil and Attack.get_target()~=nil then 
  	level=Obj.spell_level() 
  	duration = tonumber("0" .. text_dec(Logic.obj_par("spell_plague","duration"),level))
  else
  	level=tonumber(level) 
   	duration = tonumber("0" .. text_dec(Logic.obj_par("spell_plague","duration"),level))
  end  
  if level==0 then level=1 end
	if dmgts==nil then dmgts=0 end
	
	
  local power=pwr_plague(level)
  
	
	-- ������ ����. ���� ���� ��� - ����� ����������. 
 	if target==nil then target = Attack.get_target() end 	
		
		-- ���� �� ���� ��� ���������� - �����������
    if not Attack.act_is_spell(target,"spell_plague") then 
    	Attack.act_apply_spell_begin( target, "spell_plague", duration, false )
    	-- �� �� ������ ������ ������
    		if Attack.act_race(target)~="undead" then
      	  Attack.act_apply_par_spell( "health", 0, -power, 0, duration, false)
 					Attack.act_apply_par_spell("attack", 0, -power, 0, duration, false)
					Attack.act_apply_par_spell("defense", 0, -power, 0, duration, false)
				end
				Attack.act_spell_param(target, "spell_plague", "level",level)
				Attack.act_posthitmaster(target,"post_spell_plague",duration)
    	Attack.act_apply_spell_end()
    	Attack.atom_spawn(target, dmgts , "magic_greenfly",Attack.angleto(target),true)	
    -- ���� ���� ���������� - �������� ������������ �������:
		else 
		  for i=0,5 do
		  	if target==nil then target=0 end
    		local trg=Attack.cell_adjacent(target,i)
    		level = Attack.act_spell_param(target, "spell_plague", "level")
    		if (Attack.act_enemy(trg) or Attack.act_ally(trg)) and Attack.act_level(trg)<5 and Attack.act_applicable(trg) then 
          if not Attack.act_is_spell(trg,"spell_plague") then 
            local rnd = Game.Random(0,100)+1     -- ��� �� �� �������
            if rnd<=50 then 
            Attack.act_apply_spell_begin( trg, "spell_plague", duration, false )
    				-- �� �� ������ ������ ������
    				if Attack.act_race(trg)~="undead" then
      	  		Attack.act_apply_par_spell( "health", 0, -power, 0, duration, false)
 							Attack.act_apply_par_spell("attack", 0, -power, 0, duration, false)
							Attack.act_apply_par_spell("defense", 0, -power, 0, duration, false)
						end
						Attack.act_posthitmaster(trg,"post_spell_plague",duration)
						Attack.act_spell_param(trg, "spell_plague", "level",level)
    				Attack.act_apply_spell_end()
    				Attack.atom_spawn(trg, dmgts , "magic_greenfly",Attack.angleto(trg),true)	
    				end 
          end
    		end 
    	end 
		end 
	
  return true
end

-- ***********************************************
-- * Demon Slayer
-- ***********************************************

function spell_demon_slayer_attack()

  local level=Obj.spell_level()
  if level==0 then level=1 end

  local target = Attack.get_target()
  local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_demon_slayer","duration"),level))
  
		if Logic.obj_par(Obj.name(),"int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

  local script=Logic.obj_par("spell_demon_slayer","script")

  if (target ~= nil) then
    Attack.act_del_spell( target, "spell_demon_slayer" )

    Attack.act_apply_spell_begin( target, "spell_demon_slayer", duration, false )
            Attack.act_posthitmaster(target, script ,duration, tonumber(level))
    Attack.act_apply_spell_end()
    Attack.atom_spawn(target, 0, "effect_demonslayer", Attack.angleto(target))
  else
    local acnt = Attack.act_count()
    for i=1,acnt-1 do
      if Attack.act_ally(i) and Attack.act_applicable(i) then

            Attack.act_del_spell( i, "spell_demon_slayer" )

            Attack.act_apply_spell_begin( i, "spell_demon_slayer", duration, false )
                    Attack.act_posthitmaster(i, script ,duration,tostring(level))
            Attack.act_apply_spell_end()
            Attack.atom_spawn(i, 0, "effect_demonslayer", Attack.angleto(i))
      end
    end
  end


  return true
end

-- ***********************************************
-- * Dragon Slayer
-- ***********************************************

function spell_dragon_slayer_attack()

  local level=Obj.spell_level()
  if level==0 then level=1 end

  local target = Attack.get_target()
  local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_dragon_slayer","duration"),level))
  
 		if Logic.obj_par(Obj.name(),"int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

  local script=Logic.obj_par("spell_dragon_slayer","script")

  if (target ~= nil) then
    Attack.act_del_spell( target, "spell_dragon_slayer" )

    Attack.act_apply_spell_begin( target, "spell_dragon_slayer", duration, false )
            Attack.act_posthitmaster(target, script ,duration,tostring(level))
    Attack.act_apply_spell_end()
    Attack.atom_spawn(target, 0, "effect_dragonslayer", Attack.angleto(target))
  else
    local acnt = Attack.act_count()
    for i=1,acnt-1 do
      if Attack.act_ally(i) and Attack.act_applicable(i) then

            Attack.act_del_spell( i, "spell_dragon_slayer" )

            Attack.act_apply_spell_begin( i, "spell_dragon_slayer", duration, false )
              Attack.act_posthitmaster(i, script ,duration,tostring(level))
      
            Attack.act_apply_spell_end()
            Attack.atom_spawn(i, 0, "effect_dragonslayer", Attack.angleto(i))
      end
    end
  end


  return true
end



-- ***********************************************
-- * Resurrection
-- ***********************************************

function spell_resurrection_attack()

  local target = Attack.get_target()

  if (target ~= nil) then
  	local count_1,count,hp=Attack.act_size(target),0,Attack.act_hp(target)
	if Attack.get_caa(target) == nil then count_1 = 0 end
    local rephits = pwr_resurrection()

    Attack.atom_spawn(target, 0, "hll_priest_resur_cast")
    Attack.cell_resurrect(target, rephits)
    local count_2=Attack.act_size(target)
    --Attack.log_label("add_blog_burn")

    --if count_1==count_2 then count=count_1 end
    if count_2>count_1 then count=count_2-count_1 end
    if count_2<count_1 then count=count_2 end

	local N
    if Attack.act_size(target)>1 then N = '2' else N = '1' end
    if count_1 == count_2 then
	   	Attack.log("add_blog_sheal_"..N,"hero_name",blog_side_unit(target,4)..Attack.hero_name(),"spell",blog_side_unit(target,3).."<label=spell_resurrection_name>","special",Attack.act_hp(target)-hp,"target",blog_side_unit(target,-1))
	else
   		Attack.log("add_blog_sres_" ..N,"hero_name",blog_side_unit(target,4)..Attack.hero_name(),"spell",blog_side_unit(target,3).."<label=spell_resurrection_name>","special",count,"target",blog_side_unit(target,-1))
	end

  end

  return true
end

-- ***********************************************
-- * Gifts
-- ***********************************************

function spell_gifts_attack()

  local target = Attack.get_target()

  if (target ~= nil) then
    Attack.act_reload(target)
    Attack.act_charge(target,0)
    Attack.atom_spawn(target, 0, "magic_cornucopia", Attack.angleto(target))

  end

  return true
end

function calccells_gifts()

    local level=Obj.spell_level()
    if level==0 then level=1 end
    
	  local lvl = tonumber(text_dec(Logic.obj_par("spell_gifts","level"),level))

  for i=1, Attack.act_count()-1 do
    if Attack.act_ally(i) and Attack.act_applicable(i) and Attack.act_level(i)<=lvl and Attack.act_need_charge_or_reload(i) then
        Attack.marktarget(i)            -- select it
    end
  end
  return true

end

-- ***********************************************
-- * Magic Bondage
-- ***********************************************

function spell_magic_bondage_attack()

  local target = Attack.get_target()

    local level=Obj.spell_level()
    if level==0 then level=1 end

    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_magic_bondage","duration"),level))
    
		if Logic.obj_par("spell_magic_bondage","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

  if (target ~= nil) then

   Attack.act_del_spell( target, "spell_magic_bondage" )
    Attack.act_del_spell( target, "special_stupid" )
    Attack.act_apply_spell_begin( target, "spell_magic_bondage", duration, false )
            Attack.act_apply_par_spell( "disreload", 10, 0, 0, duration, false)
            Attack.act_apply_par_spell( "disspec", 10, 0, 0, duration, false)
    Attack.act_apply_spell_end()

        Attack.atom_spawn(target, 0, "magic_bondage", Attack.angleto(target))
  else
    local acnt = Attack.act_count()
    local lvl=tonumber(text_dec(Logic.obj_par("spell_magic_bondage","level"),level))
    for i=1,acnt-1 do
      if Attack.act_enemy(i) and Attack.act_applicable(i) and (Attack.act_level(i)<=lvl) then

        Attack.act_del_spell(i,"magic_bondage")
        Attack.act_del_spell(i,"special_stupid")

        Attack.act_apply_spell_begin( i, "spell_magic_bondage", duration, false )
            Attack.act_apply_par_spell( "disreload", 10, 0, 0, duration, false)
            Attack.act_apply_par_spell( "disspec", 10, 0, 0, duration, false)
        Attack.act_apply_spell_end()

        Attack.atom_spawn(i, 0, "magic_bondage", Attack.angleto(i))
      end
    end

  end 

  return true
end

-- ***********************************************
-- * Scare
-- ***********************************************

function spell_scare_attack(lvl,dmgts)
	
	if dmgts==nil then dmgts=0 end
  local target = Attack.get_target()

  if (target ~= nil) then
  
	local level = lvl
  if level==nil then 
  	level=Obj.spell_level()
  else
  	level = tonumber(level)
  end 
  if level==0 then level=1 end

    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_scare","duration"),level))
		if Logic.obj_par("spell_scare","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

    Attack.act_del_spell( target, "effect_fear" )

    Attack.act_apply_spell_begin( target, "effect_fear", duration, false )
        Attack.act_apply_par_spell("autofight", 1, 0, 0, duration, false)
    Attack.act_apply_spell_end()

        Attack.atom_spawn(target, 0, "magic_scare", Attack.angleto(target))
        Attack.act_damage_addlog(target,"add_blog_fear_")
  end

  return true
end

-- ***********************************************
-- * Cruel Fate
-- ***********************************************

function spell_crue_fate_attack(lvl,dmgts)

	if dmgts==nil then dmgts=0 end
  local target = Attack.get_target()

  if (target ~= nil) then
  
	local level = lvl
  if level==nil then 
  	level=Obj.spell_level()
  else
  	level = tonumber(level)
  end 
  if level==0 then level=1 end

    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_crue_fate","duration"),level))
		if Logic.obj_par("spell_crue_fate","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

    Attack.act_del_spell( target, "spell_crue_fate" )

    Attack.act_apply_spell_begin( target, "spell_crue_fate", duration, false )

    Attack.act_apply_spell_end()

        Attack.atom_spawn(target, dmgts, "magic_cruelfate", Attack.angleto(target))
  end

  return true
end

-- ***********************************************
-- * Pain Mirror
-- ***********************************************

function spell_pain_mirror_attack()

  local target = Attack.get_target()

  if (target ~= nil) then

     local level=Obj.spell_level()
     if level==0 then level=1 end

  	 local percent_dmg = pwr_pain_mirror(level)
     local last_dmg = tonum(Attack.val_restore(target, "last_dmg"))

     local a = Attack.atom_spawn(target, 0, "magic_mirror", Attack.angleto(target)-0.5)
     local dmgts = Attack.aseq_time(a, "x")
     Attack.atk_set_damage("magic", last_dmg*percent_dmg/100)
     common_cell_apply_damage(target, dmgts)

  end

  return true
end

-- ***********************************************
-- * Last Hero
-- ***********************************************

function spell_last_hero_attack()

  local target = Attack.get_target()

    local level=Obj.spell_level()
    if level==0 then level=1 end

  if (target ~= nil) then
    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_last_hero","duration"),level))
		if Logic.obj_par(Obj.name(),"int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

    local script=Logic.obj_par("spell_last_hero","script")

   -- local hit=Attack.act_get_par(target, "health") --AU.health(target)
   -- if (Attack.act_size(target)==1) then hit = Attack.act_hp(target) end -- ���� ���� � ������ ����� ���� ����� ����� ��� �������, � �� ����. ��������
   local hit = 1;

    Attack.act_del_spell( target, "spell_last_hero" )

    Attack.act_apply_spell_begin( target, "spell_last_hero", duration, false )
        --Attack.act_apply_par_spell("hplocked", hit, 0, 0, duration, false)
        --hplocked
        Attack.act_posthitslave(target, script, duration, "super")
    Attack.act_apply_spell_end()

        Attack.atom_spawn(target, 0, "magic_last_hero", Attack.angleto(target))
  end

  return true
end

function spell_last_hero_onremove(caa,duration_end)

   if duration_end~=true then
   end

  return true
end


-- ***********************************************
-- * Magic Source
-- ***********************************************

function spell_magic_source_attack()

  local target = Attack.get_target()

  if (target ~= nil) then

  local level=Obj.spell_level()
  if level==0 then level=1 end

    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_magic_source","duration"),level))
		if Logic.obj_par(Obj.name(),"int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

    local script=Logic.obj_par("spell_magic_source","script")
    local penalty=pwr_magic_source()
    local defense, basedefense = Attack.act_get_par(target, "defense")

    Attack.act_del_spell( target, "spell_magic_source" )

    Attack.act_apply_spell_begin( target, "spell_magic_source", duration, false )
            Attack.act_apply_par_spell( "defense", 0, penalty, 0, duration, false)
			Attack.act_spell_param(target, "spell_magic_source", "heroname", Attack.hero_name())
            Attack.act_posthitslave(target, script ,duration, tostring(level))
    Attack.act_apply_spell_end()

    Attack.atom_spawn(target, 0, "magic_mana_shield", Attack.angleto(target))
  end

  return true
end

-- ***********************************************
-- * Target
-- ***********************************************

function spell_target_attack()

  local target = Attack.get_target()

  if (target ~= nil) then
  
	  local level=Obj.spell_level()
	  if level==0 then level=1 end
    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_target","duration"),level))
		if Logic.obj_par(Obj.name(),"int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

    Attack.act_del_spell( target, "spell_target" )

    Attack.act_apply_spell_begin( target, "spell_target", duration, false )
    Attack.act_spell_param(target, "spell_target", "lvl", text_dec(Logic.obj_par("spell_target","lvl"),level))

    Attack.act_apply_spell_end()

    Attack.atom_spawn(target, 0, "magic_target", Attack.angleto(target))
  end

  return true
end

-- ***********************************************
-- * Berserker
-- ***********************************************

function spell_berserker_attack()

  local target = Attack.get_target()

  if (target ~= nil) then

    local level=Obj.spell_level()
    if level==0 then level=1 end
    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_berserker","duration"),level))
		if Logic.obj_par(Obj.name(),"int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

    local power,power=pwr_berserker(level)

    Attack.act_del_spell( target, "spell_berserker" )

    Attack.act_apply_spell_begin( target, "spell_berserker", duration, false )
        Attack.act_apply_par_spell("autofight", 1, 0, 0, duration, false)
        Attack.act_apply_par_spell("attack", 0, power, 0, duration, false)
    Attack.act_apply_spell_end()

        Attack.atom_spawn(target, 0, "magic_lioncup", Attack.angleto(target))
  end

  return true
end

function spell_berserker_onremove(target,duration_end)

   if duration_end~=true then
			if Attack.act_size(target)>1 then 
				Attack.log(0.001,"add_blog_nobers_2","name",blog_side_unit(target,1))
			else 
				Attack.log(0.001,"add_blog_nobers_1","name",blog_side_unit(target,1))
			end 
   end

  return true
end

function spell_berserker_duration_end()

  local target = Attack.get_target()
  Attack.atom_spawn(target, 0, "magic_lioncup", Attack.angleto(target))

  return true
end

-- ***********************************************
-- * Adrenalin
-- ***********************************************

function spell_adrenalin_attack()

  local target = Attack.get_target()

  if (target ~= nil) then

    local level=Obj.spell_level()
    if level==0 then level=1 end

    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_adrenalin","duration"),level))

    local power = tonumber (pwr_adrenalin(level))
    local cur_ap=Attack.act_ap( target )
    local loggg=0
    --Attack.act_del_spell( target, "spell_adrenalin" )

--	local new_ap = math.min(power+cur_ap, (Attack.act_get_par(target, "speed")));
	local new_ap;
	  if (Attack.act_again(target)) then new_ap=power -- ������ ���� ��� �������� � ������ - ������ ������������� ��� ������ ���-�� ����� ������ ���������� �� � �������
  		loggg=1
	  else new_ap=power+cur_ap end
      Attack.act_ap( target, new_ap) -- ����� ������� ����, �.�. act_apply_spell_end() ��������������� �������� ��, �.�. �� ���� ���������� ������ �� ������ ���� ������� ����� �������� ����
      
    --Attack.act_apply_spell_begin( target, "spell_adrenalin", duration, false )
    --Attack.act_apply_spell_end()
  
    Attack.atom_spawn(target, 0, "magic_horn", Attack.angleto(target))
    if loggg==1 then 
    	Attack.log_label("add_blog_sadrenlin")
    	--Attack.log("add_blog_sadrenlin_2") 
    else
    	Attack.log_label("")
    end 
  end

  return true
end

-- ***********************************************
-- * Hypnosis
-- ***********************************************

function spell_hypnosis_attack(lvl,dmgts)

	if dmgts==nil then dmgts=0 end
	local level = lvl
  if level==nil then 
  	level=Obj.spell_level()
  else
  	level = tonumber(level)
  end 
  if level==0 then level=1 end

  local target = Attack.get_target()
  
  --local bl=1
  --if Attack.act_belligerent(target) == 1 or Attack.act_belligerent(target) == 2 then bl = 4 end
  local bl = Attack.act_belligerent()
  if bl == 0 then bl = 4 end
  Attack.act_belligerent(target, bl)

  local spell = "spell_hypnosis"
  local moves = text_dec(Logic.obj_par(spell,"duration"),level)

    Attack.act_apply_spell_begin( target, spell, moves, false )
    Attack.act_apply_spell_end()

  Attack.atom_spawn(target, dmgts, "magic_hypnosis",Attack.angleto(target))

	Attack.log_label("add_blog_shypnos")

	
  return true

end

function spell_hypnosis_onremove( caa )

  -- ���������� ��� ��� ����
  Attack.act_belligerent(caa, Attack.act_belligerent( caa, nil ))
			if Attack.act_size(caa)>1 then 
				Attack.log(0.001,"add_blog_nocharm_2","name",blog_side_unit(caa))
			end 
			if Attack.act_size(caa)==1 then
				Attack.log(0.001,"add_blog_nocharm_1","name",blog_side_unit(caa))
			end

  return true

end


-- ***********************************************
-- * Fire Breath
-- ***********************************************
function spell_fire_breath_attack()

  local target = Attack.get_target()

  if (target ~= nil) then 
      local level=Obj.spell_level()
    if level==0 then level=1 end

        local power=pwr_fire_breath()
    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_fire_breath","duration"),level))
		if Logic.obj_par(Obj.name(),"int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 


    Attack.act_del_spell(target,"spell_fire_breath")

    local mindfire = Attack.act_get_dmg_min(target, "fire")
    local mindpoison = Attack.act_get_dmg_min(target, "poison")
    local mindmagic = Attack.act_get_dmg_min(target, "magic")
    local mindphysical = Attack.act_get_dmg_min(target, "physical")
    local maxdfire = Attack.act_get_dmg_max(target, "fire")
    local maxdpoison = Attack.act_get_dmg_max(target, "poison")
    local maxdmagic = Attack.act_get_dmg_max(target, "magic")
    local maxdphysical = Attack.act_get_dmg_max(target, "physical")

		local max_dmg = (maxdfire + maxdpoison + maxdmagic + maxdphysical)*power/100
  	local min_dmg = (mindfire + mindpoison + mindmagic + mindphysical)*power/100
    				
    Attack.act_apply_spell_begin( target, "spell_fire_breath", duration, false )
      Attack.act_apply_dmgmax_spell( "fire", max_dmg, 0, 0, duration, false)
      Attack.act_apply_dmgmin_spell( "fire", min_dmg, 0, 0, duration, false)
    Attack.act_apply_spell_end()

    Attack.atom_spawn(target, 0, "magic_breath" )

  end

  return true
end

-- ***********************************************
-- * Star Force
-- ***********************************************

    --local d = Attack.atom_spawn(target, time_first_effect, damage_effect )    -- ������ �����
  --local dmgtd = Attack.aseq_time(d, "x")
  --common_cell_apply_damage(target, time_first_effect+dmgtd)   -- apply damage

-- atk_set_damage("resistname", damagemin [, damagemax])

function spell_magic_missle_attack()

    local target = Attack.get_target()
    if (target ~= nil) then

        local count = tonumber(Attack.get_custom_param("count"))

    common_cell_attack(target, "magic_missile")
  end

  return true
end


-- ***********************************************
-- * Stone Skin
-- ***********************************************
function spell_stone_skin_attack()

  local level=Obj.spell_level()
  if level==0 then level=1 end

    local power,penalty=pwr_stone_skin()
  local target = Attack.get_target()
  local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_stone_skin","duration"),level))
		if Logic.obj_par("spell_stone_skin","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 


  if (target ~= nil) then

    Attack.act_del_spell(target,"spell_stone_skin")

    local initiative,initiativebase = Attack.act_get_par(target,"initiative")

        if initiative<2 then
          penalty = 0
        end

--    attackbonus = attackbase*attacklow/100

    Attack.act_apply_spell_begin( target, "spell_stone_skin", duration, false )
      Attack.act_apply_res_spell( "physical", power, 0, 0, duration, false)
            Attack.act_apply_par_spell( "initiative", -penalty, 0, 0, duration, false)
            Attack.act_apply_par_spell( "defense", 0, 0, power, duration, false)
    Attack.act_apply_spell_end()

    local a = Attack.atom_spawn(target, 0, "magic_stoneskin" )
    local dmgts = Attack.aseq_time(a, "x")
    local dmgts2 = Attack.aseq_time(a, "y")
    
    Attack.act_set_diff_tex(target, "stone_skin.dds", dmgts)
    Attack.act_set_diff_tex(target, "", dmgts2)
  end

  return true
end

function spell_stone_skin_onremove(caa)

    Attack.act_set_diff_tex(caa, "", 1)
    return true

end

-- ***********************************************
-- * Dispell
-- ***********************************************
function takeoff_spells(target, type, check_only)

	local spells_to_delete={}

	for i=0,Attack.act_spell_count(target)-1 do
		local spell_name=Attack.act_spell_name(target,i)
		local spell_type=Logic.obj_par(spell_name,"type")
		if spell_type==type and string.find(spell_name,"^totem_")==nil then
-- 			Attack.act_del_spell(target, spell_name)
			table.insert(spells_to_delete, spell_name);
		end
	end

	if check_only then return table.getn(spells_to_delete) > 0 end

	for k,v in ipairs(spells_to_delete) do
		Attack.act_del_spell(target,v)
	end

	return table.getn(spells_to_delete) > 0

end

function spell_dispell_attack()

  local level=Obj.spell_level()
  if level==0 then level=1 end

  local target = Attack.get_target()
    local mode = text_dec(Logic.obj_par("spell_dispell","spell"),level)

  if (target ~= nil) then
	  if mode == "all" then
	    Attack.act_del_spell(target)
	  else

      	local type
	    if Attack.act_ally(target) then type = "penalty" -- �� ����� ������� ��� penalty-����������
	    else type = "bonus" end -- � ���������� ������� ��� bonus-����������

		if (Attack.act_is_spell(target, "spell_hypnosis") or Attack.act_is_spell(target, "effect_charm")) and Logic.obj_par("spell_hypnosis","type")==type then -- ������ ������� � ������ �������
			Attack.act_del_spell(target, "spell_hypnosis")
			Attack.act_del_spell(target, "effect_charm")
		    if Attack.act_ally(target) then type = "penalty" -- �� ����� ������� ��� penalty-����������
		    else type = "bonus" end -- � ���������� ������� ��� bonus-����������
		end

	    takeoff_spells(target, type)

	  end
	    Attack.atom_spawn(target, 0, "magic_dispel" )
  end

  return true
end

-- ***********************************************
-- * Oil Fog
-- ***********************************************

function spell_oil_fog_attack(lvl,dmgts)

	if dmgts==nil then dmgts=0 end
  local target = Attack.get_target()

  if (target ~= nil) then

    local min_dmg,max_dmg,duration,power = pwr_oil_fog(lvl)
    local dmg_type = Logic.obj_par("spell_oil_fog","typedmg")

    Attack.atk_set_damage(dmg_type,min_dmg,max_dmg)

    local a = Attack.atom_spawn(Attack.get_target(), dmgts, "magic_oilfog")
    local dmgts1 = Attack.aseq_time(a, "x")
    common_cell_apply_damage(Attack.get_target(), dmgts1+dmgts)

    Attack.act_del_spell(target,"spell_oil_fog")
    local resist,resistbase = Attack.act_get_res(target,"fire")

        if (resist<=-1*power) or (resist>=80) then
            power = 0
        else
            power = power + resist
        end

    Attack.act_apply_spell_begin( target, "spell_oil_fog", duration, false )
      Attack.act_apply_res_spell( "fire", -power, 0, 0, duration, false)
    Attack.act_apply_spell_end()

  end

  return true
end


-- ***********************************************
-- * Poison Resist
-- ***********************************************
function spell_poison_resist_attack()

  local target = Attack.get_target()

  if (target ~= nil) then
    local duration = tonumber(Attack.get_custom_param("duration"))
    local resistbonus = tonumber(Attack.get_custom_param("resistbonus"))

    Attack.act_del_spell(target,"spell_poison_resist")

    local resist,resistbase = Attack.act_get_res(target,"poison")

    if resistbonus <= resist then
      resistbonus = 0
    else if resist>=0 then
                resistbonus = resistbonus-resist
         end
    end

    Attack.act_apply_spell_begin( target, "spell_poison_resist", duration, false )
    Attack.act_apply_res_spell( "poison", resistbonus, 0, 0, duration, false)
    Attack.act_apply_spell_end()

    Attack.atom_spawn(target, 0, "magic_antisnakes" )
  end

  return true
end

-- ***********************************************
-- * Haste
-- ***********************************************
function spell_haste_attack(level,dmgts,target)

    if level == nil then level=Obj.spell_level() end
    if level==0 then level=1 end
    if dmgts == nil then dmgts = 0 end

    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_haste","duration"),level))
    
		if Logic.obj_par("spell_haste","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 


    if target == nil then target = Attack.get_target() end

    if target~=nil  then

      Attack.act_del_spell(target,"spell_haste")
      Attack.act_del_spell(target,"spell_slow")

      local speedbonus = pwr_haste(level)

      Attack.act_apply_spell_begin( target, "spell_haste", duration, false )
        Attack.act_apply_par_spell( "speed", speedbonus, 0, 0, duration, false)
      Attack.act_apply_spell_end()

      Attack.atom_spawn(target, dmgts, "magic_reaction", Attack.angleto(target))
    else
    local acnt = Attack.act_count()
    for i=1,acnt-1 do
      if Attack.act_ally(i) and Attack.act_applicable(i) then

        Attack.act_del_spell(i,"spell_haste")
        Attack.act_del_spell(i,"spell_slow")

        local speedbonus = pwr_haste()

        Attack.act_apply_spell_begin( i, "spell_haste", duration, false )
          Attack.act_apply_par_spell( "speed", speedbonus, 0, 0, duration, false)
        Attack.act_apply_spell_end()

        Attack.atom_spawn(i, dmgts, "magic_reaction", Attack.angleto(i))
      end
    end
  end

  return true
end

-- ***********************************************
-- * Fire Resist
-- ***********************************************
function spell_fire_resist_attack()

  local target = Attack.get_target()

  if (target ~= nil) then
    local duration = tonumber(Attack.get_custom_param("duration"))
    local resistbonus = tonumber(Attack.get_custom_param("resistbonus"))

    Attack.act_del_spell(target,"spell_fire_resist")

    local resist,resistbase = Attack.act_get_res(target,"fire")

    if resistbonus <= resist then
      resistbonus = 0
    else if resist>=0 then
                resistbonus = resistbonus-resist
         end
    end

    Attack.act_apply_spell_begin( target, "spell_fire_resist", duration, false )
    Attack.act_apply_res_spell( "fire", resistbonus, 0, 0, duration, false)
    Attack.act_apply_spell_end()

    Attack.atom_spawn(target, 0, "magic_antifire" )
  end

  return true
end

-- ***********************************************
-- * Fire arrow
-- ***********************************************

function spell_fire_arrow_attack(lvl,dmgts)

		if dmgts==nil then dmgts=0 end
		
    local min_dmg,max_dmg,burn = pwr_fire_arrow(lvl)
    local dmg_type = Logic.obj_par("spell_fire_arrow","typedmg")
    local burn_rnd=Game.Random(100)+1

    Attack.atk_set_damage(dmg_type,min_dmg,max_dmg)

     local target = Attack.get_target()
     local a = Attack.atom_spawn(target, dmgts, "magic_firearrow", Attack.angleto(target)+2.5)
     local dmgts1 = Attack.aseq_time(a, "x")
     common_cell_apply_damage(target, dmgts+dmgts1)

  -- ��������� ���� �� 3 ����. �� �������� ���� 1-5*power � ������ 5+5*power ������
  if burn_rnd<=burn then
    effect_burn_attack(target,dmgts+dmgts1+1,3)
  end
  --hll_post_archmage_lighting
  return true
end

-- ***********************************************
-- * Healing
-- ***********************************************

function spell_cure_attack()

  local target = Attack.get_target()

  if (target ~= nil) then
		Attack.log_label("null")
    local min_dmg,max_dmg = pwr_healing()
        local dmg_type = Logic.obj_par("spell_healing","typedmg")

        Attack.atk_set_damage(dmg_type,min_dmg,max_dmg)

      if (Attack.act_race(target,"undead")) then
        common_cell_attack(target, "hll_priest_heal_post")
        Attack.log_label("")
      elseif (Attack.act_need_cure(target)) then
		  					local level=Obj.spell_level()
  							if level==3 then 
                	Attack.act_del_spell(target,"effect_poison")
									Attack.act_del_spell(target,"spell_weakness")
									Attack.act_del_spell(target,"spell_infection")
									Attack.act_del_spell(target,"effect_weakness")
									Attack.act_del_spell(target,"special_plague")
									Attack.act_del_spell(target,"spell_plague")
									Attack.act_del_spell(target,"special_disease")
									Attack.act_del_spell(target,"effect_infection")
								end
   	
              local cure_hp = math.floor(Game.Random(min_dmg,max_dmg+0.45))
             	local max_hp = Attack.act_get_par(target,"health")
      				local cur_hp = Attack.act_hp(target)
      				if cure_hp > max_hp - cur_hp then cure_hp = max_hp - cur_hp end 

              local a = Attack.atom_spawn(target, 0, "hll_priest_heal_post")
              local dmgts = Attack.aseq_time(a, "x")

                Attack.act_cure(target, cure_hp, dmgts)
      if Attack.act_size(target)>1 then 
       	Attack.log("add_blog_sheal_2","hero_name",blog_side_unit(target,4)..Attack.hero_name(),"spell",blog_side_unit(target,3).."<label=spell_healing_name>","special",cure_hp,"target",blog_side_unit(target,-1))
      else 
      	Attack.log("add_blog_sheal_1","hero_name",blog_side_unit(target,4)..Attack.hero_name(),"spell",blog_side_unit(target,3).."<label=spell_healing_name>","special",cure_hp,"target",blog_side_unit(target,-1))
      end 
                
     end
  end

  return true
end

-- ***********************************************
-- * Magic Axe
-- ***********************************************

function spell_magic_axe_attack(lvl,dmgts)
  
  if dmgts==nil then dmgts=0 end
  local target = Attack.get_target()
  if (target ~= nil) then

    local min_dmg,max_dmg,count = pwr_magic_axe(lvl)
    local dmg_type = Logic.obj_par("spell_magic_axe","typedmg")

    local atomX = Attack.aseq_time("magicaxe", "x")
    local unitX = Attack.aseq_time(target, "damage", "x")

    Attack.atk_set_damage(dmg_type, min_dmg, max_dmg)

	local interval = 2

    for i=0, count-1 do
        local t = i * interval -- ����� ������ ������
	    Attack.atom_spawn(target, t+dmgts, "magicaxe", Attack.angleto(target))

	    if i == count - 1 then
	    	common_cell_apply_damage(target, t + atomX+dmgts)
		else
	    	Attack.act_animate(target, "damage", t + atomX - unitX+dmgts)
		end
    end

  end
  return true
end


-- ***********************************************
-- * Smile Skull
-- ***********************************************

function spell_smile_skull_attack(lvl,dmgts)

	if dmgts==nil then dmgts=0 end
  local target = Attack.get_target()
  if (target ~= nil) then

        local min_dmg,max_dmg,poison = pwr_smile_skull(lvl)
        local dmg_type = Logic.obj_par("spell_smile_skull","typedmg")
        local poison_rnd=Game.Random(100)+1

        Attack.atk_set_damage(dmg_type,min_dmg,max_dmg)

     local a = Attack.atom_spawn(target, dmgts, "magic_skull", Attack.angleto(target))
     local dmgts1 = Attack.aseq_time(a, "x")
     common_cell_apply_damage(target, dmgts1+dmgts)

  -- ��������� ���� �� 3 ����. �� �������� ���� 1-5*power � ������ 5+5*power �����
  if poison_rnd<=poison then
    effect_poison_attack(target,dmgts1+1+dmgts,3)
  end
  --hll_post_archmage_lighting
  return true

  end
  return true
end


-- ***********************************************
-- * Fire Ball
-- ***********************************************

function spell_fire_ball_attack()

  local target = Attack.get_target()                            -- epicentre
  if (target ~= nil) then

    local a = Attack.atom_spawn(target, 0, "magic_fireball", 5.5)          -- summon fireball in epicentre
    local dmgts = Attack.aseq_time(a, "x")

        local min_e_dmg,max_e_dmg,burn = pwr_fire_ball("epicentre")
        local min_p_dmg,max_p_dmg,burn = pwr_fire_ball("periphery")
        local dmg_type = Logic.obj_par("spell_fire_ball","typedmg")

--      if (Attack.act_enemy(target)) then                          -- contains enemy
      Attack.atk_set_damage(dmg_type,min_e_dmg,max_e_dmg)
      common_cell_apply_damage(target, dmgts)                       -- apply damage in epicentre

        local burn_rnd=Game.Random(100)+1
        if burn_rnd<=burn then
        	effect_burn_attack(target,dmgts+1,3)
        end

--    end

    local n = 7

    for i=1,n-1 do
      target = Attack.get_target(i)
      if (target ~= nil) then

        Attack.atk_set_damage(dmg_type,min_p_dmg,max_p_dmg)   -- set custom damage for periphery
        common_cell_apply_damage(target, dmgts)       -- apply damage

            burn_rnd=Game.Random(100)+1
        	if burn_rnd<=burn/2 then
                effect_burn_attack(target,dmgts+1,3)
            end

      end
    end
  end

  return true
end



-- ***********************************************
-- * Fire Rain
-- ***********************************************

function spell_fire_rain_attack()

  local target = Attack.get_target()                -- epicentre
  if (target ~= nil) then

        local min_dmg,max_dmg,burn = pwr_fire_rain()
        local dmg_type = Logic.obj_par("spell_fire_ball","typedmg")

    local n = 7
    for i=0,n-1 do
      target = Attack.get_target(i)
      if (target ~= nil) then
        Attack.atk_set_damage(dmg_type,min_dmg,max_dmg)
        local deviation = Game.Random()
        local a = Attack.atom_spawn(target, deviation, "hll_firerain", 2.0)    -- summon fire rain
        local dmgts = Attack.aseq_time(a, "x")
        common_cell_apply_damage(target, deviation + dmgts)                     -- apply damage

        burn_rnd=Game.Random(100)+1
            if burn_rnd<=burn then
							effect_burn_attack(target,dmgts+2+deviation,3)
            end

      end
    end

  end

  return true
end

-- ***********************************************
-- * Ice Serpent
-- ***********************************************

function spell_ice_serpent_highlight()

  local target = Attack.get_target()
  if (not Attack.cell_is_empty(target)) and Attack.act_enemy(target) and Attack.act_takesdmg(target) and Attack.act_applicable(target) then       -- can receive this attack
  	Attack.cell_select(target,"avenemy")
  	for i = 0, 5 do
  		local cell = Attack.cell_adjacent(target,i)
  		if cell~=nil then
			if (not Attack.cell_is_empty(cell)) --[[and Attack.act_enemy(cell)]] and Attack.act_takesdmg(cell) and Attack.act_applicable(cell) then
		  		Attack.cell_select(cell,"avenemy")
		  	else
		  		Attack.cell_select(cell,"destination")
		  	end
		end
  	end
  end

  return true
end

freeze_im=0.75 --25%

function spell_ice_serpent_attack(level,dmgt,target)

    if dmgt == nil then dmgt = 0 end

  if target == nil then target = Attack.get_target() end                                -- epicentre
  if (target ~= nil) then

    local a = Attack.atom_spawn(target, dmgt, "magic_icedragon" )              -- summon iceball in epicentre
    local dmgts = Attack.aseq_time(a, "x")+dmgt

    local min_e_dmg,max_e_dmg,freeze = pwr_ice_serpent("epicentre",level)
    local min_p_dmg,max_p_dmg = pwr_ice_serpent("periphery",level)

    if Attack.act_feature(target,"freeze_immunitet") then
      min_e_dmg = min_e_dmg*freeze_im
      max_e_dmg = max_e_dmg*freeze_im
    end

    local dmg_type = Logic.obj_par("spell_ice_serpent","typedmg")

    Attack.atk_set_damage(dmg_type,min_e_dmg,max_e_dmg)
    common_cell_apply_damage(target, dmgts)                       -- apply damage in epicentre

    if Attack.act_feature(target,"freeze_immunitet") then
      freeze_rnd = freeze+10
    else
        freeze_rnd=Game.Random(100)+1
    end
    local cold_fear=Attack.act_get_res(target,"fire")
    if freeze_rnd<=freeze or cold_fear>=50 then
      effect_freeze_attack(target,dmgts+1,3)
    end

	local epicenter = target
  	for i = 0, 5 do
  	  target = Attack.cell_adjacent(epicenter,i)
      --target = Attack.get_target(i)
      if target ~= nil and Attack.cell_present(target) and Attack.get_caa(target) ~= nil and not Attack.act_equal(0,target) then

        if Attack.act_feature(target,"freeze_immunitet") then
            Attack.atk_set_damage(dmg_type,min_p_dmg*freeze_im,max_p_dmg*freeze_im)   -- set custom damage for periphery
        else
            Attack.atk_set_damage(dmg_type,min_p_dmg,max_p_dmg)   -- set custom damage for periphery
        end

        common_cell_apply_damage(target, dmgts)       -- apply damage

            if Attack.act_feature(target,"freeze_immunitet") then
              freeze_rnd = freeze+10
            else
                freeze_rnd=Game.Random(100)+1
            end
            if freeze_rnd<=freeze/2 then
            effect_freeze_attack(target,dmgts+1,3)
            end

      end
    end
  end

  return true
end

-- ***********************************************
-- * Teleport
-- ***********************************************

function spell_teleport_attack()

  local cycle = Attack.get_cycle()

  if (cycle == 0) then
    local target = Attack.get_target()
    Attack.val_store("teleport_source", target)
--    Attack.val_store("teleport_target_cell", Attack.cell_id(target))
    Attack.next(0)
  elseif (cycle == 1) then
    local source = Attack.val_restore("teleport_source")
    local target = Attack.get_target()

    if ((source ~= nil) and (target ~= nil)) then

      Attack.act_aseq(source, "telein")
      Attack.atom_spawn(source, 0, "hll_telein" )

      local t = Attack.aseq_time(source)

      Attack.act_aseq(source, "teleout" )
      Attack.atom_spawn(target, t, "hll_teleout" )

      Attack.act_teleport(source, target, t)
	  Attack.log("hero_casts_a_spell_2","hero_name",blog_side_unit(source,4)..Attack.hero_name(),"spell","<label=spell_teleport_name>","targeta",blog_side_unit(source,0))
    end

  end

  return true
end


-- ***********************************************
-- * Lightning
-- ***********************************************

function spell_lightning_attack(lvl,dmgts)

	if dmgts==nil then dmgts=0 end
	local min_dmg,max_dmg,shock,count = pwr_lightning(lvl)
	local dmg_type = Logic.obj_par("spell_lightning","typedmg")
	local shock_rnd=Game.Random(100)+1
	local dmg = Game.Random(min_dmg, max_dmg)

	Attack.atk_set_damage(dmg_type, dmg)

	local target = Attack.get_target()
	local a = Attack.atom_spawn(target, dmgts, "magic_lightning")
	local dmgts1 = Attack.aseq_time(a, "x")
	local interval = Attack.aseq_time("magic_lightning", "z")
	common_cell_apply_damage(target, dmgts1+dmgts)

	if shock_rnd<=shock then
		effect_shock_attack(target,dmgts1+1+dmgts)
	end

	local attacked_ids = {}
	attacked_ids[Attack.cell_id(target)] = true
	local front = {target}

	repeat

    	if count == 0 then break end
    	count = count - 1
		dmgts = dmgts + interval
		dmg = dmg * 0.5

		local new_front = {}
		-- ������� ������ �������� ������
		for k,target in ipairs(front) do
			local mind, atkd = 1e10, {}
			for i=1, Attack.act_count()-1 do
				local d = Attack.cell_mdist(target, i)
				if d <= 4.*1.8+.1 and (Attack.act_enemy(i) or Attack.act_ally(i)) and Attack.act_applicable(i) and Attack.act_takesdmg(i)
						and attacked_ids[Attack.cell_id(Attack.get_cell(i))] == nil then
					if math.abs(mind-d) < .1 then table.insert(atkd, i)
					elseif d < mind then mind = d; atkd = {i} end
				end
			end
			for i,act in ipairs(atkd) do -- ������� ���, ���� �������
				Attack.atk_set_damage(dmg_type, dmg)
				local d = math.min(4,math.max(1,math.floor(Attack.cell_mdist(target,act)/1.8+.5)))
				local a = Attack.atom_spawn(target, dmgts, "lightning_bolt"..d.."hx", Attack.angleto(target,act))
				local dmgts1 = Attack.aseq_time(a, "x")
				common_cell_apply_damage(act, dmgts1+dmgts)
				if Game.Random(100) < shock then
					effect_shock_attack(act,dmgts1+1+dmgts)
				end

				attacked_ids[Attack.cell_id(Attack.get_cell(act))] = true
				-- ��������� ��������� �����
				table.insert(new_front, act)
			end
		end
		front = new_front

	until table.getn(front) == 0

	--hll_post_archmage_lighting
	return true

end

-- ***********************************************
-- * Weakness
-- ***********************************************

function spell_weakness_attack(lvl,dmgts)

	if dmgts==nil then dmgts=0 end
	local level = lvl
  if level==nil then 
  	level=Obj.spell_level()
  else
  	level = tonumber(level)
  end 
  if level==0 then level=1 end

    --local healer_bonus=Logic.hero_lu_skill("healer")
    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_weakness","duration"),level))
		if Logic.obj_par("spell_weakness","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

    --+healer_bonus

  local target = Attack.get_target()

    if target~=nil  then
    --Attack.act_ally(target) then

    Attack.act_del_spell(target,"spell_weakness")
    Attack.act_del_spell(target,"spell_bless")

    local mind, mindfire = Attack.act_get_dmg_min(target, "fire")
    local mind, mindpoison = Attack.act_get_dmg_min(target, "poison")
    local mind, mindmagic = Attack.act_get_dmg_min(target, "magic")
    local mind, mindphysical = Attack.act_get_dmg_min(target, "physical")
    local maxd, maxdfire = Attack.act_get_dmg_max(target, "fire")
    local maxd, maxdpoison = Attack.act_get_dmg_max(target, "poison")
    local maxd, maxdmagic = Attack.act_get_dmg_max(target, "magic")
    local maxd, maxdphysical = Attack.act_get_dmg_max(target, "physical")



    local bonfire = maxdfire - mindfire
    local bonpoison = maxdpoison - mindpoison
    local bonmagic = maxdmagic - mindmagic
    local bonphysical = maxdphysical - mindphysical

    Attack.act_apply_spell_begin( target, "spell_weakness", duration, false )
--[[       Attack.act_apply_dmgmax_spell( "fire", -bonfire, 0, 0, duration, false)
       Attack.act_apply_dmgmax_spell( "poison", -bonpoison, 0, 0, duration, false)
       Attack.act_apply_dmgmax_spell( "magic", -bonmagic, 0, 0, duration, false)
       Attack.act_apply_dmgmax_spell( "physical", -bonphysical, 0, 0, duration, false)]]
    Attack.act_apply_spell_end()

        Attack.atom_spawn(target, dmgts, "magic_sword", Attack.angleto(target))
    else
  local acnt = Attack.act_count()
  for i=1,acnt-1 do
    if Attack.act_enemy(i) and Attack.act_applicable(i) then

      Attack.act_del_spell(i,"spell_weakness")
      Attack.act_del_spell(i,"spell_bless")

        local mind, mindfire = Attack.act_get_dmg_min(i, "fire")
        local mind, mindpoison = Attack.act_get_dmg_min(i, "poison")
        local mind, mindmagic = Attack.act_get_dmg_min(i, "magic")
        local mind, mindphysical = Attack.act_get_dmg_min(i, "physical")
        local maxd, maxdfire = Attack.act_get_dmg_max(i, "fire")
        local maxd, maxdpoison = Attack.act_get_dmg_max(i, "poison")
        local maxd, maxdmagic = Attack.act_get_dmg_max(i, "magic")
        local maxd, maxdphysical = Attack.act_get_dmg_max(i, "physical")

        local bonfire = maxdfire - mindfire
        local bonpoison = maxdpoison - mindpoison
        local bonmagic = maxdmagic - mindmagic
        local bonphysical = maxdphysical - mindphysical

        Attack.act_apply_spell_begin( i, "spell_weakness", duration, false )
--[[            Attack.act_apply_dmgmax_spell( "fire", -bonfire, 0, 0, duration, false)
            Attack.act_apply_dmgmax_spell( "poison", -bonpoison, 0, 0, duration, false)
            Attack.act_apply_dmgmax_spell( "magic", -bonmagic, 0, 0, duration, false)
            Attack.act_apply_dmgmax_spell( "physical", -bonphysical, 0, 0, duration, false)]]
        Attack.act_apply_spell_end()

            Attack.atom_spawn(i, 0, "magic_sword", Attack.angleto(i))
        end
  end
  end
  return true
end

-- ***********************************************
-- * Bless 
-- ***********************************************

function spell_bless_attack(level,dmgts,target)

    if level == nil then level=Obj.spell_level() end
    if level==0 then level=1 end
    if dmgts == nil then dmgts = 0 end
		
		local healer_bonus=tonumber(Logic.hero_lu_skill("healer"))
		if healer_bonus>0 then healer_bonus=1 end
    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_bless","duration"),level))+healer_bonus
		if Logic.obj_par("spell_bless","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

    if target == nil then target = Attack.get_target() end

    if target~=nil  then
    --Attack.act_ally(target) then

    Attack.act_del_spell(target,"spell_weakness")
    Attack.act_del_spell(target,"spell_bless")

    local mind, mindfire = Attack.act_get_dmg_min(target, "fire")
    local mind, mindpoison = Attack.act_get_dmg_min(target, "poison")
    local mind, mindmagic = Attack.act_get_dmg_min(target, "magic")
    local mind, mindphysical = Attack.act_get_dmg_min(target, "physical")
    local maxd, maxdfire = Attack.act_get_dmg_max(target, "fire")
    local maxd, maxdpoison = Attack.act_get_dmg_max(target, "poison")
    local maxd, maxdmagic = Attack.act_get_dmg_max(target, "magic")
    local maxd, maxdphysical = Attack.act_get_dmg_max(target, "physical")



    local bonfire = maxdfire - mindfire
    local bonpoison = maxdpoison - mindpoison
    local bonmagic = maxdmagic - mindmagic
    local bonphysical = maxdphysical - mindphysical

    Attack.act_apply_spell_begin( target, "spell_bless", duration, false )
--[[       Attack.act_apply_dmgmin_spell( "fire", bonfire, 0, 0, duration, false)
       Attack.act_apply_dmgmin_spell( "poison", bonpoison, 0, 0, duration, false)
       Attack.act_apply_dmgmin_spell( "magic", bonmagic, 0, 0, duration, false)
       Attack.act_apply_dmgmin_spell( "physical", bonphysical, 0, 0, duration, false)]]
    Attack.act_apply_spell_end()

        Attack.atom_spawn(target, dmgts, "magic_bless", Attack.angleto(target))
    else
  local acnt = Attack.act_count()
  for i=1,acnt-1 do
    if Attack.act_ally(i) and Attack.act_applicable(i) then

      Attack.act_del_spell(i,"spell_weakness")
      Attack.act_del_spell(i,"spell_bless")

        local mind, mindfire = Attack.act_get_dmg_min(i, "fire")
        local mind, mindpoison = Attack.act_get_dmg_min(i, "poison")
        local mind, mindmagic = Attack.act_get_dmg_min(i, "magic")
        local mind, mindphysical = Attack.act_get_dmg_min(i, "physical")
        local maxd, maxdfire = Attack.act_get_dmg_max(i, "fire")
        local maxd, maxdpoison = Attack.act_get_dmg_max(i, "poison")
        local maxd, maxdmagic = Attack.act_get_dmg_max(i, "magic")
        local maxd, maxdphysical = Attack.act_get_dmg_max(i, "physical")

        local bonfire = maxdfire - mindfire
        local bonpoison = maxdpoison - mindpoison
        local bonmagic = maxdmagic - mindmagic
        local bonphysical = maxdphysical - mindphysical

        Attack.act_apply_spell_begin( i, "spell_bless", duration, false )
--[[        Attack.act_apply_dmgmin_spell( "fire", bonfire, 0, 0, duration, false)
        Attack.act_apply_dmgmin_spell( "poison", bonpoison, 0, 0, duration, false)
        Attack.act_apply_dmgmin_spell( "magic", bonmagic, 0, 0, duration, false)
        Attack.act_apply_dmgmin_spell( "physical", bonphysical, 0, 0, duration, false)]]
        Attack.act_apply_spell_end()

            Attack.atom_spawn(i, 0, "magic_bless", Attack.angleto(i))
        end
  end
  end
  return true
end

-- ***********************************************
-- * Pacifism
-- ***********************************************
function spell_pacifism_attack()


  local target = Attack.get_target()

  if (target ~= nil) then
    local level=Obj.spell_level()
    if level==0 then level=1 end


    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_pacifism","duration"),level))
		if Logic.obj_par(Obj.name(),"int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

    local bonus,penalty = pwr_pacifism()

        Attack.act_del_spell(target,"spell_pacifism")

    local dfire, mindfire = Attack.act_get_dmg_min(target, "fire")
    local dpoison, mindpoison = Attack.act_get_dmg_min(target, "poison")
    local dmagic, mindmagic = Attack.act_get_dmg_min(target, "magic")
    local dphysical, mindphysical = Attack.act_get_dmg_min(target, "physical")
    local dfire, maxdfire = Attack.act_get_dmg_max(target, "fire")
    local dpoison, maxdpoison = Attack.act_get_dmg_max(target, "poison")
    local dmagic, maxdmagic = Attack.act_get_dmg_max(target, "magic")
    local dphysical, maxdphysical = Attack.act_get_dmg_max(target, "physical")

    local bhealth, bhealth = Attack.act_get_par(target, "health")
    local hbonus = bhealth*bonus/100

    Attack.act_apply_spell_begin( target, "spell_pacifism", duration, false )
        Attack.act_apply_dmgmax_spell( "fire", -maxdfire*penalty/100, 0, 0, duration, false)
        Attack.act_apply_dmgmax_spell( "poison", -maxdpoison*penalty/100, 0, 0, duration, false)
        Attack.act_apply_dmgmax_spell( "magic", -maxdmagic*penalty/100, 0, 0, duration, false)
        Attack.act_apply_dmgmax_spell( "physical", -maxdphysical*penalty/100, 0, 0, duration, false)
        Attack.act_apply_dmgmin_spell( "fire", -mindfire*penalty/100, 0, 0, duration, false)
        Attack.act_apply_dmgmin_spell( "poison", -mindpoison*penalty/100, 0, 0, duration, false)
        Attack.act_apply_dmgmin_spell( "magic", -mindmagic*penalty/100, 0, 0, duration, false)
        Attack.act_apply_dmgmin_spell( "physical", -mindphysical*penalty/100, 0, 0, duration, false)
           Attack.act_apply_par_spell( "health", hbonus, 0, 0, duration, false)
    Attack.act_apply_spell_end()

    Attack.atom_spawn(target, 0, "magic_dove", Attack.angleto(target))
  end

  return true
end

-- ***********************************************
-- * Defenseless
-- ***********************************************
function spell_defenseless_attack(lvl,dmgts)

	if dmgts==nil then dmgts=0 end
  local target = Attack.get_target()

  if (target ~= nil) then

	local level = lvl
  if level==nil then 
  	level=Obj.spell_level()
  else
  	level = tonumber(level)
  end 
  if level==0 then level=1 end

    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_defenseless","duration"),level))
		if Logic.obj_par("spell_defenseless","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

    local less = pwr_defenseless(lvl)

    Attack.act_del_spell(target,"spell_defenseless")

    Attack.act_apply_spell_begin( target, "spell_defenseless", duration, false )
    Attack.act_apply_par_spell( "defense", 0, 0, -less, duration, false)
    Attack.act_apply_spell_end()

    Attack.atom_spawn(target, dmgts, "magic_shield", Attack.angleto(target))
  end

  return true
end

-- ***********************************************
-- * Divine Armor
-- ***********************************************

function spell_divine_armor_attack()

  local target = Attack.get_target()

  if (target ~= nil) then
    local level=Obj.spell_level()
    if level==0 then level=1 end

    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_divine_armor","duration"),level))
		if Logic.obj_par("spell_divine_armor","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

    local resistbonus = pwr_divine_armor()

    Attack.act_del_spell( target, "spell_divine_armor" )

        local curfire,firebase = Attack.act_get_res(target,"fire")
        local curphysical,physicalbase = Attack.act_get_res(target,"physical")
        local curmagic,magicbase = Attack.act_get_res(target,"magic")
        local curpoison,poisonbase = Attack.act_get_res(target,"poison")

        local bonfire = resistbonus
        local bonphysical = resistbonus
        local bonmagic = resistbonus
        local bonpoison = resistbonus

        if firebase<bonfire then
          bonfire = bonfire-firebase
        else
          bonfire = 0
        end
        if physicalbase<bonphysical then
          bonphysical = bonphysical-physicalbase
        else
          bonphysical = 0
        end
        if magicbase<bonmagic then
          bonmagic = bonmagic-magicbase
        else
          bonmagic = 0
        end
        if poisonbase<bonpoison then
          bonpoison = bonpoison-poisonbase
        else
          bonpoison = 0
        end

    Attack.act_apply_spell_begin( target, "spell_divine_armor", duration, false )
      Attack.act_apply_res_spell( "fire", bonfire, 0, 0, duration, false)
      Attack.act_apply_res_spell( "physical", bonphysical, 0, 0, duration, false)
      Attack.act_apply_res_spell( "magic", bonmagic, 0, 0, duration, false)
      Attack.act_apply_res_spell( "poison", bonpoison, 0, 0, duration, false)
    Attack.act_apply_spell_end()

    Attack.atom_spawn(target, 0, "magic_armor", Attack.angleto(target))
  end

  return true
end

-- ***********************************************
-- * Slow
-- ***********************************************
function spell_slow_attack(lvl,dmgts)

	if dmgts==nil then dmgts=0 end
	local level = lvl
  if level==nil then 
  	level=Obj.spell_level()
  else
  	level = tonumber(level)
  end 
  if level==0 then level=1 end

  local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_slow","duration"),level))
		if Logic.obj_par("spell_slow","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

  local target = Attack.get_target()
  if (target ~= nil) then

    local speedlow = pwr_slow(lvl)
--    local cur_ap=Attack.act_ap(target)

    Attack.act_del_spell(target,"spell_slow")
    Attack.act_del_spell(target,"spell_haste")
--
--    local speed = Attack.act_get_par(target, "speed")
--
--    if speed<=speedlow then
--        speedlow=speed-1
--    end
--    if cur_ap<(speed-speedlow) then
--      cur_ap = speed-speedlow
--    end

    Attack.act_apply_spell_begin( target, "spell_slow", duration, false )
      Attack.act_apply_par_spell( "speed", -speedlow, 0, 0, duration, false)
--      Attack.act_ap( target, cur_ap)
    Attack.act_apply_spell_end()

    Attack.atom_spawn(target, dmgts, "magic_slow" )
  else
    local acnt = Attack.act_count()
    for i=1,acnt-1 do
      if Attack.act_enemy(i) and Attack.act_applicable(i) then
        local speedlow = pwr_slow()
        Attack.act_del_spell(i,"spell_slow")
        Attack.act_del_spell(i,"spell_haste")

--        local speed = Attack.act_get_par(i, "speed")
--        local cur_ap=Attack.act_ap(i)

--        if speed<=speedlow then
--          speedlow=speed-1
--        end
--        if cur_ap<(speed-speedlow) then
--          cur_ap = speed-speedlow
--        end

        Attack.act_apply_spell_begin( i, "spell_slow", duration, false )
          Attack.act_apply_par_spell( "speed", -speedlow, 0, 0, duration, false)
--          Attack.act_ap( i, cur_ap)
        Attack.act_apply_spell_end()

        Attack.atom_spawn(i, 0, "magic_slow" )

      end
    end
  end

  return true
end

-- ***********************************************
-- * Accuracy
-- ***********************************************
function spell_accuracy_attack()

    local level=Obj.spell_level()
    if level==0 then level=1 end

  local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_accuracy","duration"),level))
		if Logic.obj_par(Obj.name(),"int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

  local bonus = pwr_accuracy()

  local target = Attack.get_target()

  if (target ~= nil) then

    Attack.act_del_spell(target,"spell_accuracy")

    local mind, minfire = Attack.act_get_dmg_min(target, "fire")
    local mind, minpoison = Attack.act_get_dmg_min(target, "poison")
    local mind, minmagic = Attack.act_get_dmg_min(target, "magic")
    local mind, minphysical = Attack.act_get_dmg_min(target, "physical")
    local maxd, maxfire = Attack.act_get_dmg_max(target, "fire")
    local maxd, maxpoison = Attack.act_get_dmg_max(target, "poison")
    local maxd, maxmagic = Attack.act_get_dmg_max(target, "magic")
    local maxd, maxphysical = Attack.act_get_dmg_max(target, "physical")

    Attack.act_apply_spell_begin( target, "spell_accuracy", duration, false )
        Attack.act_apply_dmgmin_spell( "fire", minfire*bonus/100, 0, 0, duration, false)
        Attack.act_apply_dmgmin_spell( "poison", minpoison*bonus/100, 0, 0, duration, false)
        Attack.act_apply_dmgmin_spell( "magic", minmagic*bonus/100, 0, 0, duration, false)
        Attack.act_apply_dmgmin_spell( "physical", minphysical*bonus/100, 0, 0, duration, false)
        Attack.act_apply_dmgmax_spell( "fire", maxfire*bonus/100, 0, 0, duration, false)
        Attack.act_apply_dmgmax_spell( "poison", maxpoison*bonus/100, 0, 0, duration, false)
        Attack.act_apply_dmgmax_spell( "magic", maxmagic*bonus/100, 0, 0, duration, false)
        Attack.act_apply_dmgmax_spell( "physical", maxphysical*bonus/100, 0, 0, duration, false)
    Attack.act_apply_spell_end()

    Attack.atom_spawn(target, 0, "magic_accuracy", Attack.angleto(target))
  else
    local acnt = Attack.act_count()
    for i=1,acnt-1 do
      if Attack.act_ally(i) and Attack.act_applicable(i) then
        Attack.act_del_spell(i,"spell_accuracy")

        local mind, minfire = Attack.act_get_dmg_min(i, "fire")
        local mind, minpoison = Attack.act_get_dmg_min(i, "poison")
        local mind, minmagic = Attack.act_get_dmg_min(i, "magic")
        local mind, minphysical = Attack.act_get_dmg_min(i, "physical")
        local maxd, maxfire = Attack.act_get_dmg_max(i, "fire")
        local maxd, maxpoison = Attack.act_get_dmg_max(i, "poison")
        local maxd, maxmagic = Attack.act_get_dmg_max(i, "magic")
        local maxd, maxphysical = Attack.act_get_dmg_max(i, "physical")

        Attack.act_apply_spell_begin( i, "spell_accuracy", duration, false )
          Attack.act_apply_dmgmin_spell( "fire", minfire*bonus/100, 0, 0, duration, false)
          Attack.act_apply_dmgmin_spell( "poison", minpoison*bonus/100, 0, 0, duration, false)
          Attack.act_apply_dmgmin_spell( "magic", minmagic*bonus/100, 0, 0, duration, false)
          Attack.act_apply_dmgmin_spell( "physical", minphysical*bonus/100, 0, 0, duration, false)
          Attack.act_apply_dmgmax_spell( "fire", maxfire*bonus/100, 0, 0, duration, false)
          Attack.act_apply_dmgmax_spell( "poison", maxpoison*bonus/100, 0, 0, duration, false)
          Attack.act_apply_dmgmax_spell( "magic", maxmagic*bonus/100, 0, 0, duration, false)
          Attack.act_apply_dmgmax_spell( "physical", maxphysical*bonus/100, 0, 0, duration, false)
        Attack.act_apply_spell_end()

        Attack.atom_spawn(i, 0, "magic_accuracy", Attack.angleto(i))
      end
    end
  end
  return true
end

-- ***********************************************
-- * Pygmy
-- ***********************************************

function spell_pygmy_attack(lvl,dmgts)

	if dmgts==nil then dmgts=0 end
  local target = Attack.get_target()

  if (target ~= nil) then

	local level = lvl
  if level==nil then 
  	level=Obj.spell_level()
  else
  	level = tonumber(level)
  end 
  if level==0 then level=1 end

    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_pygmy","duration"),level))
		if Logic.obj_par("spell_pygmy","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

    local penalty = pwr_pygmy(lvl)

    local health = Attack.act_get_par(target,"health")
    local cur_health = Attack.act_hp(target)
    local mind, minfire = Attack.act_get_dmg_min(target, "fire")
    local mind, minpoison = Attack.act_get_dmg_min(target, "poison")
    local mind, minmagic = Attack.act_get_dmg_min(target, "magic")
    local mind, minphysical = Attack.act_get_dmg_min(target, "physical")
    local maxd, maxfire = Attack.act_get_dmg_max(target, "fire")
    local maxd, maxpoison = Attack.act_get_dmg_max(target, "poison")
    local maxd, maxmagic = Attack.act_get_dmg_max(target, "magic")
    local maxd, maxphysical = Attack.act_get_dmg_max(target, "physical")
    
    Attack.act_del_spell( target, "spell_pygmy" )
		Attack.act_enable_attack(target,"longattack",false)
    Attack.act_apply_spell_begin( target, "spell_pygmy", duration, false )
        Attack.act_small(target,1.1)
          Attack.act_apply_dmgmin_spell( "fire", -minfire*penalty/100, 0, 0, duration, false)
          Attack.act_apply_dmgmin_spell( "poison", -minpoison*penalty/100, 0, 0, duration, false)
          Attack.act_apply_dmgmin_spell( "magic", -minmagic*penalty/100, 0, 0, duration, false)
          Attack.act_apply_dmgmin_spell( "physical", -minphysical*penalty/100, 0, 0, duration, false)
          Attack.act_apply_dmgmax_spell( "fire", -maxfire*penalty/100, 0, 0, duration, false)
          Attack.act_apply_dmgmax_spell( "poison", -maxpoison*penalty/100, 0, 0, duration, false)
          Attack.act_apply_dmgmax_spell( "magic", -maxmagic*penalty/100, 0, 0, duration, false)
          Attack.act_apply_dmgmax_spell( "physical", -maxphysical*penalty/100, 0, 0, duration, false)
        Attack.act_apply_par_spell( "health", -health*penalty/100, 0, 0, duration, false)
        Attack.act_hp(target, cur_health-cur_health*penalty/100)
    Attack.act_apply_spell_end()

        Attack.atom_spawn(target, dmgts, "magic_pygmy", Attack.angleto(target))
  end

  return true
end

function spell_pygmy_onremove(caa)

	Attack.act_enable_attack(caa,"longattack")
	Attack.act_small(caa,1,false)
		
  return true
end
-- ***********************************************
-- * Blind
-- ***********************************************
function spell_blind_attack(lvl,dmgts)

	if dmgts==nil then dmgts=0 end
  local target = Attack.get_target()

  if (target ~= nil) then
	local level = lvl
  if level==nil then 
  	level=Obj.spell_level()
  else
  	level = tonumber(level)
  end 
  if level==0 then level=1 end

    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_blind","duration"),level))
		if Logic.obj_par("spell_blind","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

    Attack.act_apply_spell_begin( target, "spell_blind", duration, true )
        Attack.act_skipmove(target,duration)
    Attack.act_apply_spell_end()


    Attack.atom_spawn(target, dmgts, "magic_blind" )
  end

  return true
end

function spell_blind_attack_onremove(caa)
  Attack.act_skipmove(caa,0)
  return true
end

-- ***********************************************
-- * Reaction
-- ***********************************************
function spell_reaction_attack()

  local target = Attack.get_target()

    local level=Obj.spell_level()
    if level==0 then level=1 end

    local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_reaction","duration"),level))
		if Logic.obj_par("spell_reaction","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

    local initiativebonus = pwr_warcry()

  if (target ~= nil) then

    Attack.act_del_spell(target,"spell_reaction")

    Attack.act_apply_spell_begin( target, "spell_reaction", duration, false )
      Attack.act_apply_par_spell( "initiative", initiativebonus, 0, 0, duration, false)
    Attack.act_apply_spell_end()
    Attack.resort()

    Attack.atom_spawn(target, 0, "magic_warcry", Attack.angleto(target))
  else
    local acnt = Attack.act_count()
    for i=1,acnt-1 do
      if Attack.act_ally(i) and Attack.act_applicable(i) then
  	  	Attack.act_apply_spell_begin( i, "spell_reaction", duration, false )
      		Attack.act_apply_par_spell( "initiative", initiativebonus, 0, 0, duration, false)
    		Attack.act_apply_spell_end()
    		Attack.resort()

    		Attack.atom_spawn(i, 0, "magic_warcry", Attack.angleto(i))
    	end 
    end
	end 

  return true
end

-- ***********************************************
-- * Sacrifice
-- ***********************************************

function spell_sacrifice_attack()

  local cycle = Attack.get_cycle()
  local level=Obj.spell_level()
  if level==0 then level=1 end
	
	local damage,power=pwr_sacrifice(level)
	local count,count_1,dead=0,0,0
  local source, target
  
  if (cycle == 0) then
  	local source = Attack.get_target()
    Attack.val_store("sacrifice_source", source)
    Attack.val_store("sacrifice_hp", math.min(damage,Attack.act_totalhp(source))*power/100)
    Attack.next(0)
  elseif (cycle == 1) then
    source = Attack.val_restore("sacrifice_source")
    local magic_shield = Attack.act_is_spell(source ,"special_magic_shield")
    
    target = Attack.get_target()

    if ((source ~= nil) and (target ~= nil)) then

		count=math.floor(Attack.val_restore("sacrifice_hp")/Attack.act_get_par(target,"health"))
		--count_1=Attack.act_size(source)			
        local a = Attack.atom_spawn(source, 0, "magic_dagger", Attack.angleto(source))
        local dmgts = Attack.aseq_time(a, "x")
		    Attack.atk_set_damage("astral",damage,damage)
        common_cell_apply_damage(source, dmgts)
								
        local b = Attack.atom_spawn(target, dmgts, "hll_priest_resur_cast")
        local dmgtr = Attack.aseq_time(b, "x")
      	-- � ���� ����� � ������� ������ ������� ���������� ������ �� count
				-- ���� �� ������ ����� ��� ���� - ������� ����� ������, ��� ��� ���� ����� ������
				if magic_shield then count = math.floor(count/2) end
        Attack.act_size(target, Attack.act_size(target)+count)
        --Attack.cell_resurrect(target, rephits, dmgts+dmgtr)
        --Attack.cell_resurrect(target, rephits, dmgts+dmgtr, "x")

    end
  end
  if cycle==1 then
  	--count_2=Attack.act_size(source)
  	--count_2=math.floor(math.min(damage,Attack.act_totalhp(source))/Attack.act_get_par(source,"health"))
  	damage,dead=Attack.act_damage_results(source)
  	if dead==0 then 
  		Attack.log_label("null")
  		--count_2=Attack.act_size(source)
  		--count_2=AU.unitcount(source)
  		
--add_blog_ssacr_1=^blog_td0^[hero_name] ���������� [spell]. ������ ([targeta]) �������� [special] �����. ����� ����������� ����� [name] �� [names].
--add_blog_ssacr_2=^blog_td0^[hero_name] ���������� [spell]. ������ ([targeta]) �������� [special] �����, [target] ��������. ����� ����������� ����� [name] �� [names].
    	Attack.log("add_blog_ssacr_1","hero_name",blog_side_unit(target,4)..Attack.hero_name(),"spell",blog_side_unit(source,3).."<label=spell_sacrifice_name>","targeta",blog_side_unit(source,-1),"name",blog_side_unit(target,-1),"special",damage,"names",count)
  	else
  		Attack.log("add_blog_ssacr_2","hero_name",blog_side_unit(target,4)..Attack.hero_name(),"spell",blog_side_unit(source,3).."<label=spell_sacrifice_name>","targeta",blog_side_unit(source,-1),"name",blog_side_unit(target,-1),"special",damage,"names",count,"target",dead)
  	end 
  end 
  return true
end

-- ***********************************************
-- * Geyser
-- ***********************************************

function lerp(x,y,t) return x+(y-x)*t end

function spell_geyser_attack(level,dmgts,target)

    if dmgts == nil then dmgts = 0 end

	local a = Attack.atom_spawn(Attack.cell_get(),dmgts,"magic_armageddon_spawn")
	local spawn_shift = Attack.aseq_time(a, "x")+dmgts

	local min_dmg,max_dmg,count = pwr_geyser(level)
	local dmg_type = Logic.obj_par("spell_geyser","typedmg")
	--Attack.atk_set_damage(dmg_type,min_dmg,max_dmg)
	local start_up = Attack.aseq_time("magic_armageddon","w")
	local end_up   = Attack.aseq_time("magic_armageddon","x")
	local start_down = Attack.aseq_time("magic_armageddon","y")
	local end_down   = Attack.aseq_time("magic_armageddon","z")

	local begin, end_ = 1,Attack.act_count()-1
	if target ~= nil then begin = target; end_ = target; count = 1 end

	for i=begin, end_ do
		if target ~= nil or (Attack.act_takesdmg(i) and Attack.act_applicable(i) and Attack.act_enemy(i)) then
		    local t = dmgts+Game.Random(100)/100.-1.
			Attack.atom_spawn(i,t,"magic_armageddon")

			local hit_time = t + end_down
			if not Attack.act_pawn(i) then
				local tx,ty,tz = Attack.act_get_center(i) -- ���������� ��������� ���������
				Attack.act_move( t+start_up, t+end_up, i, tx, ty, tz+3) -- ��������� �����
				Attack.act_rotate( t+end_up, t+start_down, i, Game.Dir2Ang(Attack.act_dir(i))+2*math.pi*(2*math.random(0,1)-1) )
				--Attack.act_falldown( t+start_up, t+end_up, i, tz+3)
				local t1 = t + lerp(end_up, start_down, 1/6.)
				local t2 = t + lerp(end_up, start_down, 3/6.)
				local t3 = t + lerp(end_up, start_down, 5/6.)
				Attack.act_move( t+end_up, t1, i, tx, ty, tz+2.7)
				Attack.act_move( t1, t2, i, tx, ty, tz+3.3)
				Attack.act_move( t2, t3, i, tx, ty, tz+2.7)
				Attack.act_move( t3, t+start_down, i, tx, ty, tz+3)
				Attack.act_falldown( t+start_down, hit_time, i, tz) -- ���������� ����, ��� ���
			end

			local cold_fear=Attack.act_get_res(i,"fire")
			if  Attack.act_feature(i,"freeze_immunitet") then
				Attack.atk_set_damage(dmg_type,min_dmg*freeze_im,max_dmg*freeze_im)
      else 
      	Attack.atk_set_damage(dmg_type,min_dmg,max_dmg)
    	end

			common_cell_apply_damage(i, hit_time)
			if cold_fear>=50 then
				effect_freeze_attack(i,hit_time+1,3)
			end

			count = count - 1
			if count == 0 then break end
		end
	end

	-- ������� ���������� ������
	if count > 0 then
	    local free_cells = {}
		for i=0,Attack.cell_count()-1 do
		    local c = Attack.cell_get(i)
		    if Attack.cell_is_pass(c) and Attack.cell_is_empty(c) then
				table.insert(free_cells, c)
		    end
		end

		for i=1,count do
		    if table.getn(free_cells) == 0 then break end
		    local n = math.random(1,table.getn(free_cells))
			Attack.atom_spawn(table.remove(free_cells, n),Game.Random(100)/100.,"magic_armageddon")
		end
	end

	return true

end

-- ***********************************************
-- * Ghost Sword
-- ***********************************************

function spell_ghost_sword_attack(lvl,dmgts)

		if dmgts==nil then dmgts=0 end
    local min_dmg,max_dmg,power = pwr_ghost_sword(lvl)
    local dmg_type = Logic.obj_par("spell_ghost_sword","typedmg")

        local target = Attack.get_target()
        if target~=nil then
        Attack.atk_set_damage(dmg_type,min_dmg,max_dmg)

        local target = Attack.get_target()
     -- ��������� ��������
      effect_corrosion_attack(target,0, power, 2)

        local a = Attack.atom_spawn(target, dmgts, "magic_blade", Attack.angleto(target)+2.5)
        local dmgts1 = Attack.aseq_time(a, "x")
    		common_cell_apply_damage(target, dmgts1+dmgts)
        Attack.act_del_spell(target,"effect_corrosion")

        end

  -- ��������� ���� �� 3 ����. �� �������� ���� 1-5*power � ������ 5+5*power ������
  --hll_post_archmage_lighting
  return true
end


-- ***********************************************
-- * Phoenix call
-- ***********************************************

function ang_to_nearest_enemy(target, func)
  local nearest_dist, nearest_enemy = 10e10, nil
  for i=1,Attack.act_count()-1 do
  	if func(i) then
  		local d = Attack.cell_dist(target,i)
  		if d < nearest_dist then nearest_dist = d; nearest_enemy = i; end
  	end
  end
  if nearest_enemy ~= nil then return Attack.angleto(target, nearest_enemy) end
end

function spell_phoenix_call()

  local level=Obj.spell_level()
  if level==0 then level=1 end
  
  local unit_name = text_dec(Logic.obj_par("spell_phoenix","unit_name"),level)
  
  for i=1,Attack.act_count()-1 do
    if string.sub(actor_name(i),1,7) == "phoenix" --[[and Attack.val_restore(i,"lifes")>=0]] and Attack.act_belligerent(i) == Attack.act_belligerent() then
--[[        local e = 0
	    local fade_time = 1
	    Attack.act_fadeout(i, e, e + fade_time)
    	Attack.act_remove(i, e + fade_time + .01)]]
	    Attack.act_kill(i,true,false)
    end
  end

  local target = Attack.get_target()

  --������� ���������� �����
  local ang_to_enemy = ang_to_nearest_enemy(target, Attack.act_enemy)
  if ang_to_enemy ~= nil then
  	Attack.act_spawn(target,0,unit_name,ang_to_enemy)
  else
  	Attack.act_spawn(target,0,unit_name)
  end
  if Attack.act_belligerent() == 1 then -- ������� ������ ����� ���������
  	Game.GVSNumInc("units_kupleno", unit_name, 1)
  end
  Attack.act_aseq(target,"appear");
  Attack.val_store(target,"lifes",1);
  Attack.resort( target )
	
	Attack.log_label("null")
	Attack.log("add_blog_phoen","hero_name",blog_side_unit(target,4)..Attack.hero_name(),"targeta",blog_side_unit(target,0))
--  Attack.atom_spawn(target, 0, "magic_greenfly")

  return true

end

function phoenix_ondamage(wnm,ts,dead)
	if dead then
    	local lifes = Attack.val_restore(0,"lifes");
    	if lifes == 0 then
		    local e = wnm--ts+Attack.aseq_time(0,"death","")
		    local fade_time = 1
		    Attack.act_fadeout(0, e, e + fade_time)
	    	Attack.act_remove(0, e + fade_time + .01)
	    	--Attack.act_kill(0, true, false)
    	end
--    	Attack.val_store(0,"lifes",lifes-1);
	end
	return true
end

function phoenix_onmove(tend)

	if Attack.act_hp(0) <= 0 then
		local has_allies = false
		for i=1,Attack.act_count()-1 do
		    if Attack.act_belligerent(i, nil) == 1 and Attack.act_hp(i) > 0 then
		        has_allies = true
		        break
		    end
		end

		if not has_allies then
	        Attack.act_kill(0,true,false)
		end
	end

	return true

end



-- ***********************************************
-- * Phantom
-- ***********************************************

function spell_phantom_attack()

  local cycle = Attack.get_cycle()
	local target
	local count=0
	
  if (cycle == 0) then

    local target = Attack.get_target()
    Attack.val_store("source_unit", target)
    Attack.next(0)

  elseif (cycle == 1) then

  local source = Attack.val_restore("source_unit")

  local level=Obj.spell_level()
  if level==0 then level=1 end

  target = Attack.get_target()

  --������� ���������� �����
  local nearest_dist, nearest_enemy = 10e10, nil
  for i=1,Attack.act_count()-1 do
  	if Attack.act_enemy(i) then
  		local d = Attack.cell_dist(target,i)
  		if d < nearest_dist then nearest_dist = d; nearest_enemy = i; end
  	end
  end

  local angle = 0
  if nearest_enemy ~= nil then angle = Attack.angleto(target, nearest_enemy) end

  local spell = "spell_phantom"
  count = math.floor( Attack.act_totalhp(source) * (pwr_phantom(level)/100) / (Attack.act_get_par(source,"health")) )
  local moves = text_dec(Logic.obj_par(spell,"duration"),level)

  Attack.act_spawn(target, 0, actor_name(source), angle, count)
  Attack.act_enable_attack(target, "split", false); Attack.val_store(target, "clone", 1)
  Attack.act_enable_attack(target, "transform", false)
  --Attack.act_skipmove(target,-1)
  Attack.resort( target ) -- ����� ������ ����� �����

  --Attack.act_aseq(target,"appear");
  local effect1 = Attack.atom_spawn(source, 0, "magic_phantom_source")
  local y = Attack.aseq_time(effect1, "x")
  local effect2 = Attack.atom_spawn(target, y, "magic_phantom")
  local x = Attack.aseq_time(effect2, "x")

	Attack.act_nodraw(target, true)
	Attack.act_nodraw(target, false, x+y)
	
	Attack.act_transparency(target, .7)
	
    Attack.act_apply_spell_begin( target, spell, moves, false )
    Attack.act_apply_spell_end()

	--add_blog_sfantom_2	
  end
  	--		Attack.log_label("add_blog_sfantom_2") -- ��������
	--		Attack.log_special(count) 
	Attack.log_label("null")
		if cycle==1 then 
			Attack.log("add_blog_sfantom_2","hero_name",blog_side_unit(target,4)..Attack.hero_name(),"spell",blog_side_unit(target,3).."<label=spell_phantom_name>","target",blog_side_unit(target,0),"special",count)

		end 
  return true

end

function spell_phantom_onremove(caa)

  if Attack.act_hp(caa) > 0 then Attack.act_kill(caa,false,false) end -- ������� ������� ������ ����� �� ���, � �� ���������� ������� ������
  local t = Attack.aseq_time(caa)
  local fade_time = 2
  Attack.act_fadeout(caa, t, t + fade_time)
  
		Attack.log(0.001,"add_blog_nophant","name",blog_side_unit(caa))
  
  Attack.act_remove(caa, t + fade_time + .01)

  return true

end

-- ***********************************************
-- * Kamikaze
-- ***********************************************

function spell_kamikaze_attack()

  local target = Attack.get_target()

  if (target ~= nil) then

    local level=Obj.spell_level()
    if level==0 then level=1 end
		local min_dmg,max_dmg,dur=pwr_kamikaze()
    --Attack.act_del_spell( target, "spell_kamikaze" )

    Attack.act_apply_spell_begin( target, "spell_kamikaze", dur, false )
		Attack.val_store( target, "min", min_dmg )
		Attack.val_store( target, "max", max_dmg )	
    
		Attack.act_attach_atom( target, "kamikazebomb", 0 )
    Attack.act_apply_spell_end()

        --Attack.atom_spawn(target, 0, "magic_lioncup", Attack.angleto(target))
  end

  return true

end

function spell_bomb_explode_attack()
	local tar = Attack.get_target()
		a = Attack.atom_spawn(tar, 0, "effect_bomb" )
		local dmgts = Attack.aseq_time(a, "x")
		
		local dmg_type = Logic.obj_par("spell_kamikaze","typedmg")
		--local level = tonumber(Attack.val_restore( "level" ))
		
    Attack.atk_set_damage(dmg_type,min_dmg/math.max(1,Attack.act_size(0)),max_dmg/math.max(1,Attack.act_size(0)))
    --Attack.atk_set_damage(dmg_type,min_dmg,max_dmg)
        
    if Attack.act_hp(tar)>0 then 
    	common_cell_apply_damage(tar, dmgts, true) 
			local count=Attack.act_size(tar)
			local damage,dead=Attack.act_damage_results(tar)

			if dead>0 then 
				local td=""
			if count==dead then td="<label=troop_defeated>" end 
    		Attack.log(0.001,"add_blog_skam_2","damage",damage,"dead",dead,"target",blog_side_unit(tar,-1),"troopdef",td,"name",blog_side_unit(tar,1))
  		else
  			Attack.log(0.001,"add_blog_skam_1","damage",damage,"target",blog_side_unit(tar,-1),"name",blog_side_unit(tar,1))
  		end 
		else
			Attack.log(0.001,"add_blog_skam_0","name",unit_name)
		end    
    
	  for c=0,Attack.cell_count()-1 do

    local i = Attack.cell_get(c)
    if (Attack.act_enemy(i) or Attack.act_ally(i) or Attack.act_pawn(i)) and Attack.cell_dist(tar,i)==1 then 
      if Attack.act_applicable(i) and Attack.act_takesdmg(i) then      -- can receive this attack
        common_cell_apply_damage(i, dmgts, true)

     		local count=Attack.act_size(i)    
				local damage,dead=Attack.act_damage_results(i)
				if damage ~= nil then
--add_blog_skam_2=^blog_td0^[name]: ����������� ����� � ������� [damage] �����. [target]: [dead] ��������. [troopdef]
				if dead>0 then 
					local td=""
				if count==dead then td="<label=troop_defeated>" end 
    			Attack.log(0.001,"add_blog_skam_02","damage",damage,"dead",dead,"troopdef",td,"name",blog_side_unit(i,0))
  			else
  				Attack.log(0.001,"add_blog_skam_01","damage",damage,"name",blog_side_unit(i,0))
  			end 
  			end
      end
    end
    
    end
	return true

end

function spell_kamikaze_onremove(target,duration_end,on_dmg)

	if duration_end or on_dmg or Attack.act_hp(target) <= 0 then --�������� �����

		--local cell = Attack.get_cell(0)
		--local color=blog_side_unit(cell,"arrow")..blog_side_unit(cell,"color")
		min_dmg=tonumber(Attack.val_restore(target, "min")) --/Attack.act_size(0)
		max_dmg=tonumber(Attack.val_restore(target, "max")) --/Attack.act_size(0)
		unit_name=blog_side_unit(target,1)
		Game.ExecSpell("spell_bomb_explode",Attack.cell_id(Attack.get_cell(target)),nil)
  end

	Attack.act_deattach_atom( target, "kamikazebomb", 0 )
	return true

end


-- ***********************************************
-- * Ram
-- ***********************************************

function spell_ram_attack(lvl,dmgts)

	if dmgts==nil then dmgts=0 end
  local target = Attack.get_target()

  if (target ~= nil) then

	local level = lvl
  if level==nil then 
  	level=Obj.spell_level()
  else
  	level = tonumber(level)
  end 
  if level==0 then level=1 end

	local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_ram","duration"),level))
		if Logic.obj_par("spell_ram","int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

		Attack.act_del_spell(target)
    Attack.act_apply_spell_begin( target, "spell_ram", duration, false)--true )
        Attack.act_apply_par_spell("autofight", 1, 0, 0, duration, false)--true)
        Attack.act_apply_par_spell("hitback", 0, -100, 0, duration, false)--true)
        --Attack.act_posthitslave(target, "ram_posthit", duration)
    Attack.act_apply_spell_end()

        --Attack.atom_spawn(target, 0, "magic_lioncup", Attack.angleto(target))
    Attack.val_store(target,"initial_tag",actor_name(target))
    local l = 1
	Attack.act_fadeout(target, 0, l)
  Attack.act_change_tag(target,"ram",l)
	Attack.atom_spawn(target, 0, "magic_ram")
  
    Attack.atom_spawn(target, l - Attack.aseq_time("magic_ram", "x"), "magic_ram", Attack.angleto(target))
	Attack.act_fadeout(target, l, l*2, 1)
	Attack.log_label("add_blog_sram")

  end

  return true

end

function actor_name(act)
	local name = Attack.act_name(act)
	if name == "ram" then return Attack.val_restore(act,"initial_tag") end
	return name
end

function spell_ram_onremove(target,duration_end,on_dmg)

	local itag = Attack.val_restore(target,"initial_tag")

	if --[[Attack.act_name(target) == "ram" and]] (not on_dmg and Attack.act_hp(target) > 0) or duration_end then -- �������� ����. ������� ������ ���� �-��: ����� ��������, ������ �� �����, ������ � ������ �����
	
	    local t = Attack.aseq_time(target)
	    e = t + 1
		Attack.act_fadeout(target, t, e)
		t = e
    	Attack.act_change_tag(target, itag, t)
    	
     
    	Attack.atom_spawn(target, 0, "magic_ram_end")
	    Attack.atom_spawn(target, t - Attack.aseq_time("magic_ram", "x"), "magic_ram_end", Attack.angleto(target))

    	e = t + 1
		Attack.act_fadeout(target, t, e, 1)
--[[    	if  then
			Attack.act_animate(target,"death",Attack.aseq_time( itag, "teleout", ))
		end]]
		
		--Attack.log_label("null")
		
    end

	if Attack.act_hp(target) > 0 then
		
      if Attack.act_size(target)>1 then 
       	Attack.log(0.001,"add_blog_noram_2","name",blog_side_unit(target,1),"target","<label=cpsn_"..itag..">")
      else 
     		Attack.log(0.001,"add_blog_noram_1","name",blog_side_unit(target,1),"target","<label=cpn_"..itag..">")
      end

    end
    
	return true

end

function ram_ondmg(wnm,ts,dead)

	if dead then
		local s, l = ts+2, 1

		local itag = Attack.val_restore(0,"initial_tag")
		Attack.atk_min_time(s+l--[[+Attack.aseq_time(itag, "death", "")]]+.01)

		Attack.act_fadeout(0, s, s+l)
		Attack.atom_spawn(0, s, "magic_ram_end")
		Attack.act_change_tag(0, itag, s+l)
	    --Attack.atom_spawn(0, s+l - Attack.aseq_time("magic_ram", "x"), "magic_ram_end", Attack.angleto(0))
		Attack.act_fadeout(0, s+l, s+l*2, 1)
	end
	return true

end


-- ***********************************************
-- * Evil Book
-- ***********************************************

function spell_evilbook_attack()

  local target = Attack.get_target()

  if (target ~= nil) then

    local level=Obj.spell_level()
    if level==0 then level=1 end

--    Attack.act_apply_spell_begin( target, "spell_evilbook", duration, false )
--    Attack.act_apply_spell_end()

      --Attack.atom_spawn(target, 0, "magic_lioncup", Attack.angleto(target))
--	local caa = Attack.get_caa(target) -- ����� ����, ��� ����� �� ���� ������, ����� ��������� ���
	Attack.act_spawn(target,0,"evilbook"..level)
	--Attack.act_enable_attack(target,"attack_spell")
	--Attack.act_charge(target,1-level,"attack_spell")
    Attack.act_animate(target,"appear");
    Attack.resort( target )
--    Attack.act_nodraw(caa, true, Attack.aseq_time(target,"x")); -- �������� ����, ����� ����� ��� ����������
--    Attack.val_store(target,"unit_inside",caa)
--    Attack.act_skipmove(caa,-100)
	
  end
  Attack.log_label("null")
	Attack.log("add_blog_phoen","hero_name",blog_side_unit(target,4)..Attack.hero_name(),"targeta",blog_side_unit(target,0))
	
  return true

end

function spell_evilbook_onremove(target,duration_end)

	if Attack.act_hp(target) > 0 then
	    local t = Attack.aseq_time(target)
	    e = t + 1
		Attack.act_fadeout(target, t, e)
		t = e
		local itag = Attack.val_restore(target,"initial_tag")
    	Attack.act_change_tag(target, itag, t)
    	e = t + 1
		Attack.act_fadeout(target, t, e, 1)
--[[    	if  then
			Attack.act_animate(target,"death",Attack.aseq_time( itag, "teleout", ))
		end]]
    end
	return true

end

function evilbook_ondmg(wnm,ts,dead)

--[[	local s, l = wnm, 1
	Attack.act_fadeout(target, s, s+l)
    Attack.act_change_tag(target,Attack.val_restore(target,"initial_tag"),s+l)
	Attack.act_fadeout(target, s+l, s+l*2, 1)]]
	return true

end

-- ***********************************************
-- * Trap
-- ***********************************************

function spell_trap_attack()

  local level=Obj.spell_level()
  if level==0 then level=1 end
	local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_trap","duration"),level))-- +1
		if Logic.obj_par(Obj.name(),"int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 


	local target = Attack.get_target()
	local trap = Attack.atom_spawn(target, 0, "trap")
	Attack.act_aseq(trap,"appear")
	
	Attack.cell_set_trap(target, trap)
	
	local min_dmg,max_dmg=pwr_trap(level)
	
	Attack.val_store( trap, "min", min_dmg )
	Attack.val_store( trap, "max", max_dmg )	
	Attack.val_store( trap, "level", level )
	Attack.val_store( trap, "time_last", duration)
	Attack.log_label("add_blog_trap")
	return true

end

function special_trap()
	
	local cell = Attack.get_cell(0)
	local color=blog_side_unit(cell,"arrow")..blog_side_unit(cell,"color")
	
	--Attack.atom_spawn(0, 0, "magic_dispel" )
	local min_dmg=tonumber(Attack.val_restore( "min"))
	local max_dmg=tonumber(Attack.val_restore( "max"))
	local dmg_type = Logic.obj_par("spell_trap","typedmg")
	local level = tonumber(Attack.val_restore( "level" ))
		
    Attack.atk_set_damage(dmg_type,min_dmg,max_dmg)
    local start = 0
    local dmgts = start + Attack.aseq_time("trap","death","x")
    common_cell_apply_damage(cell, dmgts)
    --Attack.act_kill()
	Attack.cell_set_trap(cell, nil, start)
    if tonumber(level)==3 and not Attack.act_feature(cell,"poison_immunitet") then
    	effect_poison_attack(cell,dmgts+1,3)
    end 

	local count=Attack.act_size(cell)    
	local damage,dead=Attack.act_damage_results(cell)    
--add_blog_trap_2=^blog_td0^[spell] ����������� � ������� [damage] �����. [target]: [dead] ��������. [troopdef]	
	if dead>0 then 
		local td=""
		if count==dead then td="<label=troop_defeated>" end 
    Attack.log("add_blog_trap_2","spell",color.."<label=spell_trap_name>","damage",damage,"dead",dead,"target",blog_side_unit(cell,-1),"troopdef",td)
  else
  	Attack.log("add_blog_trap_1","spell",color.."<label=spell_trap_name>","damage",damage,"target",blog_side_unit(cell,-1))
  end 
	return true

end

function special_trap_attack()

	local caa = Attack.get_caa(Attack.get_cell(0))
	if caa ~= nil then
		local mt = Attack.act_mt(caa)
		if mt == 0 or mt == 2 then special_trap(); return true end
	end

	return false

end

function trap_time()

	--special_trap_attack()
	totem_time()

	return true

end

function select_attack0()
	return Attack.change_attack(0)
end

function select_attack1()
	return Attack.change_attack(1)
end

-- ***********************************************
-- * Shroud
-- ***********************************************

function spell_shroud_attack()

  local level=Obj.spell_level()
  if level==0 then level=1 end
  local duration = tonumber("0" .. text_dec(Logic.obj_par("spell_shroud","duration"),level))
		if Logic.obj_par(Obj.name(),"int")=="1" then
			duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		end 

  local penalty  = pwr_shroud(level)

  local target, atom = Attack.get_target()
  local cid = Attack.cell_id(Attack.get_cell(target))
  for i=1,Attack.act_count()-1 do
  	if Attack.act_name(i) == "shroud" and Attack.cell_id(Attack.get_cell(i)) == cid then
  	    if Attack.act_size(i) <= 0 then break end -- ������ ������
  		atom = i
  		break
  	end
  end

  if atom == nil then
	atom = Attack.atom_spawn(target, 0, "shroud")
	Attack.act_aseq(atom,"appear")
  end
  
  Attack.val_store(atom, "belligerent", Attack.act_belligerent())
  Attack.val_store(atom, "time_last",  duration)
  Attack.val_store(atom, "penalty", penalty)
  Attack.val_store(atom, "power", penalty)

  return true

end

--[[function shroud_mods()

	local mod = shroud_takeoff()
	local penalty = Attack.val_restore("penalty")

	for i=1, Attack.act_count()-1 do
		for r=0,Logic.resistance()-1 do
    		Attack.act_attach_dmg_modificator_to_throw_attacks(i, Logic.resistance(r), mod, 0, -penalty)
    	end
    end

	return true

end

function shroud_takeoff() -- ������� �������� ���� ������ �� ���� ������

	local mod = Attack.act_name(0)..Attack.act_uid(0)
    for i=1, Attack.act_count()-1 do
    	Attack.act_del_modificator(i, mod, true)
    end
    return mod

end

function shroud_turn()

  local ttl = Attack.val_restore( 0, "moves" )
  ttl = ttl - 1
  if ttl == 0 then
    shroud_takeoff()
    Attack.log("add_blog_nopelena","name",blog_side_unit(0))
  	Attack.act_remove(0, .5)
  else
    Attack.val_store( 0, "moves", ttl )
  end

  return true

end]]

-- ***********************************************
-- * Light power
-- ***********************************************

function spell_light_power_attack()

	local level=Obj.spell_level()
	if level==0 then level=1 end

    local cure_hp = text_dec(Logic.obj_par("spell_light_power","cure_hp"),level)
    local damage  = text_dec(Logic.obj_par("spell_light_power","damage"),level)

	local center = Attack.get_target()
    local a = Attack.atom_spawn(center, 0, "magic_fireball") -- ����� ���������� ������
	local dmgts = Attack.aseq_time(a, "x")
	Attack.atk_set_damage("magic", damage*2.)

	for i=0,6 do
	    local t = Attack.get_target(i)
	    if t ~= nil and (Attack.act_enemy(t) or Attack.act_ally(t)) then
		    local race = Attack.act_race(t)
		    if race ~= "undead" and race ~= "demon" and Attack.act_need_cure(t) then --�������
				Attack.atom_spawn(t, 0, "effect_total_cure")
				Attack.act_cure(t, cure_hp)
		    end
		    if race == "undead" and i == 0 then --������ ������ � ����������� ������
				common_cell_apply_damage(t, dmgts)
		    end
		end
	end

	Attack.atk_set_damage("magic", damage) -- ���� � ������� ������� ������

	local busy_cells = {}

	for d=0,5 do
	    local t = Attack.cell_adjacent(center, d)
	    if t ~= nil and (Attack.act_enemy(t) or Attack.act_ally(t)) then
		    local race = Attack.act_race(t)
	    	if race == "undead" then
				common_cell_apply_damage(t, dmgts) -- ������
				-- �����!
	    	    for k,i in {6,5,7} do -- ����������, ����� �� ������ ����-���� ������
					local c = Attack.cell_adjacent(t, math.mod(d + i, 6))
					if c ~= nil and Attack.cell_present(c) and Attack.cell_is_empty(c) and not busy_cells[Attack.cell_id(c)] then
					    Attack.aseq_rotate(t, c)
						Attack.aseq_start(t, "start", "move")
						Attack.act_aseq(t, "stop")
					    busy_cells[Attack.cell_id(c)] = true
					    break
					end
	    	    end
	    	end
		end
	end

	return true

end

-- ***********************************************
-- * Holy Rain
-- ***********************************************

function spell_holy_rain_attack()

  local target = Attack.get_target()
  local center = target
  local dmg,dmgts=0,0

  if (target ~= nil) then

    local a = Attack.atom_spawn(target, 0, "effect_lightpower")
    dmgt=Attack.aseq_time(a, "x")

    local min_dmg,max_dmg = pwr_holy_rain()
    local dmg_type = Logic.obj_par("spell_holy_rain","typedmg")

    local n = Attack.get_targets_count()
    for i=0,n-1 do

    local tgt = Attack.get_target(i)
    if tgt~=nil then
     if Attack.act_enemy(tgt) or Attack.act_ally(tgt) then
      if (Attack.act_race(tgt,"undead")) then
        Attack.atk_set_damage(dmg_type,min_dmg,max_dmg)
        local b = Attack.atom_spawn(tgt, dmgt, "effect_hard_bless")
        dmgs=Attack.aseq_time(b, "x")
        common_cell_apply_damage(tgt,dmgt+dmgs)
      elseif (Attack.act_need_cure(tgt)) and not (Attack.act_race(tgt,"demon")) and not Attack.act_temporary(tgt) and not (Attack.act_feature(tgt,"golem,magic_immunitet")) and Attack.act_ally(tgt) then
         local rephits = math.floor(Game.Random(min_dmg,max_dmg))
         local max_hp = Attack.act_get_par(tgt,"health")
         local cure=Attack.act_hp(tgt)
         if rephits>max_hp-cure then rephits=max_hp-cure end 
         Attack.act_cure(tgt, rephits, dmgt)
         Attack.atom_spawn(tgt, dmgt, "effect_total_cure")
         --add_blog_sheal_2=^blog_td0^[hero_name] ���������� [spell]. [target] ��������������� [g][special][/c] ��������.
         if Attack.act_size(tgt)>1 then 
         		Attack.log(0.001,"add_blog_cure_2","target",blog_side_unit(tgt,0),"special",rephits)
         else 
         		Attack.log(0.001,"add_blog_cure_1","target",blog_side_unit(tgt,0),"special",rephits)
         end 
      end
     end
    end
    end
  end

	local busy_cells = {}

	for d=0,5 do
	    local t = Attack.cell_adjacent(center, d)
	    if t ~= nil and (Attack.act_enemy(t) or Attack.act_ally(t)) then
		    local race = Attack.act_race(t)
	    	if race == "undead" and Attack.act_get_par(t, "dismove") == 0 then
	--			common_cell_apply_damage(t, dmgts) -- ������
				-- �����!
	    	    for k,i in {6,5,7} do -- ����������, ����� �� ������ ����-���� ������
					local c = Attack.cell_adjacent(t, math.mod(d + i, 6))
					if c ~= nil and Attack.cell_present(c) and Attack.cell_is_pass(c) and Attack.cell_is_empty(c) and not busy_cells[Attack.cell_id(c)] then
					    Attack.aseq_rotate(t, c)
						Attack.aseq_start(t, "start", "move")
						Attack.act_aseq(t, "stop", false, true)
						
						Attack.act_damage_addlog(t,"add_blog_run_")
						
					    busy_cells[Attack.cell_id(c)] = true
					    break
					end
	    	    end
	    	end
		end
	end

  return true


end

-- ***********************************************
-- * Invisibility
-- ***********************************************

function spell_invisibility()

	local target = Attack.get_target()
	local level=Obj.spell_level()
	if level==0 then level=1 end

	for i=1,Attack.act_count()-1 do -- ������� �� ���� ����� �����������
	    if Attack.act_ally(i) then
			Attack.act_del_spell( i, "spell_invisibility" )
		end
	end

    local duration = text_dec(Logic.obj_par("spell_invisibility","duration"),level)

   	Attack.act_apply_spell_begin( target, "spell_invisibility", duration, false )
   	Attack.act_apply_spell_end()

   	Attack.act_transparency(target, .5)
	Attack.atom_spawn(target, 0, "magic_invisi")
   	Attack.act_onattack(target, "spell_invisibility_onatack")
		
    if Attack.act_size(target)>1 then 
   		Attack.log_label("add_blog_sinvis_2")
    else 
   		Attack.log_label("add_blog_sinvis_1")
    end 

	return true

end

function spell_invisibility_onremove(target,duration_end)

    if Attack.act_hp(target) <= 0 then return true end

   	Attack.act_transparency(target, 1.)
   	Attack.act_fadeout(target, 0., 1., 1., .5)
   	Attack.act_onattack(target, "")
	
    if Attack.act_size(target)>1 then 
   		Attack.log("add_blog_noinvis_2","name",blog_side_unit(target,1))
    else 
   		Attack.log("add_blog_noinvis_1","name",blog_side_unit(target,1))
    end 
return true
end

function spell_invisibility_onatack()
	Attack.act_del_spell( 0, "spell_invisibility" )
	return true
end

-- ***********************************************
-- * Demonologist
-- ***********************************************

function spell_demonologist()

	local target = Attack.get_target()
	local level=Obj.spell_level()
	if level==0 then level=1 end

    local lead,time = pwr_demonologist()
    local unit = Logic.obj_par("spell_demonologist","units"..level)
    local par_count=text_par_count(unit)
    unit = text_dec(unit, Game.Random(1,par_count))

	local a = Attack.atom_spawn(target, 0, "demon_portal")
	Attack.act_aseq(a,"appear")

	Attack.val_store( a, "belligerent", Attack.act_belligerent() )
	Attack.val_store( a, "time", time )
	Attack.val_store( a, "unit", unit )
	Attack.val_store( a, "count", math.ceil(lead/Attack.atom_getpar(unit,"leadership")) )

	Attack.cell_passability(target, 2) -- ��������� ������ �� ���� ������
	Attack.log_label("add_blog_demongate")
	
	return true

end

function demon_portal()

	local time = tonumber(Attack.val_restore("time"))-1
	if time <= 0 then
		Attack.act_kill()
		local x = Attack.aseq_time(0, "x")
		local y = Attack.aseq_time(0, "y")
		local z = Attack.aseq_time(0, "z")
		local count=tonumber(Attack.val_restore("count"))
		Attack.act_spawn(0, Attack.val_restore("belligerent"), Attack.val_restore("unit"), 0, count)
		local target = Attack.get_caa(Attack.get_cell(0))
		Attack.act_nodraw(target, true)
		Attack.act_nodraw(target, false, x)
		local tx,ty,tz = Attack.act_get_center(target)
		Attack.act_move( 0, 0.1, target, tx,ty,tz-1.2*(1+(Attack.act_level(target)-2)*0.3)) -- ��������� �����
		Attack.act_move( x, y, target, tx,ty,tz ) -- ��������� �����
		Attack.act_fadeout(target, x, y, 0.5, 0)
		Attack.act_fadeout(target, y, z, 1, 0.5)		
		
		local bel = Attack.val_restore("belligerent")
		Attack.act_belligerent(target, bel)
		--Attack.act_ap(target,Attack.act_get_par(target,"speed"))
		Attack.resort( target )
		Attack.cell_passability(0, 0)

		Attack.log("add_blog_demon_summon_"..loadstring("if "..count..">1 then return 2 else return 1 end")(),
			"name",blog_side_unit(target,2,bel)..blog_side_unit(target,3,bel),"target",blog_side_unit(target,0,bel),"special",count)
	else
		Attack.val_store("time", time)
	end

	return true

end

-- ***********************************************
-- * Tactics
-- ***********************************************

function spell_army_disposition()

	local target = Attack.get_target()
	local cycle = Attack.get_cycle()
	if cycle == 0 then
		Attack.val_store("source", Attack.get_target())
		Attack.next(0)
	else
		local source = Attack.val_restore("source")
		if Attack.get_caa(source) ~= nil then

		    local target = Attack.get_target()

			local effect1 = Attack.atom_spawn(source, 0, "effect_unit_out", 0)
			local effect2 = Attack.atom_spawn(target, 0, "effect_unit_in", 0)

			Attack.act_teleport(source, target, 0)

		end
	end

	return true

end

-- ***********************************************
-- * Armageddon
-- ***********************************************

function spell_armageddon_attack()

  local ccnt, cx, cy = Attack.cell_count(), 0, 0
  for i=0, ccnt-1 do
    local c = Attack.cell_get(i)
    local x,y = Attack.act_get_center(c)
    cx = cx + x
    cy = cy + y
  end

  local center = Attack.find_nearest_cell(cx/ccnt, cy/ccnt)

  if (center ~= nil and Attack.cell_present(center)) then

  local level=Obj.spell_level()
	if level==0 then level=1 end
	local min_dmg,max_dmg, burn = pwr_armageddon()
  local prc=tonumber(text_dec(Logic.obj_par("spell_armageddon","prc"),level))
  
  local dmg_type = Logic.obj_par("spell_armageddon","typedmg")

    local y = Attack.aseq_time("magic_fireaxle", "y")
    local z = Attack.aseq_time("magic_fireaxle", "z")

	for i=0, ccnt-1 do

		local c = Attack.cell_get(i)

		local d = Attack.cell_dist(center, c)
		local atom, t
		if d == 0 then atom = "magic_fireaxle"; t = 0; else atom = "magic_fireground_"..math.random(1,3); t = z + y*(d-1); end
		local a = Attack.atom_spawn(c, t, atom, Game.Dir2Ang(math.random(0,5)))
		local dmgts = Attack.aseq_time(a, "x") + t

			if Attack.act_ally(c) then 
				-- Attack.atk_set_damage(dmg_type,min_dmg/2,max_dmg/2)
				Attack.atk_set_damage(dmg_type,min_dmg/100*prc,max_dmg/100*prc)
			else
				Attack.atk_set_damage(dmg_type,min_dmg,max_dmg)
			end 

		if common_cell_apply_damage(c, dmgts) then
			if Game.Random(100) < burn then
			  effect_burn_attack(c,dmgts+1,3)
			end
		end

    end

  end

  return true

end
