
// ������

// �������������� ���������� ����� 5++ ����� ������������� ������ ��������� �������
split_difficulty=35
split_probability=35
split_percent_min=25
split_percent_max=75

// ����������� ��� �������� ��������� ����� � ���� ������
exp_from_lead_k=0.018 //0.016

// ������������ ��� �������/�������
default_buy_coeff=0
default_sell_coeff=0.25
default_sell_scroll_coeff=0.2

// ����� �������� ������ ��� �������, ����� ���� ��������� �����
infinity_units=9999

// ���� �����/ ���� ���� ���� ���� ��� ������� �� �����
rel_strength=army_veryweak=.3,army_weak=.6,army_weaker=.8,army_equal=1.0,army_stronger=1.15,army_strong=1.5,army_verystrong=2.0,army_fatal=3.0,army_invincible
// ����� ������ ���� ������� ���������� �����, ����� ��� ����� ��������� � 2 ���� �������. ��� 20 ������ � 2 ����, ��� 30 � 2.5 ����.
enemy_hero_level_leadk=20
// ������� ����� � ������ �� ��� �� ������ ����. ����� Exp=1.5+k*LV
enemy_hero_money_exp_bonus_k=0.03

//����-� �������� ����� � ���� ������ ��� ���������
i_wanna_compensation_for_my_army_defeated_k=0.9
compensation_pay_limit=5
compensation_after_limit_reached=0.2

// ����������� ���������� ����� ��� ��������� ����� 100%
diffmap_to_kmoneyarmy=3.5 //3.5

// ����, �������������, ����������� ������
races=human,elf,orc,dwarf,undead,demon,neutral
resistances=physical,poison,magic,fire,astral
features=armor,shot,mage,undead,eyeless,demon,dragon,plant,mind_immunitet,fire_immunitet,magic_immunitet,poison_immunitet,freeze_immunitet,holy,bone,golem,humanoid,beast,beauty,nonecro,barrier,archer,boss,pawn,orb

//����-� �������� ����, ����� ������ = 0
mana_gain_k_when_no_rage=3

// ��������� ��������� ����
difficulty_k {

      exp=0.5|1.0|1.30|1.7   // ����. ���������� ������� ����� �����
    alead=0.5|1.0|1.30|1.7   // ����. ��������� �����
    spexp=0.5|1.0|1.15|1.3   // ����. ���������� ������� ����� ����� ������
    money=4.0|1.0|0.77|0.6   // ����. ���������� �����
     rage=2.0|1.0|0.85|0.7   // ����. ������� ������ � ���
 manarage=2.0|1.0|0.80|0.5   // ����. ������� ���� �� �����, � ��� ������ �������� �������


  bossatk=1.0|1.0|1.4|2.0   // ����. ����� �����
   bosshp=1.0|1.0|1.4|2.0   // ����. ����� �����

deadmoney=1.0|1.0|0.5|0.0   // ����. ���������� ����� �� ������ � ���

}

// DefaultParam Mini, Small, Average, Big, Huge - ������������ ��� ��������� ���������
parpresets {

  leadership {
    0=04:2:10
    1=12:2:18 // ����� ���� * ���������� 
    2=20:2:26 // ������� ���� * ���������� 
    3=28:2:34 // ������� ���� * ����������
    4=36:2:42
    //0=10:5:15
    //1=20:5:25 // ����� ���� * ���������� 
    //2=30:5:40 // ������� ���� * ���������� 
    //3=45:5:50 // ������� ���� * ����������
    //4=55:5:70
  }
  money {
    0=0080:10:0150
    1=0200:20:0300
    2=0400:20:0600
    3=0800:20:1000
    4=1200:20:1600
    //0=70:10:150
    //1=150:20:300
    //2=300:20:500
    //3=500:20:800
    //4=800:20:1200
  }

  experience {
    0=15:5:30
    1=30:5:50
    2=50:5:70
    3=70:5:90
    4=90:5:110
    //0=20:5:40
    //1=40:5:60
    //2=60:5:80
    //3=80:5:100
    //4=100:5:120
  }
}

difficulty {

  //################################# maps
  // MapParam

  10 { // Difficulty 0..10%

    k_money=1
    k_lead=1
    k_exp=1
    k_mana=1
    k_rage=1
    profit=80,20,0,0,0  //50,25,12,10,3
    ecmoney=1
  }

  20 { // Difficulty 11..20%

    k_money=2
    k_lead=1 //1.5
    k_exp=2 //1.5
    k_mana=1.5
    k_rage=1.5
    profit=70,30,0,0,0
    ecmoney=1
  }

 30 {

    k_money=4 //5
    k_lead=2
    k_exp=3 //2
    k_mana=2
    k_rage=2
    profit=50,40,10,0,0
    ecmoney=1
  }

 40 {
    k_money=8 //10
    k_lead=2.5
    k_exp=5
    k_mana=2.4
    k_rage=2.4
    profit=40,40,15,5,0
    ecmoney=1
  }

 50 {

    k_money=14 //20
    k_lead=3
    k_exp=10
    k_mana=2.7
    k_rage=2.7
    profit=30,34,24,10,2
    ecmoney=1
  }

 60 {

    k_money=20 //30
    k_lead=3.5
    k_exp=20
    k_mana=3
    k_rage=3
    profit=24,28,28,15,5
    ecmoney=1
  }

 70 {

    k_money=25 //40
    k_lead=4
    k_exp=40
    k_mana=3.5
    k_rage=3.5
    profit=15,25,30,20,10
    ecmoney=1
  }

 80 {

    k_money=30 //50
    k_lead=4.5
    k_exp=60
    k_mana=4
    k_rage=4
    profit=10,20,30,25,15
    ecmoney=1
  }

 90 {

    k_money=40 //60
    k_lead=5
    k_exp=80
    k_mana=4.5
    k_rage=4.5
    profit=5,15,30,30,20
    ecmoney=1
  }

 100 {

    k_money=50 //70
    k_lead=5
    k_exp=100
    k_mana=5
    k_rage=5
    profit=0,5,25,40,30
    ecmoney=1
  }

  ///////////////////////////////////


  // QuestRewardParam
  //################################# quests

   q10 { // Difficulty 0..10%

    k_money=2 //1.5
    k_lead=1
    k_exp=1
    k_mana=1
    k_rage=1
    profit=80,20,0,0,0  //50,25,12,10,3
    ecmoney=1
  }

  q20 { // Difficulty 0..20%

    k_money=4 //3.5
    k_lead=1.5
    k_exp=1.4   //1.3
    k_mana=1.5
    k_rage=1.5
    profit=70,30,0,0,0
    ecmoney=1
  }

 q30 {

    k_money=6 //4.5
    k_lead=2
    k_exp=2.0 //1.6
    k_mana=2
    k_rage=2
    profit=50,40,10,0,0
    ecmoney=1
  }

 q40 {

    k_money=10 //8
    k_lead=2.4
    k_exp=5
    k_mana=2.4
    k_rage=2.4
    profit=40,40,15,5,0
    ecmoney=1
  }

 q50 {

    k_money=15 //11
    k_lead=2.7
    k_exp=10
    k_mana=2.7
    k_rage=2.7
    profit=30,34,24,10,2
    ecmoney=1
  }

 q60 {

    k_money=20 //15
    k_lead=3
    k_exp=20
    k_mana=3
    k_rage=3
    profit=24,28,28,15,5
    ecmoney=1
  }

 q70 {

    k_money=30 //22
    k_lead=3.5
    k_exp=40
    k_mana=3.5
    k_rage=3.5
    profit=15,25,30,20,10
    ecmoney=1
  }

 q80 {

    k_money=45 //40
    k_lead=4
    k_exp=60
    k_mana=4
    k_rage=4
    profit=10,20,30,25,15
    ecmoney=1
  }

 q90 {

    k_money=60 //60
    k_lead=4.5
    k_exp=80
    k_mana=4.5
    k_rage=4.5
    profit=5,15,30,30,20
    ecmoney=1
  }

 q100 {

    k_money=80  //80
    k_lead=5
    k_exp=100
    k_mana=5
    k_rage=5
    profit=0,5,25,40,30
    ecmoney=1
  }

}

