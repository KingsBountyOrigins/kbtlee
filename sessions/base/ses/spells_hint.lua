function gen_dmg_necromancy(level)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
--  if Obj.where()==6 and (Obj.spell_level()<tonumber(level) and Obj.spell_level()~=0) then
--    sel_fnt=Game.Config("txt_font_config/hint_dis")
--    def_fnt=Game.Config("txt_font_config/hint_dis")
--  end 

  local power = pwr_necromancy(tonumber(level))

  return sel_fnt..power..def_fnt

end

function gen_dmg_dragon_arrow(level)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
--  if Obj.where()==6 and (Obj.spell_level()<tonumber(level) and Obj.spell_level()~=0) then
--    sel_fnt=Game.Config("txt_font_config/hint_dis")
--    def_fnt=Game.Config("txt_font_config/hint_dis")
--  end 

  local power = pwr_dragon_arrow(tonumber(level))

  return sel_fnt..power..def_fnt

end

function gen_dmg_trap(par)

  local level=tonumber(par)
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
 
    local min_dmg,max_dmg = pwr_trap(level)

    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end

end

function gen_dmg_demonologist(par)

  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
 
    local lead,time,lvl = pwr_demonologist(level)

		if string.find(par,"level") then
			return sel_fnt..lvl..def_fnt
		end 
		if string.find(par,"time") then
			return sel_fnt..tostring(time)..def_fnt
		end 
		if string.find(par,"lead") then
			return sel_fnt..tostring(lead)..def_fnt
		end 
		return sel_fnt..tostring(lead)..def_fnt

end

function gen_dmg_kamikaze(par)

  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

    local min_dmg,max_dmg,dur = pwr_kamikaze(level)

	if string.find(par,"power") then
		return sel_fnt..tostring(dur)..def_fnt
	else 
    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end
  end 

end

function gen_dmg_evilbook(par)
  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local level,kill = pwr_evilbook(level)
  if string.find(par,"kill") then
    return sel_fnt..kill.."%"..def_fnt
  else
    return sel_fnt.."1-"..tostring(level)..def_fnt
  end
end

function gen_dmg_plague(level)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
--  if Obj.where()==6 and (Obj.spell_level()<tonumber(level) and Obj.spell_level()~=0) then
--    sel_fnt=Game.Config("txt_font_config/hint_dis")
--    def_fnt=Game.Config("txt_font_config/hint_dis")
--  end 

  local power = pwr_plague(tonumber(level))

  return sel_fnt.."-"..power.."%"..def_fnt

end

function gen_dmg_fire_breath(level)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
--  if Obj.where()==6 and (Obj.spell_level()<tonumber(level) and Obj.spell_level()~=0) then
--    sel_fnt=Game.Config("txt_font_config/hint_dis")
--    def_fnt=Game.Config("txt_font_config/hint_dis")
--  end 

  local power = pwr_fire_breath(tonumber(level))

  return sel_fnt.."+"..power.."%"..def_fnt

end

function gen_dmg_pain_mirror(level)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
--  if Obj.where()==6 and (Obj.spell_level()<tonumber(level) and Obj.spell_level()~=0) then
--    sel_fnt=Game.Config("txt_font_config/hint_dis")
--    def_fnt=Game.Config("txt_font_config/hint_dis")
--  end 

  local power = pwr_pain_mirror(tonumber(level))

  return sel_fnt..power.."%"..def_fnt

end

function gen_dmg_berserker(par)
  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local level,power = pwr_berserker(level)
  if string.find(par,"power") then
    return sel_fnt.."+"..power.."%"..def_fnt
  else
    return sel_fnt.."1-"..tostring(level)..def_fnt
  end
end

function gen_dmg_stone_skin(par)

  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local power,penalty = pwr_stone_skin(level)

  if string.find(par,"power") then
    return sel_fnt.."+"..power.."%"..def_fnt
  else
    return sel_fnt.."-"..penalty..def_fnt
  end

end

function gen_dmg_last_hero(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end
    
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
    
  local lvl = tonumber(text_dec(Logic.obj_par("spell_last_hero","level"),level))
  return sel_fnt.."1-"..lvl..def_fnt

end

function gen_dmg_gifts(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end
    
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
    
  local lvl = tonumber(text_dec(Logic.obj_par("spell_gifts","level"),level))
  return sel_fnt.."1-"..lvl..def_fnt

end

function gen_dmg_phantom(par)

	local level = tonumber(text_dec(par,1))
  local power = pwr_phantom(tonumber(level))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  if string.find(par,"level") then 
  	local lvl = tonumber(text_dec(Logic.obj_par("spell_phantom","level"),level))
  	return sel_fnt.."1-"..lvl..def_fnt
  else 
		return sel_fnt..power.."%"..def_fnt
	end 
end

function gen_dmg_demon_slayer(level)

  local power = pwr_demon_slayer(tonumber(level))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  return sel_fnt..power.."%"..def_fnt

end

function gen_dmg_dragon_slayer(level)

  local power = pwr_dragon_slayer(tonumber(level))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  return sel_fnt..power.."%"..def_fnt

end

function gen_dmg_ghost_sword(par)

  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local min_dmg,max_dmg,power = pwr_ghost_sword(level)

    if string.find(par,"power") then
    return sel_fnt..power.."%"..def_fnt
  else
    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end
  end

end

function gen_dmg_magic_source(par)

  local level=tonumber(text_dec(par,1))
  local penalty,mana = pwr_magic_source(level)
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  if string.find(par,"mana") then
    return sel_fnt.."+"..mana..def_fnt
  else
    return sel_fnt.."+"..penalty.."%"..def_fnt
  end

end

function gen_dmg_shroud(level)

  local penalty = pwr_shroud(level)
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

    return sel_fnt.."-"..penalty.."%"..def_fnt

end

function gen_dmg_teleport(level)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_def")

  local dist = text_dec(Logic.obj_par("spell_teleport","dist"),tonumber(level))

  return sel_fnt..dist..def_fnt

end

function gen_dmg_armageddon(par)

  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local min_dmg,max_dmg,burn = pwr_armageddon(level)
  local prc=tonumber(text_dec(Logic.obj_par("spell_armageddon","prc"),level))

  if string.find(par,"prc") then
    return sel_fnt..prc.."%"..def_fnt
  end
  if string.find(par,"burn") then
    return sel_fnt..burn.."%"..def_fnt
  else
    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end
  end

end

function gen_dmg_holy_rain(level)

  local min_dmg,max_dmg = pwr_holy_rain(tonumber(level))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end
end

function gen_dmg_warcry(level)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local bonus = pwr_warcry(tonumber(level))

  return sel_fnt.."+"..tostring(bonus)..def_fnt

end

function gen_dmg_magic_bondage(level)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local bonus = pwr_magic_bondage(tonumber(level))

  return sel_fnt.."1-"..tostring(bonus)..def_fnt

end

function gen_dmg_blind(level)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local bonus = pwr_blind(tonumber(level))

  return sel_fnt.."1-"..tostring(bonus)..def_fnt

end

function gen_dmg_ram(level)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local bonus = pwr_ram(tonumber(level))

  return sel_fnt.."1-"..tostring(bonus)..def_fnt

end

function gen_dmg_crue_fate(level)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local bonus = pwr_crue_fate(tonumber(level))

  return sel_fnt.."1-"..tostring(bonus)..def_fnt

end

function gen_dmg_hypnosis(par)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
	local level=tonumber(text_dec(par,1))
	local lvl,power = pwr_hypnosis(level)
	
	if string.find(par,"level") then
	  return sel_fnt.."1-"..lvl..def_fnt
  else 
  	return sel_fnt..tostring(power)..def_fnt
  end 

end

function gen_dmg_pygmy(par)

	local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local bonus = pwr_pygmy(tonumber(level))

	if string.find(par,"level") then
    local lvl = tonumber(text_dec(Logic.obj_par("spell_pygmy","level"),level))
	  return sel_fnt.."1-"..lvl..def_fnt
  else 
  	return sel_fnt.."-"..tostring(bonus).."%"..def_fnt
  end 

end

function gen_dmg_accuracy(level)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local bonus = pwr_accuracy(tonumber(level))

  return sel_fnt.."+"..tostring(bonus).."%"..def_fnt

end

function gen_dmg_slow(level)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local bonus = pwr_slow(tonumber(level))

  return sel_fnt.."-"..tostring(bonus)..def_fnt

end

function gen_dmg_haste(level)

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local bonus = pwr_haste(tonumber(level))

  return sel_fnt.."+"..tostring(bonus)..def_fnt

end

function gen_dmg_divine_armor(par)

  level=tonumber(par)
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
  
  local bonus = pwr_divine_armor(level)

  return sel_fnt..tostring(bonus).."%"..def_fnt

end

function gen_dmg_defenseless(level)

  local bonus = pwr_defenseless(level)
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
 
  return sel_fnt.."-"..tostring(bonus).."%"..def_fnt

end

function gen_dmg_adrenalin(level)

  local bonus = pwr_adrenalin(level)
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  return sel_fnt..tostring(bonus)..def_fnt

end

function gen_dmg_pacifism(par)

  local level=tonumber(text_dec(par,1))
  local bonus,penalty = pwr_pacifism(level)
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

    if string.find(par,"bonus") then
    return sel_fnt.."+"..tostring(bonus).."%"..def_fnt
  else
    return sel_fnt.."-"..tostring(penalty).."%"..def_fnt
  end

end

function gen_dmg_resurrection(par)

	local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

	local ref = pwr_resurrection(level)
	
	if string.find(par,"level") then
    local lvl = tonumber(text_dec(Logic.obj_par("spell_resurrection","level"),level))
	  return sel_fnt.."1-"..lvl..def_fnt
  else 
  	return sel_fnt..tostring(ref)..def_fnt
  end 

end

function gen_dmg_healing(level)

  local min_dmg,max_dmg = pwr_healing(tonumber(level))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
 
    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end
end

function gen_dmg_oil_fog(par)

  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local min_dmg,max_dmg,dur,power = pwr_oil_fog(level)

    if string.find(par,"power") then
    return sel_fnt..power.."%"..def_fnt
  else
    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end
  end

end

function gen_dmg_lightning(par)

  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

    local min_dmg,max_dmg,shock = pwr_lightning(level)

  if string.find(par,"shock") then
    return sel_fnt..shock.."%"..def_fnt
  else
    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end
  end

end

function gen_dmg_magic_axe(par)

  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local min_dmg,max_dmg,count = pwr_magic_axe(level)

  if string.find(par,"count") then
    return sel_fnt..count..def_fnt
  else
    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end
  end

end

function gen_dmg_fire_ball(par)

  local min_dmg,max_dmg,burn = pwr_fire_ball(par)

  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
 
  local min_dmg,max_dmg,burn = pwr_fire_ball(par)

    if string.find(par,"burn") then
    return sel_fnt..tostring(burn).."%"..def_fnt
  else
    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end
  end
end

function gen_dmg_ice_serpent(par)

  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
 
  local min_dmg,max_dmg,freeze = pwr_ice_serpent(par)

    if string.find(par,"freeze") then
    return sel_fnt..tostring(freeze).."%"..def_fnt
  else
    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end
  end
end

function gen_dmg_sacrifice(par)

  local level=tonumber(text_dec(par,1))
  
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
	
	local damage,power = pwr_sacrifice(tonumber(level))
	
  if string.find(par,"power") then
  	return sel_fnt..tostring(power).."%"..def_fnt
  else 
  	return sel_fnt..tostring(damage)..def_fnt
  end
end

function gen_dmg_fire_arrow(par)

  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
 
  local min_dmg,max_dmg,burn = pwr_fire_arrow(level)

    if string.find(par,"burn") then
    return sel_fnt..burn.."%"..def_fnt
  else
    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end
  end

end

function gen_dmg_geyser(par)

  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
 
  local min_dmg,max_dmg,count = pwr_geyser(level)

    if string.find(par,"count") then
    return sel_fnt..count..def_fnt
  else
    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end
  end

end


function gen_dmg_fire_rain(par)

  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

  local min_dmg,max_dmg,burn = pwr_fire_rain(level)

    if string.find(par,"burn") then
    return sel_fnt..burn.."%"..def_fnt
  else
    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end
  end

end

function gen_dmg_bless()
  local healer_bonus=Logic.hero_lu_skill("healer")
  if healer_bonus>0 then healer_bonus=1 end
  local res=""
  if healer_bonus>0 then
    res="<br>+"..healer_bonus.." <label=spell_bonus_healer_skill> "
  end
  return res
end

function gen_dmg_smile_skull(par)

  local level=tonumber(text_dec(par,1))
  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")
 
    local min_dmg,max_dmg,poison = pwr_smile_skull(level)

    if string.find(par,"poison") then
    return sel_fnt..poison.."%"..def_fnt
  else
    if min_dmg == max_dmg then
      return sel_fnt..tostring(min_dmg)..def_fnt
    else
      return sel_fnt..tostring(min_dmg).."-"..tostring(max_dmg)..def_fnt
    end
  end

end

-- ***************************************
-- * ���� ����-�������������
-- ***************************************

function gen_spell_end()

  local mana=""
  local duration=""
  local sc=""
  local school=""

  if (Obj.where()==1) and (Logic.obj_par(Obj.name(),"duration")~="0") or (Obj.where()==6) and (Logic.obj_par(Obj.name(),"duration")~="0") then
    local level=Obj.spell_level()
    if level==0 then level=1 end

    if Obj.where()==6 and Obj.spell_level()~=0 then level=level+1 end

-- ����������� ������� ������� ������� � �����������!!!
    dur=tonumber(text_dec(Logic.obj_par(Obj.name(),"duration"),level))
--    if int~="1" then
    if spell~="spell_adrenalin" or Logic.obj_par(Obj.name(),"int")=="1" then 
      dur=dur + math.ceil(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))-1
    end 
--    end
    duration="<label=spell_hintMD_font>".."<label=spell_hint_d> "..dur.."     "
    --Game.Config("txt_font_config/hint_cap").."<label=spell_hint_d> ".."<label=hint_Def_font>"..dur.."  "
  end
  if Obj.where()==5 then
--    local dur = tonumber(Attack.act_spell_duration(cellunit,spellna�e))
    duration="<label=spell_hintMD_font>".."<label=spell_hint_d> "..2
    --Game.Config("txt_font_config/hint_cap").."<label=spell_hint_d> ".."<label=hint_Def_font>".."2"
  end

  if Obj.spell_level()>0 and (Obj.where()==2 or Obj.where()==6 or Obj.where()==1) then
    local up=0
    if Obj.where()==6 then up=1 end

    local mc = Obj.spell_mana(Obj.spell_level()+up)
    mana="<label=spell_hintMD_font>".."<label=spell_hint_mana> "..mc
    --Game.Config("txt_font_config/hint_cap").."<label=spell_hint_m> ".."<label=hint_Def_font>"..mc
  end

  spell_end=mana
  --duration..mana

  return spell_end

end

function gen_spell_school()

  local sc=""
  local school=""

  -- 1-����� 2-������� 5-����
  if Obj.where()==2 then
    sc = Obj.spell_school()

    if sc == 1 then
      sc="<label=spell_hint_MOrd>"
    end
    if sc == 2 then
      sc="<label=spell_hint_MDis>"
    end
    if sc == 3 then
      sc="<label=spell_hint_MCha>"
    end
    if sc == 4 then
      sc="<label=spell_hint_MAdv>"
    end
    school="<br><label=spell_hint_TM_font><label=spell_hint_s> <label=hint_Def_font>"..sc
  end

  return school

end

function gen_spell_name()

  local sn = ""
  if Obj.where()==1 then     -- 1-����� 2-������� 5-����
    sn = Game.Config("txt_font_config/spell_cap")
  end

  return sn

end

function gen_spell_level()

  local lvl=Obj.spell_level()
  local level="<label=spell_hintLev_font>"
--  local lll=""
--  local sl=Game.Config("txt_font_config/hint_def")

  if Obj.where()<5 then     -- 1-����� 2-������� 5-����
    level=level.."<label=spell_level> "
    if lvl==0 then lvl=1 end
    for i=1,lvl do 
      level=level.."I"
    end
    if Obj.spell_level()== 0 then
      level=level.." <label=spell_scroll>".."<br>"
    else
      level=level.."<br>"
    end
   	return level..Game.Config("txt_font_config/hint_def")
  end 
  if Obj.where()==6 then
    if lvl==0 then
      level = level.."<label=spell_next_scroll>"
    else 
      lvl = lvl + 1
      level=level.."<label=spell_level> "
        for i=1,lvl do 
          level=level.."I"
        end
        level=level.."<br>"
    end 
    return level..Game.Config("txt_font_config/hint_def")
  end
  
  if Obj.where()==5 then
    return Game.Config("txt_font_config/hint_def")
  end
end

function gen_spell_desc()

  local desc=""
  -- 1-����� 2-������� 5-����
  if Obj.where()~=5 then
    desc = Game.Config("txt_font_config/hint_def").."<label="..Obj.name().."_desc>"
  else
    desc = Game.Config("txt_font_config/hint_def").."<label="..Obj.name().."_small>"
  end

  return desc
end

function gen_spell_desc_complex()

    local level=Obj.spell_level()
    if level==0 then level=1 end


  if Obj.where()==6 and Obj.spell_level()~=0 then level=level+1 end

  local desc=""
  -- 1-����� 2-������� 5-����
  if Obj.where()~=5 then
    count = text_dec(Logic.obj_par(Obj.name(),"unit_count"),level)
    desc = Game.Config("txt_font_config/hint_def").."<label="..Obj.name().."_desc_"..count..">"
  else
    desc = Game.Config("txt_font_config/hint_def").."<label="..Obj.name().."_small>"
  end

  return desc
end

function gen_spell_desc_combo()

  local level=Obj.spell_level()
  if level==0 then level=1 end

  if Obj.where()==6 and Obj.spell_level()~=0 then level=level+1 end

  local desc=""
  -- 1-����� 2-������� 5-����
  if Obj.where()~=5 then
    desc = Game.Config("txt_font_config/hint_def").."<label="..Obj.name().."_desc_"..level..">"
  else
    desc = Game.Config("txt_font_config/hint_def").."<label="..Obj.name().."_small>"
  end

  return desc
end


-- ***************************************
-- * ���� ����� ����-�������������
-- ***************************************
function gen_spell_desc_new(par)
-- par = simple,complex,multi
  local desc=""
  local spell=Obj.name()
  local level=Obj.spell_level()
  local dur = ""
  local round = ""
  local duration = 0
  if level==0 then level=1 end

  -- 1-����� 2-������� 5-���� 6-��������
  if Obj.where()<5 then
    if par=="simple" then
      desc = Game.Config("txt_font_config/hint_def").."<label="..spell.."_desc>"..Game.Config("txt_font_config/hint_sys").."<br>".."<label="..spell.."_text_"..level..">"
    end
    if par=="complex" then
      local count = text_dec(Logic.obj_par(spell,"unit_count"),level)
      desc = Game.Config("txt_font_config/hint_def").."<label="..spell.."_desc>"..Game.Config("txt_font_config/hint_sys").."<br>".."<label="..spell.."_text_"..level..">".." ".."<label=spell_C"..count..">"
    end
    if par=="complex0" then
    	local count=""
--      if spell=="spell_gifts" or spell=="spell_target" then
--      	count="one"
--      else
--      	count = text_dec(Logic.obj_par(spell,"unit_count"),level)
--      end
      if count=="all" then 
      desc = Game.Config("txt_font_config/hint_def").."<label="..spell.."_desc>"..Game.Config("txt_font_config/hint_sys").."<br>".."<label="..spell.."_text_"..level..">".."<label=spell_C"..count..">"
      else 
      	if spell == "spell_bless" then
      		if level==3 then 
      			desc = Game.Config("txt_font_config/hint_def").."<label="..spell.."_desc>"..Game.Config("txt_font_config/hint_sys").."<br>".."<label="..spell.."_text_"..level..">".."<label=spell_Call>"
      		else
      			desc = Game.Config("txt_font_config/hint_def").."<label="..spell.."_desc>"..Game.Config("txt_font_config/hint_sys").."<label="..spell.."_text_"..level..">"
      		end
      	else
      		desc = Game.Config("txt_font_config/hint_def").."<label="..spell.."_desc>"
      	end
      end
    end
    if par=="multi" then
      --local count = text_dec(Logic.obj_par(Obj.name(),"unit_count"),level)
      desc = Game.Config("txt_font_config/hint_def").."<label="..spell.."_desc_"..level..">"
    end

    duration = tonumber("0" .. text_dec(Logic.obj_par(spell,"duration"),1))
    if duration ~= 0 then
      duration = tonumber("0" .. text_dec(Logic.obj_par(spell,"duration"),level))
      
      
      if spell~="spell_adrenalin"  and Logic.obj_par(spell,"int")=="1" then
        duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
      end

      if duration < 10 then
        round = "<label=spell_hint_round_"..duration..">"
      else
        round = "<label=spell_hint_round_10>"
      end
      dur=Game.Config("txt_font_config/hint_sys").."<br>".."<label=spell_duration>".." "..Game.Config("txt_font_config/hint_sel")..duration..Game.Config("txt_font_config/hint_sys").." "..round
    else
      dur=""
    end

    local mc=Obj.spell_mana(level)
    if mc<0 then 
    	local tmp=Logic.hero_lu_item("mana","limit")
    	mc=-1*tmp*mc/100
    end 
    local mana=Game.Config("txt_font_config/hint_sys").."<label=spell_hint_mana>".." ".."<image=book_mana_icon.png> ".."<label=hint_Sel_font>"..mc

    if Obj.spell_level()~=0 then
      desc=desc..dur.."<br>"..mana
    else
      if dur~="" then
        desc=desc..dur
      end
    end
  end 
  
  if Obj.where()==5 then
    desc = Game.Config("txt_font_config/hint_def").."<label="..Obj.name().."_small>"
  end 
  
  if Obj.where()==6 then
    level = level + 1
   if Obj.spell_level()~=0 then
    if par=="simple" then
      desc = Game.Config("txt_font_config/hint_def").."<label="..spell.."_desc>"..Game.Config("txt_font_config/hint_sys").."<br>".."<label="..spell.."_text_"..level..">"
    end
    if par=="complex" then
      local count = text_dec(Logic.obj_par(spell,"unit_count"),level)
      desc = Game.Config("txt_font_config/hint_def").."<label="..spell.."_desc>"..Game.Config("txt_font_config/hint_sys").."<br>".."<label="..spell.."_text_"..level..">".." ".."<label=spell_C"..count..">"
    end
    if par=="complex0" then
    	local count=""
      if spell=="spell_gifts" or spell=="spell_target" then
      	count="one"
      else
      	count = text_dec(Logic.obj_par(spell,"unit_count"),level)
      end
      if count=="all" then 
      desc = Game.Config("txt_font_config/hint_def").."<label="..spell.."_desc>"..Game.Config("txt_font_config/hint_sys").."<br>".."<label="..spell.."_text_"..level..">".."<label=spell_C"..count..">"
      else 
      	if spell == "spell_bless" then
      		desc = Game.Config("txt_font_config/hint_def").."<label="..spell.."_desc>"..Game.Config("txt_font_config/hint_sys").."<label="..spell.."_text_"..level..">"
      	else
      		desc = Game.Config("txt_font_config/hint_def").."<label="..spell.."_desc>"
      	end
      end
    end
    if par=="multi" then
      desc = Game.Config("txt_font_config/hint_def").."<label="..spell.."_desc_"..level..">"
    end
    
        duration = tonumber("0" .. text_dec(Logic.obj_par(spell,"duration"),1))
    if duration ~= 0 then
      duration = tonumber("0" .. text_dec(Logic.obj_par(spell,"duration"),level))
      if spell~="spell_adrenalin"  and Logic.obj_par(spell,"int")=="1" then
        duration = duration + math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
      end

      if duration < 10 then
        round = "<label=spell_hint_round_"..duration..">"
      else
        round = "<label=spell_hint_round_10>"
      end
      dur=Game.Config("txt_font_config/hint_sys").."<br>".."<label=spell_duration>".." "..Game.Config("txt_font_config/hint_sel")..duration..Game.Config("txt_font_config/hint_sys").." "..round
    else
      dur=""
    end

    local mc=Obj.spell_mana(level)
    if mc<0 then 
    	local tmp=Logic.hero_lu_item("mana","limit")
    	mc=-1*tmp*mc/100
    end 

    local mana="<label=hint_TM_font>".."<label=spell_hint_mana>".." ".."<image=book_mana_icon.png> ".."<label=hint_Sel_font>"..mc
	
      desc=desc..dur.."<br>"..mana
  end 
    --���������� � ��������
        local cur_cr=Logic.hero_lu_item("crystals","count")
        local need_cr=Obj.spell_crystals(Obj.spell_level()+1)
        local descUp="<label=spell_up_need>".." ".."<image=book_crystals_icon.png>"

        if need_cr>cur_cr or gen_spell_skill()~="false" then
          descUp="<label=spell_hintNoUp_font>"..descUp
        else 
          descUp=Game.Config("txt_font_config/hint_sys")..descUp
        end 
        if need_cr>cur_cr then 
          descUp=descUp.." "..need_cr
        else
          descUp=descUp..Game.Config("txt_font_config/hint_sel").." "..need_cr  
        end 
        if gen_spell_skill()~="false" then 
          descUp=descUp..gen_spell_skill()
        end 
        if need_cr<=cur_cr and gen_spell_skill()=="false" then 
            descUp=descUp.."<br><label=spell_hintYesUp_font>"
            if Obj.spell_level()==0 then
                descUp=descUp.."<label=spell_Memorize>"
            else
                descUp=descUp.."<label=spell_Upgrade>"
            end
        end
			desc=desc.."<br>"..descUp

  end -- ����� ������������ �����

    return desc
end

function gen_spell_mana()

  local mana=""

    local level=Obj.spell_level()
    if level==0 then level=1 end

    if Obj.where()==6 and Obj.spell_level()~=0 then level=level+1 end

    if Obj.spell_level()>0 and (Obj.where()==2 or Obj.where()==6 or Obj.where()==1) then
      local up=0
      if Obj.where()==6 then up=1 end

    local mc = Obj.spell_mana(Obj.spell_level()+up)
    mana="<label=spell_hint_mana> "..mc
    --Game.Config("txt_font_config/hint_cap").."<label=spell_hint_m> ".."<label=hint_Def_font>"..mc
    end

  return mana

end

function gen_spell_skill()

  local result = "false"
    local skill=0

    local level=Obj.spell_level()
    local school=Obj.spell_school()
    if school==1 and level>=Logic.hero_lu_skill("order") then 
      local itogo="<label=skill_hintNoUp_font>"
      local sk_need_name="<label=skill_".."order".."_name>"
      local sk_need_level=level+1
      result=itogo.."<br>'"..sk_need_name.."' "..sk_need_level.." <label=skill_level_need>"
    end 
    if school==2 and level>=Logic.hero_lu_skill("dist") then 
      local itogo="<label=skill_hintNoUp_font>"
      local sk_need_name="<label=skill_".."dist".."_name>"
      local sk_need_level=level+1
      result=itogo.."<br>'"..sk_need_name.."' "..sk_need_level.." <label=skill_level_need>"
    end 
    if school==3 and level>=Logic.hero_lu_skill("chaos") then 
      local itogo="<label=skill_hintNoUp_font>"
      local sk_need_name="<label=skill_".."chaos".."_name>"
      local sk_need_level=level+1
      result=itogo.."<br>'"..sk_need_name.."' "..sk_need_level.." <label=skill_level_need>"
    end 

  return result 

end

function get_spell_name()
--	if string.find(Obj.name(),"spell") or string.find(Obj.name(),"effect") or string.find(Obj.name(),"special") then
  	return "<label="..Obj.name().."_name>"
--  else
--    return "<label="..Obj.name().."_head>"
--  end 
  
end

function gen_spell_del_hint()

  local res = ""
	  if Obj.where()==1 and Obj.spell_level()==0 then
	   if Game.InsideBuilding() then
	   	res = "<label=spell_sell_scroll>"
	   else
  		res = "<label=spell_delete_scroll>"
  	 end
  	end 
	  if Obj.where()==1 and Obj.spell_level()>1 then
  		res = "<label=spell_level_cast>"
  	end 

  return res
end 

function gen_spell_caption()
	
	
  local sc = Obj.spell_school()
  local name=Game.Config("txt_font_config/hint_cap")
  
  if Obj.where()==1 then     -- 1-����� 2-������� 5-����
    return name
  end

	if Obj.where()==2 then     -- 1-����� 2-������� 5-����
  	if sc == 1 then
    	name="<label=spell_CapMOrd_font>"
  	end
  	if sc == 2 then
    	name="<label=spell_CapMDis_font>"
  	end
  	if sc == 3 then
    	name="<label=spell_CapMCha_font>"
  	end
  	if sc == 4 then
    	name="<label=spell_CapMAdv_font>"
  	end
	end 
	
	if Obj.where()==5 then     -- 1-����� 2-������� 5-����
		local spell_name=Obj.name()
		--if string.find(spell_name,"spell") then
			local spell_type=Logic.obj_par(spell_name,"type")
			if spell_type=="penalty" then
				name="<label=spell_bad_font>"
			end
			if spell_type=="bonus" then
				name="<label=spell_good_font>"
			end
		--else 
		
		--end 
	end 
  	return name
  --sc

end

function gen_arm_prc()

  local sel_fnt=Game.Config("txt_font_config/hint_sel")
  local def_fnt=Game.Config("txt_font_config/hint_sys")

    local level=Obj.spell_level()
    if level==0 then level=1 end
    if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    local prc=tonumber(text_dec(Logic.obj_par("spell_armageddon","prc"),level))

	return sel_fnt..prc.."%"..def_fnt
end

function gen_lvl_target()
    local level=Obj.spell_level()
    if level==0 then level=1 end
    if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    

	return "1-"..text_dec(Logic.obj_par("spell_target","lvl"),level)
end

function gen_lvl_target2()
	return "1-"..Attack.act_spell_param(0, "spell_target", "lvl")
end
