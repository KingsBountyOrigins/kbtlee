function scn_boat_stop ( param )

    if (param == nil) then

        Scenario.stop()
        Scenario.delay( 1, "b_000" )

    elseif (param == "b_000") then
        return true
    end

    return false
end

function scn_boat_takebox ( param )

    Scenario.stop()
    return true
end

function scn_boat_entercastle ( param )

    Scenario.stop()
    return true
end

function scn_boat_enterboat ( param )

    Scenario.stop()
    return true

end

function scn_boat_teleport1 ( param )

    local teletarget = "dev_map_0.start"

    if (param == nil) then

      -- �������...
      Scenario.formsolo( "map" )         -- �������� ������ map ���������
      Scenario.delay( 0.1, "b_001" )  -- ��������� 0.1 ���

    elseif (param == "b_001") then

      -- ���������. ����������....
      if not Atom.is_onboat() then
        Scenario.animate("telein","b_002")  -- �������� ����� � ���� ����������
      else
        Scenario.teleport( teletarget, "b_003" ) -- ������� ���������� �������
      end

    elseif (param == "b_002") then
      -- �������� ���������
      Scenario.teleport( teletarget, "b_003" ) -- ������� ���������� �������

    elseif (param == "b_003") then

      Scenario.animate("teleout","b_004")  -- �������� ������ �� ���������

    elseif (param == "b_004") then
        -- �������� ���������, ������ ������ ������
        return true
    end

    return false
end

function boat_scenario ( scn, param ) --ftag:mascn

    local scenario_handlers = {
        stop = scn_boat_stop,
        takebox = scn_boat_takebox,
        entercastle = scn_boat_entercastle,
        enterboat = scn_boat_enterboat,
        teleport1 = scn_boat_teleport1
    }

    local f = scenario_handlers[ scn ]

    if (f ~= nil) then
        return f(param)
    end

end

