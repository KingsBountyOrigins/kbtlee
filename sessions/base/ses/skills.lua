function skill_caption()

    local name=""
--  (level, group, rune_might, rune_magic, rune_mind)
    local level, group, rune_might, rune_mind, rune_magic =Logic.hero_lu_skill()

  if level< 1 then
    name = "<label=skill_name_closed_font>"
  else
    name = "<label=skill_name_open_font>"
  end

    return name

end


function skill_name_hint()

    local name=""
--  (level, group, rune_might, rune_magic, rune_mind)
    local level, group, rune_might, rune_mind, rune_magic=Logic.hero_lu_skill()

  if group == 0 then
    name = "<label=skill_CapMight_font>"
  end
  if group == 1 then
    name = "<label=skill_CapMind_font>"
  end
  if group == 2 then
    name = "<label=skill_CapMagic_font>"
  end

    return Game.Config("txt_font_config/hint_cap")
    --name

end

function skill_level()

    local count="<label=skill_level>"
--  (level, group, rune_might, rune_magic, rune_mind)
    local level, group, rune_might, rune_mind, rune_magic=Logic.hero_lu_skill()
    local max_level=Logic.skill()
        count=count.." "..level.." / "..max_level

    if level == 0 then
        count=count.."<label=skill_hintDis_font>"
    else
        count=count.."<label=skill_hintDef_font>"
    end

    return count

end

function skill_text_hint()

    local current=""
    local level=Logic.hero_lu_skill()
    local max_level=Logic.skill()

    local s_name="skill_"..Logic.skill_name().."_text_"
--

    if level==0 then
        s_name=s_name.."1"
        --"skill_rage_text_l1" --"skill_"..Logic.skill_name().."text_l1"
        current="<label="..s_name..">"
    else
      if level==max_level then
          s_name=s_name..max_level
          current="<label="..s_name..">"
      else
          local n_name=s_name..(level+1)
          s_name=s_name..level
          current="<label="..s_name..">"
          local cur_head="<label=skill_next_level>"
          local cur_next="<label="..n_name..">"
          current=current.."<br><br><label=skill_hintLev_font>"..cur_head.."<br><label=skill_hintDis_font>"..cur_next
      end
    end
    --skill_rage_hint

    return current

end

function skill_learn()

    local itogo=""
    if Logic.hero_lu_skill_upg() then
        itogo = "<br><label=skill_hintYesUp_font><label=skill_Upgrade>"
    end

    return itogo

end

function skill_rune()

    local count=""
--  (level, group, rune_might, rune_magic, rune_mind)
    local level, group, rune_might, rune_mind, rune_magic=Logic.hero_lu_skill()

    if rune_might~=nil and  rune_mind~=nil and rune_magic~=nil then
        if (rune_might+rune_mind+rune_magic)>0 then
            if Logic.hero_lu_skill_upg() then
                count=count.."<label=skill_hint_TM_font><label=skill_rune_need> "
            else
                count=count.."<label=skill_hintNoUp_font><label=skill_rune_need> "
            end
        end

        if rune_might>0 then
            if Logic.hero_lu_item("rune_might","count")<rune_might then
                r="<image=Message_icon2_rune_red.png><label=skill_hintNoUp_font>"..rune_might
            else
                r="<image=Message_icon2_rune_red.png><label=skill_hint_TM_font>"..rune_might
            end
        else
            r=""
        end

        if rune_mind>0 then
            if Logic.hero_lu_item("rune_mind","count")<rune_mind then
                g="<image=Message_icon2_rune_green.png><label=skill_hintNoUp_font>"..rune_mind
            else
                g="<image=Message_icon2_rune_green.png><label=skill_hint_TM_font>"..rune_mind
            end
        else
            g=""
        end

        if rune_magic>0 then
            if Logic.hero_lu_item("rune_magic","count")<rune_magic then
                b="<image=Message_icon2_rune_blue.png><label=skill_hintNoUp_font>"..rune_magic
            else
                b="<image=Message_icon2_rune_blue.png><label=skill_hint_TM_font>"..rune_magic
            end
        else
            b=""
        end

        count=count..r..g..b
    end
    return count

end

function skill_head_hint()

    local level=Logic.hero_lu_skill()
    local max_level=Logic.skill()

    local s_name="skill_"..Logic.skill_name().."_text_"
--
        local current="<label=skill_"..Logic.skill_name().."_header><br>"
    if level==0 then
        s_name=s_name.."1"
        --"skill_rage_text_l1" --"skill_"..Logic.skill_name().."text_l1"
        current=current.."<label="..s_name..">"
    else
      if level==max_level then
          s_name=s_name..max_level
          current=current.."<label="..s_name..">"
      else
          local n_name=s_name..(level+1)
          s_name=s_name..level
          current=current.."<label="..s_name..">"
          local cur_head="<label=skill_next_level>"
          local cur_next="<label="..n_name..">"
          current=current.."<br><br><label=skill_hintLev_font>"..cur_head.."<br><label=skill_hintDis_font>"..cur_next
      end
    end
    --skill_rage_hint

    return current

end

function skill_need_for_learn()

    local itogo=""
    local learn, skill_need = Logic.hero_lu_skill_upg()

        if skill_need~="" then
            skill_need=string.gsub(skill_need, "/", ",")
            itogo = "<label=skill_hintNoUp_font>"

            local count = text_par_count(skill_need)/2
            for i=0,count-1 do
                local sk_need_name = text_dec(skill_need, i*2+1)
                sk_need_name = "<label=skill_"..sk_need_name.."_name>"
                local sk_need_level = text_dec(skill_need, i*2+2)
                itogo=itogo.."<br>'"..sk_need_name.."' "..sk_need_level.." <label=skill_level_need>"
            end
        end

    return itogo

end

function skill_name()

    return "<label=skill_"..Logic.skill_name().."_name>"
end

--skill [skillname [, level]] ���� �� �����. ���� ��� �������� level'�, �� ��������� ���������� �������.
-- ���� ������� level, �� ��� ���� ������ ������-��������. �� ��������� ��� ����� ������ � gen_text

function skill_param(param)

    local par=""
    local level=tonumber(text_dec(param,1))-1
    local number_of_param=tonumber(text_dec(param,2))
--    if level=0 then
--      level=Logic.hero_lu_skill()
--    else
--      level=Logic.hero_lu_skill()+1
--    end

    local max_level=Logic.skill()
    local skillname=Logic.skill_name()

    local max_level=Logic.skill()
    if level>max_level then return "level error" end
		if text_dec(param,2) == "necro" then
			return "+"..tostring(level+1).."0%"
		end
        local par_string=Logic.skill(skillname,level)
        par_string=string.gsub(par_string, "/", "")
        par=text_dec(par_string, number_of_param)
    return par
end

function skill_power(skillname,param,level)
        level = tonumber(level)
    if level == nil then level=Logic.hero_lu_skill(skillname)-1 end
    if level<0 then return 0 end
  --local skillname=Logic.skill_name()

        local par_end=0
        local par_string=Logic.skill(skillname,level)
        local par=""
        if par_string~="" and par_string~=nil then
            par_string=string.gsub(par_string, "/", "")
            par=text_dec(par_string, param)
            if par~="" and par~=nil then
        local par_e=string.gsub(par, "%D", "")
                par_end=tonumber(par_e)
            end
        end

    return par_end
end

function skill_power2(skillname,param,level)
	if Attack.act_belligerent() ~= 1 then return 0 end
	return skill_power(skillname,param,level)
end

function hero_item_count(name)
    local count = Logic.hero_lu_item(name,"count")
    if count == nil then return 0 end
    return count
end

function hero_item_count2(name)
	if Attack.act_belligerent() ~= 1 then return 0 end
    local count = Logic.hero_lu_item(name,"count")
    if count == nil then return 0 end
    return count
end

function hero_item_limit(name)
    local limit = Logic.hero_lu_item(name,"limit")
    if limit == nil then return 0 end
    return limit
end

-- ������������ ���������� ��� ��������� ����
function skill_trader()
    return skill_power("trader",1)/100
end


-- ����������� ������
-- +�������� ������ (������), +����� (���������), +������ ������ (����), +�������� (���� � ������)
function skill_glory(name,level)
    local bonus = skill_power(name,1,level-1)
    if level >1 then bonus=bonus-skill_power(name,1,level-2) end
    local cur_lead=Logic.hero_lu_item("leadership","count")
        Logic.hero_lu_item("leadership","count",cur_lead+bonus)

        return true
end
function skill_rage(name,level)
    local bonus = skill_power(name,1,level-1)
    if level >1 then bonus=bonus-skill_power(name,1,level-2) end
    local cur_rage=Logic.hero_lu_item("rage","limit")
        Logic.hero_lu_item("rage","limit",cur_rage+bonus)
        return true
end
--booksize
function skill_wizdom(name,level)
    local bonus = skill_power(name,1,level-1)
    local bonus2 = skill_power(name,2,level-1)
    if level >1 then
        bonus=bonus-skill_power(name,1,level-2)
        bonus2=bonus2-skill_power(name,2,level-2)
    end
    local cur_mana=Logic.hero_lu_item("mana","limit")
    local cur_booksize=Logic.hero_lu_item("booksize","count")
        Logic.hero_lu_item("booksize","count",cur_booksize+bonus2)
        Logic.hero_lu_item("mana","limit",cur_mana+bonus)
        return true
end
function skill_rune_stone(name,level)
    local bonus = skill_power(name,1,level-1)
    local cur_might=Logic.hero_lu_item("rune_might","count")
    local cur_magic=Logic.hero_lu_item("rune_magic","count")
        Logic.hero_lu_item("rune_might","count",cur_might+bonus)
        Logic.hero_lu_item("rune_magic","count",cur_magic+bonus)
        return true
end
----
function skill_archer(name,level)
    local bonus = skill_power(name,1,level-1)
    if level >1 then bonus=bonus-skill_power(name,1,level-2) end
        local cur_archer=Logic.hero_lu_item("sp_lead_archer","count")
        Logic.hero_lu_item("sp_lead_archer","count",bonus+cur_archer)
        return true
end
function skill_archmage(name,level)
    local bonus = skill_power(name,1,level-1)
    if level >1 then bonus=bonus-skill_power(name,1,level-2) end
        local cur_archer=Logic.hero_lu_item("sp_lead_archmage","count")
        Logic.hero_lu_item("sp_lead_archmage","count",bonus+cur_archer)
        return true
end
function skill_spirit(name,level)
    local bonus = skill_power(name,1,level-1)
    if level >1 then bonus=bonus-skill_power(name,1,level-2) end
        local cur_exp=Logic.hero_lu_item("sp_addexp_spirit","count")
        Logic.hero_lu_item("sp_addexp_spirit","count",bonus+cur_exp)
        return true
end
--sp_lead_warrior
function skill_warrior(name,level)
    local bonus = skill_power(name,1,level-1)
    if level >1 then bonus=bonus-skill_power(name,1,level-2) end
        local cur_lead=Logic.hero_lu_item("sp_lead_warrior","count")
        Logic.hero_lu_item("sp_lead_warrior","count",bonus+cur_lead)
        return true
end
-- ����������� ������ ��������. ������ �� �����������!
function skill_exp(name,level)
    local bonus = skill_power(name,1,level-1)
    if level >1 then bonus=bonus-skill_power(name,1,level-2) end
        local cur_exp1=Logic.hero_lu_item("sp_addexp_battle","count")
        local cur_exp2=Logic.hero_lu_item("sp_addexp_quest","count")
        Logic.hero_lu_item("sp_addexp_battle","count",bonus+cur_exp1)
        Logic.hero_lu_item("sp_addexp_quest","count",bonus+cur_exp2)
        return true
end
-- ����������� ������ ��������. ������ �� �����������!
function skill_gold_quest(name,level)
    local bonus = skill_power(name,1,level-1)
    if level >1 then bonus=bonus-skill_power(name,1,level-2) end
    local cur_gold=Logic.hero_lu_item("sp_addgold_quest","count")
        Logic.hero_lu_item("sp_addgold_quest","count",bonus+cur_gold)
        return true
end

--sp_addgold_quest {}



function army_text_to_table(army,max)

    local tbl = {}
    for i=1,max do
        local atom, count = Logic.army_get(army, i)
        if count > 0 then
            if tbl[atom] == nil then tbl[atom] = count
            else tbl[atom] = tbl[atom] + count end
        end
    end
    return tbl

end

function diplomacy_check()

    local perc = skill_power("diplomacy", 1)
    if perc > 0 then

        local enemy = army_text_to_table(Logic.cur_lu_army(),10)
        local hero  = army_text_to_table(Logic.hero_lu_army(),5) -- ��������� ������ �������� �����, ��� �������
        local hero_ld = Logic.hero_lu_item("leadership","count")

        local join_max = {} -- ������, ������� ����� �������������� �� ���������
        local join_maxs = 0
        local join_other = {} -- ������, ������� ����� ��������������, �� �� �� ���������
        local join_others = 0

        for atom, count in pairs(hero) do
            if enemy[atom] ~= nil then -- � ����� ���� ����� �� ����� ��� � � �����
                local can_join = math.floor(hero_ld / Logic.cp_leadship(atom)) - count -- ������� ����� ����� �� ���������
                local can_capture_from_enemy = math.floor(enemy[atom]*perc/100.) -- ������� ����� ������ ����
                if can_join > 0 and can_capture_from_enemy > 0 then
                    if can_join >= can_capture_from_enemy then
                        join_max[atom] = can_capture_from_enemy
                        join_maxs = join_maxs + 1
                        join_max[join_maxs] = atom
                    else -- ��������� ����� ����������� �� ��������� - �� ������� ���������, ������� ���� ������ ������������ � ��������� �������
                        join_other[atom] = can_join
                        join_others = join_others + 1
                        join_other[join_others] = atom
                    end
                end
            end
        end

        joining_cp = nil
        if join_maxs > 0 then
            joining_cp = join_max[Game.Random(1, join_maxs)]
            joining_count = join_max[joining_cp]
        elseif join_others > 0 then
            joining_cp = join_other[Game.Random(1, join_others)]
            joining_count = join_other[joining_cp]
        end

        if joining_cp ~= nil then
            local text = "<label=diplomacy_mes><imu="..joining_cp..".png><br><label=cpsn_"..joining_cp..">: "..joining_count
            Logic.cur_lu_var("diplomacy", text)
            return true
        end

    end
    return false

end

function do_diplomacy()

    Logic.hero_lu_army_change(joining_cp, joining_count)
    Logic.cur_lu_army_change(joining_cp, -joining_count)
    return false -- no break execution

end