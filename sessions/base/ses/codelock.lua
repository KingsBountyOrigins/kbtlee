
-- Attack.act_custom_event
-- 0 - TODO_SPAWN
-- 1 - TODO_SPAWN_CANCEL_REQUIRED
-- 2 - TODO_CANCEL

--Index
-- see editor.txt -> custevs

--custom event target
-- "$self"
-- "$send"
-- "$tz"
-- "$hero"
-- "$spwn"
-- luname   - SPECIFIC logic unit


function codelock_init () --ftag:action

  Logic.cur_lu_var( "c0", "1" )
  Logic.cur_lu_var( "c1", "2" )
  Logic.cur_lu_var( "c2", "3" )
  Logic.cur_lu_var( "c3", "0" )
  Logic.cur_lu_var( "c4", "4" )

  Logic.cur_lu_var( "r0", "3" )
  Logic.cur_lu_var( "r1", "2" )
  Logic.cur_lu_var( "r2", "1" )
  Logic.cur_lu_var( "r3", "4" )
  Logic.cur_lu_var( "r4", "0" )

  Logic.cur_lu_var( "cur", "" )

  return true
end


function codelock_click () --ftag:action

  local click_pos = Atom.nearest_attachement()
  if click_pos ~= "" then

   local prev_click_pos = Logic.cur_lu_var( "cur" )
   if prev_click_pos == "" then
     Logic.cur_lu_var( "cur", click_pos )
     return true;
   end

   local pimpa_at_prev_pos = Logic.cur_lu_var( "c" .. prev_click_pos )
   local pimpa_at_click_pos = Logic.cur_lu_var( "c" .. click_pos )

   Logic.cur_lu_var( "cur", "" )

   Atom.animate( pimpa_at_prev_pos .. ",move" .. prev_click_pos .. click_pos .. ",idle" .. click_pos )
   Atom.animate( pimpa_at_click_pos .. ",move" .. click_pos .. prev_click_pos .. ",idle" .. prev_click_pos )

   Logic.cur_lu_var( "c" .. prev_click_pos, pimpa_at_click_pos )
   Logic.cur_lu_var( "c" .. click_pos, pimpa_at_prev_pos )

   local solved = Logic.cur_lu_var( "c0" ) == Logic.cur_lu_var( "r0" ) and
                  Logic.cur_lu_var( "c1" ) == Logic.cur_lu_var( "r1" ) and
                  Logic.cur_lu_var( "c2" ) == Logic.cur_lu_var( "r2" ) and
                  Logic.cur_lu_var( "c3" ) == Logic.cur_lu_var( "r3" ) and
                  Logic.cur_lu_var( "c4" ) == Logic.cur_lu_var( "r4" )

   if solved then
     Action.act_custom_event( 0, 10, "$self" )
   end

  end

  return true
end

