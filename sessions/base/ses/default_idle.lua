

function default_idle ( ) --ftag:idle

  Atom.animate( "idle" )

end


function default_smart_idle ( ) --ftag:idle

  local idleanim = "idle";
  if Logic.is_available() then
    local myidle = Logic.cur_lu_var("idle")
    if myidle ~= nil and myidle ~= "" then
      idleanim = myidle
    end
  end
  Atom.animate( idleanim )

end

function no_idle()

end
