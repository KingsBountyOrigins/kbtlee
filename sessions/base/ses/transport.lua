function scn_dirigible_unmoor1 ( param )

    if param == "hvatit" then
      Action.act_custom_event( 0, 7, "$self" ) -- unmoor complete
      Scenario.animate("idle")
      return true
    end

    Scenario.onescape( "hvatit" )
    Scenario.onlocswitch( "hvatit" )
    Scenario.animate("unmoor","hvatit")
    Scenario.playcamtrack("zeppelin_takeoff_default.track", 60)

    return false

end

function scn_dirigible_unmoor2 ( param )

    if param == "hvatit" then
      Action.act_custom_event( 0, 7, "$self" ) -- unmoor complete
      Scenario.animate("idle")
      return true
    end

    Scenario.onescape( "hvatit" )
    Scenario.onlocswitch( "hvatit" )
    Scenario.animate("unmoor","hvatit")
    Scenario.playcamtrack("zeppelin_takeoff_01.track", 60)

    return false

end

function scn_dirigible_moor1 ( param )

    if param == "hvatit" then
      Action.act_custom_event( 0, 9, "$self" ) -- moor complete
      Scenario.animate("idle")
      return true
    end

    Scenario.onescape( "hvatit" )
    Scenario.animate("moor","hvatit")
    Scenario.playcamtrack("zeppelin_landing_default.track")

    return false

end

function scn_dirigible_moor2 ( param )

    if param == "hvatit" then
      Action.act_custom_event( 0, 9, "$self" ) -- moor complete
      Scenario.animate("idle")
      return true
    end

    Scenario.onescape( "hvatit" )
    Scenario.animate("moor","hvatit")
    Scenario.playcamtrack("zeppelin_landing_01.track")

    return false

end

function scn_dirigible_idle ( param )

    Scenario.animate("idle")
    return true

end

function dirigible_scenario ( scn, param ) --ftag:mascn

    local scenario_handlers = {
        unmoor1 = scn_dirigible_unmoor1,
        unmoor2 = scn_dirigible_unmoor2,
        moor1 = scn_dirigible_moor1,
        moor2 = scn_dirigible_moor2,
        idle_dir = scn_dirigible_idle
    }

    local f = scenario_handlers[ scn ]

    if (f ~= nil) then
        return f(param)
    end

end

function scn_train_departure ( param )
    if (param == nil) then
      Scenario.animate("turn","start")
      Scenario.onescape( "start" )

    elseif (param == "start") then
            Scenario.animate("start","start")
        return true
    end
    return false
end

function scn_train_arrival ( param )

    if param~="idle" then
    Scenario.animate("finish","idle")
    Scenario.onescape( "idle" )
  else
    Scenario.animate("idle")
  end
    return true

end

function train_scenario ( scn, param ) --ftag:mascn

    local scenario_handlers = {
      departure = scn_train_departure,
      arrival = scn_train_arrival
    }

    local f = scenario_handlers[ scn ]

    if (f ~= nil) then
        return f(param)
    end

end

function scn_arch_finish ( param )

    if (param == nil) then
      Scenario.animate("finish","end")
      Scenario.onescape( "end" )
      Scenario.playcamtrack("archacopter_finish.track", 60)
    elseif (param == "end") then
            Scenario.animate("idleend","appear")
        return true
    end
    return false
end

function scn_arch_start ( param )

    if param == "hvatit" then
      Action.act_custom_event( 0, 7, "$self" ) -- unmoor complete
      Scenario.animate("idle")
      return true
    end

    Scenario.onescape( "hvatit" )
    Scenario.onlocswitch( "hvatit" )
    Scenario.animate("start","idle")
    Scenario.playcamtrack("archacopter_start.track", 60)
    return false

end

function arch_scenario ( scn, param ) --ftag:mascn

    local scenario_handlers = {
      arch_finish = scn_arch_finish,
      arch_start = scn_arch_start
    }

    local f = scenario_handlers[ scn ]

    if (f ~= nil) then
        return f(param)
    end

end

function scn_submarine_departure ( param )
        if param=="end"
      then
        Game.ProceedWaitingMessages()
        return true
      else
        Scenario.animate("special","end")
        Scenario.onescape( "end" )
            end
   return false
end

function scn_submarine_arrival ( param )

    if (param == nil) then
      Scenario.animate("appear","end")
      Scenario.onescape( "end" )

    elseif (param == "end") then
        Scenario.animate("disappear","appear")
        return true
    end
    return false

end

function submarine_scenario ( scn, param ) --ftag:mascn

    local scenario_handlers = {
      sub_departure = scn_submarine_departure,
      sub_arrival = scn_submarine_arrival
    }

    local f = scenario_handlers[ scn ]

    if (f ~= nil) then
        return f(param)
    end

end

function scn_liner_departure ( param )

      if param=="hvatit"
      then
            Game.ProceedWaitingMessages()
            return true
      else
        Scenario.onescape( "hvatit" )
        Scenario.onlocswitch( "hvatit" )
        Scenario.animate("start","hvatit")
      local rnd=Game.Random(1,100)+1

      if rnd<50 then
        Scenario.playcamtrack("liner_start.track", 60)
      else
        Scenario.playcamtrack("liner_start2.track", 60)
      end

        end
      return false
end

function scn_liner_arrival ( param )

    if (param == nil) then
      Scenario.animate("stop","end")
      local rnd=Game.Random(1,100)+1
      if rnd<50 then
        Scenario.playcamtrack("liner_stop.track", 60)
      else
        Scenario.playcamtrack("liner_stop2.track", 60)
      end

    Scenario.onescape( "end" )

    elseif (param == "end") then
      Game.ProceedWaitingMessages()
      Scenario.animate("idlehide")
      --Scenario.animend()
      return true
    end
    return false

end

function liner_scenario ( scn, param ) --ftag:mascn

    local scenario_handlers = {
      liner_departure = scn_liner_departure,
      liner_arrival = scn_liner_arrival
    }

    local f = scenario_handlers[ scn ]

    if (f ~= nil) then
        return f(param)
    end

end
