function armygen_random( par1, par2, par3, par4 ) --ftag:armygen

  local army = ""
  local count = tonumber( "0" .. par1 ) -- assume par1 is count
  for i=1,count do
    local index = math.floor( Game.Random(0,Logic.cp()) )
    local atom = Logic.cp( index )

    army = Logic.army_add( army, atom, Game.Random(1, 10) )
    -- army = Logic.army_add( army, atom, Game.Random(1, 10), slot )
    -- ���� �� ������ - ����������� � army
  end

  return army
  -- ����|����|����|����|..
end

function text_substr(text_string, NPos, NSimbol) -- ���������� ������ ��: �� ������� N, Q ��������
--  local len = string.len(text_string)
    local result = string.sub(text_string, NPos, NPos+NSimbol-1)

    return result
end

function text_par_count(text_string, delim) -- ���������� ���������� ���������� � ������, ������������� ����� �������

	if delim == nil then delim = "," end
    local number = 1
    local len = string.len(text_string)
    local temp=""

    for i=1,len do
      temp = string.sub(text_string, i,i)
      if temp == delim then
        number=number+1
      end
    end

    return number
end

function text_range_count(text_string) -- ���������� ���������� ���������� � ������, ������������� ����� �������

    local number = 0
    local len = string.len(text_string)
    local temp=""

    for i=1,len do
      temp = string.sub(text_string, i,i)
      if temp == "-" then
        number=number+1
      end
    end

    return number
end

function text_instr(text_string) -- ���������� ������, ���� ���� "="

    local number = false
    local len = string.len(text_string)
    local temp=""

    for i=1,len do
      temp = string.sub(text_string, i,i)
      if temp == "=" then
        number=true
      end
    end

    return number
end

function text_dec(text_string, number, delim) -- ���������� �� ������ ������� Number (0 - �� "=", 1..N ����� �����)

	if text_string == nil or text_string == "" then return "" end
	if delim == nil then delim = "," end

    local start, pos, num = 0
    local len = string.len(text_string)
    local result, temp=""


    if number == nil or number == 0 then
        if text_instr(text_string) then
            pos=string.find(text_string, "=")
            result = string.sub(text_string, 1,pos-1)
        else
            result = text_string
        end
    else

        if (number==1) then
            if text_par_count(text_string, delim)>1 then
                pos = string.find(text_string, delim)-1
            else
                pos=len
            end
            if text_instr(text_string) then
                start = string.find(text_string, "=")+1
            else
                start=1
            end
            result = string.sub(text_string, start,pos)
        end

        if number>1 then
            temp = text_string
            if text_instr(temp) then
                pos = string.find(temp, "=")+1
                temp = string.sub(temp,pos,len)
            end
            for i=1,(number-1) do
                pos = string.find(temp, delim)+1
                temp= string.sub(temp, pos, len)
            end
            pos = string.find(temp, delim)
            if pos~=nil then
                temp = string.sub(temp,1,pos-1)
            end
        result=temp
        end

    end

    len = string.len(result)

    if string.sub(result, 1,1)==" " then
      result=string.sub(result,2,len)
    end

    len = string.len(result)

    if string.sub(result, -1,-1)==" " then
      result=string.sub(result,1,len-1)
    end


--  if text_range_count(result)>0 then
--      result=text_range_ret(result)
--  end

    return result
end

function text_range_ret(text_string,par) -- ���������� ��������� �������� � ���������

    local len = string.len(text_string)
    local pos = string.find(text_string, "-")
    local N,M = 0

    N=tonumber(string.sub(text_string, 1, pos-1))
    M=tonumber(string.sub(text_string, pos+1,len))

		if par~=nil then 
			result = math.floor(Game.Random(N,M+1))
		else 
    	result = math.floor(Game.Mutate(N,M+1,par))
    end 

    return result
end

function text_range_dec(text_string) -- ���������� ��� � ���� ��������

  local len = string.len(text_string)

  local mind, maxd = 0, 0
  local pos = string.find(text_string, "-")

  if pos~= nil then
    mind=tonumber(string.sub(text_string, 1, pos-1))
    maxd=tonumber(string.sub(text_string, pos+1,len))
    return mind,maxd
  else
    mind=tonumber(text_string)
    maxd=tonumber(text_string)
  end

  return mind, maxd

end

function text_substr(text_string, NPos, NSimbol) -- ���������� ������ ��: �� ������� N, Q ��������

    local result = string.sub(text_string, NPos, NPos+NSimbol-1)

    return result
end

function text_instr_ret(text_string) -- ���������� ������-��������
    local len = string.len(text_string)

    local pos=string.find(text_string, "|")
    if pos~=nil then
        result=string.sub(text_string, pos+1,len)
    else
        result="def"
    end

    return result
end

function level_count(level) -- ���������� ������ ��: �� ������� N, Q ��������
    local count={
    "1-4",
    "5-9",
    "10-19",
    "20-49",
    "50-99"}

    result = text_range_ret(count(level))

    return result
end

function text_par_ret(par,text1,text2,text3,text4) -- ���������� ������ ���������� � ����������� par
-- �� ("u","u/rnd=10,11,20-50;c/d=casper,percas","d/rnd=1-100","cort") ������ ������ "u/rnd=10,11,20-50"
-- �������� �� ��, �������� ������ ������� ���������� ��� ��� - �� ��������. ������� ����� ������� � "uplink"
-- ����������� ������ ������ ������ ������ ��� ����� ������ ����� ����� ";"
-- ���� ��������� ����� ���������� �� ������� ������ - ���������� ��������� �� ���

    local result="error"
    local temp=""

    if text1~=nil then
--      if string.sub(text1, 1,1) == par then
        pos=string.find(text1, ";")
        if (pos~= nil) then
            temp=string.sub(text1,1,1)
            if temp==par then
                result=string.sub(text1,1,pos-1)
            end
            temp=string.sub(text1,pos+1,pos+1)
            if temp==par then
                result=string.sub(text1,pos+1,string.len(text1))
            end
        else
            temp=string.sub(text1,1,1)
            if temp==par then
                result=text1
            end
        end
    end

    if text2~=nil then
        pos=string.find(text2, ";")
        if (pos~= nil) then
            temp=string.sub(text2,1,1)
            if temp==par then
                result=string.sub(text2,1,pos-1)
            end
            temp=string.sub(text2,pos+1,pos+1)
            if temp==par then
                result=string.sub(text2,pos+1,string.len(text2))
            end
        else
            temp=string.sub(text2,1,1)
            if temp==par then
                result=text2
            end
        end
    end

    if text3~=nil then
        pos=string.find(text3, ";")
        if (pos~= nil) then
            temp=string.sub(text3,1,1)
            if temp==par then
                result=string.sub(text3,1,pos-1)
            end
            temp=string.sub(text3,pos+1,pos+1)
            if temp==par then
                result=string.sub(text3,pos+1,string.len(text3))
            end
        else
            temp=string.sub(text3,1,1)
            if temp==par then
                result=text3
            end
        end
    end

    if text4~=nil then
        pos=string.find(text4, ";")
        if (pos~= nil) then
            temp=string.sub(text4,1,1)
            if temp==par then
                result=string.sub(text4,1,pos-1)
            end
            temp=string.sub(text4,pos+1,pos+1)
            if temp==par then
                result=string.sub(text4,pos+1,string.len(text4))
            end
        else
            temp=string.sub(text4,1,1)
            if temp==par then
                result=text4
            end
        end
    end

    if result=="error" then
        return nil
    else
        return result
    end
end

function armygen_default( par1, par2, par3, par4 ) --ftag:armygen
-- default unit - ������������ �������������
-- ���-�� - ������������ ������������� ��� �������� ���������� n/d (����������/�������� - def, rnd, fix, prp)
-- def=rnd ��������� ������� ������
-- fix - 1 �������=1 ����. 1-�� ������� ������ ��������� ����. ��������� �� 1 ������ ��� � u/d
-- prp - ���������������. 1-�� ������� ������ ����������. ��������� - ��������� �� ������. �������� �� 2 ������ ��� � u/d
-- ����� ������ - �� ��������� 3-5, �������� ���������� s/d ��� ������������ ��� n/fix
-- �������������� ����� - �������� ���������� u/d. d= fix(def) = rnd
-- fix - �� ����������+1 ���������� ����� ������. � ����� ������ ����� �������� n/d.
-- rnd - 1-�� ������ = 1-3, ���������� ������. ������ ���� ���� �����, �� ������� �������� ���������� ��������� ����������
-- ����������� - ������������� �� ������������, �������� ���������� g/d (����������/��������)

  local army = ""
--  local unit_name=Logic.cur_lu_atom()
  local instr_count=""
  local unit_count=10

  local index = math.floor( Game.Random(0,Logic.cp()) )
  local unit_name = Logic.cp( index )

    local number_unit = text_par_ret("n",par1,par2,par3,par4)
    if number_unit~=nil then -- ���� ������� ������-���������� ��� ���������� ������
        local nu_instr = text_instr_ret(text_dec(number_unit,0)) -- �������� �������� ����������
        unit_count=666
    if (nu_instr=="fix") or (nu_instr=="f") then  -- ���� �������� = fix
      unit_count=100
    end
    if (nu_instr=="r") or (nu_instr=="rnd") or (nu_instr=="def") or (nu_instr=="def") then -- ���� �������� = rnd ��� def
    -- ���������� ������ �������� �� ������
        unit_count=text_rnd(number_unit,par2)
    end
    --unit_count=text_dec(text_par_ret("n",par1,par2,par3,par4), 1)
  else
    if par1~="" then
            if text_range_count(par1)>0 then
                unit_count=text_range_ret(par1)
            else
                unit_count=par1
            end
        else
            unit_count=1000 --Logic.cp_level(unit_name)
        end
  end

    local slot_count = math.floor( Game.Random(3,6))
  local unit_in_slot = unit_count
  -- math.floor(unit_count / slot_count)

  for i=1,slot_count do
    army = Logic.army_add( army, unit_name, unit_in_slot)
    -- army = Logic.army_add( army, atom, Game.Random(1, 10), slot )
    -- ���� �� ������ - ����������� � army

  end

  return army
  -- ����|���-��|����|���-��|..

end

function text_rnd(text,n) -- ���������� �� ���� ��������� �������� ������-������
    local num_elem=tonumber(text_par_count(text))
    local mas={}
    local res1=nil
    local res2=nil
    local res3=nil

    for i=1,num_elem do
        mas[i]=text_dec(text,i)
    end
    res1 = mas[1]--math.floor( Game.Random(1,num_elem+1))]

    return res1 --, res2, res3
end

function tonum(a)
	a = tonumber(a)
	if a == nil then return 0 end
	return a
end


function armyinfo_count_text(count)

	if count < 10 then return "<label=army_troop_few>" end
	if count < 20 then return "<label=army_troop_some>" end
	if count < 50 then return "<label=army_troop_pack>" end
	if count < 100 then return "<label=army_troop_lots>" end
	if count < 250 then return "<label=army_troop_horde>" end
	if count < 1000 then return "<label=army_troop_huge>" end
	return "<label=army_troop_legion>"

end


--���� (1-4) 
--��������� (5-9) 
--������ (10-19) 
--����� (20-49) 
--���� (50-99) 
--����� (100-249) 
--���� (250-499) 
--���� (500-999) 
--������ (1000+)