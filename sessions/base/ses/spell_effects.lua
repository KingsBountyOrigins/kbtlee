function effect_fear_onremove(caa)

	local target=Attack.get_target()
	if target==nil then target = 0 end 
	if Attack.act_size(caa)>1 then
		Attack.log(caa,0.001,"add_blog_nofear_2", "name", blog_side_unit(target,1))
	else
		Attack.log(caa,0.001,"add_blog_nofear_1", "name", blog_side_unit(target,1))
	end 

  return true
end

-- ***********************************************
-- * Corrosion
-- ***********************************************
function effect_corrosion_attack(target,pause, power, duration, options)
        -- options = 0/1 (������� ��� �������)
    if pause==nil then
      pause=1
    end

  if Attack.act_ally(target) or Attack.act_enemy(target) then
  local resist,resistbase = Attack.act_get_res(target,"physical")
  local change_resist=0
-- ��� ��������� �� ��������� � ������� ����������������. �� ������ ��������� ���� ��� ��-�� ������ � ����
    if duration==nil then
        duration = tonumber(Logic.obj_par("effect_corrosion","duration"))
    end
    if power==nil then
      power = tonumber(Logic.obj_par("effect_corrosion","power"))
    end
    if power>100 then power=100 end

        if options==nil then options=0 end
        if options==0 then
            change_resist=power*resist/100
        else
            change_resist=power*resistbase/100
        end

-- �������� ������������!
--    Attack.act_del_spell(target,"effect_corrosion")

    Attack.act_apply_spell_begin( target, "effect_corrosion", duration, false )
      Attack.act_apply_res_spell( "physical", -change_resist, 0, 0, duration, false)
    Attack.act_apply_spell_end()

--    Attack.atom_spawn(target, pause, "effect_poison")

  end

  return true
end

-- ***********************************************
-- * Shock
-- ***********************************************
function effect_shock_attack(target,pause,duration)

  --local target = Attack.get_target()
  if pause==nil then
    pause = 1
  end
  if target==nil then
    target = Attack.get_target()
  end
  if duration==nil then
    duration = tonumber(Logic.obj_par("effect_shock","duration"))
  end

  if (Attack.act_ally(target) or Attack.act_enemy(target)) and not Attack.act_feature(target,"boss,pawn") then
    --local duration = tonumber(Attack.get_custom_param("duration"))
    local inbonus = tonumber(Logic.obj_par("effect_shock","inbonus"))
    local speedbonus = tonumber(Logic.obj_par("effect_shock","speedbonus"))
--    local speed = Attack.act_get_par(target, "speed")

--    local change_speed = speed - speedbonus

    Attack.act_del_spell(target,"effect_shock")

		Attack.act_ap( target, speedbonus)
    Attack.act_apply_spell_begin( target, "effect_shock", duration, false )
        Attack.act_apply_par_spell( "initiative", -inbonus, 0, 0, duration, false)
    Attack.act_apply_spell_end()
    Attack.resort()
		
		Attack.atom_spawn(target, pause, "hll_post_archmage_lighting",0,true)
		Attack.act_damage_addlog(target,"add_blog_shock_")
  end

  return true
end

-- ***********************************************
-- * ������ ���� �� ������
-- ***********************************************

function effect_holy_attack(target,pause,duration,power)
		
    if power==nil then power=tonumber(Logic.obj_par("effect_holy","power")) end  -- ��������� �����
    if duration==nil then duration=tonumber(Logic.obj_par("effect_holy","duration")) else duration = tonumber(duration) end 
		if pause==nil then pause=0 end 
	if target==nil then
    target = Attack.get_target()
  end
		
			Attack.act_del_spell( target, "effect_holy" )
      Attack.act_apply_spell_begin(target, "effect_holy", duration, false )
				Attack.act_apply_par_spell("attack", 0, -power, 0, duration, false)
				Attack.act_apply_par_spell("defense", 0, -power, 0, duration, false)
      Attack.act_apply_spell_end()
      Attack.act_damage_addlog(target,"add_blog_holy_")
			Attack.atom_spawn(target, pause, "effect_hard_bless",0,true)
  return true
end

-- ***********************************************
-- * Sleep
-- ***********************************************
function effect_sleep_attack(target,pause,duration,dod)

  --local target = Attack.get_target()
  if pause==nil then
    pause = 1
  end
  if duration==nil then
    duration = tonumber(Logic.obj_par("effect_sleep","duration"))
  end
  if dod==nil then
    dod = true
  end
	if target~=nil then
  if (Attack.act_ally(target) or Attack.act_enemy(target)) and not Attack.act_feature(target,"boss,pawn") then
    --local duration = tonumber(Attack.get_custom_param("duration"))
    --Attack.act_del_spell(target,"effect_sleep")
    Attack.act_apply_spell_begin( target, "effect_sleep", duration, dod )
			Attack.act_skipmove(target,duration)
    Attack.act_apply_spell_end()
--    Attack.act_damage_addlog(target,"add_blog_sleep_")
    Attack.atom_spawn(target, pause, "effect_sleep",Attack.angleto(target),true)
  end
  else if target==nil then
  		target = Attack.get_target()
  		if Attack.act_need_cure(target ) and string.find(Attack.act_name(target ),"bear") then
  			local cur_hp=Attack.act_hp(target )
  			local max_hp=Attack.act_get_par(target ,"health")
  			local need_cure=max_hp-cur_hp
  			Attack.act_cure(target ,need_cure)
  			Attack.atom_spawn(target , 0, "effect_total_cure")
  		end 
  	end
  end

  return true
end

function sleep_onremove(caa,duration_end)

	--if string.sub(Attack.act_name(caa),1,4) == "bear" then
		Attack.act_skipmove(caa, 0)
		if duration_end~=true then --and not Attack.act_is_spell(caa,"effect_sleep") then 
			if Attack.act_size(caa)>1 then 
				Attack.log(caa,0.002,"add_blog_nosleep_2","name",blog_side_unit(caa))
			else 
				Attack.log(caa,0.002,"add_blog_nosleep_1","name",blog_side_unit(caa))
			end 
		end 
	--end
	return true

end

-- ***********************************************
-- * Slow
-- ***********************************************
function effect_slow_attack(target,pause,duration)

--  local target = Attack.get_target()
    if pause==nil then
      pause=1
    end

  if (Attack.act_ally(target) or Attack.act_enemy(target)) and not Attack.act_feature(target,"boss,pawn") then
    --local duration = tonumber(Attack.get_custom_param("duration"))
    if duration==0 or duration==nil then duration = tonumber(Logic.obj_par("effect_slow","duration")) end
    local speedbonus = tonumber(Logic.obj_par("effect_slow","speedbonus"))
    local speed = Attack.act_get_par(target, "speed")

    --if speed == 1 then speedbonus = 0 end

    Attack.act_del_spell(target,"spell_haste")
    Attack.act_del_spell(target,"spell_slow")

    Attack.act_apply_spell_begin( target, "spell_slow", duration, false )
        Attack.act_apply_par_spell( "speed", -speedbonus , 0, 0, duration, false)
    Attack.act_apply_spell_end()
      Attack.act_damage_addlog(target,"add_blog_slow_")
    Attack.atom_spawn(target, pause, "magic_slow",0,true)
  end

  return true

end

-- ***********************************************
-- * Poison
-- ***********************************************
function effect_poison_attack(target,pause, duration)

  if pause==nil then
    pause=1
  end
  local typedmg=Logic.obj_par("effect_poison","typedmg")
	
	if target~=nil and --[[(Attack.act_ally(target) or Attack.act_enemy(target))]] Attack.get_caa(target)~=nil then
    local poisonresist = Attack.act_get_res(target,"poison")
    if (poisonresist<80) and not Attack.act_feature(target,"boss,poison_immunitet") then

      if duration==nil then
        duration = tonumber(Logic.obj_par("effect_poison","duration"))
      end
      
    	local dmg_min,dmg_max = text_range_dec(Logic.obj_par("effect_poison","damage"))

      local power = tonumber(Logic.obj_par("effect_poison","power"))
      local attack = Attack.act_get_par(target, "attack")

      Attack.act_del_spell(target,"effect_poison")

      Attack.act_apply_spell_begin( target, "effect_poison", duration, false )
      Attack.act_apply_par_spell( "attack", -attack/100*power , 0, 0, duration, false)
      Attack.act_apply_spell_end()
   	  Attack.act_spell_param(target, "effect_poison", "dmg_min", dmg_min,"dmg_max", dmg_max)
			
      Attack.atom_spawn(target, pause, "effect_poison",0,true)
      Attack.act_damage_addlog(target,"add_blog_poison_")
    end
  else if target==nil then
  		  target = Attack.get_target()
  		  if Attack.get_caa(target) == nil then return true end
			
			if target~=nil then
  			local dmg_min=tonumber(Attack.act_spell_param(target, "effect_poison", "dmg_min"))
  			local dmg_max=tonumber(Attack.act_spell_param(target, "effect_poison", "dmg_max"))
  			-- ��� ��������� ������ ����� ���� ������� ���������� 
				Attack.act_del_spell(target,"effect_charm")

  			Attack.atk_set_damage(typedmg,dmg_min,dmg_max)
  			Attack.atom_spawn(target, 0, "effect_poison",0,true)
  			common_cell_apply_damage(target, 1)
  		end 	
			local count=Attack.act_size(target)    
			local damage,dead=Attack.act_damage_results(target)
			if dead==nil then dead=0 end
--add_blog_trap_2=^blog_td0^[spell] ����������� � ������� [damage] �����. [target]: [dead] ��������. [troopdef]	
			local td=""
			if damage ~= nil then
				if dead>0 then
					if count==dead then td="<label=troop_defeated>" end
	    			Attack.log("add_blog_dampoison_2","damage",damage,"dead",dead,"name",blog_side_unit(target,-1),"troopdef",td)
	  			else
	  				Attack.log("add_blog_dampoison_1","damage",damage,"dead",dead,"name",blog_side_unit(target,-1),"troopdef",td)
	  			end
			end
  	end
  end

  return true
end


-- ***********************************************
-- * Burn
-- ***********************************************
function effect_burn_attack(target, pause, duration)

  --local target = Attack.get_target()
	if pause==nil then
		pause=1
	end
	
	if target~=nil and Attack.get_caa(target)~=nil then
		local fireresist = Attack.act_get_res(target,"fire")
		if fireresist<80 and not Attack.act_feature(target,"boss,fire_immunitet") then
			--local duration = tonumber(Attack.get_custom_param("duration"))
		
			if duration==nil then
				duration = tonumber(Logic.obj_par("effect_burn","duration"))
			end
			local power = tonumber(Logic.obj_par("effect_burn","power"))
			local defense = Attack.act_get_par(target, "defense")
			local dmg_min,dmg_max = text_range_dec(Logic.obj_par("effect_burn","damage"))

			Attack.act_del_spell(target,"effect_burn")
		
			Attack.act_apply_spell_begin( target, "effect_burn", duration, false )
			Attack.act_apply_par_spell( "defense", -defense/100*power , 0, 0, duration, false)
			Attack.act_apply_spell_end()
			Attack.act_spell_param(target, "effect_burn", "dmg_min", dmg_min,"dmg_max", dmg_max)
		
			Attack.atom_spawn(target, pause, "effect_burn",0,true)
			Attack.act_damage_addlog(target,"add_blog_burn_")
		end
	else if target==nil then
	  target = Attack.get_target()
	  if Attack.get_caa(target) ~= nil then
		local typedmg=Logic.obj_par("effect_burn","typedmg")
		local dmg_min=tonumber(Attack.act_spell_param(target, "effect_burn", "dmg_min"))
		local dmg_max=tonumber(Attack.act_spell_param(target, "effect_burn", "dmg_max"))
			-- ��� ��������� ������ ����� ����� ������� ���������� 
			Attack.act_del_spell(target,"effect_charm")
		
		Attack.atk_set_damage(typedmg,dmg_min,dmg_max)
		Attack.atom_spawn(target, 0, "effect_burn",0,true)
		common_cell_apply_damage(target, 1)  
		
		local count=Attack.act_size(target)    
		local damage,dead=Attack.act_damage_results(target)
		--add_blog_trap_2=^blog_td0^[spell] ����������� � ������� [damage] �����. [target]: [dead] ��������. [troopdef]	
		local td=""
		if dead>0 then
			if count==dead then td="<label=troop_defeated>" end
				Attack.log("add_blog_damfire_2","damage",damage,"dead",dead,"name",blog_side_unit(target,-1),"troopdef",td)
			else
				Attack.log("add_blog_damfire_1","damage",damage,"dead",dead,"name",blog_side_unit(target,-1),"troopdef",td)
			end 
		end 
	  end
	end

	return true

end

-- ***********************************************
-- * Stun
-- ***********************************************
function effect_stun_attack(target,pause,duration)

  --local target = Attack.get_target()
  if pause==nil then
    pause = 1
  end
  if target==nil then
    target = Attack.get_target()
  end
	if duration==nil then
		local duration = tonumber(Logic.obj_par("effect_stun","duration"))
	end 
  if (Attack.act_ally(target) or Attack.act_enemy(target)) and not Attack.act_feature(target,"boss,pawn") then
    --local duration = tonumber(Attack.get_custom_param("duration"))
    
    local inbonus = tonumber(Logic.obj_par("effect_stun","inbonus"))
    local speedbonus = tonumber(Logic.obj_par("effect_stun","speedbonus"))
--    local speed = Attack.act_get_par(target, "speed")

--    local change_speed = speed - speedbonus

    Attack.act_del_spell(target,"effect_stun")

    Attack.act_apply_spell_begin( target, "effect_stun", duration, false )
      Attack.act_apply_par_spell( "disreload", 10, 0, 0, duration, false)
      Attack.act_apply_par_spell( "disspec", 10, 0, 0, duration, false)
        Attack.act_apply_par_spell( "initiative", -inbonus, 0, 0, duration, false)
        Attack.act_apply_par_spell( "speed", -speedbonus , 0, 0, duration, false)
    Attack.act_apply_spell_end()
		 Attack.act_damage_addlog(target,"add_blog_stun_")
    Attack.atom_spawn(target, pause, "effect_stun",0,true)
  end

  return true
end

-- ***********************************************
-- * Weakness
-- ***********************************************
function effect_weakness_attack(target,pause,duration)

  --local target = Attack.get_target()
  if pause==nil then
    pause = 1
  end
  if target==nil then
    target = Attack.get_target()
  end
	if duration==nil then
		local duration = tonumber(Logic.obj_par("effect_weakness","duration"))
	end 
  if (Attack.act_ally(target) or Attack.act_enemy(target)) and not Attack.act_feature(target,"boss,pawn") then
    
    local power = tonumber(Logic.obj_par("effect_weakness","power"))

    Attack.act_del_spell(target,"effect_weakness")

    Attack.act_apply_spell_begin( target, "effect_weakness", duration, false )
        Attack.act_apply_par_spell( "attack", 0, -power, 0, duration, false)
    Attack.act_apply_spell_end()
		 Attack.act_damage_addlog(target,"add_blog_weakness_")
    Attack.atom_spawn(target, pause, "effect_weakness",0,true)
  end

  return true
end

-- ***********************************************
-- * Freeze
-- ***********************************************
function effect_freeze_attack(target,pause,duration)

--  local target = Attack.get_target()
    if pause==nil then
      pause=1
    end
  if target==nil then
    target = Attack.get_target()
  end

  if Attack.get_caa(target) == nil then return true end

  if (Attack.act_ally(target) or Attack.act_enemy(target)) and not Attack.act_feature(target,"boss,pawn") then
    --local duration = tonumber(Attack.get_custom_param("duration"))
    if duration==0 or duration==nil then duration = tonumber(Logic.obj_par("effect_freeze","duration")) end
    local speedbonus = tonumber(Logic.obj_par("effect_freeze","speedbonus"))
    local speed = Attack.act_get_par(target, "speed")

    --if speed == 1 then speedbonus = 0 end
    Attack.resort()

    Attack.act_del_spell(target,"effect_freeze")

    Attack.act_apply_spell_begin( target, "effect_freeze", duration, false )
        Attack.act_apply_par_spell( "speed", -speedbonus , 0, 0, duration, false)
    Attack.act_apply_spell_end()
		Attack.act_damage_addlog(target,"add_blog_freeze_")
    Attack.atom_spawn(target, pause, "effect_freeze",0,true)
  end

  return true

end

-- ***********************************************
-- * Curse 
-- ***********************************************
function effect_curse_attack(target,pause,duration)

  --local target = Attack.get_target()
  if pause==nil then
    pause = 0
  end
  if target==nil then
    target = Attack.get_target()
  end
	if duration==nil then
    duration = tonumber(Logic.obj_par("effect_curse","duration"))
  end
  local power = tonumber(Logic.obj_par("effect_curse","power"))  
  
  if (Attack.act_ally(target) or Attack.act_enemy(target)) and not Attack.act_feature(target,"boss,pawn") then
    --local duration = tonumber(Attack.get_custom_param("duration"))

    Attack.act_del_spell(target,"effect_curse")
    Attack.act_apply_spell_begin( target, "effect_curse", duration, false )
			Attack.act_apply_par_spell( "moral", -power, 0, 0, duration, false)
    Attack.act_apply_spell_end()
			Attack.act_damage_addlog(target,"add_blog_curse_")
    Attack.atom_spawn(target, pause, "effect_curse",0,true)
  end

  return true
end

-- ***********************************************
-- * Charm
-- ***********************************************
function effect_charm_attack(target,pause,duration,effect)

  --local target = Attack.get_target()
  if pause==nil then pause = 1 end
  if target==nil then target = Attack.get_target() end
  if duration==nil then duration = tonumber(Logic.obj_par("effect_charm","duration")) end
  if effect==nil then effect="effect_charm" end 
  
  if (Attack.act_ally(target) or Attack.act_enemy(target)) and not Attack.act_feature(target,"boss,pawn,mind_immunitet") then
    --local duration = tonumber(Attack.get_custom_param("duration"))

    Attack.act_del_spell(target,"effect_charm")
    Attack.act_apply_spell_begin( target, "effect_charm", duration, false )
			Attack.act_spell_param(target, "effect_charm", "effect", effect)
    Attack.act_apply_spell_end()

--	  local bl=1
  	--if Attack.act_belligerent(target) == 1 or Attack.act_belligerent(target) == 2 then bl = 4 end
    local bl = Attack.act_belligerent()
    if bl == 0 then bl = 4 end
    Attack.act_belligerent(target, bl)

    Attack.atom_spawn(target, pause, effect, Attack.angleto(target), true)
    return true
  end

  return false
end

function effect_charm_onremove(caa)

  -- ���������� ��� ��� ����
  Attack.act_belligerent(caa, Attack.act_belligerent( caa, nil ))
  --local effect = Attack.act_spell_param(caa, "effect_charm", "effect")
  --if effect==nil then effect = "effect_charm" end 
  --Attack.atom_spawn(caa, 1, effect, 0, true)

			if Attack.act_size(caa)>1 then 
				Attack.log(caa,0.001,"add_blog_nocharm_2","name",blog_side_unit(caa))
			else 
				Attack.log(caa,0.001,"add_blog_nocharm_1","name",blog_side_unit(caa))
			end 

  return true

end

-- ***********************************************
-- * Shock
-- ***********************************************
function effect_blood_attack(target,pause,duration)

  --local target = Attack.get_target()
  if pause==nil then
    pause = 1
  end
  if target==nil then
    target = Attack.get_target()
  end
  if duration==nil then
    duration = tonumber(Logic.obj_par("effect_blood","duration"))
  end

  if (Attack.act_ally(target) or Attack.act_enemy(target)) and not Attack.act_feature(target,"boss,pawn") then
    --local duration = tonumber(Attack.get_custom_param("duration"))
    local inbonus = tonumber(Logic.obj_par("effect_blood","inbonus"))
    local moral = tonumber(Logic.obj_par("effect_blood","moral"))

    Attack.act_del_spell(target,"effect_blood")

    Attack.act_apply_spell_begin( target, "effect_blood", duration, false )
        Attack.act_apply_par_spell( "initiative", inbonus, 0, 0, duration, false)
        Attack.act_apply_par_spell( "moral", moral, 0, 0, duration, false)
    Attack.act_apply_spell_end()

    Attack.atom_spawn(target, pause, "magic_haste")
  end

  return true
end

-- ***********************************************
-- * OOC
-- ***********************************************
function effect_ooc()

	for i=0,Attack.get_targets_count()-1 do
		local target = Attack.get_target(i)
		Attack.atom_spawn(target, 0, "effect_out_of_control", 0)
		Attack.log(.01, "out_of_control_log", "special", "<label=cpsn_"..Attack.act_name(target)..">")
	end

	return true

end

function effect_buc()

	for i=0,Attack.get_targets_count()-1 do
		local target = Attack.get_target(i)
		Attack.atom_spawn(target, 0, "effect_control", 0)
		Attack.log(.01, "return_control_log", "special", "<label=cpsn_"..Attack.act_name(target)..">")
	end

	return true

end
