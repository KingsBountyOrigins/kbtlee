
timeline {

  1.0=black_off

  1.0=speech1
  1.0=fade_out_page1
  1.5=fade_out_text
  2.0=fade_in_page2
  2.5=set_text2
  2.5=fade_in_text

  2.5=speech2
  2.5=fade_out_page2
  3.0=fade_out_text
  3.5=fade_in_page3
  4.0=set_text3
  4.0=fade_in_text

  4.0=speech3
  4.0=fade_out_page3
  4.5=fade_out_text
  5.0=fade_in_page4
  5.5=set_text4
  5.5=fade_in_text
  5.5=speech4

  5.6=black_on
  7.6=endez

}


init {

  alpha=255
  time=0
  ctl=intro_black
  music=

}


init {

  alpha=255
  time=0
  ctl=intro_page1

}

init {

  alpha=0
  time=0
  ctl=intro_page2

}

init {

  alpha=0
  time=0
  ctl=intro_page3

}

init {

  alpha=0
  time=0
  ctl=intro_page4

}


black_off {

  alpha=0
  time=2.0
  ctl=intro_black

}

black_on {

  alpha=255
  time=2.0
  ctl=intro_black

}

fade_out_text {
  ctl=intro_text
  alpha=0
  time=1.0
}

fade_in_text {
  ctl=intro_text
  alpha=255
  time=1.0
}


fade_out_page1 {
  checkpoint=1
  ctl=intro_page1
  alpha=0
  time=1.0
}

fade_in_page2 {
  ctl=intro_page2
  alpha=255
  time=1.0
}

fade_out_page2 {
  checkpoint=1
  ctl=intro_page2
  alpha=0
  time=1.0
}

fade_in_page3 {
  ctl=intro_page3
  alpha=255
  time=1.0
}
fade_out_page3 {
  checkpoint=1
  ctl=intro_page3
  alpha=0
  time=1.0
}

fade_in_page4 {
  ctl=intro_page4
  alpha=255
  time=1.0
}


fade_in_page4 {
  ctl=intro_page4
  alpha=255
  time=1.0
}

set_text2 {
  ctl=intro_text
  text=intro_text2
}

set_text3 {
  ctl=intro_text
  text=intro_text3
}

set_text4 {
  ctl=intro_text
  text=intro_text4
}


speech1 {
  sound=intro_speech_1
}

speech2 {
  sound=intro_speech_2
}

speech3 {
  sound=intro_speech_3
}

speech4 {
  sound=intro_speech_4
}

endez {
  end=1
}
