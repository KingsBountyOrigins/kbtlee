
-- reasons:
-- 0 - initiate
-- 1 - anim ends
-- 2 - anim sequence ends


function scn_handle_idle( reason )

    Atom.animate( "idle" );
    Atom.setparam( "" );

    return true
end

function scn_handle_walk( reason )

    if ( reason == 0 ) then

        if (Atom.cpscenario("scn_walk")) then
            -- already walking. stop walking!
            local par = Atom.getparam()

            if (par == "0" or par == "1") then
                Atom.setparam( "100" );
            end
            return true;
        end

        if (Atom.cpfreedirection() == 0) then

        else

            Atom.animate( "start" );
            Atom.setparam( "0" );

        end

    elseif ( reason == 1 ) then

        local par = Atom.getparam()

        if (par == "l") then

            Atom.cpdirection( Atom.cpdirection() - 1);
            par = "0"
        elseif (par == "r") then

            Atom.cpdirection( Atom.cpdirection() + 1);
            par = "0"
        end

        if (par == "0" or par == "1") then

            Atom.move();
            local ainf = Atom.cpfreedirection()

            if (ainf ~= 0) then
                Atom.animate( "move" );
                Atom.setparam( "1" );
            else
                -- check other directions

                local dir = Atom.cpdirection()
                local fdl = Atom.cpfreedirection(dir-1)
                local fdr = Atom.cpfreedirection(dir+1)

                if (fdl~=0) then
                    Atom.move(0,1);
                    Atom.setparam( "l" );
                    return true
                end

                if (fdr~=0) then
                    Atom.move(0,0);
                    Atom.setparam( "r" );
                    return true
                end


                Atom.animate( "stop" );
                Atom.setparam( "2" );
                return true
            end

        elseif (par == "2") then
            return false
        elseif (par == "100") then
            Atom.move();
            Atom.animate( "stop" );
            Atom.setparam( "2" );
            return true
        end
    end

    return true
end



function scn_handle_rotleft( reason )

    if ( reason == 0 ) then

        Atom.animate( "contra" );
        return true

    elseif ( reason == 1 ) then

        Atom.cpdirection( Atom.cpdirection() - 1);
        return false
    end
end

function scn_handle_rotright( reason )

    if ( reason == 0 ) then

        Atom.animate( "clock" );
        return true

    elseif ( reason == 1 ) then

        Atom.cpdirection( Atom.cpdirection() + 1);
        return false
    end
end

function scn_handle_rare( reason )

    if ( reason == 0 ) then

        Atom.animate( "rare" );
        return true

    elseif ( reason == 1 ) then

        return false
    end
end

function scn_handle_cast( reason )

    if ( reason == 0 ) then

        Atom.aseq( "cast" )
        return true

    else
        return false
    end
end

function scn_handle_rush( reason )

    if ( reason == 0 ) then

--        Atom.aseq( "slew", 3 )
--        Atom.aseq( "start", 0, true )
--        Atom.aseq( "stop" )
--        Atom.aseq( "slew", 3 )
--        Atom.aseq( "start", 0, true )
        Atom.aseq( "rush" )
        return true

    else
        return false
    end
end

function scn_handle_attack( reason )

    if ( reason == 0 ) then

        -- Atom.aseq( "slew", 3 )
        -- Atom.aseq( "slew", 3 )
        Atom.aseq( "attack" )
        return true

    else
        return false
    end
end

function scn_handle_damage( reason )

    if ( reason == 0 ) then

        Atom.aseq( "damage" )
        return true

    else
       return false
    end
end

function scn_handle_slew( reason )

    if ( reason == 0 ) then

        Atom.aseq( "slew" )
        return true

    else
       return false
    end
end


function scn_handle_all_walk( reason )

    if ( reason == 0 ) then

        Atom.animate( "start" );
        Atom.setparam( "a" );
        return true

    elseif ( reason == 1 ) then

        local par = Atom.getparam()
        if (par == "a") then

            Atom.move();
            Atom.animate( "move" );
            Atom.setparam( "b" );

        elseif (par == "b") then

            Atom.move();
            Atom.move(0,1);
            Atom.setparam( "c" );

        elseif (par == "c") then

            Atom.cpdirection( Atom.cpdirection() - 1);
            Atom.move();
            Atom.animate( "stop" );
            Atom.setparam( "d" );

        elseif (par == "d") then
            Atom.animate( "clock" );
            Atom.setparam( "e" );

        elseif (par == "e") then
            Atom.cpdirection( Atom.cpdirection() + 1);
            Atom.animate( "attack" );
            Atom.setparam( "f" );

        elseif (par == "f") then
            Atom.animate( "start" );
            Atom.setparam( "g" );

        elseif (par == "g") then
            Atom.move();
            Atom.move(0,0);
            Atom.setparam( "h" );

        elseif (par == "h") then
            Atom.cpdirection( Atom.cpdirection() + 1);
            Atom.move();
            Atom.animate( "move" );
            Atom.setparam( "i" );

        elseif (par == "i") then
            Atom.move();
            Atom.animate( "rush" );
            Atom.setparam( "j" );

        elseif (par == "j") then
            Atom.animate( "damage" );
            Atom.setparam( "k" );

        elseif (par == "k") then
            Atom.animate( "cast" );
            Atom.setparam( "l" );

        elseif (par == "l") then
            Atom.animate( "contra" );
            Atom.setparam( "m" );

        elseif (par == "m") then
            Atom.cpdirection( Atom.cpdirection() - 1);
            Atom.animate( "contra" );
            Atom.setparam( "n" );

        elseif (par == "n") then
            Atom.cpdirection( Atom.cpdirection() - 1);
            Atom.animate( "contra" );
            Atom.setparam( "o" );

        elseif (par == "o") then
            Atom.cpdirection( Atom.cpdirection() - 1);
            Atom.animate( "start" );
            Atom.setparam( "q" );

        elseif (par == "q") then
            Atom.move();
            Atom.move(0,1);
            Atom.setparam( "r" );

        elseif (par == "r") then
            Atom.cpdirection( Atom.cpdirection() - 1);
            Atom.move();
            Atom.animate( "move" );
            Atom.setparam( "s" );

        elseif (par == "s") then
            Atom.move();
            Atom.animate( "move" );
            Atom.setparam( "t" );

        elseif (par == "t") then
            Atom.move();
            Atom.animate( "move" );
            Atom.setparam( "u" );

        elseif (par == "u") then
            Atom.move();
            Atom.animate( "stop" );
            Atom.setparam( "v" );

        elseif (par == "v") then
            Atom.animate( "slew" );
            Atom.setparam( "w" );

        elseif (par == "w") then
            Atom.cpdirection( Atom.cpdirection() + 3);
            return false
        end
        return true
    end
end


function scn_handle_all_fly( reason )

    if ( reason == 0 ) then

        Atom.animate( "takeoff" );
        Atom.setparam( "a" );
        return true

    elseif ( reason == 1 ) then

        local par = Atom.getparam()
        if (par == "a") then

            Atom.move();
            Atom.animate( "flight" );
            Atom.setparam( "b" );

        elseif (par == "b") then

            Atom.move();
            Atom.move(1,1);
            Atom.setparam( "c" );

        elseif (par == "c") then

            Atom.cpdirection( Atom.cpdirection() - 1);
            Atom.move();
            Atom.animate( "descent" );
            Atom.setparam( "d" );

        elseif (par == "d") then
            Atom.animate( "clock" );
            Atom.setparam( "e" );

        elseif (par == "e") then
            Atom.cpdirection( Atom.cpdirection() + 1);
            Atom.animate( "attack" );
            Atom.setparam( "f" );

        elseif (par == "f") then
            Atom.animate( "takeoff" );
            Atom.setparam( "g" );

        elseif (par == "g") then
            Atom.move();
            Atom.move(1,0);
            Atom.setparam( "h" );

        elseif (par == "h") then
            Atom.cpdirection( Atom.cpdirection() + 1);
            Atom.move();
            Atom.animate( "flight" );
            Atom.setparam( "i" );

        elseif (par == "i") then
            Atom.move();
            Atom.animate( "descent" );
            Atom.setparam( "j" );

        elseif (par == "j") then
            Atom.animate( "damage" );
            Atom.setparam( "k" );

        elseif (par == "k") then
            Atom.animate( "cast" );
            Atom.setparam( "l" );

        elseif (par == "l") then
            Atom.animate( "contra" );
            Atom.setparam( "m" );

        elseif (par == "m") then
            Atom.cpdirection( Atom.cpdirection() - 1);
            Atom.animate( "contra" );
            Atom.setparam( "n" );

        elseif (par == "n") then
            Atom.cpdirection( Atom.cpdirection() - 1);
            Atom.animate( "contra" );
            Atom.setparam( "o" );

        elseif (par == "o") then
            Atom.cpdirection( Atom.cpdirection() - 1);
            Atom.animate( "takeoff" );
            Atom.setparam( "q" );

        elseif (par == "q") then
            Atom.move();
            Atom.move(1,1);
            Atom.setparam( "r" );

        elseif (par == "r") then
            Atom.cpdirection( Atom.cpdirection() - 1);
            Atom.move();
            Atom.animate( "waft" );
            Atom.setparam( "s" );

        elseif (par == "s") then
            Atom.move();
            Atom.move();
            Atom.move();
            Atom.animate( "descent" );
            Atom.setparam( "v" );

        elseif (par == "v") then
            Atom.animate( "slew" );
            Atom.setparam( "w" );

        elseif (par == "w") then
            Atom.cpdirection( Atom.cpdirection() + 3);
            return false
        end
        return true
    end
end


function default_scenario ( scn, reason ) --ftag:escn

    local scenario_handlers = {
        scn_idle = scn_handle_idle,
        scn_walk = scn_handle_walk,
        scn_attack = scn_handle_attack,
        scn_cast = scn_handle_cast,
        scn_rot_left = scn_handle_rotleft,
        scn_rot_right = scn_handle_rotright,
        scn_rare = scn_handle_rare,
        scn_damage = scn_handle_damage,
        scn_all = scn_handle_all_walk,
        scn_all_fly = scn_handle_all_fly,
        scn_rush = scn_handle_rush,
        scn_slew = scn_handle_slew
    }

    local f = scenario_handlers[scn]

    if (f ~= nil) then
        return f(reason)
    end

    return false
end

