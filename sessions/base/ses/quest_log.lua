function quest_caption()
	local caption="<label=hint_Def_font>"
end 

function reward_text_generator( name, what, count )
  -- what:
  -- 0 - count
  -- 1 - limit
  -- 2 - object
  -- 3 - scroll

  if what == 0 and (string.find(name,"rune")) then
    return " <image=Quests_icon_"..name..".png><label=reward_"..name .. "> ("..tostring(count).." <label=reward_count>)"
  end 
  if what == 0 and (string.find(name,"crystals")) then
    return " <image=Quests_icon_crystal.png><label=reward_"..name .. "> ("..tostring(count).." <label=reward_count>)"
  end 
  if what == 0 and (string.find(name,"money")) then
    --return " <image=Quests_icon_money.png> "..tostring(count).." <label=reward_"..name .. ">"
    return " <image=Quests_icon_money.png><label=reward_"..name .. "> ("..tostring(count).." <label=reward_money_count>)"
  end 
  if what == 0 then
    return " "..tostring(count).." <label=reward_"..name .. ">"
  end 
  if what == 1 then
    return " +"..tostring(count).." <label=reward_"..name.. ">"
  end 
  if what == 2 and count == 1 then
    return " <label=itm_"..name .. "_name>"
  end 
  if what == 2 and count > 1 then
    return " <label=itm_"..name .. "_name> (" .. tostring(count).." <label=reward_count>)"
  end 
  if what == 3 and count == 1 then
    return " <image=Quests_icon_scroll.png><label=reward_scroll1> '<label="..name .. "_name>'"
  end 
  if what == 3 and count > 1 then
    return " <image=Quests_icon_scroll.png><label=reward_scroll> '<label="..name .. "_name>' ("..tostring(count).." <label=reward_count>)"
  end 

  return "no reward string found"
end