-- ***********************************************
-- * ���������� ��������� �����
-- ***********************************************
function HInt()
    return Logic.get_hero_intellect()
end


-- ******************************************************************
-- * ������������� ������� ������ ����� � ������ ������ � ����������
-- ******************************************************************

function pwr_dragon_arrow(level)

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end
		
		-- �� ������ 10 ���������� +1 ������ �� � ����� �� ������ 5 �����
		local int_bonus=math.floor(HInt()/tonumber(Game.Config("spell_power_config/int_duration")))
		local count = tonumber("0" .. text_dec(Logic.obj_par("spell_dragon_arrow","count"),level))+int_bonus
	  if count>5 then int_bonus = 5 end 
  			
    return count

end


function pwr_necromancy(level)

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

	  local int_power = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
	  if Attack.act_belligerent() == 1 then 
	  	int_power=int_power+tonumber(Logic.hero_lu_skill("necromancy"))*10/100
	  end 
  	local power = tonumber("0" .. text_dec(Logic.obj_par("spell_necromancy","power"),level))
		
    power = math.ceil(power*int_power)

    return power

end

function pwr_trap(level)

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end
	
		   local min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_trap","damage"))
        local destroyer_skill=tonumber(skill_power2("destroyer",1)) --+Logic.hero_lu_item("sp_spell_attack","count")
        
        local lvl_dmg = tonumber(Logic.obj_par("spell_trap","lvl_dmg"))
        local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
        int_dmg = int_dmg * (1+math.floor(HInt()/7)/10) --�� ������ 7 ���������� +10% �����
        
        min_dmg = math.floor(min_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
        max_dmg = math.floor(max_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
				
				min_dmg=dmg_round(min_dmg)
				max_dmg=dmg_round(max_dmg)
				
        return min_dmg,max_dmg

end


function pwr_kamikaze(level)

  local min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_kamikaze","damage"))

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local lvl_dmg = tonumber(Logic.obj_par("spell_kamikaze","lvl_dmg"))
    local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
    int_dmg = int_dmg * (1+math.floor(HInt()/7)/10) --�� ������ 7 ���������� +10% �����
        
		local dur = tonumber("0" .. text_dec(Logic.obj_par("spell_kamikaze","power"),level))
		
    local destroyer_skill=tonumber(skill_power2("destroyer",1))+Logic.hero_lu_item("sp_spell_attack","count")

    min_dmg = math.floor(min_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
    max_dmg = math.floor(max_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)

		min_dmg=dmg_round(min_dmg)
		max_dmg=dmg_round(max_dmg)

    return min_dmg,max_dmg,dur

end

function pwr_ram(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

  local level = tonumber("0" .. text_dec(Logic.obj_par("spell_ram","level"),level))

    return level

end

function pwr_demonologist(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
    if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

  	local lead = tonumber("0" .. text_dec(Logic.obj_par("spell_demonologist","leadership"),level))
  	local time = tonumber("0" .. text_dec(Logic.obj_par("spell_demonologist","time"),level))
  	local lvl = text_dec(Logic.obj_par("spell_demonologist","level"),level)
  	local int_lead = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
  	lead=math.floor(lead*int_lead)

    return lead,time,lvl
end

function pwr_evilbook(level)

--tonumber("0" .. text_dec(Logic.obj_par("spell_bless","duration"),level))
  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

  	local level = tonumber("0" .. text_dec(Logic.obj_par("spell_evilbook","level"),level))
    local kill = tonumber("0" .. text_dec(Logic.obj_par("spell_evilbook","kill"),level))

    return level,kill

end

function pwr_plague(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end
		
		local power = tonumber("0" .. text_dec(Logic.obj_par("spell_plague","power"),level))

    return power

end

function pwr_berserker(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local power = math.floor(tonumber("0" .. text_dec(Logic.obj_par("spell_berserker","attack_bonus"),level))+math.floor(HInt()/2))
    local level = tonumber("0" .. text_dec(Logic.obj_par("spell_berserker","level"),level))

		--if power>70 then power = 70 end 
    return level,power

end

function pwr_pain_mirror(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

		local destroyer_skill=tonumber(skill_power2("destroyer",1))+hero_item_count2("sp_spell_attack","count")
    local power = math.floor(tonumber("0" .. text_dec(Logic.obj_par("spell_pain_mirror","power"),level))*(1+destroyer_skill/100)+hero_item_count2("sp_spell_mirror","count")+math.floor(HInt()/2))
		
		--if power > 70 then power = 70 end 
    return power

end

function pwr_phantom(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local power = math.floor(tonumber("0" .. text_dec(Logic.obj_par("spell_phantom","health"),level))+ math.floor(HInt()/3))

		--if power > 40 then power = 40 end 
    return power

end

function pwr_fire_breath(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local power = math.floor(tonumber("0" .. text_dec(Logic.obj_par("spell_fire_breath","power"),level))*(1+hero_item_count2("sp_spell_fire","count")/100))
		
		if power > 80 then power = 80 end 
		return power

end

function pwr_stone_skin(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local penalty = tonumber("0" .. text_dec(Logic.obj_par("spell_stone_skin","penalty"),level))
    local power = math.floor(tonumber("0" .. text_dec(Logic.obj_par("spell_stone_skin","power"),level)))

		if power > 80 then power = 80 end 
    return power,penalty

end

function pwr_magic_source(level) 

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local penalty = tonumber("0" .. text_dec(Logic.obj_par("spell_magic_source","penalty"),level)) 
    local mana = tonumber("0" .. text_dec(Logic.obj_par("spell_magic_source","mana"),level))

		if penalty > 50 then penalty = 50 end 
    return penalty,mana

end


function pwr_dragon_slayer(level) 

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local power = tonumber("0" .. text_dec(Logic.obj_par("spell_dragon_slayer","bonus_damage"),level))+ math.floor(HInt()/2)
		
		if power > 70 then power = 70 end 
    return power

end

function pwr_demon_slayer(level) 

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local power = tonumber("0" .. text_dec(Logic.obj_par("spell_demon_slayer","bonus_damage"),level))+ math.floor(HInt()/2)

		if power > 70 then power = 70 end 
    return power

end

function pwr_shroud(level) 

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local penalty = tonumber("0" .. text_dec(Logic.obj_par("spell_magic_source","penalty"),level))

    return penalty

end

function pwr_ghost_sword(level) -- ������� ���������� ��������� ����� � ������� ����. ����� ����� �������.

  local min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_ghost_sword","damage"))

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local destroyer_skill=tonumber(skill_power2("destroyer",1))+hero_item_count2("sp_spell_attack","count")
    local lvl_dmg = tonumber(Logic.obj_par("spell_ghost_sword","lvl_dmg"))
    local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
    int_dmg = int_dmg * (1+math.floor(HInt()/7)/10) --�� ������ 7 ���������� +10% �����
        
    local power = tonumber("0" .. text_dec(Logic.obj_par("spell_ghost_sword","power"),level))

    min_dmg = math.floor(min_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
    max_dmg = math.floor(max_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)

				min_dmg=dmg_round(min_dmg)
				max_dmg=dmg_round(max_dmg)

    return min_dmg,max_dmg,power

end

function pwr_warcry(level)

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local power = tonumber("0" .. text_dec(Logic.obj_par("spell_reaction","bonus"),level))

    return power

end

function pwr_hypnosis(level)

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local lvl = tonumber("0" .. text_dec(Logic.obj_par("spell_hypnosis","level"),level))
    local power = math.floor(tonumber("0" .. text_dec(Logic.obj_par("spell_hypnosis","power"),level))/100*hero_item_count2("leadership","count"))

    return lvl,power

end

function pwr_blind(level)

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local power = tonumber("0" .. text_dec(Logic.obj_par("spell_blind","level"),level))

    return power

end

function pwr_crue_fate(level)

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local power = tonumber("0" .. text_dec(Logic.obj_par("spell_crue_fate","level"),level))

    return power

end

function pwr_magic_bondage(level)

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local power = tonumber("0" .. text_dec(Logic.obj_par("spell_magic_bondage","level"),level))

    return power

end

function pwr_pygmy(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local power = math.floor(tonumber("0" .. text_dec(Logic.obj_par("spell_pygmy","penalty"),level)))

    return power

end

function pwr_accuracy(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

		
    local power = tonumber("0" .. text_dec(Logic.obj_par("spell_accuracy","bonus"),level))+ math.floor(HInt()/2)
    --    power=power+int_bonus*10 -- �������� �������� ����� �� ������ 10 ���������� �����
    
    if power > 50 then power = 50 end 
    return power

end

function pwr_slow(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local power = tonumber("0" .. text_dec(Logic.obj_par("spell_slow","penalty"),level))+hero_item_count2("sp_spell_speed","count")

    return power

end

function pwr_haste(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local power = tonumber("0" .. text_dec(Logic.obj_par("spell_haste","bonus"),level))+hero_item_count2("sp_spell_speed","count")

    return power

end

function pwr_divine_armor(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local healer_skill=tonumber(skill_power2("healer",1))
    local power = math.floor(tonumber("0" .. text_dec(Logic.obj_par("spell_divine_armor","power"),level))*(1+healer_skill/100+hero_item_count2("sp_spell_holy","count")/100))+ math.floor(HInt()/3)
		
		if power > 70 then power = 70 end 
    return power

end

function pwr_defenseless(level)

--tonumber("0" .. text_dec(Logic.obj_par("spell_bless","duration"),level))
  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local defless = tonumber(Logic.obj_par("spell_defenseless","defless"))
    local lvl_def = tonumber(Logic.obj_par("spell_defenseless","lvl_def"))

    def = defless*(1+(level-1)*lvl_def/100)
		
		if def > 80 then def = 80 end 
    return def

end

function pwr_adrenalin(level)

--tonumber("0" .. text_dec(Logic.obj_par("spell_bless","duration"),level))
  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local bonus = tonumber("0" .. text_dec(Logic.obj_par("spell_adrenalin","power"),level))

    return bonus

end

function pwr_pacifism(level)

--tonumber("0" .. text_dec(Logic.obj_par("spell_bless","duration"),level))
  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local bonus = tonumber("0" .. text_dec(Logic.obj_par("spell_pacifism","bonus"),level))+hero_item_count2("sp_spell_peace","count")
    local penalty = tonumber("0" .. text_dec(Logic.obj_par("spell_pacifism","penalty"),level))-math.floor(HInt()/2)

		if penalty>80 then penalty=80 end
		if penalty<0 then penalty=0 end
    return bonus,penalty

end

function pwr_resurrection(level)
    local ref = text_range_dec(Logic.obj_par("spell_resurrection","rephits"))

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local healer_skill=tonumber(skill_power2("healer",1))
    local lvl_ref = tonumber(Logic.obj_par("spell_resurrection","lev_ref"))
    local int_ref = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100

		
    ref = math.floor(ref*(1+(level-1)*lvl_ref/100)*(1+healer_skill/100+hero_item_count2("sp_spell_holy","count")/100)*int_ref)
		
		ref = dmg_round(ref)
		
    return ref

end

function pwr_healing(level)
    local min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_healing","rephits"))

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local healer_skill=tonumber(skill_power2("healer",1))
    local lvl_dmg = tonumber(Logic.obj_par("spell_healing","lvl_dmg"))
    local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
		
    min_dmg = math.floor(min_dmg*(1+(level-1)*lvl_dmg/100)*(1+healer_skill/100+hero_item_count2("sp_spell_holy","count")/100)*int_dmg)
    max_dmg = math.floor(max_dmg*(1+(level-1)*lvl_dmg/100)*(1+healer_skill/100+hero_item_count2("sp_spell_holy","count")/100)*int_dmg)

				min_dmg=dmg_round(min_dmg)
				max_dmg=dmg_round(max_dmg)

    return min_dmg,max_dmg

end

function pwr_oil_fog(level)

    local min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_oil_fog","damage"))

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local lvl_dmg = tonumber(Logic.obj_par("spell_oil_fog","lvl_dmg"))
    local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
    int_dmg = int_dmg * (1+math.floor(HInt()/7)/10) --�� ������ 7 ���������� +10% �����
    
    local destroyer_skill=tonumber(skill_power2("destroyer",1))+hero_item_count2("sp_spell_attack","count")+hero_item_count2("sp_spell_fire","count")
    local base_duration = tonumber(text_dec(Logic.obj_par("spell_oil_fog","duration"),level))
    local duration = base_duration + math.floor(HInt()/Game.Config("spell_power_config/int_duration"))

    local power = tonumber(text_dec(Logic.obj_par("spell_oil_fog","resistlow"),level))+ math.floor(HInt()/2)

    min_dmg = math.floor(min_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
    max_dmg = math.floor(max_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)

				min_dmg=dmg_round(min_dmg)
				max_dmg=dmg_round(max_dmg)

		if power > 100 then power = 100 end 
    return min_dmg,max_dmg,duration,power

end

function pwr_fire_ball(par)

    if par~=nil and string.find(par,"periphery") then
    min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_fire_ball","periphery_damage"))
--  end
  --if string.find(par,"epicentre") then
  else
    min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_fire_ball","damage"))
  end
  burn = Logic.obj_par("spell_fire_ball","burn")

  local level=tonumber(text_dec(par,1))
    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local destroyer_skill=tonumber(skill_power2("destroyer",1))+hero_item_count2("sp_spell_attack","count")+hero_item_count2("sp_spell_fire","count")
    local lvl_dmg = tonumber(Logic.obj_par("spell_fire_ball","lvl_dmg"))
    local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
    int_dmg = int_dmg * (1+math.floor(HInt()/7)/10) --�� ������ 7 ���������� +10% �����
    
    burn=burn*level+HInt()
    if burn>100 then burn=100 end 

    min_dmg = math.floor(min_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
    max_dmg = math.floor(max_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)

				min_dmg=dmg_round(min_dmg)
				max_dmg=dmg_round(max_dmg)

    return min_dmg,max_dmg,burn

end

function pwr_ice_serpent(par,level)

    if par~=nil and string.find(par,"periphery") then
    min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_ice_serpent","periphery_damage"))
--  end
  --  if string.find(par,"epicentre") then
  else
    min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_ice_serpent","damage"))
  end
  local freeze = Logic.obj_par("spell_ice_serpent","freeze")

  --local level=tonumber(text_dec(par,1))
    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local destroyer_skill=tonumber(skill_power2("destroyer",1))+hero_item_count2("sp_spell_attack","count")
    local lvl_dmg = tonumber(Logic.obj_par("spell_ice_serpent","lvl_dmg"))
    local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
    int_dmg = int_dmg * (1+math.floor(HInt()/7)/10) --�� ������ 7 ���������� +10% �����
    
    freeze=freeze*level+HInt()
    if freeze>100 then freeze=100 end 
    
    
    min_dmg = math.floor(min_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
    max_dmg = math.floor(max_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)

				min_dmg=dmg_round(min_dmg)
				max_dmg=dmg_round(max_dmg)

    return min_dmg,max_dmg,freeze

end

function pwr_sacrifice(level)

  if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local destroyer_skill=tonumber(skill_power2("destroyer",1))+hero_item_count2("sp_spell_attack","count")
    local power = tonumber("0" .. text_dec(Logic.obj_par("spell_sacrifice","power"),level))
    local damage = tonumber(Logic.obj_par("spell_sacrifice","damage"))
    local lvl_dmg = tonumber(Logic.obj_par("spell_sacrifice","lvl_dmg"))
    local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
    damage=math.floor(damage*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
		
		damage = dmg_round(damage)
		
    return damage,power

end

function pwr_fire_rain(level)

    local min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_fire_rain","damage"))
    local burn = Logic.obj_par("spell_fire_rain","burn")

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local destroyer_skill=tonumber(skill_power2("destroyer",1))+hero_item_count2("sp_spell_attack","count")+hero_item_count2("sp_spell_fire","count")
    local lvl_dmg = tonumber(Logic.obj_par("spell_fire_rain","lvl_dmg"))
    local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
		int_dmg = int_dmg * (1+math.floor(HInt()/7)/10) --�� ������ 7 ���������� +10% �����
				
    min_dmg = math.floor(min_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
    max_dmg = math.floor(max_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
    burn=burn*level+HInt()
    if burn>100 then burn=100 end 

				min_dmg=dmg_round(min_dmg)
				max_dmg=dmg_round(max_dmg)


    return min_dmg,max_dmg,burn

end

function pwr_lightning(level)

  local min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_lightning","damage"))

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local lvl_dmg = tonumber(Logic.obj_par("spell_lightning","lvl_dmg"))
    local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
    int_dmg = int_dmg * (1+math.floor(HInt()/7)/10) --�� ������ 7 ���������� +10% �����
    
    local shock = tonumber(Logic.obj_par("spell_lightning","shock"))

    local destroyer_skill=tonumber(skill_power2("destroyer",1))+hero_item_count2("sp_spell_attack","count")+hero_item_count2("sp_lightning_attack","count")
    
    min_dmg = math.floor(min_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
    max_dmg = math.floor(max_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
    shock = shock*level+math.floor(HInt()/2)
		if shock>100 then shock=100 end 
		
				min_dmg=dmg_round(min_dmg)
				max_dmg=dmg_round(max_dmg)

    return min_dmg,max_dmg,shock,tonumber(text_dec(Logic.obj_par("spell_lightning","count"),level))

end

function pwr_fire_arrow(level) -- ���������� ��������� � ���� ������������

  local min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_fire_arrow","damage"))

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local destroyer_skill=tonumber(skill_power2("destroyer",1))+hero_item_count2("sp_spell_attack","count")+hero_item_count2("sp_spell_fire","count")
    local lvl_dmg = tonumber(Logic.obj_par("spell_fire_arrow","lvl_dmg"))
    local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
    int_dmg = int_dmg * (1+math.floor(HInt()/7)/10) --�� ������ 7 ���������� +10% �����
    
    local burn = tonumber(Logic.obj_par("spell_fire_arrow","burn"))

    min_dmg = math.floor(min_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
    max_dmg = math.floor(max_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
        burn=burn*level+HInt()
    if burn>100 then burn=100 end 

				min_dmg=dmg_round(min_dmg)
				max_dmg=dmg_round(max_dmg)

    return min_dmg,max_dmg,burn

end

function pwr_smile_skull(level) -- ���������� ��������� � ���� ������������

  local min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_smile_skull","damage"))

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local destroyer_skill=tonumber(skill_power2("destroyer",1))+hero_item_count2("sp_spell_attack","count")
    local lvl_dmg = tonumber(Logic.obj_par("spell_smile_skull","lvl_dmg"))
    local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
    int_dmg = int_dmg * (1+math.floor(HInt()/7)/10) --�� ������ 7 ���������� +10% �����
    
    local poison = tonumber(Logic.obj_par("spell_smile_skull","poison"))

    min_dmg = math.floor(min_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
    max_dmg = math.floor(max_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)

    poison = poison*level+HInt()
    if poison>100 then poison=100 end 

				min_dmg=dmg_round(min_dmg)
				max_dmg=dmg_round(max_dmg)

    return min_dmg,max_dmg,poison

end

function pwr_magic_axe(level)

  local min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_magic_axe","damage"))

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local destroyer_skill=tonumber(skill_power2("destroyer",1))+hero_item_count2("sp_spell_attack","count")
    local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
    int_dmg = int_dmg * (1+math.floor(HInt()/7)/10) --�� ������ 7 ���������� +10% �����
    
    min_dmg = math.floor(min_dmg*(1+destroyer_skill/100)*int_dmg)*level
    max_dmg = math.floor(max_dmg*(1+destroyer_skill/100)*int_dmg)*level

				min_dmg=dmg_round(min_dmg)
				max_dmg=dmg_round(max_dmg)

    return min_dmg,max_dmg,level

end

function pwr_geyser(level)

    local min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_geyser","damage"))
    
    if tonumber(level)==0 or level==nil then
	    level=Obj.spell_level()
	    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end
		
    local lvl_dmg = tonumber(Logic.obj_par("spell_geyser","lvl_dmg"))
    local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
    int_dmg = int_dmg * (1+math.floor(HInt()/7)/10) --�� ������ 7 ���������� +10% �����
    
    local destroyer_skill=tonumber(skill_power2("destroyer",1))+hero_item_count2("sp_spell_attack","count")

    min_dmg = math.floor(min_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
    max_dmg = math.floor(max_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)

		min_dmg=dmg_round(min_dmg)
		max_dmg=dmg_round(max_dmg)

    return min_dmg,max_dmg,tonumber(text_dec(Logic.obj_par("spell_geyser","count"),level))

end

function pwr_armageddon(level)

    local min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_armageddon","damage"))
    local burn = Logic.obj_par("spell_armageddon","burn")

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

        local destroyer_skill=tonumber(skill_power2("destroyer",1))+hero_item_count2("sp_spell_attack","count") --+hero_item_count2("sp_spell_fire","count")
        local lvl_dmg = tonumber(Logic.obj_par("spell_armageddon","lvl_dmg"))
        local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
        int_dmg = int_dmg * (1+math.floor(HInt()/7)/10) --�� ������ 7 ���������� +10% �����
        
        burn=burn*level+HInt()
		    if burn>100 then burn=100 end 

        min_dmg = math.floor(min_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
        max_dmg = math.floor(max_dmg*(1+(level-1)*lvl_dmg/100)*(1+destroyer_skill/100)*int_dmg)
				
				min_dmg=dmg_round(min_dmg)
				max_dmg=dmg_round(max_dmg)
				
        return min_dmg,max_dmg,burn

end

function pwr_holy_rain(level)
    local min_dmg,max_dmg = text_range_dec(Logic.obj_par("spell_holy_rain","rephits"))

    if tonumber(level)==0 or level==nil then
    level=Obj.spell_level()
    if level==0 then level=1 end
        if Obj.where()==6  and Obj.spell_level()~=0 then level=level+1 end
    else
        level = tonumber(level)
    end

    local healer_skill=tonumber(skill_power2("healer",1))+hero_item_count2("sp_spell_attack","count")+hero_item_count2("sp_spell_holy","count")
    local lvl_dmg = tonumber(Logic.obj_par("spell_holy_rain","lvl_dmg"))
    local int_dmg = 1+tonumber(Game.Config("spell_power_config/int_power"))*HInt()/100
    int_dmg = int_dmg * (1+math.floor(HInt()/7)/10) --�� ������ 7 ���������� +10% �����
    
    min_dmg = math.floor(min_dmg*(1+(level-1)*lvl_dmg/100)*(1+healer_skill/100)*int_dmg)
    max_dmg = math.floor(max_dmg*(1+(level-1)*lvl_dmg/100)*(1+healer_skill/100)*int_dmg)

				min_dmg=dmg_round(min_dmg)
				max_dmg=dmg_round(max_dmg)

    return min_dmg,max_dmg

end
