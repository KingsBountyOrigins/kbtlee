function gen_tab (par)

   return "    "

end
function gen_space (par)

   return " "

end

function gen_hero_item (par)

  local item = Logic.hero_lu_item( par, "count" )
  if item==nil or item<0 then item=0 end
  return tostring(item)

end

function race_gen(par1,par2)
  local desc=""
  local race=AU.race(par1)

  if par2=="0" then
      return "<label=inf_"..race.."_race_head>"
  else
      return "<label=inf_"..race.."_race_hint>"
  end
   -- return "no race /"..par1.." "..par2
end

function resource_gen(par)
    local res = Logic.hero_lu_item( par, "count" )
    return tostring(res)

end

function resource_gen_temp()
    local itm = Game.GVStr( "temp" )
    return "<br><imb=" .. Obj.image(itm) .. "><br><label=itm_".. itm .."_name>"
end

function map_hint_gen(par)
    local rs = Logic.cur_lu_var("object")
    return "itm_"..rs.."_"..par

end


function kilo_money()

    local money = Logic.hero_lu_item("money","count")
    if money == nil then return "" end
    if money >= 100000 then
        money = math.floor(money/1000)
        if money >= 10000 then
          money = math.floor(money/1000)
          return money.."M"
        end
        return money.."K"
    else
        return tostring(money)
    end

end

function gen_army_map(data,par)
local text=""
    if par=="header" then
        return "<label=cpsn_"..AU.name(data)..">"

    end
    if par=="end" then
        return "<label=map_unit_info_hint>"
    end

        local lead = AU.curlead(data)
        local max_count=Logic.hero_lu_item("leadership","count")
        local count_lead=math.floor(max_count/lead)

        local count=AU.unitcount(data)

        if count_lead<count then

            count="<label=hint_unit_nolead>"..count.."</color>"
            text="<label=hint_morale_bad><br>"
        end

        text = text.."<label=hint_morale_count> "..count.."/"..count_lead.."<br>"
        local hint = "<label=hint_morale_0>: "
    if AU.moral(data)<0 then --<label=hint_morale_low>
        hint=hint.."<label=hint_morale_l"..tostring(-1*AU.moral(data))..">"
    end
    if AU.moral(data)==0 then --<label=hint_morale_neit>
        hint=hint.."<label=hint_morale_n0>"
    end
    if AU.moral(data)>0 then --<label=hint_morale_hi>
        hint=hint.."<label=hint_morale_h"..tostring(AU.moral(data))..">"
    end


return text..hint
end

function gen_unit_damage(data)
local text=""

        local res_count=AU.rescount()
        for i=0,res_count-1 do
          local min_ = AU.minresdmg( data, i )
      local max_ = AU.maxresdmg( data, i )
      if min_+max_>0 then
        text="<br>���� ���� "..i.."="..min_.."-"..max_
      end
    end

return text

end

function gen_unit_res(data)
local text=""

        local res_count=AU.rescount()
        for i=0,res_count-2 do
          local res = AU.resistance( data, i )
          local t=""
          if res>95 then res=95 end
          if res> 0 then t="<label=cpi_defense_res_D>" end
          if res< 0 then t="<label=cpi_defense_res_U>" end
        text=text.."<br> -<label=cpi_defense_res_"..(i+1)..">: "..res.."% "..t

    end

return text
end
function gen_unit_res(data)
local text=""
        local res_count=AU.rescount()
        for i=0,res_count-2 do
          local res = AU.resistance( data, i )
          if res>95 then res = 95 end
          local t=""
          if res> 0 then t="<label=cpi_defense_res_D>" end
          if res< 0 then t="<label=cpi_defense_res_U>" end
        text=text.."<br> -<label=cpi_defense_res_"..(i+1)..">: "..res.."% "..t

    end

return text
end

function gen_lead_count(data,par)
local text=""
    local lead= Logic.hero_lu_item("leadership","count")
    if par=="hero" then
        return tostring(lead)
    end

    if par=="unit" then
        count=AU.unitcount(data)
        lead=AU.abslead(data)
        return tostring(lead*count)
    end

        local max_ = AU.curlead(data)
        local count_lead=math.floor(lead/max_)

        return tostring(count_lead)


end

function gen_army_align(data,par)
    local align=Logic.cur_lu_var("align")
    local fur=Logic.cur_lu_var("furious")
    if align=="enemy" or fur=="1" then return "<label=enemy_01_hint>" end
    if align=="friend" then return "<label=enemy_03_hint>" end
    if align=="neutral" then return "<label=enemy_02_hint>" end

    return "<label=enemy_01_hint>"

end


function gen_day_time()
    return "<label=int_time_"..Game.DayTime()..">"
end

function gen_pawn_health()
    local hp = Attack.act_hp(0)
    local health = Attack.act_get_par(0,"health")
    return hp.."/"..health
end

function gen_max_hp()
    if Game.LocIsArena() then
        return "<br><label=cpi_health_hall> "..Attack.act_totalhp(0)
    end
    return ""
end

function time_gen()
	local time = Game.Time()
	local h = math.floor(time)
	local mt = (time*10 - math.floor(time)*10)/10
	local mnt = math.floor(mt*60)
	if mnt<10 then mnt="0"..tostring(mnt) end 

	return h..":"..mnt
end 