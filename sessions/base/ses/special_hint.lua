
function gen_special_hint(unit,par)
  local text=""
  local apars = Logic.attack_params()
      
  if par=="fdamage" then
    local damage = apars.damage.fire
    text = string.gsub(damage,",","-")
  end 
  if par=="pdamage" then
    local damage = apars.damage.poison
    text = string.gsub(damage,",","-")
  end 
  if par=="adamage" then
    local damage = apars.damage.astral
    text = string.gsub(damage,",","-")
  end 
  if par=="damage" then
    local damage = apars.damage.physical
    text = string.gsub(damage,",","-")
  end 
  if par=="mdamage" then
    local damage = apars.damage.magic
    text = string.gsub(damage,",","-")
  end 
  if par=="sdamage" then
    local damage = apars.custom_params.damage
    text = string.gsub(damage,",","-")
  end 
  if par=="ap" then
    local ap = apars.custom_params.ap
    text = ap
  end   
  if par=="cure" then
    local unit_count=AU.unitcount(unit)
    local cure=tonumber(apars.custom_params.heal)
    text = tostring(unit_count*cure)
  end 
  if par=="duration" then
    local attack_class = apars.class
    if attack_class == "spell" or (attack_class == "spell" and string.find(apars.spell,"special")) then 
      local spell=apars.spell
      text = Logic.obj_par(spell,"duration")
    else
      text = apars.custom_params.duration
    end
  end 
  if par=="shock" then
    local attack_class = apars.class
    if attack_class == "spell" then 
      local spell=apars.spell
      text = Logic.obj_par(spell,"shock").."%"
    else
      text = apars.custom_params.shock.."%"
    end
  end 
  if string.find(par,"health") then
    text = apars.custom_params.health
    if par=="health%" then text = text.."%" end 
    if par=="health_count" then 
    	text = tonumber(text)*Attack.act_size(0).."" 
    end 
  end 
  if string.find(par,"power") then
    local attack_class = apars.class
    if attack_class == "spell" or (attack_class == "spell" and string.find(apars.spell,"special")) then 
      local spell=apars.spell
      text = Logic.obj_par(spell,"power")
      if par=="power%" then text = text.."%" end 
    else
      text = apars.custom_params.power
      if par=="power%" then text = text.."%" end 
    end
  end 
  if string.find(par,"penalty") then
    local attack_class = apars.class
    if attack_class == "spell" or (attack_class == "spell" and string.find(apars.spell,"special")) then 
      local spell=apars.spell
      text = Logic.obj_par(spell,"penalty")
      if par=="penalty%" then text = text.."%" end 
    else
      text = apars.custom_params.penalty
      if par=="penalty%" then text = text.."%" end 
    end
  end 
  if par=="summon" then
    local count_min,count_max = text_range_dec(apars.custom_params.count)
    local unit_count=AU.unitcount(unit)
    text = count_min*unit_count.."-"..count_max*unit_count
  end 
  if par=="lsummon" then
    local count_min,count_max = text_range_dec(apars.custom_params.k)
    local unit_count=AU.unitcount(unit)
    local unit_lead=AU.abslead(unit)
    if count_min~=count_max then 
      text = math.floor(count_min*unit_count*unit_lead/100).."-"..math.floor(count_max*unit_count*unit_lead/100)
    else
      text = tostring(math.floor(count_min*unit_count*unit_lead/100))
    end 
  end 
  if par=="level" then
    local attack_class = apars.class
    if attack_class == "spell" or (attack_class == "spell" and string.find(apars.spell,"special")) then 
      local spell=apars.spell
      text = "1-"..Logic.obj_par(spell,"level")
    else
      text = "1-"..apars.custom_params.level
    end
  end 
  if par=="poison" then
    local attack_class = apars.class
    if attack_class == "spell" then 
      local spell=apars.spell
      text = Logic.obj_par(spell,"poison").."%"
    else
      text = apars.custom_params.poison.."%"
    end
  end 
  if par=="burn" then
    local attack_class = apars.class
    if attack_class == "spell" then 
      local spell=apars.spell
      text = Logic.obj_par(spell,"burn").."%"
    else
      text = apars.custom_params.burn.."%"
    end
  end 
  if par=="mana" then
    local count_min,count_max = text_range_dec(apars.custom_params.mana)
    local max_mana=tonumber(apars.custom_params.max)
    local unit_count=AU.unitcount(unit)
    count_min = count_min*unit_count
    count_max = count_max*unit_count
    
    if count_min>max_mana then count_min=max_mana end 
    if count_max>max_mana then count_max=max_mana end
    
    if count_min==count_max then 
      text = tostring(count_min)
    else
      text = count_min.."-"..count_max
    end 
  end 


  return text
end

function gen_special_charge(unit,par)
  local text=""
  --<hnt=moves> // special_charge
  --<hnt=reload> // special_reload
  local apars = Logic.attack_params()
  local moves = apars.moves
  local lmoves = apars.moves_left
  local lreload = apars.reload_left
  local reload = apars.reload
  
  if moves ~= nil then  
    if par=="text" then
      text = "<br><label=special_charge> "
    else
      if Game.LocIsArena() then 
      	if lmoves == 0 then text = text.."<label=skill_hintNoUp_font>" end
        text = text..lmoves.."/"..moves
      else 
        text = moves
      end 
    end 
  elseif reload ~= nil then
    if par=="text" then
    	if Game.LocIsArena() and lreload == 0 then
    	  text = text.."<label=hint_Dis_font>"
    	end 
      text = text.."<br><label=special_reload> "
    else
      if Game.LocIsArena() then 
      	if lreload == 0 then -- ������
         	text = "<label=hint_Dis_font>"..reload-1
        else
        	text = "<label=skill_hintNoUp_font>"..(lreload)
        end 
      else 
        text = reload-1
      end 

    end
  end 
  return tostring(text)
end

function gen_special_name()

		local apars = Logic.attack_params()
		local sa_name=""
			sa_name=apars.hinthead
		if sa_name~=nil then			
			sa_name=string.gsub(sa_name, "_head", "_name", 1)
		else 
		  sa_name=""
		end 
		--if string.find(spell_name,"spell") then
--			local spell_type=Logic.obj_par(spell_name,"type")
--			if spell_type=="penalty" then
--				name="<label=special_bad_font>"
--			end
--			if spell_type=="bonus" then
--				name="<label=special_good_font>"
--			end
		--else 
--		return name.."<label="..spell_name.."_name>"
	return "<label="..sa_name..">"
end

function gen_unit_par(unit,par)
	local hp=""
	local name=AU.name(unit)
	
	if par=="cur_health" then 
		hp = AU.hp(unit)
	end 
	if par=="health" then 
		hp = AU.health(unit)
	end 
	if par=="defense" then 
		hp = "???"--AU.defence(unit)
	end 
	if par=="attack" then 
		hp = "???"--AU.attack(unit)
	end 
	if par=="initiative" then 
		hp = "???"-- Attack.atom_getpar(name,"initiative")
	end 
	
	return tostring(hp)
end 