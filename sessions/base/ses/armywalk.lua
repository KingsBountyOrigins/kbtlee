
-- Action.act_run_scenario
-- 0          Stop (break path)
-- 1          Move to hero
-- 2 (param)  Scripted scenario
-- 3          Box taken, show mb
-- 4          Load to boat
-- 5          Unload boat
-- 6 (param)  Proceed path
-- 7          Box taken, show ffo
-- 8          Terminate scripted scenario
-- 9          Reintersect
-- 10         Logic state: reset
-- 11         Logic state: AGGRO
-- 12         Logic state: CONFUSED
-- 15 (param) play sound
-- 16 (param) Scripted scenario and freeze hero

--Timer command
-- 0 - SUBSCRIBE
-- 1 - UNSUBSCRIBE
-- 2 - SUBSCRIBE_ONCE

--Timer target
-- "$self"
-- "$send"
-- "$tz"
-- "$hero"
-- "$spwn"
-- luname   - SPECIFIC logic unit

--Message tags:
--INTERACTION_ACTIVE_ON
--INTERACTION_ACTIVE_OFF
--INTERACTION_PASSIVE_ON
--INTERACTION_PASSIVE_OFF
--OUT_CHASE
--CUSTOM_EVENT
--TIMER
--SCENARIO_END

function army_chase_continue()

  Action.act_run_scenario( 1 ) -- move to hero

end

function army_chase( followmode )

  local fardist = Action.tool_fardist()
  local seeradius = Action.tool_layer_radius( 3 )
  local pathlen = Action.tool_hero_dist_path()
  if (pathlen ~= nil) and ((seeradius * fardist) >= pathlen) then

    Atom.waitmode()
    Action.act_run_scenario( 8 ) -- no script scenario
    Action.act_run_scenario( 1 ) -- move to hero
    if not followmode then
      Action.act_run_scenario( 11 ) -- AGGRO state
    end
    Logic.cur_lu_var( "mode", "1" ) -- set mode to chase

    -- 0        - subscribe
    -- 4        - chase think timer (see editor.txt)
    -- "$self"  - subscribe self
    -- 0.7      - period 0.7 seconds
    Action.act_timer(0,4,"$self",0.7)
    Logic.cur_lu_flag( "FREEZE", false )
    return true
  end
  return false
end

function army_tupka()

  Logic.cur_lu_flag( "FREEZE", true )
  Logic.cur_lu_var( "mode", "2" ) -- set mode tupka
  --Action.act_run_scenario( 0 )          -- stop and break path
  Action.act_run_scenario( 2, "tupka" ) -- tupim
  Action.act_run_scenario( 12 ) -- CONFUSED state

end

function davarval( vname, def )
  local v = Logic.cur_lu_var( vname )
  if v == nil then return def end
  v = tonumber(v)
  if v == nil then return def end
  return v
end

function army_stop_if_chasing ( param ) --ftag:action
  local mode = davarval( "mode", 0 )
  if mode == 1 then
    -- chasing...
    Action.act_run_scenario( 0 ) -- stop now!
    Logic.cur_lu_var( "mode", "0" ) -- set mode walk
    Action.act_timer(1,4)
  end
  return false
end

function army_walk_thinker ( param ) --ftag:action

-- my mode (cur_lu_var(mode)):
-- nil or 0 = default path
--        1 = chase hero
--        2 = tupka

  if param == "4" then
    -- no more talk! chase!
    army_chase( false )
    return false
  end

  if param == "5" then
    -- recheck shapes
    Action.act_run_scenario( 9 )
    return false
  end

  local followmode = Logic.cur_lu_var( "follow" )
  if followmode == nil then
    followmode = false
  elseif followmode == "1" then
    followmode = true
  else
    followmode = false
  end


  local mode = davarval( "mode", 0 )
  if mode == 0 then
    -- walking...
    if Action.msg( "INTERACTION_PASSIVE_ON" ) then
      if Action.msg_interaction_layer( 2 ) then
        if followmode then
          -- to close! tupim
          army_tupka()
        else
          -- sniff radius! chase hero
          army_chase( false )
        end
        return false
      end
      if Action.msg_interaction_layer( 3 ) then
        -- I see hero!

        -- but can I see hero? check fov
        local fov = davarval( "fov", 270 )
        if Action.tool_is_visible_hero( fov ) then
          if Action.tool_hero_in_chase() then
            army_chase( followmode )
            return false
          end
        end
        Action.act_timer(0,4,"$self",0.7)
        return false
      end
    end
    if Action.msg( "TIMER" ) then
      if Action.tool_hero_in_chase() and Action.tool_hero_in_shape(3) then
        local fov = davarval( "fov", 270 )
        if Action.tool_is_visible_hero( fov ) then
          army_chase( followmode )
        end
      end
    end

    return false
  end

  if mode == 1 then
    -- chase

    if Action.msg( "TIMER" ) then
      if (Action.tool_hero_in_chase() or Action.tool_hero_in_shape(2)) and Action.tool_hero_in_shape(4) then
        if (not Action.tool_hero_in_shape(1)) and (Action.tool_hero_dist_path() ~= nil) then
          army_chase_continue()
        else
          Logic.cur_lu_var( "mode", "0" ) -- set mode walk
          Action.act_timer(1,4)
        end
      else
        army_tupka()
      end
      return false
    end

    if followmode then
      if Action.msg( "INTERACTION_PASSIVE_ON" ) then
        if Action.msg_interaction_layer( 2 ) then
          army_tupka()
          return false
        end
      end
    end

    if Action.msg( "INTERACTION_PASSIVE_OFF" ) then
      if Action.msg_interaction_layer( 4 ) and (not Action.tool_hero_in_shape(4)) then
        -- lost contact
        Action.act_timer(1,4)
        army_tupka()
        return false
      end
    end

    return false
  end

  if mode == 2 then
    -- tupka mode

    if Action.msg( "SCENARIO_END" ) then
      if followmode then
        if Action.tool_hero_in_shape(2) then
          -- still close
          Action.act_run_scenario( 2, "tupka" ) -- tupim
          return false
        end
      end

      Logic.cur_lu_flag( "FREEZE", false )
      Logic.cur_lu_var( "mode", "0" ) -- set mode walk
      -- Action.act_run_scenario( 10 ) -- reset logic state
      return false
    end

    if followmode then
      if Action.msg( "INTERACTION_PASSIVE_OFF" ) then
        if Action.msg_interaction_layer( 2 ) then
          army_chase( true )
          return false
        end
      end
    end

    -- tupka off?
    if Action.msg( "INTERACTION_PASSIVE_ON" ) then
      if Action.msg_interaction_layer( 2 ) then
        -- sniff radius! chase hero
        if not followmode then
          army_chase( false )
        end
        return false
      end
      if Action.msg_interaction_layer( 4 ) then
        -- Back to lost contact radius
        -- but can I see hero? check fov
        local fov = davarval( "fov", 270 )
        if Action.tool_is_visible_hero( fov ) then
          if Action.tool_hero_in_chase() then
            army_chase( followmode )
          end
        end

        return false
      end
    end

    return false
  end

  return false

end
