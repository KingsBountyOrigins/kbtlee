function quest_idle()

  if Logic.is_available() and Logic.cur_lu_var("dead") == "1" then
    local ok = Atom.animate("dead")
    if not ok then
      Game.MessageBox( "animation [dead] not found" )
    end
  else
    Atom.animate("idle")
  end

end

function scn_qo_destroy ( param )

    if (param == nil) then

      Scenario.animate("death","death")
      Logic.cur_lu_var("dead", "1")

    elseif (param == "death") then
        return true
    end

    return false

end
function scn_qo_open ( param )

    if (param == nil) then

      Scenario.animate("open","open")
      Atom.swstate( "open" )
      Logic.cur_lu_var("dead", "1")
    end
    return true
end

function scn_prepare ( param )

    if (param == nil) then
      Atom.swstate( "ready" )
    end
    return true
end

function scn_qo_close ( param )

    if (param == nil) then

      --Scenario.animate("death","death")
      Atom.swstate( "close" )
      Logic.cur_lu_var("dead", "0")
    end
    return true
end
function scn_qo_magic ( param )
    if (param == nil) then
      Scenario.animate("magic","magic")
    elseif (param == "magic") then
        return true
    end
    return false
end
function scn_qo_cast ( param )
    if (param == nil) then
      Scenario.animate("cast","cast")
    elseif (param == "cast") then
        return true
    end
    return false
end
function scn_qo_damage ( param )
    if (param == nil) then
      Scenario.animate("damage","damage")
    elseif (param == "damage") then
        return true
    end
    return false
end
function scn_qo_rare ( param )
    if (param == nil) then
      Scenario.animate("rare","rare")
    elseif (param == "rare") then
        return true
    end
    return false
end
function scn_qo_victory ( param )
    if (param == nil) then
      Scenario.animate("victory","victory")
    elseif (param == "victory") then
        return true
    end
    return false
end
function scn_qo_spare ( param )
    if (param == nil) then
      Scenario.animate("spare","spare")
    elseif (param == "spare") then
        return true
    end
    return false
end
function scn_qo_extra ( param )
    if (param == nil) then
      Scenario.animate("extra","extra")
    elseif (param == "extra") then
        return true
    end
    return false
end
function scn_qo_attack ( param )
    if (param == nil) then
      Scenario.animate("attack","attack")
    elseif (param == "attack") then
        return true
    end
    return false
end
function scn_qo_appear ( param )
    if (param == nil) then
      Scenario.animate("appear","appear")
    elseif (param == "appear") then
        return true
    end
    return false
end
function scn_qo_special ( param )
    if (param == nil) then
      Scenario.animate("special","special")
    elseif (param == "special") then
        return true
    end
    return false
end

function scn_qo_idle ( param )
    if (param == nil) then
      Scenario.animate("idle","idle")
    elseif (param == "idle") then
        return true
    end
    return false
end
function scn_qo_idle5 ( param )
    if (param == nil) then
      Scenario.animate("idle",5)
    elseif (param ~= "0") then
      Scenario.animate("idle",tonumber(param)-1)
    elseif (param == "0") then
      return true
    end
    return false
end

function quest_scenario ( scn, param ) --ftag:mascn

    local scenario_handlers = {
      destroy = scn_qo_destroy,
      cast = scn_qo_cast,
      magic = scn_qo_magic,
      appear = scn_qo_appear,
      rare = scn_qo_rare,
      spare = scn_qo_spare,
      extra = scn_qo_extra,
      damage = scn_qo_damage,
      attack = scn_qo_attack,
      special = scn_qo_special,
      victory = scn_qo_victory,
      idle = scn_qo_idle,
      idle5 = scn_qo_idle5,
      open = scn_qo_open,
      close = scn_qo_close,
      tupka = scn_army_tupka,
      stop1 = scn_army_stop1,
      stop2 = scn_army_stop2,
      animation1 = scn_army_animation1,
      animation2 = scn_army_animation2,
      animation3 = scn_army_animation3,
      await = scn_army_wait,
      fullstop = scn_fullstop,
      stop_and_work = scn_stop_and_work,
      miner_stop = scn_miner_stop,
      prepare = scn_prepare

    }

    local f = scenario_handlers[ scn ]

    if (f ~= nil) then
        return f(param)
    end

end

function run_animation(param) --ftag:action
    Atom.animbreak()
    Atom.animate(param)
    return true
end
