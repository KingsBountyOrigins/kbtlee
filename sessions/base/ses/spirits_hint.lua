--Game.SpiritRest("therock")

function gen_spirit_rest(data,par)
	local hint="<label=spirit_rest_last> "
	local name=""
	if par=="rage" then 
		hint=Attack.get_custom_param("rage")
	end 
	if par=="rest" then 
		hint=Attack.get_custom_param("rest")
	end 
	
	return " "..hint
end

function gen_lina_hint(data,par)
	local hint=Attack.get_custom_param(par)
	if hint==1 or hint=="1" then 
		return "<label=lina_yes>"
	else
		return "<label=lina_no>"
	end 
		
end

function gen_spirit_hint(data,par)
	local hint=""
	if par=="rage" then 
		hint=Attack.get_custom_param("rage")
	end 
	if par=="rest" then 
		hint=Attack.get_custom_param("rest")
	end 
	
	return " "..hint
end

function gen_spirit_level()
	local level=""
	
	return level
end


function gen_spirit_damage()

	local min, max = 0, 0
	for i=0, Logic.resistance()-1 do
	    local r = Logic.resistance(i)
	    local m = Attack.get_custom_param("damage."..r..".0")
	    if m ~= "" then
	        min = min + m
	        max = max + Attack.get_custom_param("damage."..r..".1")
	    end
	end
	if min==0 then
		min = Attack.get_custom_param("dmg.0")
		max = Attack.get_custom_param("dmg.1")
	end 
	
	if min==max then 
		return min 
	else
		return min..'-'..max
	end

end

function gen_spirit_energo(par)

	local min, max = 0, 0
	if par=="mana" then
		min = Attack.get_custom_param("manabonus.0")
		max = Attack.get_custom_param("manabonus.1")
	else
		min = Attack.get_custom_param("ragebonus.0")
		max = Attack.get_custom_param("ragebonus.1")
  end 	
	if min==max then 
		return min 
	else
		return min..'-'..max
	end

end

function gen_spirit_param(data,par)

		return Attack.get_custom_param(par)
end

function gen_spirit_sleep(par)
	local name = ""
	if par=="1" then 
		name = "therock"
	end 
	if par=="2" then 
		name = "slime"
	end 
	if par=="4" then 
		name = "death"
	end 
	if par=="3" then 
		name = "lina"
	end 
	return tostring(Game.SpiritRest(name))

end 