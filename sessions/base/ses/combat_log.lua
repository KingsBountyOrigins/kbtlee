function gen_combat_hint ( )
    local min_damage,max_damage,min_dead,max_dead,reverse_strike=Logic.dmg_params()
    local damage="<label=dmg_hint_dmg>"
    local dead=""
    local ret=""


    if min_dead+max_dead~=0 then
        dead="<br>".."<label=dmg_hint_dead>"
    end
    if reverse_strike>0 then
        ret="<br>".."<label=dmg_hint_ret>"
--    else
--        ret="<br>���� �� ������� �� ����."
    end
    return damage..dead..ret
end

function range_to_text(a,b)
	if a == b then return tostring(a) end
	return a..'-'..b
end

--����: Min-Max. ��������: Min-Max �������. ���� Min=Max, �� ��������� ���� ����� ��������: Min �������. ���� �������� Min=0, � Max>1 �� ��������� �������� �� max �������. ���� Min=������� �����, �� ��������� ������� ����� ����. ���� ���� �������� �� �����, � ������� �������� ����, �� ��� ����������� ������� ���� �������.

function gen_combat_damage ()
    local min_damage,max_damage,min_dead,max_dead,reverse_strike=Logic.dmg_params()
    return range_to_text(min_damage,max_damage)
end

function gen_combat_dead ()
    local min_damage,max_damage,min_dead,max_dead,reverse_strike=Logic.dmg_params()
    return range_to_text(min_dead,max_dead)
end

function blog_side_unit(unit,color,bel)
-- color=-1,0,1,2,3 ��� �����, ������ ���� � ������, � ������ � ��������, ������ ������� ��� �����, ������ ���� ��� �����
-- color=no,only,all,arrow,color ��� �����, ������ ���� � ������, � ������ � ��������, ������ ������� ��� �����, ������ ���� ��� �����
  if color==nil then color=1 end
  local size=Attack.act_size(unit)
  --local ally=Attack.act_ally(unit)
  --if bel ~= nil then ally=Attack.act_ally(unit,bel) end
  if bel == nil then bel = Attack.act_belligerent(unit) end
  local ally = bel == 1 or bel == 2
  --[[if Attack.act_is_spell(unit,"spell_magic_source") and not ally and not pawn then
  	ally=true
  elseif Attack.act_is_spell(unit,"spell_magic_source") and ally and not pawn then
  	ally=false
  end]]
  
  --if bel=="ally" then ally=true end 
  --if bel=="enemy" then ally=false end 
  
  local pawn=Attack.act_pawn(unit)
  local name="<label=cpsn_"..Attack.act_name(unit)..">"
  local arrow="<label=blog_ally_unit_img>"

  local side="<label=blog_ally_color>"
  if ally~=true then 
  	arrow=string.gsub(arrow, "ally", "enemy", 2) 
  	side=string.gsub(side, "ally", "enemy", 2) 
  end
  if size<=1 then name=string.gsub(name, "cpsn", "cpn", 1) end
  
  if color==1 or color=="all" then 
  	return arrow..side..name.."</color>"
	end 
  if color==-1 or color=="no" then 
  	return name
	end 
  if color==0 or color=="only" then 
  	return side..name.."</color>"
	end 
  if color==2 or color=="arrow" then 
  	return arrow
	end 
  if color==3 or color=="color" then 
  	return side
	end 
  if color==4 or color=="spell" then 
  	return "<label=blog_ally_spell_img>"
	end 
	
  return name

end


function add_target(target, res, added_target_ids)

	local id = Attack.cell_id(Attack.get_cell(Attack.get_caa(target)))
	if added_target_ids[id] then return res end
	--if next(added_target_ids) ~= nil then res = res..'<br>' end
	added_target_ids[id] = true

	if Attack.act_ally(target) then res = res.."<label=dmg_hint_format_ally>" else res = res.."<label=dmg_hint_format_enemy>" end

    local min_dmg,min_dead = Attack.damage_results(target, 1)
    local max_dmg,max_dead = Attack.damage_results(target, 2)
    if Attack.act_size(target) > 1 then
    	res = res.."<label=cpsn_"..Attack.act_name(target)..">"..". "
    else
    	res = res.."<label=cpn_"..Attack.act_name(target)..">"..". "
    end
    res = res.."</color><label=dmg_hint_simple_dmg> "..range_to_text(min_dmg,max_dmg)..'.'
    if max_dead > 0 then
		res = res.."<br><label=dmg_hint_simple_dead> "..range_to_text(min_dead,max_dead)..'.'
	end
	return res..'<br>'

end

function main_target_hint(target)

	local res,targetid = ""

	if target ~= nil and Attack.get_caa(target) ~= nil and Attack.act_takesdmg(target) and Attack.act_applicable(target) then
	    targetid = Attack.cell_id(Attack.get_cell(Attack.get_caa(target))) -- ����� ������� ���������� ����� ��� �������������� ������
	    local min_dmg,min_dead = Attack.damage_results(target, 1)
	    local max_dmg,max_dead = Attack.damage_results(target, 2)

	    res = "<label=dmg_hint_just_dmg> "..range_to_text(min_dmg,max_dmg)..'.'
	    if max_dead > 0 then
			res = res.."<br><label=dmg_hint_just_dead> "..range_to_text(min_dead,max_dead)..'.'
		end
		res = res..'<br>'
	end

	return res,targetid

end

function magic_attack_hint_gen()

	local target = Attack.get_target()

	if Obj.name() == "spell_resurrection" then
	    local rephits = pwr_resurrection()
		local real = math.max(0, math.min(rephits, Attack.act_get_par(target,"health")*Attack.act_initsize(target) - Attack.act_totalhp(target)))
		local raise = math.ceil((Attack.act_totalhp(target)+real)/Attack.act_get_par(target,"health")) - Attack.act_size(target)
		return "<label=dmg_hint_just_raise> "..raise..".<br><label=dmg_hint_just_effectiveness> "..math.floor(real/rephits*100)..'%.'
	end

	if Obj.name() == "spell_pain_mirror" then
		local percent_dmg = pwr_pain_mirror()
		local last_dmg = tonum(Attack.val_restore(target, "last_dmg"))
		Attack.atk_set_damage("magic", last_dmg*percent_dmg/100)
		return (main_target_hint(target))
	end

	if Obj.name() == "spell_ghost_sword" then
		local min_dmg,max_dmg,power = pwr_ghost_sword()
		local k = 1-math.floor((Attack.act_get_res(target,"physical"))*(100-power)/100)/100
		Attack.atk_set_damage("physical", 0)
		Attack.atk_set_damage("astral", min_dmg*k, max_dmg*k)
		local r = main_target_hint(target)
		Attack.atk_set_damage("astral", 0)
		return r
	end

	if Obj.name() == "spell_sacrifice" then
		if Attack.get_cycle() == 0 then
			local damage, power = pwr_sacrifice()
		    Attack.atk_set_damage("astral", damage, damage)
		    return (main_target_hint(target))
		else
			local count = math.floor(Attack.val_restore("sacrifice_hp")/Attack.act_get_par(target,"health"))
			if Attack.act_is_spell(Attack.val_restore("sacrifice_source"),"special_magic_shield") then count = math.floor(count/2) end
    		return "<label=dmg_hint_sacrifice_bonus> "..count..'.'
    	end
    end

    local dmg_type = Logic.obj_par(Obj.name(),"typedmg")
    local func_name = string.gsub(Obj.name(), "^spell_", "pwr_")
    if type(_G[func_name]) == "function" and dmg_type ~= "" and dmg_type ~= nil then

		local min_dmg,max_dmg,par1,par2 = _G[func_name]()
		if Obj.name() == "spell_ice_serpent" then
		    if Attack.act_feature(target,"freeze_immunitet") then min_dmg = min_dmg*freeze_im; max_dmg = max_dmg*freeze_im end
		end
	    Attack.atk_set_damage(dmg_type,min_dmg,max_dmg)

		local res,targetid = main_target_hint(target)

		if Obj.name() == "spell_healing" and not Attack.act_race(target,"undead") then
			local cure_hp = math.floor(Game.Random(min_dmg,max_dmg+0.45))
			local real_cure = math.min(cure_hp, Attack.act_get_par(target,"health") - Attack.act_hp(target))
			-- ���� �� ����� ���� �� ����� �� ������� ������� � ����������� �� �������� � �� ���������� �����
			-- t �� ����� - ������ ��� ���� ��������� � ���������� ������������� ��������
			-- !��� ����� ������� ���������� ���. �������� �� ��������� ����. ��� �� �����, �.�. ������� ���� �������, � ����� ��������� ����!
			--[[if Attack.act_is_spell(target, "spell_plague") then 
				local atom_cure = Attack.atom_getpar(Attack.act_name(target),"hitpoint")
				local unit_cure = Attack.act_get_par(target,"health")
				real_cure = math.max(atom_cure- Attack.act_hp(target),unit_cure- Attack.act_hp(target))
			end ]]
			return "<label=dmg_hint_just_heal> "..real_cure..".<br><label=dmg_hint_just_effectiveness> "..math.floor(real_cure/cure_hp*100)..'%.'
		end

		local added_target_ids = {}

		if Obj.name() == "spell_lightning" and Obj.spell_level() > 1 then

        	res = ""
        	if targetid ~= nil then res = add_target(target, res, added_target_ids) end

			local attacked_ids = {}
			attacked_ids[Attack.cell_id(target)] = true
			local front = {target}
			local count, dmg = par2, 1

			repeat

		    	if count == 0 then break end
		    	count = count - 1
				dmg = dmg * 0.5

				local new_front = {}
				-- ������� ������ �������� ������
				for k,target in ipairs(front) do
					local mind, atkd = 1e10, {}
					for i=1, Attack.act_count()-1 do
						local d = Attack.cell_mdist(target, i)
						if d <= 4.*1.8+.1 and (Attack.act_enemy(i) or Attack.act_ally(i)) and Attack.act_applicable(i) and Attack.act_takesdmg(i)
								and attacked_ids[Attack.cell_id(Attack.get_cell(i))] == nil then
							if math.abs(mind-d) < .1 then table.insert(atkd, i)
							elseif d < mind then mind = d; atkd = {i} end
						end
					end
					for i,act in ipairs(atkd) do -- ������� ���, ���� �������
						Attack.atk_set_damage(dmg_type, min_dmg*dmg, max_dmg*dmg)
						res = add_target(act, res, added_target_ids)

						attacked_ids[Attack.cell_id(Attack.get_cell(act))] = true
						-- ��������� ��������� �����
						table.insert(new_front, act)
					end
				end
				front = new_front

			until table.getn(front) == 0

		end

		if Obj.name() == "spell_ice_serpent" then
        	res = ""
        	if targetid ~= nil then res = add_target(target, res, added_target_ids) end

			local min_p_dmg,max_p_dmg = pwr_ice_serpent("periphery")
			local epicenter = target
			for i = 0, 5 do
				local target = Attack.cell_adjacent(epicenter,i)
				if target ~= nil and Attack.cell_present(target) and Attack.get_caa(target) ~= nil and Attack.act_takesdmg(target) and Attack.act_applicable(target) then
					if Attack.act_feature(target,"freeze_immunitet") then
						Attack.atk_set_damage(dmg_type,min_p_dmg*freeze_im,max_p_dmg*freeze_im)   -- set custom damage for periphery
					else
						Attack.atk_set_damage(dmg_type,min_p_dmg,max_p_dmg)   -- set custom damage for periphery
					end

					res = add_target(target, res, added_target_ids)
				end
			end
		end

		if Obj.name() == "spell_fire_rain" or Obj.name() == "spell_fire_ball" then

        	res = ""
        	if targetid ~= nil then res = add_target(target, res, added_target_ids) end

			if Obj.name() == "spell_fire_ball" then
				local min_p_dmg, max_p_dmg = pwr_fire_ball("periphery")
				Attack.atk_set_damage(dmg_type, min_p_dmg, max_p_dmg)
			end

			for i=1,6 do
				local target = Attack.get_target(i)
				if target ~= nil and Attack.get_caa(target) ~= nil and Attack.act_takesdmg(target) and Attack.act_applicable(target) then
					res = add_target(target, res, added_target_ids)
				end
			end

		end

		return res

	end

end


function spirit_attack_hint_gen()

	local res = ""
	local added_target_ids = {}

	if Attack.atk_name() == "rockfall" then

	  for i=0,Attack.get_targets_count()-1 do
	    local target = Attack.get_target(i)
	    if target ~= nil then
	      if not Attack.cell_is_empty(target) and rockfall_check_target(target) then
	        res = add_target( target, res, added_target_ids )
	      end
	    end
	  end
	  return res

	end

	local target = Attack.get_target()

	if Attack.atk_name() == "reaping" then
		Attack.atk_set_damage( "astral", tonumber(Attack.get_custom_param("kill"))*Attack.act_totalhp(target)/100.0 )
	end

	return (main_target_hint(target))

end
