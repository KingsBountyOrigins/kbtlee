function itm_eqsword(param) --ftag:action

  if Obj.onbody() then
    local moral=Obj.moral()

    if param=="" or param==nil then -- ������ ��� ����������
        if moral<1 then
            Obj.replace("dark_sword",50)
            Game.InvokeMsgBox("warning","<label=globstr_1312047335>")
        end
        if moral>99 then
            Obj.replace("light_sword",50)
            Game.InvokeMsgBox("warning","<label=globstr_1020491087>")
        end
    else
        if moral<1 then
            Obj.replace("equilibrium_sword",50)
            Game.InvokeMsgBox("warning","<label=globstr_1772041475>")
        end
    end -- ������ ���

    return false
  end
    return false
end

function itm_replace(param) --ftag:action

-- ��� ������� ������ ��� ��������!
    if Obj.moral()==0 then return false end

    local item=Obj.lvar("replace")
    if param~="" and param~=nil then
        item=param
    end
    if item=="" then
     item=Obj.upg()
    end

   
    if Obj.moral()==nil then 
   	  Obj.replace(item)    
	   	Game.GVStr("temp", item)
    	Game.InvokeMsgBox("warning","<label=message_upgraded>")
    elseif  Obj.moral()>1 then
   	  Obj.replace(item)    
	   	Game.GVStr("temp", item)
    	Game.InvokeMsgBox("warning","<label=message_upgraded>")
		end 

    return false
end

function itm_gen_param(param) --ftag:action

  if Obj.onbody() then
    local mode=Obj.lvar("mode")

    local par_count=text_par_count(param)
        local rnd = Game.Random(1,par_count+0.4)

    Obj.lvar(text_dec(param,0),text_dec(param,rnd))
    Obj.lvar("rnd",rnd)
        --Game.InvokeMsgBox("warning","<label=globstr_870492816>")
    return false
  end
    return false
end

function itm_upgrade(param) --ftag:action

  local moral=Obj.moral()
  local par_count=0
    if moral==0 then -- ���� ������� � ������� 0 �� ��� ��������. ����������� ������ �������� ��� ��������� 50
        if param~="" and param~=nil then
        Obj.moral(tonumber(param))
        else
            Obj.moral(50)
    end
  else  -- ���� ������ ���� 1 �� ������� ��������� �� ������ � ��������� ������� ��� ��������� � ��������
    if param~="" and param~=nil then
        par_count=text_par_count(param)
    end
      local set_morale = 50
    local new_item = Obj.upg()
    if  par_count>1 then
        set_morale=tonumber(text_dec(param,2))
        new_item=text_dec(param,1)
    end
    if  par_count==1 then
        set_morale=tonumber(param)
    end

    if new_item~="" and new_item~=nil then

--    if Obj.moral()>99 then 
--        Obj.replace(new_item,set_morale)
--        Game.GVStr("temp", new_item)
--       	Game.InvokeMsgBox("warning","<label=message_auto_upgraded>")
--       	return false
--		end 

        Obj.replace(new_item,set_morale)
        Game.GVStr("temp", new_item)
        Game.InvokeMsgBox("warning","<label=message_upgraded>")

    end
  end
    return false
end
function itm_auto_upgrade(param) --ftag:action

    local par_count=0
  local moral=Obj.moral()
    if moral ~= nil and moral>99 and Obj.onbody() then -- ���� ������� � ������� 100 � �� ����. ����������� ����� ���
    if param~="" and param~=nil then
        par_count=text_par_count(param)
    end
      local set_morale = 50
    local new_item = Obj.upg()
    if  par_count>1 then
        set_morale=tonumber(text_dec(param,2))
        new_item=text_dec(param,1)
    end
    if  par_count==1 then
        set_morale=tonumber(param)
    end

    if new_item=="pandemonik_mask" then 
      Obj.replace(new_item,set_morale)
      Game.GVStr("temp", new_item)
      Game.InvokeMsgBox("warning","<label=globstr_796900633>") 
      return false
	  end  
    if new_item~="" and new_item~=nil then
        Obj.replace(new_item,set_morale)
        Game.GVStr("temp", new_item)
        Game.InvokeMsgBox("warning","<label=message_auto_upgraded>")
    end
  end
    return false
end

-- ������ ������ � ����������� �� ������ ����� ����� �� ������ ��� ��� ����� ����������
-- ��������� - "thorn,+10" ��� "arhcer,bowman,elf,elf2,-3" - ����� ���� �� ���� ���� ���� -3
-- ����� �������� ����� �� ������� �����, ����� �������� ������� ��������� ���
function itm_race_command(param) --ftag:action

if Obj.onbody() then

  local hero_army = Logic.cur_lu_army()
--  local enemy_army = ""
--  if Logic.src_present() then enemy_army = Logic.src_lu_army() end

    -- ���� � ����� ���� �����-�� ���� - ���������� ����������

  local change_par=Obj.lvar("change_morale_after_combat")
  if change_par=="" or change_par==nil then
    change_par=0
  else
    change_par=tonumber(change_par)
  end

    if param~=nil and param~="" then

    local par_count=text_par_count(param)
    local unit_found=0
    -- ������� ������� �� ������ ���� � �����
    for i=1,par_count-1 do
        if Logic.army_race_count( hero_army, text_dec(param,i))>0 then
            unit_found=unit_found+1
        end
    end
    -- � ���� ���� �� 1 ������ - ������ ��������
    if unit_found>0 then
        change_par = change_par + tonumber(text_dec(param,par_count))
    end

  end

  Obj.lvar("change_morale_after_combat", change_par)

end
 return false

end

function itm_race_kill(param) --ftag:action

if Obj.onbody() then

  local enemy_army = ""
  if Logic.src_present() then enemy_army = Logic.src_lu_army() end

  local change_par=Obj.lvar("change_morale_after_combat")
  if change_par=="" or change_par==nil then
    change_par=0
  else
    change_par=tonumber(change_par)
  end

    if param~=nil and param~="" then

    local par_count=text_par_count(param)
    local unit_found=0
    -- ������� ������� �� ������ ���� � �����
    for i=1,par_count-1 do
        if Logic.army_race_count( enemy_army, text_dec(param,i))>0 then
            unit_found=unit_found+1
        end
    end
    -- � ���� ���� �� 1 ������ - ������ ��������
    if unit_found>0 then
        change_par = change_par + tonumber(text_dec(param,par_count))
    end

  end

  Obj.lvar("change_morale_after_combat", change_par)

end
 return false

end

-- ������ ������ � ����������� �� ������ ����� ����� �� ������ ��� ��� ����� ����������
-- ��������� - "thorn,+10" ��� "arhcer,bowman,elf,elf2,-3" - ����� ���� �� ���� ���� ���� -3
-- ����� �������� ����� �� ������� �����, ����� �������� ������� ��������� ���
function itm_unit_command(param) --ftag:action

if Obj.onbody() then

  local hero_army = Logic.cur_lu_army()
--  local enemy_army = ""
--  if Logic.src_present() then enemy_army = Logic.src_lu_army() end

    -- ���� � ����� ���� �����-�� ���� - ���������� ����������

  local change_par=Obj.lvar("change_morale_after_combat")
  if change_par=="" or change_par==nil then
    change_par=0
  else
    change_par=tonumber(change_par)
  end

    if param~=nil and param~="" then

    local par_count=text_par_count(param)
    local unit_found=0
    -- ������� ������� �� ������ ���� � �����
    for i=1,par_count-1 do
        if Logic.army_cp_count( hero_army, text_dec(param,i))>0 then
            unit_found=unit_found+1
        end
    end
    -- � ���� ���� �� 1 ������ - ������ ��������
    if unit_found>0 then
        change_par = change_par + tonumber(text_dec(param,par_count))
    end

  end

  Obj.lvar("change_morale_after_combat", change_par)

end
 return false

end

function itm_unit_kill(param) --ftag:action

if Obj.onbody() then

  local enemy_army = ""
  if Logic.src_present() then enemy_army = Logic.src_lu_army() end

  local change_par=Obj.lvar("change_morale_after_combat")
  if change_par=="" or change_par==nil then
    change_par=0
  else
    change_par=tonumber(change_par)
  end

    if param~=nil and param~="" then

    local par_count=text_par_count(param)
    local unit_found=0
    -- ������� ������� �� ������ ���� � �����
    for i=1,par_count-1 do
        if Logic.army_cp_count( enemy_army, text_dec(param,i))>0 then
            unit_found=unit_found+1
        end
    end
    -- � ���� ���� �� 1 ������ - ������ ��������
    if unit_found>0 then
        change_par = change_par + tonumber(text_dec(param,par_count))
    end

  end

  Obj.lvar("change_morale_after_combat", change_par)

end
 return false

end

function itm_after_pacify(param) --ftag:action

  if Obj.moral() == nil then
    return false
  end

 local par_count=text_par_count(param)

if (Obj.onbody() and par_count==1) or par_count>1 then

    if par_count>1 then param = text_dec(param,1) end

  local change_par=Obj.lvar("change_morale_after_combat")
  if change_par=="" or change_par==nil then
    change_par=0
  else
    change_par=tonumber(change_par)
  end

  if param~=nil and param~="" then
    change_par=change_par+tonumber(param)
  end

  local was_moral = Obj.moral()
  local new_moral = was_moral + change_par

  Obj.moral(new_moral)

  if was_moral <= 0 and new_moral >= 1 then
    Game.GVStr("temp", Obj.name())
    Game.InvokeMsgBox("warning","<label=message_pacified>")
  end


  if Obj.moral()<0 then Obj.moral(0) end
  Obj.lvar("change_morale_after_combat", 0)
end
 return false

end

function itm_after_combat(param) --ftag:action

  if Obj.moral() == nil then
    return false
  end

 local par_count=text_par_count(param)

if (Obj.onbody() and par_count==1) or par_count>1 then

    if par_count>1 then param = text_dec(param,1) end

  local change_par=Obj.lvar("change_morale_after_combat")
  if change_par=="" or change_par==nil then
    change_par=0
  else
    change_par=tonumber(change_par)
  end

  if param~=nil and param~="" then
    change_par=change_par+tonumber(param)
  end

  local was_moral = Obj.moral()
  local new_moral = was_moral + change_par

  Obj.moral(new_moral)

  --if was_moral <= 0 and new_moral >= 1 then
  --  Game.GVStr("temp", Obj.name())
  --  Game.InvokeMsgBox("warning","<label=message_pacified>")
  --end

  if Obj.moral()<0 then Obj.moral(0) end
  Obj.lvar("change_morale_after_combat", 0)
end
 return false

end

function dark_sword_fight()

if Obj.onbody() then
  --Obj.moral( Obj.moral() * 0.5 ) -- � ��� ���� ��������� ������
  local hero_army = Logic.cur_lu_army()
  local enemy_army = ""
  if Logic.src_present() then enemy_army = Logic.src_lu_army() end
  local undead_enemy = Logic.army_race_count( enemy_army, "undead" )
  local demon_enemy = Logic.army_race_count( enemy_army, "demon" )
  local human_enemy = Logic.army_race_count( enemy_army, "human" )
  local elf_enemy = Logic.army_race_count( enemy_army, "elf" )
  local undead_hero = Logic.army_race_count( hero_army, "undead" )
  local demon_hero = Logic.army_race_count( hero_army, "demon" )
  local human_elf = Logic.army_race_count( hero_army, "elf" )

    if (undead_enemy>0) then
        Obj.moral( Obj.moral() - 40 )
    end
    if (demon_enemy>0) then
        Obj.moral( Obj.moral() - 40 )
    end
    if (human_enemy>0) then
        Obj.moral( Obj.moral() + 20 )
    end
    if (elf_enemy>0) then
        Obj.moral( Obj.moral() + 20 )
    end

    if (undead_hero>0) then
        Obj.moral( Obj.moral() + 20 )
    end
    if (demon_hero>0) then
        Obj.moral( Obj.moral() + 20 )
    end
    if (elf_hero>0) then
        Obj.moral( Obj.moral() - 40 )
    end

    if Obj.moral()> 100 then
        Obj.moral(100)
    end

    if Obj.moral()<= -100 then
        Obj.replace("sword_of_equilibrium")
    end

end

  return true

end

function light_sword_fight()

if Obj.onbody() then
  --Obj.moral( Obj.moral() * 0.5 ) -- � ��� ���� ��������� ������
  local hero_army = Logic.cur_lu_army()
  local enemy_army = ""
  if Logic.src_present() then enemy_army = Logic.src_lu_army() end
  local undead_enemy = Logic.army_race_count( enemy_army, "undead" )
  local demon_enemy = Logic.army_race_count( enemy_army, "demon" )
  local human_enemy = Logic.army_race_count( enemy_army, "human" )
  local undead_hero = Logic.army_race_count( hero_army, "undead" )
  local demon_hero = Logic.army_race_count( hero_army, "demon" )
  local human_hero = Logic.army_race_count( hero_army, "human" )
    local elf_enemy = Logic.army_race_count( enemy_army, "elf" )
  local elf_hero = Logic.army_race_count( hero_army, "elf" )

    if (undead_enemy>0) then
        Obj.moral( Obj.moral() + 20 )
    end
    if (demon_enemy>0) then
        Obj.moral( Obj.moral() + 20 )
    end
    if (human_enemy>0) then
        Obj.moral( Obj.moral() - 40 )
    end
    if (elf_enemy>0) then
        Obj.moral( Obj.moral() - 40 )
    end

    if (undead_hero>0) then
        Obj.moral( Obj.moral() - 40 )
    end
    if (demon_hero>0) then
        Obj.moral( Obj.moral() - 40 )
    end

    if Obj.moral()> 100 then
        Obj.moral(100)
    end

    if Obj.moral()<= -100 then
        Obj.replace("sword_of_equilibrium")
    end

end

  return true

end

function equilibrium_sword_fight()

if Obj.onbody() then
  --Obj.moral( Obj.moral() * 0.5 ) -- � ��� ���� ��������� ������
  local hero_army = Logic.cur_lu_army()
  local enemy_army = ""
  if Logic.src_present() then enemy_army = Logic.src_lu_army() end
  local undead_enemy = Logic.army_race_count( enemy_army, "undead" )
  local demon_enemy = Logic.army_race_count( enemy_army, "demon" )
  local human_enemy = Logic.army_race_count( enemy_army, "human" )
  local undead_hero = Logic.army_race_count( hero_army, "undead" )
  local demon_hero = Logic.army_race_count( hero_army, "demon" )
  local human_hero = Logic.army_race_count( hero_army, "human" )
    local elf_enemy = Logic.army_race_count( enemy_army, "elf" )
  local elf_hero = Logic.army_race_count( hero_army, "elf" )

    if (undead_enemy>0) then
        Obj.moral( Obj.moral() + 20 )
    end
    if (demon_enemy>0) then
        Obj.moral( Obj.moral() + 20 )
    end
    if (human_enemy>0) then
        Obj.moral( Obj.moral() - 20 )
    end
    if (elf_enemy>0) then
        Obj.moral( Obj.moral() - 20 )
    end

    if (undead_hero>0) then
        Obj.moral( Obj.moral() - 20 )
    end
    if (demon_hero>0) then
        Obj.moral( Obj.moral() - 20 )
    end
    if (human_hero>0) then
        Obj.moral( Obj.moral() + 20 )
    end
    if (elf_hero>0) then
        Obj.moral( Obj.moral() + 20 )
    end

    if Obj.moral()>= 100 then
        Obj.replace("sword_of_light")
    end

    if Obj.moral()<= -100 then
        Obj.replace("sword_of_dark")
    end

end

  return true

end

function fightboxtest()

  Obj.moral( Obj.moral() * 0.5 ) -- � ��� ���� ��������� ������
  local hero_army = Logic.cur_lu_army()
  local enemy_army = ""
  if Logic.src_present() then enemy_army = Logic.src_lu_army() end
  local demoncount = Logic.army_race_count( hero_army .. "/" .. enemy_army, "demon" )
  if ( demoncount > 0 ) then
     Obj.moral( Obj.moral() + 40 )
  end
  return true

end

function fightboxtest2()

  if ( Bonus.info_enemy_dead_count() > 0 ) then
     Obj.moral( Obj.moral() + 45 )
  end
  return true

end

function fightboxtest3()

  Obj.moral( Obj.moral() + 1 )
  return true

end

function afterpacifyfightbox()

  Obj.moral( 33 )
  return true
end

function itm_plugen_count(param) --ftag:action

    local count=tonumber(Logic.hero_lu_item("plugen","count"))
    Logic.hero_lu_var("eat",count)

    return false
end
