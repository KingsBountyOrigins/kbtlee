function scn_object_recharge ( param )
	
	if  Logic.cur_cl_box_empty()== true then  
  	Logic.cur_cl_box_empty(false)
    Scenario.animate("idle", "object_001")
		return true					
  else
   	 return false
	end
end

function object_scenario ( scn, param ) --ftag:mascn

    local scenario_handlers = {
        recharge=scn_object_recharge
    }

    local f = scenario_handlers[ scn ]

    if (f ~= nil) then
        return f(param)
    end

end

function scn_object_multi_hint ( param ) --ftag:vv

	local number=Logic.cur_lu_var( "hint_number" )
	local hint=Logic.cur_lu_var( "hint"..number )		
	hint="<label="..hint..">"

	return hint

end

function scn_object_multi_msg ( param ) --ftag:vv

	local number=Logic.cur_lu_var( "msg_number" )
	local msg=Logic.cur_lu_var( "msg"..number )
	msg="<label="..msg..">"

	return msg

end
