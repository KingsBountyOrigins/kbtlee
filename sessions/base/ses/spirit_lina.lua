--cam_lina_rare_devatron_closeup


function lina_appear_idle()

  Attack.act_aseq( 0, "appear" )

  Attack.act_aseq( 0, "rare" )

end

--------------
--Enegretics--
--------------

function lina_enboxes()
  local r = Game.Random()
  if Attack.is_short_spirit_seq() then
    Attack.act_aseq(0, "2cast")
    Attack.act_fadeout(0, 0, 1./25., 0., 0.)
    Attack.cam_track_duration(7.6)
    if Game.ArenaShape() == 4 then
        if r <0.7 then
			Attack.cam_track(0, 0, "spirit_cam_short_ships.track" )
		else
            Attack.cam_track(0, 0, "spirit_cam_short_2left.track" )
		end
    else
      if r <= 0.44 then
        Attack.cam_track(0, 0, "spirit_cam_short_centre.track" )
      elseif r <= 0.88 then
        Attack.cam_track(0, 0, "spirit_cam_short_2left.track" )
      else
        Attack.cam_track(0, 0, "spirit_cam_short_2right.track" )
      end
    end
  else
    if Game.ArenaShape() == 4 then --  ships
    	Attack.cam_track_duration(14.8)
        if r <0.7 then
			Attack.cam_track(0, 0, "spirit_cam_ships.track" )
		else
            Attack.cam_track(0, 0, "spirit_cam_2left.track" )
		end
    elseif Game.ArenaShape() == 1 then --  castle
         if r <= 0.35 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2r_castle.track" )
         elseif r <= 0.7 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2l_castle.track" )
         elseif r <= 0.9 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_r_castle.track" )
         else
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_closeup_castle.track" )
         end
    elseif Game.ArenaShape() == 5 then --  water
         if r <= 0.5 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2r_castle.track" )
         else
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2l_castle.track" )
         end
    else
         if r <= 0.2 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2r.track" )
         elseif r <= 0.4 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2l.track" )
         elseif r <= 0.6 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_r.track" )
         else
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_closeup.track" )
         end
    end
    lina_appear_idle()
    Attack.act_aseq(0, "cast")
  end

    local start = Attack.aseq_time(0, "x")

    local count = tonumber(Attack.get_custom_param("count"))
    local mana_count = {Attack.get_custom_param("manabonus.0"), Attack.get_custom_param("manabonus.1")}
    local rage_count = {Attack.get_custom_param("ragebonus.0"), Attack.get_custom_param("ragebonus.1")}

    local cells = {}
    for i=0, Attack.cell_count()-1 do
        local cell = Attack.cell_get(i)
        if Attack.cell_is_empty(cell) and Attack.cell_is_pass(cell) and Attack.cell_bonus(cell)==nil then
            table.insert(cells, cell)
        end
    end

    for i=1, math.min(table.getn(cells),count) do

        local n = Game.Random(1,table.getn(cells))
        local c = cells[n]
        table.remove(cells, n)

        local name, val
        if math.mod(i,2) == 0 then
            name = "mana"
            val = Game.Random(unpack(mana_count))
        else
            name = "rage"
            val = Game.Random(unpack(rage_count))
        end
        local t = start + (i-1)*.3
        local a = Attack.atom_spawn(c, t, "energo_"..name)
        Attack.act_aseq(a, "appear")
        Attack.aseq_timeshift(a, t)

        Attack.val_store(a, "val", val)
        Attack.val_store(a, "name", name)

        Attack.cell_bonus(c, a)

    end

    spirit_after_hit()
    Attack.log("lina_enbox", "name", "<label=cpn_lina>")

    return true

end

function enbox_bonus(t,cl)

    local name = Attack.val_restore("name")

    if name == "" then return 0 end

    Attack.val_store("name", "") -- ����� ������ �� ������ ���� �����
    if Attack.act_human(cl) then
        Logic.hero_lu_item(name,"count", Logic.hero_lu_item(name,"count")+tonumber(Attack.val_restore("val")) )
	elseif Attack.act_belligerent(cl) == 4 and name == "mana" then
	    local count = Logic.enemy_lu_item(name,"count")
	    if count ~= nil then
        	Logic.enemy_lu_item(name,"count", count+tonumber(Attack.val_restore("val")) )
		end
    end
    Attack.act_aseq(0, "disappear", true)
    Attack.aseq_timeshift(0, t)
    return tonumber(Attack.val_restore("ap"))

end

function enbox_modificators()

    local caa = Attack.get_caa(Attack.get_cell(0))
    if caa~=nil then
        local ap = enbox_bonus(0, caa)
        Attack.act_ap(caa, Attack.act_ap(caa) + ap)
    end
    return true

end


--------------
-----Orb------
--------------

function lina_orb()
    local r = Game.Random()
    local target, start = Attack.get_target(), 0

  if Attack.is_short_spirit_seq() then
    Attack.act_aseq(0, "2orbcast")
    Attack.act_fadeout(0, 0, 1./25., 0., 0.)
    Attack.act_rotate( 0, .1, 0, target )
    Attack.cam_track_duration(7.6)
    if Game.ArenaShape() == 4 then
        if r <0.7 then
			Attack.cam_track(0, 0, "spirit_cam_short_ships.track" )
		else
            Attack.cam_track(0, 0, "spirit_cam_short_2left.track" )
		end
    else
      if r <= 0.44 then
        Attack.cam_track(0, 0, "spirit_cam_short_centre.track" )
      elseif r <= 0.88 then
        Attack.cam_track(0, 0, "spirit_cam_short_2left.track" )
      else
        Attack.cam_track(0, 0, "spirit_cam_short_2right.track" )
      end
    end
  else
    lina_appear_idle()

    if Game.ArenaShape() == 4 then --  ships
    	Attack.cam_track_duration(14.8)
        if r <0.7 then
			Attack.cam_track(0, 0, "spirit_cam_ships.track" )
		else
            Attack.cam_track(0, 0, "spirit_cam_2left.track" )
		end
    elseif Game.ArenaShape() == 1 then --  castle
--         if r <= 0.35 then
--             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2r_castle.track" )
--         elseif r <= 0.7 then
--             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2l_castle.track" )
--         elseif r <= 0.9 then
--             Attack.cam_track(0, 0, "cam_lina_rare_devatron_r_castle.track" )
--         elseif r <= 1.0 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_closeup_castle.track" )
--         end
    elseif Game.ArenaShape() == 5 then --  water
        if r <0.4 then
			Attack.cam_track(0, 0, "spirit_cam_centre.track" )
		elseif r <0.7 then
            Attack.cam_track(0, 0, "spirit_cam_2right.track" )
		else
            Attack.cam_track(0, 0, "spirit_cam_2left.track" )
		end
    else
         if r <= 0.45 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2r.track" )
         elseif r <= 0.75 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2l.track" )
         elseif r <= 0.9 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_r.track" )
         elseif r <= 1.0 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_closeup.track" )
         end
    end


    Attack.act_rotate( Attack.aseq_time(0, "x"), Attack.aseq_time(0, "y"), 0, target )

     start = Attack.aseq_time( 0 );

    Attack.act_aseq(0, "orbcast")
  end

--  local orb = Attack.atom_spawn(0, orb_start, "orb", Attack.angleto(0, target))

  for i=1,Attack.act_count()-1 do
    if Attack.act_name(i) == "orb" then
	    Attack.act_aseq(i, "death", true)
    end
  end

    local c = Attack.get_cell(0)
    Attack.act_spawn(c, 0, "orb", Attack.angleto(0,target))
    Game.GVSNumInc("units_kupleno", "orb", 1)
    Attack.act_nodraw(c, true)
    Attack.act_animate(c, "appear", start)
    Attack.act_nodraw(c, false, start)
    Attack.act_move(Attack.aseq_time("orb", "appear", "x")+start, Attack.aseq_time("orb", "appear", "y")+start, c, target)
    --Attack.val_store(c, "damage", Attack.get_custom_param("damage.physical.0")..'-'..Attack.get_custom_param("damage.physical.1"))
    local health = Attack.get_custom_param("health")
    Attack.act_hp( c, health )
    Attack.act_set_par( c, "health", health )
    Attack.resort( c )

    Attack.log("slime_fog", "name", "<label=cpn_lina>", "target", "<label=cpn_orb>")
    spirit_after_hit()

    return true

end

function orb_check_target(c)
	return Attack.act_takesdmg(c) and not Attack.act_ally(c)
end

function orb_calccells()

    for i=0,5 do
        local c = Attack.cell_adjacent(0, i)

        if empty_cell(c) then
            while true do
                Attack.marktarget(c)
                c = Attack.cell_adjacent(c, i)
                if not empty_cell(c) then break end
            end
        end

        if c~=nil and orb_check_target(c) then
            Attack.marktarget(c)
        end
        --[[if c~=nil and Attack.cell_present(c) and ((Attack.cell_is_pass(c) and Attack.cell_is_empty(c)) or Attack.act_takesdmg(c)) then
            Attack.marktarget(c)
        end]]
    end

    Attack.val_store(0, "cell_id", Attack.cell_id(Attack.get_cell(0)))
    --Attack.atk_set_damage("physical", text_range_dec(Attack.val_restore("damage")))

    return true

end

function orb_posthit(damage,addrage,attacker,receiver,minmax,userdata,hitbacking)

		local dmg_k=100 -- %���������� ����� �� ������ ������ ������� ���������� ���
    local c = Attack.cell_id(Attack.val_restore(attacker, "cell_id")) -- ������������ ������, ��� ����� ���, ����� ������ ���, �.�. ��������� ���� �� ������ ������ �������� - ��� ����� �����������
    local k = Attack.cell_dist(c, receiver)-1
    return damage*(1+k*dmg_k/100), addrage*(1+k*dmg_k/100)

end

function orb_highlight()

    local target, prev = Attack.get_target()
    local dir = Game.Ang2Dir(Attack.angleto(0,target))
    target = Attack.cell_adjacent(0, dir) -- �� ��� ������, ����� target �������� ����� ��� �� 1 ������ �� ����

    if empty_cell(target) then
        while true do
            Attack.cell_select(target, "path")

            prev = target
            target = Attack.cell_adjacent(target, dir)

            if not empty_cell(target) then break end
        end
    end

    if target~=nil and orb_check_target(target) then
        Attack.cell_select(target, "avenemy")
    elseif prev ~= nil then
        Attack.cell_select(prev, "destination")
    end

    --[[
    if empty_cell(target) then

        Attack.cell_select(target,"pathstart")
        while true do
            target = Attack.cell_adjacent(target, dir)
            if not empty_cell(target) then break end
            Attack.cell_select(target,"pathline")
        end
        Attack.cell_select(target,"pathend")

    end

    if target~=nil and Attack.act_takesdmg(target) then
        Attack.cell_select(target,"avenemy")
    end
]]
    return true

end

function orb_rollattack()

    local target = Attack.get_target()
    local dir = Game.Ang2Dir(Attack.angleto(0,target))
    target = Attack.cell_adjacent(0, dir)

    Attack.aseq_rotate(0, target)

	Attack.log_label('')
    --target = Attack.cell_adjacent(target, dir)
    if empty_cell(target) then

        Attack.act_no_cell_update(0)
        Attack.aseq_start(0, "start", "move")

        while true do
            target = Attack.cell_adjacent(target, dir)
            if not empty_cell(target) then break end
            Attack.aseq_move(0, "move")
        end

        if target~=nil and orb_check_target(target) then
            Attack.act_aseq(0, "rush", false, true)
            common_cell_apply_damage(target, Attack.aseq_time(0, "x"))
        else
			Attack.log_label('null')
            Attack.act_aseq(0, "stop", false, true)
        end

    elseif orb_check_target(target) then
        Attack.act_aseq(0, "attack")
        common_cell_apply_damage(target, Attack.aseq_time(0, "x"))
    end

    Attack.atk_min_time(Attack.aseq_time(0)+.2)

    return true

end

--------------
----Gizmo-----
--------------

function get_gizmo_ids()
    local gizmo_ids = {} -- ���������� ������, ��� ����� �����, ����� ��� �� ������ �� ���� ������
    for i=1, Attack.act_count()-1 do
        if Attack.act_name(i) == "gizmo" then
            gizmo_ids[Attack.cell_id(Attack.get_cell(i))] = true
        end
    end
    return gizmo_ids
end

function gizmo_calccells()

    local gizmo_ids = get_gizmo_ids()

    local ccnt = Attack.cell_count()
    for i=0,ccnt-1 do               -- for all cells
        if gizmo_ids[Attack.cell_id(Attack.cell_get(i))] ~= true then
            Attack.marktarget(Attack.cell_get(i))     -- select
        end
    end

    return true

end

function lina_gizmo()

    local target = Attack.get_target()
    local r = Game.Random()
    
  if Attack.is_short_spirit_seq() then
    Attack.act_aseq(0, "2cast")
    Attack.act_fadeout(0, 0, 1./25., 0., 0.)
    Attack.act_rotate( 0, .1, 0, target )
    Attack.cam_track_duration(7.6)
    if Game.ArenaShape() == 4 then
        if r <0.7 then
			Attack.cam_track(0, 0, "spirit_cam_short_ships.track" )
		else
            Attack.cam_track(0, 0, "spirit_cam_short_2left.track" )
		end
    else
      if r <= 0.44 then
        Attack.cam_track(0, 0, "spirit_cam_short_centre.track" )
      elseif r <= 0.88 then
        Attack.cam_track(0, 0, "spirit_cam_short_2left.track" )
      else
        Attack.cam_track(0, 0, "spirit_cam_short_2right.track" )
      end
    end
  else
    lina_appear_idle()

    --******************************** camera control
    if Game.ArenaShape() == 4 then --  ships
    	Attack.cam_track_duration(14.8)
        if r <0.7 then
			Attack.cam_track(0, 0, "spirit_cam_ships.track" )
		else
            Attack.cam_track(0, 0, "spirit_cam_2left.track" )
		end
    elseif Game.ArenaShape() == 1 then --  castle
         if r <= 0.35 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2r_castle.track" )
         elseif r <= 0.7 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2l_castle.track" )
         elseif r <= 0.9 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_r_castle.track" )
         elseif r <= 1.0 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_closeup_castle.track" )
         end
    elseif Game.ArenaShape() == 5 then --  water
    	Attack.cam_track_duration(14.8)
         if r <= 0.4 then
             Attack.cam_track(0, 0, "spirit_cam_centre.track" )
         elseif r <= 0.7 then
             Attack.cam_track(0, 0, "spirit_cam_2right.track" )
         else
             Attack.cam_track(0, 0, "spirit_cam_2left.track" )
         end
    else
         if r <= 0.45 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2r.track" )
         elseif r <= 0.75 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2l.track" )
         elseif r <= 0.9 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_r.track" )
         elseif r <= 1.0 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_closeup.track" )
         end
    end
    --********************************* End of camera control

    Attack.act_rotate( Attack.aseq_time(0, "x"), Attack.aseq_time(0, "y"), 0, target )

    Attack.act_aseq(0, "cast")
  end

    local start = Attack.aseq_time( 0, "y" );

    local atom = Attack.atom_spawn(target, start, "gizmo", 0)
    Attack.act_animate(atom, "appear", start)
    Attack.val_store(atom, "belligerent", Attack.act_belligerent())
    Attack.val_store(atom, "charges", 3)
    Attack.val_store(atom, "damage", Attack.get_custom_param("dmg.0")..'-'..Attack.get_custom_param("dmg.1"))
    Attack.val_store(atom, "heal", Attack.get_custom_param("heal"))
    Attack.val_store(atom, "apally", Attack.get_custom_param("apally"))
    Attack.val_store(atom, "apenemy", Attack.get_custom_param("apenemy"))
    Attack.val_store(atom, "dispellbad", Attack.get_custom_param("dispellbad"))
    Attack.val_store(atom, "dispellgood", Attack.get_custom_param("dispellgood"))
    Attack.resort( atom )

    spirit_after_hit()

    Attack.log("slime_fog", "name", "<label=cpn_lina>", "target", "<label=cpn_gizmo>")

    return true

end

function gizmo_attack()

	Attack.log_label("null")
    local bel = Attack.val_restore("belligerent")
    local charges = Attack.val_restore("charges")
    local apally = Attack.val_restore("apally") > 0
    local apenemy = Attack.val_restore("apenemy") > 0
    local dispellbad = Attack.val_restore("dispellbad") > 0
    local dispellgood = Attack.val_restore("dispellgood") > 0

    local under, target = Attack.get_caa(Attack.get_cell(0))

    local function ally_check(i)
    	if Attack.act_pawn(i) then return false end
        return --[[Attack.act_need_charge_or_reload(i) or ]](Attack.act_get_par(i,"health")-Attack.act_hp(i) > tonumber(Attack.val_restore("heal"))/3)
			or (dispellbad and takeoff_spells(i,"penalty",true))
			or (apally and Attack.act_ap(i) == 0) -- ������ ����� ���� ��� �������, ����� ������� � ����� (������ ������ �� ���� ������, ������� ����� ���� 1 �� ������ ��� - ������� ��� ������)
	end

    local can_stay_here = false
    if under ~= nil then
        if Attack.act_ally(under, bel) then
            can_stay_here = ally_check(under)
        elseif Attack.act_enemy(under, bel) then
            can_stay_here = true
        end
    end

    local gizmo_ids = {} -- ���������� ������, ��� ����� �����, ����� ��� �� ������ �� ���� ������
    for i=1, Attack.act_count()-1 do
        if Attack.act_name(i) == "gizmo" then
            gizmo_ids[Attack.cell_id(Attack.get_cell(i))] = true
        end
    end

    local ap, rand = 4, Game.Random(100)
    if can_stay_here and rand < 33 then
        -- ������� �� �����
        target = under
    else
        local dist = 1e3
        for a=1, Attack.act_count()-1 do
            local i = Attack.get_cell(a)
            if (under == nil or not Attack.act_equal(under, i)) and (Attack.act_enemy(i, bel) or (Attack.act_ally(i, bel) and ally_check(i))) then

                local d = Attack.cell_mdist(0, i)
                if d < dist and gizmo_ids[Attack.cell_id(Attack.get_cell(i))] ~= true then -- ������ �������� �����, ����� ����� �� �����������
                    dist = d
                    target = i
                end

            end
        end
        if can_stay_here and
            (dist / 1.8 > ap*1.5 or -- ��������� ���� ������� ������
            (dist / 1.8 > ap + 1 and rand < 66)) then -- ���� �� ����� ������, �� ��� ������� �������� (������ ���� �� ������, ����� ��������), ����� � ������������ 33% �� ������� �� ��, � � ����� �� - ��������� �� �����
                target = under
        end
    end

    if target ~= nil then
        local d = Attack.cell_mdist(0, target) / 1.8
        if d > ap + 1 then -- ����� +1 �����, ����� �� ���� ���, ��� ����� ����� �� ������ � ������ � ��� ���� ������ �� ������

            for i=ap,1,-1 do
                local dist = 1.8 * i
                local x, y = Attack.act_get_center(0)
                local dx, dy = Attack.act_get_center(target)
                dx = dx - x; dy = dy - y
                local len = math.sqrt(dx*dx + dy*dy)
                dx = dx*dist/len; dy = dy*dist/len
                local dest = Attack.find_nearest_cell(x + dx, y + dy)

                if gizmo_ids[Attack.cell_id(dest)] ~= true then
                    Attack.act_move(0, i, 0, dest)
                    break
                end
            end

        else

            Attack.act_move(0, d, 0, target)
            if Attack.act_ally(target, bel) then

				Attack.act_aseq(0, "cast"..charges)
				local max_hp = Attack.act_get_par(target,"health")
				local cure_hp=tonumber(Attack.val_restore("heal"))
				local cur_hp = Attack.act_hp(target)

				if cure_hp > max_hp - cur_hp then cure_hp = max_hp - cur_hp end

				--Attack.act_reload(target)
				--Attack.act_charge(target, 0)
				--- �����!
				Attack.act_cure(target, cure_hp, d + Attack.aseq_time(0, "x"))
				-- ����������� � �����
				if apally then
				    local new_ap
					if (Attack.act_again(target)) then new_ap=1 else new_ap=Attack.act_ap(target)+1 end
					Attack.act_ap(target, new_ap)
				end
				-- ������ ������� �������
				if dispellbad then takeoff_spells(target, "penalty") end
				--cure_1=^blog_td0^[name] ���������� [saname]. [targets] ��������������� [g][special][/c] ��������.
				if cure_hp >= 1 then
					Attack.log("cure_1","name","<label=blog_enemy_unit_img><label=blog_enemy_color><label=cpn_gizmo></color>","saname","<label=blog_ally_color><label=sp_gizmo_heal>","targets",blog_side_unit(target,-1),"special",cure_hp)
				end

            else

            -- �������!

				Attack.log_label("")
                Attack.act_aseq(0, "attack"..charges)
                Attack.atk_set_damage("astral", text_range_dec(Attack.val_restore("damage")))
                common_cell_apply_damage(target, d + Attack.aseq_time(0, "x"))

				if apenemy then
				    local cur_ap = Attack.act_ap(target)
				    if cur_ap > 1 then Attack.act_ap(target, cur_ap-1) end
				end

				if dispellgood then takeoff_spells(target, "bonus") end

				-- ��������� ������ ������ ��� ��������� �����
	            if charges == 1 then
	                Attack.act_aseq(0, "disappear")
	                Attack.act_remove(0, d + Attack.aseq_time(0))
	            else
	                Attack.val_store(0, "charges", charges-1, d+.1)
	            end

            end

            Attack.aseq_timeshift(0, d)

        end
    end

    return true

end

function gizmo_idle()

    Atom.animate("idle" .. Attack.val_restore("charges"))

end

-----------------
----Devatron-----
-----------------

function devatron_calccells()

  Attack.multiselect(3)

  for i=0, Attack.cell_count()-1 do
    local cell = Attack.cell_get(i)
    Attack.marktarget(cell)
  end

  return true

end

function get_devatron_cells()

    local ids = {}

    -- ���������� ���� ������� ��� 3-� ��������� ������
    for tar=0,2 do
        local t = Attack.get_target(tar)
        if t ~= nil then
            for i=0,5 do
                local c = Attack.cell_adjacent(t, i)
                if c ~= nil and Attack.cell_present(c) then
                    ids[Attack.cell_id(c)] = true
                end
            end
        end
    end

    -- ��������� ���� ��������� ������
    for tar=0,2 do
        local t = Attack.get_target(tar)
        if t ~= nil then
            ids[Attack.cell_id(t)] = nil
        end
    end

    local cells = {}
    for id in pairs(ids) do
        local c = Attack.cell_id(id)
        if c~=nil and Attack.cell_is_empty(c) and Attack.cell_is_pass(c) then
            table.insert(cells, c)
        end
    end

    return cells

end

function devatron_highlight()

    for i,c in ipairs(get_devatron_cells()) do
        Attack.cell_select(c, "destination")
    end

    return true

end

function lina_devatron()
    local r = Game.Random()
    local target = Attack.get_target()

  if Attack.is_short_spirit_seq() then
    Attack.act_aseq(0, "2cast")
    Attack.act_fadeout(0, 0, 1./25., 0., 0.)
    Attack.act_rotate( 0, .1, 0, target )
    Attack.cam_track_duration(7.6)
    if Game.ArenaShape() == 4 then
        if r <0.7 then
			Attack.cam_track(0, 0, "spirit_cam_short_ships.track" )
		else
            Attack.cam_track(0, 0, "spirit_cam_short_2left.track" )
		end
    else
      if r <= 0.44 then
        Attack.cam_track(0, 0, "spirit_cam_short_centre.track" )
      elseif r <= 0.88 then
        Attack.cam_track(0, 0, "spirit_cam_short_2left.track" )
      else
        Attack.cam_track(0, 0, "spirit_cam_short_2right.track" )
      end
    end
  else
    lina_appear_idle()

    --******************************** camera control
    if Game.ArenaShape() == 4 then --  ships
    	Attack.cam_track_duration(14.8)
        if r <0.7 then
			Attack.cam_track(0, 0, "spirit_cam_ships.track" )
		else
            Attack.cam_track(0, 0, "spirit_cam_2left.track" )
		end
    elseif Game.ArenaShape() == 1 then --  castle
         if r <= 0.35 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2r_castle.track" )
         elseif r <= 0.7 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2l_castle.track" )
         elseif r <= 0.9 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_r_castle.track" )
         elseif r <= 1.0 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_closeup_castle.track" )
         end
    elseif Game.ArenaShape() == 5 then --  water
         if r <= 0.5 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2r_castle.track" )
         else
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2l_castle.track" )
         end
    else
         if r <= 0.2 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2r.track" )
         elseif r <= 0.4 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_still2l.track" )
         elseif r <= 0.6 then
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_r.track" )
         else
             Attack.cam_track(0, 0, "cam_lina_rare_devatron_closeup.track" )
         end
    end
    --********************************* End of camera control

    Attack.act_rotate( Attack.aseq_time(0, "x"), Attack.aseq_time(0, "y"), 0, target )

    Attack.act_aseq(0, "cast")
  end

    local start = Attack.aseq_time( 0, "z" );

    for i,c in ipairs(get_devatron_cells()) do
        local t = start + Game.Random(000,600)/1000.
        local atom = Attack.atom_spawn(c, t, "devatron")
        Attack.act_animate(atom, "appear", t)
    end

    spirit_after_hit()
    Attack.log("slime_fog", "name", "<label=cpn_lina>", "target", "<label=cpsn_devatron>")

    return true

end
