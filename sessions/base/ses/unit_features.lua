function features_looter(damage,addrage,attacker,receiver,minmax,userdata,hitbacking)
	
	if (minmax==0) then
	local gold=10 -- 
	local damage,dead=Attack.act_damage_results(receiver)	
	if dead~=nil then 
  if dead>0 and Attack.act_human(receiver)	 then
  	local lead=Attack.act_leadership(receiver)
  	local cost=math.ceil(dead*lead*gold/100)
  	local cur_gold=Logic.hero_lu_item("money","count")
  	Logic.hero_lu_item("money","count",cur_gold+cost)
  	
  	Attack.log(.01, "looter_log", "special", cost)
  end   
  end 
  end
   
  return damage,addrage
end

function features_increase_anger(damage,addrage,attacker,receiver,minmax,userdata,hitbacking)
	
	if (minmax==0) and not hitbacking then
    local bonus=tonumber(Logic.obj_par("feat_increase_anger","power")) -- ��������� �����
    local krit_bonus=tonumber(Logic.obj_par("feat_increase_anger","krit_bonus")) -- ��������� �����
    local duration=tonumber(Logic.obj_par("feat_increase_anger","duration"))
    local cur_bonus=0
    local cur_krit_bonus=0
    
		if Attack.act_is_spell(attacker,"feat_increase_anger") then -- ���� ����� �����, ���� �� ���� ������
    	cur_bonus=tonumber(Attack.act_spell_param(attacker, "feat_increase_anger", "dmg_bonus"))
    	cur_krit_bonus=tonumber(Attack.act_spell_param(attacker, "feat_increase_anger", "dmg_bonus"))
    end 
    
    if cur_bonus==nil then cur_bonus=0 end 
    if cur_krit_bonus==nil then cur_bonus=0 end 
    if cur_bonus<15 then 
    	-- ����� ����������� �����, � ��� ������ ����� � �����
			Attack.act_del_spell( attacker, "feat_increase_anger" )
      Attack.act_apply_spell_begin(attacker, "feat_increase_anger", duration, false )
				Attack.act_spell_param(attacker, "feat_increase_anger", "dmg_bonus", bonus+cur_bonus)
				--Attack.act_apply_par_spell("attack", bonus+cur_bonus, 0, 0, 1, false)
				Attack.act_apply_dmg_spell("physical", bonus+cur_bonus, 0, 0, duration, false)
				Attack.act_apply_par_spell("krit", krit_bonus+cur_krit_bonus, 0, 0, duration, false)
      Attack.act_apply_spell_end()
			  Attack.act_damage_addlog(receiver,"add_blog_anger_",true)
			  Attack.log_special(blog_side_unit(attacker,-1)) -- ��������  
			      
      --Attack.atom_spawn(attacker, 0, "magic_haste", Attack.angleto(attacker))
     end
   end 
    return damage,addrage
end

function features_brutality(damage,addrage,attacker,receiver,minmax,userdata,hitbacking)

if (minmax==0 and Attack.get_caa(attacker)~=nil) then -- ���� ��� �� �����
    local bonus=Game.Random(text_range_dec(Logic.obj_par("feat_brutality","power"))) -- ��������� �����
    local duration=tonumber(Logic.obj_par("feat_brutality","duration"))
    local cur_bonus=0
--    local added=Logic.obj_par("feat_brutality","added")
  
    
		if not Attack.act_is_spell(receiver,"feat_brutality") and not hitbacking then -- ���� ����� �����, ���� �� ���� ������
       Attack.act_apply_spell_begin(receiver, "feat_brutality", duration, false )
				Attack.act_apply_par_spell("attack", 0, bonus, 0, duration, false)
      Attack.act_apply_spell_end()
			Attack.atom_spawn(receiver, 0, "effect_brutality", Attack.angleto(receiver), true)
			
		local dmg,dead=Attack.act_damage_results(receiver)
		if dead~=Attack.act_size(receiver) then
   	if Attack.act_size(receiver)>1 then 
			Attack.log(receiver,0.001,"add_blog_brutality_2","special",blog_side_unit(attacker,2)..blog_side_unit(attacker,3)..blog_side_unit(receiver,-1).."</color>")
		else
			Attack.log(receiver,0.001,"add_blog_brutality_1","special",blog_side_unit(attacker,2)..blog_side_unit(attacker,3)..blog_side_unit(receiver,-1).."</color>")
		end 
		end 
			  
			  --Attack.log_special() -- ��������  
			     
    end   
end    
    return damage,addrage
end

function features_soul_drain(damage,addrage,attacker,receiver,minmax )

	if (minmax==0) and damage>0 then
	if Attack.act_enemy(receiver) and not (Attack.act_feature(receiver,"undead")) and not  (Attack.act_feature(receiver,"plant")) and not (Attack.act_feature(receiver,"golem")) and not (Attack.act_feature(receiver,"pawn"))then
    local k=tonumber(Attack.get_custom_param("k"))
  if k>0 then 
    --local receiver=Attack.get_target(1)  -- ����?
    --local attacker=Attack.get_target(0)  -- ���?
    --local count_1 = Attack.act_size(attacker)
    
    local killed = math.min( AU.unitcount( receiver ) ,damage/ AU.health( receiver )) -- ������� ��������
    local drain=damage --������ ����
    local real_max_hp=Attack.act_totalhp(receiver)
    if real_max_hp<drain then drain = real_max_hp end
    drain=math.floor(drain*k/100)
    local attacker_count = AU.unitcount( attacker )
    local add_unit=math.floor(drain/AU.health( attacker )) --�� �������� ����������� ��� ������
    local heal_unit=drain-add_unit*AU.health( attacker )  -- �� ������� ������� ��������
		
		Attack.act_size(attacker, attacker_count + add_unit)
		Attack.act_cure(attacker, heal_unit, 1)
		
 	    --local count_2 = Attack.act_size(attacker)
 	    
    	Attack.atom_spawn(attacker, 0, "effect_total_cure")
    	local log_msg="add_blog_vamp_0"
	    local special = heal_unit
	    
    	if add_unit>0 then 
    		log_msg="add_blog_soul_"
	    	special = add_unit
	   	end 
			  Attack.act_damage_addlog(receiver,log_msg,true)
			  Attack.log_special(special) -- ��������  

	end 
	end 
	end 
    return damage,addrage
end

function features_vampirism( damage,addrage,attacker,receiver,minmax ) -- minmax, ������ 1 ��� 2 ��������, ��� ������� ���������� ������ ��� ����������� ���/���� ����� �� ���.���������

    if (minmax==0) and damage>0 then

    -- ������� ����� � ������
	   if --[[Attack.act_need_cure(attacker) or ]]Attack.cell_need_resurrect(attacker) then
	   if Attack.act_enemy(receiver) and not (Attack.act_feature(receiver,"undead")) and not  (Attack.act_feature(receiver,"plant")) and not (Attack.act_feature(receiver,"golem")) and not (Attack.act_feature(receiver,"pawn")) then
			local count_1 = Attack.act_size(attacker)

	   	local vamp = math.min(Attack.act_totalhp(receiver), damage)
	   	local hp1 = Attack.act_totalhp(attacker)
 	    Attack.act_resurrect(attacker, math.floor(vamp+.5))

 	    local count_2 = Attack.act_size(attacker)

    	Attack.atom_spawn(attacker, 0, "effect_total_cure")
    	local log_msg="add_blog_vamp_"

		local special=count_2-count_1

    	if count_2==count_1  then
    		log_msg=log_msg.."0"
    		special = Attack.act_totalhp(attacker) - hp1
    	end

	  Attack.act_damage_addlog(receiver,log_msg,true)
	  Attack.log_special(special) -- ��������  
 				   	
    end 
    end

   end

    return damage,addrage
end

function features_fire_shield( damage,addrage,attacker,receiver,minmax )

    if (minmax==0) then

    --local receiver=Attack.get_target(1)  -- ����?
    --local attacker=Attack.get_target(0)  -- ���?
    -- ������� ����� � ������
    local min_dmg,max_dmg

	if receiver~= nil then

--	if Attack.cell_dist(attacker,receiver)==1 then
   	if Attack.act_name(receiver)=="cerberus" then
   		min_dmg,max_dmg=text_range_dec(Logic.obj_par("feat_fire_shield","cerberus"))
   	else 
   		min_dmg,max_dmg=text_range_dec(Logic.obj_par("feat_fire_shield","phoenix"))
    end
    local count=Attack.act_size(receiver)
    local dmg_type = Logic.obj_par("feat_fire_shield","typedmg")
    
--    if Attack.act_need_cure(attacker) then
--     Attack.atk_set_damage(dmg_type,min_dmg,max_dmg)
     
     --local a = Attack.atom_spawn(receiver, 0, "magic_haste")
     local dmgts = Attack.aseq_time(a, "x")
--     common_cell_apply_damage(attacker, dmgts)
--    end
--    end
	end
	
	end
    return damage,addrage
end

function features_shock( damage,addrage,attacker,receiver,minmax )

    if (minmax==0)  and damage>0 then

    --local receiver=Attack.get_target(1)  -- ����?
    
    local shock=nil
    
    if Attack.act_name(attacker)=="archmage" and Attack.act_is_spell(attacker,"special_battle_mage") then
    	shock=tonumber(Attack.act_spell_param(attacker, "special_battle_mage", "shock"))
    end 
    if shock==nil then shock=tonumber(Attack.get_custom_param("shock")) end
    local rnd=Game.Random(100)
            
    if rnd<=shock and Attack.act_level(receiver)<5 and not Attack.act_feature(receiver,"pawn") and not Attack.act_feature(receiver,"boss") then 
        effect_shock_attack(receiver,1,duration)
    end

    end 

    return damage,addrage
end

function features_poison( damage,addrage,attacker,receiver,minmax )

    if (minmax==0)  and damage>0 then

    --local receiver=Attack.get_target(1)  -- ����?
    local poison=tonumber(Attack.get_custom_param("poison"))
    local rnd=Game.Random(100)
    
    if rnd<=poison then -- and (not Attack.act_feature(receiver,"poison_immunitet") or Attack.act_race("undead")) then 
        effect_poison_attack(receiver,0,3)
    end

    end 

    return damage,addrage
end

-- ***********************************************
-- * Stun
-- ***********************************************

function features_stun(damage,addrage,attacker,receiver,minmax)
	if (minmax==0)  and damage>0 then
	
	local stun=tonumber(Attack.get_custom_param("stun"))

				
	local rnd=Game.Random(100)+1
  if rnd<=stun and Attack.act_level(receiver)<5 then
  	 effect_stun_attack(receiver,0,1)
  	 return -damage,addrage
  end 
  end 
	
  return damage,addrage
end


function features_burn( damage,addrage,attacker,receiver,minmax )

    if (minmax==0)  and damage>0 then

    --local receiver=Attack.get_target(1)  -- ����?
    local burn=tonumber(Attack.get_custom_param("burn"))
    local rnd=Game.Random(100)
    
    if rnd<=burn then --and (not Attack.act_feature(receiver,"poison_immunitet") or Attack.act_race("undead")) then 
        effect_burn_attack(receiver,0,3)
    end

    end 

    return damage,addrage
end


--*************************************************************************
--   ������ �����������
--*************************************************************************

function post_spell_dragon_slayer(damage,addrage,attacker,receiver,minmax,userdata )

		local spell = "spell_dragon_slayer"
		
    --local receiver=Attack.get_target(1)  -- ����?
    --local attacker=Attack.get_target(0)  -- ���?
    
    if Attack.act_is_spell(attacker,spell) then
    
		local level=tonumber(userdata);

  		local bonus_damage = pwr_dragon_slayer(level)
    
    	if Attack.act_feature(receiver,"dragon") then 
      	return damage*(1+bonus_damage/100),addrage*(1+bonus_damage/100)
    	end 
	else       	
		    if (minmax==0) then	Attack.act_posthitmaster(attacker,"post_spell_dragon_slayer",0) end
  	end
	return damage,addrage
end

function features_holy_attack(damage,addrage,attacker,receiver,minmax,userdata)
	
	if Attack.act_is_spell(attacker,"special_holy_rage") then
		if Attack.act_race(receiver,"undead") then
			local bonus=50
			--tonumber(userdata)
			damage=damage*(1+bonus/100)
			addrage=addrage*(1+bonus/100)
		end  
	else
		 if (minmax==0) then	Attack.act_posthitmaster(attacker,"special_holy_rage",0) end
	end 
	
	return damage,addrage
end 

function special_priest(damage,addrage,attacker,receiver,minmax,userdata)

	if Attack.act_race(receiver,"undead") then
		damage = damage * 2
		addrage = addrage * 2
	end
	return damage, addrage

end

function post_spell_demon_slayer(damage,addrage,attacker,receiver,minmax,userdata )

	local spell = "spell_demon_slayer"
		
    --local receiver=Attack.get_target(1)  -- ����?
    --local attacker=Attack.get_target(0)  -- ���?

    if Attack.act_is_spell(attacker,spell) then
    
		local level=tonumber(userdata);

  		local bonus_damage = pwr_demon_slayer(level)
    
    	if Attack.act_feature(receiver,"demon") then 
	      	return damage*(1+bonus_damage/100),addrage*(1+bonus_damage/100)
    	end

	else

	    if (minmax==0) then
			Attack.act_posthitmaster(attacker,"post_spell_demon_slayer",0)
		end

  	end

	return damage,addrage

end

function post_spell_mana_source(damage,addrage,attacker,receiver,minmax,userdata )

	if (minmax==0) and damage>0 then
	
		local spell = "spell_magic_source"
		
--    local receiver=Attack.get_target(1)  -- ����?
--    local attacker=Attack.get_target(0)  -- ���?

    if Attack.act_is_spell(receiver,spell) then
    
    --	local level=math.floor(Game.Random(1,4))
    	--local level=Obj.spell_level()
  --  	if level==0 then level=1 end
		local level=tonumber(userdata);

  		local mana = tonumber(text_dec(Logic.obj_par(spell,"mana"),level))
			local cur_mana=Logic.hero_lu_item("mana","count")
			local max_mana=Logic.hero_lu_item("mana","limit")
			
			local add_mana=math.min(mana,max_mana-cur_mana)
			Logic.hero_lu_item("mana","count",cur_mana+add_mana)

--add_blog_mana_source_1=^blog_td0^[name] �������� �� ����. ���������� [special] ���� ����� [hero_name] [g][target][/c] ����.			
			local heroname = Attack.act_spell_param(receiver, spell, "heroname")

			if add_mana>0 then
			
		    local dur = Attack.act_spell_duration(receiver, "spell_magic_source")
			    if dur <= 1 then
					Attack.act_posthitslave(receiver,"post_spell_mana_source",0)
					Attack.act_del_spell( receiver, "spell_magic_source" )
				else
				    Attack.act_spell_duration(receiver, "spell_magic_source", dur-1)
				end

			
			if Attack.act_size(receiver)>1 then 
				Attack.log(0.001,"add_blog_mana_source_2","name",blog_side_unit(receiver),"special",blog_side_unit(receiver,3).."<label=spell_magic_source_name></color>","target",add_mana,"hero_name",heroname)
			else 
				Attack.log(0.001,"add_blog_mana_source_1","name",blog_side_unit(receiver),"special",blog_side_unit(receiver,3).."<label=spell_magic_source_name></color>","target",add_mana,"hero_name",heroname)
			end 
			end
		
			return damage,addrage
		else       	
		   Attack.act_posthitslave(receiver,"post_spell_mana_source",0)
  		   return damage,addrage
  	  end

     else
         return damage,addrage
	 end
end



function post_spell_last_hero(damage,addrage,attacker,receiver,minmax )

		local spell = "spell_last_hero"

--    local receiver=Attack.get_target(1)  -- ����?
--    local attacker=Attack.get_target(0)  -- ���?

    if Attack.act_is_spell(receiver,spell) then

--    	local level=math.floor(Game.Random(1,4))
    	--local level=Obj.spell_level() 
--    	if level==0 then level=1 end
--		local deadR = Attack.act_damage(receiver,true)
		local dam, thp = math.floor(damage+.5), Attack.act_totalhp(receiver)
		local deadA = dam >= thp--Attack.act_damage(attacker,true)
		if deadA and Attack.act_size(receiver) > 0 then
--			local hitR=Attack.act_hp(receiver) -- ������� ���� �����
--			local countR=Attack.act_size(receiver) -- ������� ������ � ������. ���� ����������� �������� � ������ ���������
			--[[local hitA=Attack.act_hp(attacker) -- ������� ���� �����
			local countA=Attack.act_size(attacker) -- ������� ������ � ������. ���� ����������� �������� � ������ ���������

--			if count==1 then 
				--damage=damage-hit	-- ���������� ��� � ����� ������ �������� ���� � ����������� ����������
		    Attack.atom_spawn(attacker, 0, "hll_priest_resur_cast")
		    Attack.cell_resurrect(attacker, hit)
--			else 
				--damage=damage-hit
--		    Attack.atom_spawn(receiver, 0, "hll_priest_resur_cast")
--		    Attack.cell_resurrect(receiver, hit)
--			end ]]
			if minmax == 0 then
			    local dur = Attack.act_spell_duration(receiver, spell)
			    if dur <= 1 then
					Attack.act_posthitslave(receiver,"post_spell_last_hero",0)
					Attack.act_del_spell( receiver, spell )
				else
				    Attack.act_spell_duration(receiver, spell, dur-1)
				end
				Attack.log(0.001,"add_blog_last_hero","target",blog_side_unit(receiver,-1),"special","<label=spell_last_hero_name></color>")
			end
			return thp-1
		end
		return damage, addrage
	else       	
		Attack.act_posthitslave(receiver,"post_spell_last_hero",0)
  		return damage, addrage
  	end

end



-- ***********************************************
-- * Freeze or Burn
-- ***********************************************

function special_bowman(damage,addrage,attacker,receiver,minmax)

	local freeze_im=0.75 --25%
	local freeze=tonumber(Attack.get_custom_param("freeze"))

	local dragon=tonumber(Attack.get_custom_param("dragon"))
	
	if dragon == 1 then	
		return feat_dragon_arrow(damage,addrage,attacker,receiver,minmax)
	end 

	if minmax == 0 and damage ~= 0 then -- damage=0 ����� �� �������������

		local burn=tonumber(Attack.get_custom_param("burn"))

		--	if burn==nil then burn=100 end
		--	if freeze==nil then freeze=100 end

		local rnd=Game.Random(100)+1
		if rnd<=burn and not Attack.act_feature(receiver,"fire_immunitet") then
			effect_burn_attack(receiver,0,3)
			Attack.log_label("")
		end
		local cold_fear=Attack.act_get_res(receiver,"fire")
		if (rnd<=freeze or (cold_fear>=50 and freeze>10)) and not Attack.act_feature(receiver,"freeze_immunitet") and damage>0 and not Attack.act_feature(receiver,"pawn") and not Attack.act_feature(receiver,"boss") then
			effect_freeze_attack(receiver,0,3)
			--Attack.log_label("add_blog_freeze_") -- ��������
		end

	end

	if freeze>0 and Attack.act_feature(receiver,"freeze_immunitet") then
		damage=damage*freeze_im
		addrage=addrage*freeze_im
	end

	local dragon=Attack.get_custom_param("dragon")
  if dragon=="1" then
		return feat_dragon_arrow(damage,addrage,attacker,receiver,minmax)
	end 

  return damage,addrage
end

function feat_dragon_arrow(damage,addrage,attacker,receiver,minmax)
	
	local dragon=Attack.get_custom_param("dragon")
  if dragon=="1" then

  if damage<0 then
    damage=0
  end

	local k = AU.attack( attacker ) - AU.defence( receiver )
	local k2 = AU.attack( attacker )

  if k>=0 and k<60 then
    damage = damage /(1+k*0.0333)
  end
  if k>=60 then
    damage = damage /3
  end
  if k<0 and k>=-60 then
    damage = damage * (1-k*0.0333)
  end
  if k<-60 then
    damage = damage * 3
  end

  if damage<0 then
    damage=0
  end

 	  if k2<60 then
    	damage = damage *(1+k2*0.0333)
  	end
  	if k2>=60 then
    	damage = damage *3
  	end
  end 

  return damage
end

-- ***********************************************
-- * Poison or Unbaff
-- ***********************************************

function special_archer(damage,addrage,attacker,receiver,minmax)

	local dragon=tonumber(Attack.get_custom_param("dragon"))
	
	if dragon == 1 then	
		return feat_dragon_arrow(damage,addrage,attacker,receiver,minmax)
	end 

	if (minmax==0) and damage>0 then
	
	local poison=tonumber(Attack.get_custom_param("poison"))
	local tranc=tonumber(Attack.get_custom_param("tranc"))

	local spell_name=""
--	if burn==nil then burn=100 end 
--	if freeze==nil then freeze=100 end
				
	local rnd=Game.Random(100)+1
  if rnd<=poison and not Attack.act_feature(receiver,"poison_immunitet") then
  	 effect_poison_attack(receiver,0,3)
  end 
  if tranc>0 then
    local spells_to_delete={}
    	local spell_count=Attack.act_spell_count(receiver)
      for i=0,spell_count-1 do 
      	spell_name=Attack.act_spell_name(receiver,i)
      	local spell_type=Logic.obj_par(spell_name,"type")
      	if spell_type=="bonus" and string.find(spell_name,"^totem_")==nil then 
					table.insert(spells_to_delete,spell_name);
  	 		end 
   		end 
		local spell_del_count=table.getn(spells_to_delete)
		if spell_del_count>0 then
			local del_spell=Game.Random(1,spell_del_count)
			Attack.act_del_spell(receiver,spells_to_delete[del_spell])

		  Attack.act_damage_addlog(receiver,"add_blog_unbaff_")
		  Attack.log_special("<label="..spells_to_delete[del_spell].."_name>") -- ��������  
			
			Attack.atom_spawn(receiver, 0, "magic_dispel",0,true)
    end
  end
	end 
	
	local dragon=Attack.get_custom_param("dragon")
  if dragon=="1" then
  	local attack=Attack.act_get_par(attacker)
 	  if attack<60 then
    	damage = damage *(1+attack*0.0333)
  	end
  	if attack>=60 then
    	damage = damage *3
  	end
  end 
	
  return damage,addrage
end


-- ***********************************************
-- * ������������ 
-- ***********************************************

function features_bleeding(damage,addrage,attacker,receiver,minmax,userdata,hitbacking)
		
	if minmax == 0 then	
    local power=tonumber(Logic.obj_par("feat_bleeding","power")) -- ��������� �����
    local duration=tonumber(Logic.obj_par("feat_bleeding","duration"))
		
		if not Attack.act_feature(receiver,"plant") and not Attack.act_feature(receiver,"undead") and not Attack.act_feature(receiver,"golem") and not hitbacking and Attack.act_enemy(receiver)  and damage>0 and not Attack.act_feature(receiver,"pawn") and not Attack.act_feature(receiver,"boss") then
			Attack.act_del_spell( receiver, "feat_bleeding" )
      Attack.act_apply_spell_begin(receiver, "feat_bleeding", duration, false )
				Attack.act_apply_par_spell("attack", 0, -power, 0, duration, false)
				Attack.act_apply_par_spell("defense", 0, -power, 0, duration, false)
      Attack.act_apply_spell_end()
 			  Attack.act_damage_addlog(receiver,"add_blog_bleeding_")
			  Attack.log_special(blog_side_unit(receiver,-1)) -- ��������  
		end 
  end 
  return damage,addrage
end

-- ***********************************************
-- * ����� �������� � ���������
-- ***********************************************

function special_alchemist( damage,addrage,attacker,receiver,minmax )

  if (minmax==0) and damage>0 then
		
   	local poison=tonumber(Attack.get_custom_param("poison"))
		local burn=tonumber(Attack.get_custom_param("burn"))
		local holy=tonumber(Attack.get_custom_param("holy"))

    local rnd=Game.Random(100)+1
    if poison == 10 then
       local a = Attack.atom_spawn(0,0,"hll_acidcannon1",Attack.angleto(0,receiver))
      local atk_x = Attack.aseq_time(0, "x")                   -- x time of attacker
	    local acid_x = Attack.aseq_time(a, "x")
  	  local acid_y = Attack.aseq_time(a, "y")
    	Attack.aseq_timeshift( 0, acid_x - atk_x )
    end 
    
    if rnd<=poison then -- and (not Attack.act_feature(receiver,"poison_immunitet") or Attack.act_race("undead")) then 
        effect_poison_attack(receiver,0,3)
        --Attack.atom_spawn(receiver, 0, "hll_shaman_post")
    end
    if rnd<=burn then -- and (not Attack.act_feature(receiver,"poison_immunitet") or Attack.act_race("undead")) then 
        effect_burn_attack(receiver,0,3)
    end
    if rnd<=holy then -- and (not Attack.act_feature(receiver,"poison_immunitet") or Attack.act_race("undead")) then 
        effect_holy_attack(receiver,0,3)
    end
  end

    return damage,addrage
end

function features_weakness(damage,addrage,attacker,receiver,minmax )

    if (minmax==0) then

    --local receiver=Attack.get_target(1)  -- ����?
    local weakness=tonumber(Attack.get_custom_param("weakness"))
    local rnd=Game.Random(100)+1
    
    if rnd<=weakness  and damage>0 and not Attack.act_feature(receiver,"pawn") and not Attack.act_feature(receiver,"boss") and Attack.act_level(receiver)<5 then
        effect_weakness_attack(receiver,1,1)
    end

    end 

    return damage,addrage
end

-- ***********************************************
-- * Push or Stun
-- ***********************************************

function special_cyclop(damage,addrage,attacker,receiver,minmax)
	if (minmax==0) then
	
	local stun=tonumber(Attack.get_custom_param("stun"))
	local push=tonumber(Attack.get_custom_param("push"))
	
--	if burn==nil then burn=100 end 
--	if freeze==nil then freeze=100 end
				
	local rnd=Game.Random(100)+1
  if rnd<=stun  and damage>0 and not Attack.act_feature(receiver,"pawn") and not Attack.act_feature(receiver,"boss") then
  	 effect_stun_attack(receiver,0,1)
  end 
  if rnd<=push then --and not Attack.act_feature(receiver,"freeze_immunitet") then
  	local t = receiver --Attack.get_target()
  	local dir = Attack.act_dir(0) --cell_dir( t )
  	local c = Attack.cell_adjacent( t, dir )
  	local push1 = Attack.cell_present(c) and Attack.cell_is_empty(c)

  	Attack.act_aseq( 0, "attack" )
  	local movtime0 = Attack.aseq_time( 0, "x" )

  	if push1 then
    	local movtime1 = Attack.aseq_time( 0, "y" )

--    Attack.atk_set_damage(dmg_type,min_dmg,max_dmg) --
		if Attack.act_get_par(t, "dismove") == 0 then
      		Attack.act_move( movtime0, movtime1, t, c )
		end
--      common_cell_apply_damage(t, (movtime0+movtime1)/2)
  	end

  	if (not push1) then
  --    Attack.atk_set_damage(dmg_type,min_dmg*2,max_dmg*2) --
  --    common_cell_apply_damage(t, movtime0)
  	end  
  	 --effect_freeze_attack(receiver,0,3)
  	end 
	end 
	
  return damage,addrage
end

-- ***********************************************
-- * ���������
-- ***********************************************

function features_rabid(damage,addrage,attacker,receiver,minmax)
	if (minmax==0) then		
    local level=tonumber(Logic.obj_par("feat_rabid","level")) -- ��������� �����
	  local duration=tonumber(Logic.obj_par("feat_rabid","duration"))
		local rabid=tonumber(Attack.get_custom_param("rabid"))
		if duration==nil then duration=tonumber(Attack.get_custom_param("duration")) end 
		
		local rnd=Game.Random(100)+1
		if rnd<rabid and Attack.act_level(receiver)<=level and not Attack.act_feature(receiver,"undead") and not Attack.act_feature(receiver,"golem") and not Attack.act_feature(receiver,"plant")  and damage>0 and not Attack.act_feature(receiver,"pawn") and not Attack.act_feature(receiver,"boss") then
			
  		Attack.act_belligerent(receiver, 16)

			Attack.act_del_spell( receiver, "feat_rabid" )
      Attack.act_apply_spell_begin(receiver, "feat_rabid", duration, false )
				Attack.act_spell_param(receiver, "feat_rabid", "rabid", 1)
            	Attack.act_apply_par_spell("autofight", 1, 0, 0, duration, false)
				Attack.atom_spawn(receiver, 0, "effect_bullhead",0,true)				
      Attack.act_apply_spell_end()
		  Attack.act_damage_addlog(receiver,"add_blog_rabid_")
		  Attack.log_special(blog_side_unit(receiver,-1)) -- ��������  
		end 
	end 
  return damage,addrage
end

function features_rabid_onremove(caa)

  -- ���������� ��� ��� ����
  Attack.act_belligerent(caa, Attack.act_belligerent( caa, nil ))
  Attack.atom_spawn(caa, 0, "effect_bullhead", 0, true)
			if Attack.act_size(caa)>1 then 
				Attack.log(0.001,"add_blog_norabid_2","name",blog_side_unit(caa))
			else 
				Attack.log(0.001,"add_blog_norabid_1","name",blog_side_unit(caa))
			end 
  return true

end


-- ***********************************************
-- * ���������
-- ***********************************************

function features_curse(damage,addrage,attacker,receiver,minmax)
	if (minmax==0) then		
    local level = tonumber(Logic.obj_par("effect_curse","level"))
		local duration = tonumber(Logic.obj_par("effect_curse","duration")) 
		local curse=tonumber(Attack.get_custom_param("curse"))		
		
		local rnd=Game.Random(100)+1
		if (rnd<curse) and (Attack.act_level(receiver)<=level) and not (Attack.act_feature(receiver,"undead"))  and damage>0 and not Attack.act_feature(receiver,"pawn") and not Attack.act_feature(receiver,"boss") then
			effect_curse_attack(receiver,1,duration)     
		end 
	end 
  return damage,addrage
end

-- ***********************************************
-- * ��������� 
-- ***********************************************

function features_sleep(damage,addrage,attacker,receiver,minmax)
	if (minmax==0) then		
    local level = tonumber(Logic.obj_par("effect_sleep","level"))
    local duration = tonumber(Attack.get_custom_param("duration"))
    if duration == nil then duration = tonumber(Logic.obj_par("effect_sleep","duration")) end 
    
    local special=tonumber(Attack.get_custom_param("special"))		
		local dod=Attack.get_custom_param("dod")
		local ddd=true
			if dod=="yes" then ddd=true else ddd=false end
		local sleep=tonumber(Attack.get_custom_param("sleep"))		
	local rnd=Game.Random(100)+1	
	if special==0 then
		if (rnd<sleep) and (Attack.act_level(receiver)<=level) and not (Attack.act_feature(receiver,"undead")) and not (Attack.act_feature(receiver,"mind_immunitet"))  and damage>0 and not Attack.act_feature(receiver,"pawn") and not Attack.act_feature(receiver,"boss") then
			effect_sleep_attack(receiver,2,duration,ddd)     
			Attack.act_damage_addlog(receiver,"add_blog_sleep_")

		end 
	else if not (Attack.act_feature(receiver,"undead")) and not (Attack.act_feature(receiver,"mind_immunitet"))  and damage>0 and not Attack.act_feature(receiver,"pawn") and not Attack.act_feature(receiver,"boss") then
			if Attack.act_level(receiver)==1 then
				effect_sleep_attack(receiver,2,duration,ddd)
				Attack.act_damage_addlog(receiver,"add_blog_sleep_")
			end	
			if Attack.act_level(receiver)==2 and rnd<50 then
				effect_sleep_attack(receiver,2,duration,ddd)
				Attack.act_damage_addlog(receiver,"add_blog_sleep_")
			end	
			if Attack.act_level(receiver)==3 and rnd<25 then
				effect_sleep_attack(receiver,2,duration,ddd)     
				Attack.act_damage_addlog(receiver,"add_blog_sleep_")
			end	
			if Attack.act_level(receiver)==4 and rnd<10 then
				effect_sleep_attack(receiver,2,duration,ddd)     
				Attack.act_damage_addlog(receiver,"add_blog_sleep_")
			end	
		end 	
	end 
	
	end 
  return damage,addrage
end

-- ***********************************************
-- * ����������
-- ***********************************************

function features_charm(damage,addrage,attacker,receiver,minmax)
	if (minmax==0) then		
    local level = tonumber(Logic.obj_par("effect_charm","level"))
		local duration = tonumber(Logic.obj_par("effect_charm","duration"))
		local charm=tonumber(Attack.get_custom_param("charm"))		

		local rnd=Game.Random(100)+1

		if (rnd<charm) and (Attack.act_level(receiver)<=level) and not (Attack.act_feature(receiver,"undead")) and (Attack.act_feature(receiver,"humanoid"))  and damage>0 and not Attack.act_feature(receiver,"pawn") and not Attack.act_feature(receiver,"boss") then
			local caster_count=Attack.act_size(attacker)	-- ������� �����
		--��������� ����� � ���� 
			local caster_lead=Attack.act_leadership(attacker)
			local target_lead=Attack.act_leadership(receiver)
			local target_count=Attack.act_size(receiver)
		-- ������� ����� ���������� �� ���������
			if caster_lead*caster_count>target_lead*target_count and Attack.act_level(receiver)<5 then

				--local dmgts = Attack.aseq_time(target, "x")
				effect_charm_attack(receiver,0,duration)
				Attack.log_label("charm_")
				return CHARM, 0
			end 
		end 
	end 

  Attack.log_label("")
  return damage,addrage
end

function features_regeneration()
		
	    if Attack.act_need_cure(0) then
	    	local cur_hp = Attack.act_hp(0)
	    	local max_hp = Attack.act_get_par(0,"health")
								
				Attack.atom_spawn(0, 0, "effect_total_cure")
				--dmgts = Attack.aseq_time("hll_priest_heal_post", "y")
				Attack.act_cure(0, max_hp - cur_hp, 0)
    		Attack.log_label("add_blog_regen_1") -- ��������
    		Attack.act_aseq( 0, "idle" )	

			if Attack.act_size(0)>0 then 
				Attack.log("add_blog_regen_2","name",blog_side_unit(0))
			else 
				Attack.log("add_blog_regen_1","name",blog_side_unit(0))
			end 

    	end 

    return true
end

function features_auto_dispell(tend)
		
		if takeoff_spells(0, "penalty") then 		

	  		Attack.act_aseq( 0, "special" )	
--		  	local dmgts = Attack.aseq_time(0, "x")

			if Attack.act_size(0)>0 then 
				Attack.log(tend+.05,"add_blog_autodispell_2","name",blog_side_unit(0))
			else 
				Attack.log(tend+.05,"add_blog_autodispell_1","name",blog_side_unit(0))
			end 
--			Attack.atom_spawn(0, dmgts, "magic_dispel")
		
		end
			    
    return true
end


function features_devil_fear(damage,addrage,attacker,receiver,minmax,userdata,hitbacking )

	if minmax == 0 and not hitbacking then
--				Attack.aseq_remove(attacker,"attack")
        --Attack.atom_spawn(receiver, 0, "magic_dispel")
--  			Attack.act_aseq(attacker, "special" )	        
		local fear=tonumber(Attack.get_custom_param("fear"))
		local level=tonumber(Attack.get_custom_param("level"))
		local duration=tonumber(Attack.get_custom_param("duration"))
		local rnd=Game.Random(0,100)+1
		if rnd<fear and Attack.act_level(receiver)<=level and not Attack.act_feature(receiver,"mind_immunitet") and not Attack.act_feature(receiver,"undead")  and damage>0 and not Attack.act_feature(receiver,"pawn") and not Attack.act_feature(receiver,"boss") then
            Attack.act_apply_spell_begin( receiver, "effect_fear", duration, false )
            	Attack.act_apply_par_spell("autofight", 1, 0, 0, duration, false)
            Attack.act_apply_spell_end()
					Attack.act_damage_addlog(receiver,"add_blog_fear_")
					Attack.atom_spawn(receiver, 0, "magic_scare", Attack.angleto(receiver),true)
					Attack.act_damage_addlog(receiver,"add_blog_fear_")
        return -damage,addrage
    end
	end
	
    return damage,addrage

end


function features_feeling_of_comradeship()

	local attack, attack_base = Attack.act_get_par(0, "attack")
	local max_bonus=11 -- ��������, ������� ����� �������� ������� ����� ������� �������
	local need_count=30 -- ���������� ��������, ������ +1 �����
	
	Attack.act_attach_modificator(0, "attack", "comradeship_mod", math.min(math.floor(Attack.act_size(0)/need_count),max_bonus - attack_base))
	return true

end


-- ***********************************************
-- * �������
-- ***********************************************

function bear_slave(damage,addrage,attacker,receiver,minmax,userdata,hitbacking)

    local dam, addrg = damage, addrage

	if minmax == 0 then
		dam, addrg = features_brutality(damage,addrage,attacker,receiver,minmax,userdata,hitbacking)
					   
		if Attack.act_is_spell(receiver, "effect_sleep") then
			return -dam, addrg
		end
	end

	return dam, addrg

end

function bear_after_move(pass)

	if not pass and Attack.val_restore("was_action") == "0" and not Attack.act_is_spell(0, "effect_sleep") then
		effect_sleep_attack(0, 0, 3, true)
		if Attack.act_size(0)>1 then 
			Attack.log(0.001,"add_blog_bsleep_2","name",blog_side_unit(0,1))
		else
			Attack.log(0.001,"add_blog_bsleep_1","name",blog_side_unit(0,1))
		end 
	end
	return true

end


function features_decay(wtm,tshift,dead)
	local level = 2
	if dead == true then
		local a = Attack.atom_spawn(0, tshift, "magic_decay")		
		local dmgts = Attack.aseq_time(a, "x") + tshift
	  Attack.act_damage_addlog(0,"add_blog_decay_",true)
--	  Attack.log_special(blog_side_unit(receiver,false)) -- ��������  

		for c=0,Attack.cell_count()-1 do
	  local i = Attack.cell_get(c)
		
		if Attack.cell_dist(0,i)==1 and (Attack.act_enemy(i) or Attack.act_ally(i)) then                  -- can receive this attack
			if not (Attack.act_race(i,"demon")) and not (Attack.act_feature(i,"plant")) and not (Attack.act_feature(i,"golem")) and not Attack.act_feature(i,"pawn") and not Attack.act_feature(i,"boss") then
--      local rnd=Game.Random(0,100)+1
  --    if rnd<=var then
      	spell_plague_attack(i,level,dmgts)
      end 
    end
		end
  end

	return true

end


function features_bone_creature(damage,addrage,attacker,receiver,minmax )

	if Attack.get_custom_param("arrows") == "1" then
		return damage*.3, addrage*.3
	end
	return damage, addrage

end

function features_initiative_penalty()

	for i=1,Attack.act_count()-1 do
		if Attack.act_enemy(i) and Attack.act_level(i) < 5 then
			Attack.act_attach_modificator(i,"initiative","initiative_penalty",-1)
		end
	end

	return true

end

function black_dragon_ondamage(wnm,ts,dead)

	if dead then
		for i=1,Attack.act_count()-1 do
			Attack.act_del_modificator(i,"initiative_penalty")
		end
	end
	return true

end

function features_evasion(damage,addrage,attacker,receiver,minmax)

	if minmax == 0 then	
	-- ������ �������� - ���� �� ����� �����, �� ������� ������
		if not Attack.act_is_spell(receiver, "feat_evasion") then Attack.act_posthitmaster(receiver, "feat_evasion",0) end	
	end 
	
	return damage,addrage

end

function features_saturation(damage,addrage,attacker,receiver,minmax,userdata,hitbacking )

	if minmax == 0 and not hitbacking and damage>10 then
		local charge=Attack.get_custom_param("charge")
		if charge~=nil and charge~="" then 
			Attack.act_charge(attacker)
			Attack.act_enable_attack(0,"gulp",false)  
		end 
	end 
	
	return damage,addrage

end

function post_magic_shield(damage,addrage,attacker,receiver,minmax,userdata)
	
	if Attack.act_is_spell(receiver,"special_magic_shield") then
		damage=damage*.5
		addrage=addrage*.5
	else
		 if (minmax==0) then Attack.act_posthitslave(receiver,"post_magic_shield",0) end
	end 
	
	return damage,addrage
end 

function features_dragon_slayer(damage,addrage,attacker,receiver,minmax,userdata )

    --local receiver=Attack.get_target(1)  -- ����?
    --local attacker=Attack.get_target(0)  -- ���?
    
 		local bonus_damage = tonumber(Attack.get_custom_param("dragonslayer"))
    
  	if Attack.act_feature(receiver,"dragon") then 
     	return damage*(1+bonus_damage/100),addrage*(1+bonus_damage/100)
   	end 
	  	
	return damage,addrage
end
