//
// ACHTUNG!: menu_music is special predefined playlist!
// ACHTUNG!: all themes in playlists must be not looped!
//
// FORMAT:
//
// playlist_name {
//   probability=tech_name
// }
//
// Where:
//
// probability - integer (1..65535)
// tech_name  - music tech name
//


menu_music {
  50=menu_music
}

intro {
  50=glory_music
}

battle_defeat {
  1=defeat
}

battle_victory {
  1=victory
}


/////TERRITORY/////

Darion1 {
    25=home_music
    25=water_music
    25=quite_music
    40=heroic_music
    15=glory_music
}

Darion2 {
    25=glory_music
    40=home_music
    25=heroic_music
    25=quite_music
}

Darion3 {
    25=turtle_bog
    40=dragon
    25=feat_of_arms
    25=hammer_of_the_chaos
}

Darion4 {
    25=heroic_music
    25=home_music
    40=quite_music
    25=way_to_the_dark_castle
}

Kordar1 {
    10=menu_music
    25=heroic_music
    25=water_music
    40=mountain_of_the_dwarf_king
}
Kordar2 {
    25=mountain_of_the_dwarf_king
    25=quite_music
    40=knight
    10=campaign

}
Kordar_dungeon_1 {
    25=spider_cave
    25=hammer_of_the_chaos
    25=dark_eclipse
}
Kordar_dungeon_2 {
    25=spider_cave
    25=dragon
    25=dark_eclipse
}
Island_1 {
    25=sturm_music
    25=temple_of_eternity
    40=water_music
    25=home_music
}

Island_2 {
    40=heroic_music
    25=warlord_horn
    25=water_music
    25=glory_music
}

Ellinia_1 {
    40=valley_of_a_thousand_rivers
    25=heroic_music
    25=quite_music
    25=home_music
    20=glory_music
}

Ellinia_2 {
    30=valley_of_a_thousand_rivers
    25=quite_music
    40=under_the_shadow_of_the_oak
    25=sturm_music
}
Ellinia_3 {
    25=valley_of_a_thousand_rivers
    25=quite_music
    25=under_the_shadow_of_the_oak
    25=heroic_music

}
Undead_1 {
    20=dark_eclipse
    20=hammer_of_the_chaos
    20=dragon
    20=turtle_bog
    20=fear
    20=dead_land
}
Undead_2 {
    20=dark_eclipse
    20=hammer_of_the_chaos
    20=dragon
    10=knight
    20=fear
    20=the_deep
}
Murok {
    25=hammer_of_the_chaos
    25=mushroom_cave
    25=temple_of_eternity
    25=turtle_bog
}
Mehgard {
    40=mountain_of_the_dwarf_king
    30=dwarf_mine_music
    15=heroic_music
    20=feat_of_arms

}
Demon {
    25=dark_eclipse
    25=warlord_horn
    25=battle_music_02
    25=necros_music

}


/////DUNGEON_AND_SPECIAL_TERRITORY/////

Dungeon_darion {
    25=campaign
    25=dragon
    25=mushroom_cave
}
Dungeon_undead_crypt {
    25=knight
    25=spider_cave
    25=temple_of_eternity
    25=the_deep
}
Dungeon_undead_dragon {
    25=necros_music
    25=turtle_bog
    25=hammer_of_the_chaos
    25=dead_land
}
Dungeon_ellinia {
    25=quite_music
    25=arena_music
    25=knight
}
Dungeon_ellinia_treelife {
    5=arena_music
    10=campaign
    30=spider_cave
    50=under_the_shadow_of_the_oak

}
Dungeon_island {
    25=knight
    25=mushroom_cave
    25=way_to_the_dark_castle

}
Dungeon_kordar {
    25=feat_of_arms
    25=mushroom_cave
    25=temple_of_eternity

}
Dungeon_kordar_demon {
    25=dark_eclipse
    25=dragon
    25=necros_music
}
Dungeon_kordar_demonroad {
    25=dark_eclipse
    25=warlord_horn
    25=spider_cave
}
Darion_4_magictower {
    25=knight
    25=warlord_horn
    25=mushroom_cave
}
Darion_kingtomb {
    25=knight
    25=temple_of_eternity
    25=feat_of_arms
}
Turtle_island {
    25=turtle_bog
}
Turtle_head {
    25=sturm_music
    25=necros_music
    25=turtle_bog
}
Gremlin_castle {
    25=way_to_the_dark_castle
    25=hammer_of_the_chaos
    25=feat_of_arms
}
Infish {
    25=campaign
    25=knight
    25=hammer_of_the_chaos
}
Demon_ultrax {
    25=dark_eclipse
    25=warlord_horn
    25=heroic_music
}
Island_2_dungeon_2 {
    100=dwarf_mine_music
}


/////ARENA/////


arena_coast {
    25=battle_music_01
    5=battle_music_02
    25=duel_music
    10=sturm_music
    5=arena_music
}
arena_forest {
    25=battle_music_01
    5=battle_music_02
    25=duel_music
    10=sturm_music
    5=arena_music
}

arena_dark_forest {
    20=battle_music_01
    15=battle_music_02
    40=duel_music
    30=arena_music
}

arena_dwarf_dungeon {
    10=battle_music_01
    20=battle_music_02
    20=duel_music
    30=sturm_music
    40=arena_music
}

arena_dwarf {
    10=battle_music_01
    15=battle_music_02
    20=duel_music
    20=sturm_music
    40=arena_music
}

arena_demon_land {
    30=battle_music_01
    25=battle_music_02
    10=duel_music
    20=sturm_music
    5=warlord_horn
    40=arena_music
}

arena_elf_forest {
    25=battle_music_01
    15=battle_music_02
    50=duel_music
    2=sturm_music
    30=warlord_horn
    5=arena_music
}

arena_cave {
    25=battle_music_01
    25=battle_music_02
    10=duel_music
    10=sturm_music
    30=arena_music
}

arena_murok {
    15=battle_music_01
    25=battle_music_02
    10=duel_music
    30=sturm_music
    20=warlord_horn
    5=arena_music
}

arena_death_land {
    20=battle_music_01
    25=battle_music_02
    10=warlord_horn
    50=arena_music
}
arena_crypt {
    20=battle_music_01
    25=battle_music_02
    10=warlord_horn
    50=arena_music
}
arena_ice_land {
    25=battle_music_01
    10=battle_music_02
    40=duel_music
    10=warlord_horn
    15=arena_music
}
arena_death_mountain {
    15=battle_music_01
    20=battle_music_02
    30=sturm_music
    30=warlord_horn
    50=arena_music
}

arena_ship {
    25=battle_music_01
    15=battle_music_02
    35=duel_music
    50=arena_music
}

arena_ghost_ship {
    25=battle_music_01
    15=battle_music_02
    35=duel_music
    50=arena_music
}

arena_boss_kraken {
    30=battle_music_01
    10=battle_music_02
    10=warlord_horn
    50=arena_music
}
arena_boss_turtle {
    40=sturm_music
    40=warlord_horn
}

arena_boss_spider {
    15=battle_music_01
    25=battle_music_02
    40=sturm_music
    20=warlord_horn
    30=arena_music
}

arena_castle_human {
    30=battle_music_01
    20=battle_music_02
    50=duel_music
    20=warlord_horn
    10=arena_music
}
arena_castle_undead {
    20=battle_music_02
    10=sturm_music
    30=warlord_horn
    50=arena_music
}
arena_castle_elf {
    20=battle_music_01
    15=battle_music_02
    40=duel_music
    5=sturm_music
    20=warlord_horn
}
arena_castle_pirat {
    30=battle_music_01
    20=battle_music_02
    50=duel_music
    20=warlord_horn
    10=arena_music
}
arena_castle_orc {
    15=battle_music_01
    25=battle_music_02
    40=sturm_music
    40=warlord_horn
    20=arena_music
}
arena_castle_dwarf {
    10=battle_music_01
    25=battle_music_02
    15=duel_music
    50=sturm_music
    40=warlord_horn

}
arena_castle_demon {
    10=battle_music_01
    15=battle_music_02
    20=sturm_music
    20=warlord_horn
    50=arena_music
}
arena_tournament {
    25=battle_music_01
    25=battle_music_02
    30=duel_music
    20=warlord_horn
    50=arena_music
}
arena_turtle_head {
    25=battle_music_01
    25=battle_music_02
    50=sturm_music
    20=warlord_horn

}
arena_ultrax {
    25=battle_music_01
    25=battle_music_02
    20=duel_music
    20=warlord_horn
    25=arena_music
}
arena_agremlincastle {
    25=battle_music_01
    25=battle_music_02
    20=duel_music
    20=warlord_horn
    20=arena_music
}
arena_true {
    15=battle_music_02
    40=duel_music
    30=arena_music
}
arena_item {
    100=battle_of_minds
}
