{
    left_top=hint2_lt.png
    right_top=hint2_rt.png
    left_bottom=hint2_lb.png
    right_bottom=hint2_rb.png

    top=hint2_top.png
    bottom=hint2_bottom.png
    left=hint2_left.png
    right=hint2_right.png

    bg=hint2_bg.png
    x_offset=4
    y_offset=4
}
{
    left_top=warspiritmenu1_lt.png
    right_top=warspiritmenu1_rt.png
    left_bottom=warspiritmenu1_lb.png
    right_bottom=warspiritmenu1_rb.png

    top=warspiritmenu1_top.png
    bottom=warspiritmenu1_bottom.png
    left=warspiritmenu1_left.png
    right=warspiritmenu1_right.png

    bg=warspiritmenu_bg.png
    x_offset=10
    y_offset=5
}
{
    left_top=standard_lt.png
    right_top=standard_rt.png
    left_bottom=standard_lb.png
    right_bottom=standard_rb.png
    top=standard_top.png
    bottom=standard_bottom.png
    left=standard_left.png
    right=standard_right.png

    bg=standard_background.png

    bg=warspiritmenu_bg.png
    x_offset=5
    y_offset=5

    bok_normal=buy_button_ok2_normal.png
    bok_focused=buy_button_ok2_onmouse.png
    bok_pressed=buy_button_ok2_onpress.png
    bok_disabled=buy_button_ok2_disable1.png

    bcancel_normal=buy_button_cancel2_normal.png
    bcancel_focused=buy_button_cancel2_onmouse.png
    bcancel_pressed=buy_button_cancel2_onpress.png
    bcancel_disabled=buy_button_cancel2_disable1.png
}
{
    left_top=map_menu_topleft.png
    right_top=map_menu_topright.png
    left_bottom=map_menu_bottomleft.png
    right_bottom=map_menu_bottomright.png

    top=map_menu_top.png
    bottom=map_menu_bottom.png
    left=map_menu_left.png
    right=map_menu_right.png

    bg=map_menu_bg.png
    bg_top=map_menu_bg_gradient.png
    x_offset=2
    y_offset=2
}
{
    left_top=newgame_menu_topleft.png
    right_top=newgame_menu_topright.png
    left_bottom=newgame_menu_bottomleft.png
    right_bottom=newgame_menu_bottomright.png

    top=newgame_menu_top.png
    bottom=newgame_menu_bottom.png
    left=newgame_menu_left.png
    right=newgame_menu_right.png

    bg=newgame_menu_bg.png
    x_offset=2
    y_offset=2
}
